INFO_PLIST="""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" 
    "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>CFBundleDevelopmentRegion</key>
  <string>English</string>
  <key>CFBundleExecutable</key>
  <string>%s</string>
  <key>CFBundleHelpBookFolder</key>
  <string>OpenStructure Manual</string>
  <key>CFBundleHelpBookName</key>
  <string>_BUNDLE_ Help</string>
  <key>CFBundleIconFile</key>
  <string>icon.icns</string>
  <key>CFBundleIdentifier</key>
  <string>org.openstructure.OpenStructure</string>
  <key>CFBundleInfoDictionaryVersion</key>
  <string>6.0</string>
  <key>CFBundlePackageType</key>
  <string>APPL</string>
  <key>CFBundleSignature</key>
  <string>????</string>
  <key>CFBundleVersion</key>
  <string></string>
  <key>NSMainNibFile</key>
  <string>MainMenu</string>
  <key>NSPrincipalClass</key>
  <string>NSApplication</string>
</dict>
</plist>"""

import os
from shutil import copy,rmtree
import stat
def _WritePkgInfo(bundle):
    print "writing Pkg Info for bundle %s" % (bundle)
    pkg_info=open(os.path.join(bundle+'.app', 'Contents/PkgInfo'), 'w+')
    pkg_info.write('APPL????')
    pkg_info.close()

def _WriteInfoPList(bundle,app):
    print "writing Info Plist for bundle %s" % (bundle)
    info_plist=open(os.path.join(bundle+'.app', 'Contents/Info.plist'), 'w+')
    info_plist.write(INFO_PLIST % (app))
    info_plist.close()

def _WriteScript(bundle,app):
  print "writing script for bundle %s" % (bundle)
  script="""#!/bin/sh
  if [ -e $HOME/Library/IPLT/bin/%s ]; then
    $HOME/Library/IPLT/bin/%s
  else
    /Library/IPLT/bin/%s
  fi
  """ % (app,app,app)
  bin_path=os.path.join('%s.app' % bundle, 'Contents/MacOS/%s' % app)
  bin=open(bin_path, 'w+')
  bin.write(script)
  bin.close()
  mode=stat.S_IMODE(os.lstat(bin_path)[stat.ST_MODE])
  mode|=stat.S_IXUSR
  mode|=stat.S_IXGRP
  mode|=stat.S_IXOTH
  os.chmod(bin_path,mode)

def _CreateBundleSkeleton(bundle,app,icons_dir):
  print "creating skeleton for bundle %s" % (bundle)
  bin_path=os.path.join('%s.app' % bundle, 'Contents/MacOS')
  if not os.path.exists(bin_path):
    os.makedirs(bin_path)
  _WritePkgInfo(bundle)
  _WriteInfoPList(bundle,app)
  _WriteScript(bundle,app)
  res_dir='%s.app/Contents/Resources' % bundle
  if not os.path.exists(res_dir):
    os.makedirs(res_dir) 
  if os.path.exists(os.path.join(icons_dir,'icon.icns')):
    copy(os.path.join(icons_dir,'icon.icns'), res_dir)

def create_bundle(bundle_name, bundle_app,icons_dir='../../graphics'):
  print "creating bundle %s" % (bundle_name)
  if os.path.exists(os.path.join('%s.app' % bundle_name)):
    rmtree(os.path.join('%s.app' % bundle_name))
  _CreateBundleSkeleton(bundle_name, bundle_app,icons_dir)