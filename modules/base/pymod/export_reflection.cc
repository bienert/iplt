//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/reflection.hh>
#include <iplt/reflection_io.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;

namespace {

// exception translator
void translator(const ReflectionException& x) {
  PyErr_SetString(PyExc_UserWarning, x.what() );
}

class ReflectionIterator {
public:
  ReflectionIterator(const ReflectionProxyter& p): proxyter_(p) {}

  ReflectionProxyter& Iter() {
    return proxyter_;
  }

  ReflectionProxyter Next() {
    if(proxyter_.AtEnd()) {
      PyErr_SetObject(PyExc_StopIteration, Py_None);
      throw_error_already_set();
    }

    return proxyter_++;
  }


private:
  ReflectionProxyter proxyter_;
};

ReflectionIterator IteratorFromProxyter(ReflectionProxyter& p)
{
  return ReflectionIterator(p);
}

ReflectionIterator IteratorFromList(ReflectionList& l)
{
  return ReflectionIterator(l.Begin());
}

void IncrementProxyter(ReflectionProxyter& rp)
{
  ++rp;
}

}
//void exportaph1(const ReflectionList& rl,const String& file,const String& title="",const std::vector<int>& order){ExportAph(rl,file,title,order)}
//void exportaph1(const ReflectionList& rl,const String& file,const String& title){ExportAph(rl,file,title)}
//void exportaph1(const ReflectionList& rl,const String& file){ExportAph(rl,file)}

BOOST_PYTHON_FUNCTION_OVERLOADS(export_aph_overloads, ExportAph, 2, 4)

void export_reflection()
{
  register_exception_translator<ReflectionException>(&translator);

  class_<ReflectionIndex>("ReflectionIndex",init<int,int,optional<Real> >())
    .def(init<const ReflectionIndex&, Real>())
    .def(init<const ost::img::Point&>())
    .def(init<Real, const ost::img::Point&>())
    .def(init<const geom::Vec3&>())
    .def("GetH",&ReflectionIndex::GetH)
    .def("GetK",&ReflectionIndex::GetK)
    .def("GetZStar",&ReflectionIndex::GetZStar)
    .def("ToVec3",&ReflectionIndex::ToVec3)
    .def("AsDuplet",&ReflectionIndex::AsDuplet)
    .def("AsTriplet",&ReflectionIndex::AsTriplet)
    .def("GetL",&ReflectionIndex::GetL)
    .def("IsEqual",&ReflectionIndex::IsEqual)
    .def(- self)
    .def(self_ns::str(self))
    .def(self == self)
    .def(self != self)
    .def(self < self)
    .def(self <= self)
    .def(self > self)
    .def(self >= self)
    ;

  implicitly_convertible<Point,ReflectionIndex>();

  class_<ReflectionIterator>("ReflectionIterator", no_init)
    .def("__iter__",&ReflectionIterator::Iter,
	 return_internal_reference<>() )
    .def("next",&ReflectionIterator::Next)
    ;

  Real (ReflectionProxyter::*get1)(const String&) const = &ReflectionProxyter::Get;
  Real (ReflectionProxyter::*get2)(unsigned int) const = &ReflectionProxyter::Get;

  void (ReflectionProxyter::*set1)(const String&, Real) = &ReflectionProxyter::Set;
  void (ReflectionProxyter::*set2)(unsigned int, Real) = &ReflectionProxyter::Set;
    
  class_<ReflectionProxyter>("ReflectionProxyter",init<const ReflectionProxyter&>())
    .def("Get",get1)
    .def("Get",get2)
    .def("Set",set1)
    .def("Set",set2)
    .def("Delete",&ReflectionProxyter::Delete)
    .def("AtEnd",&ReflectionProxyter::AtEnd)
    .def("IsValid",&ReflectionProxyter::IsValid)
    .def("Index",&ReflectionProxyter::Index)
    .def("GetIndex",&ReflectionProxyter::GetIndex)
    .def("GetResolution",&ReflectionProxyter::GetResolution)
    .def("Inc",IncrementProxyter)
    .def("__iter__", &IteratorFromProxyter)
    ;

  ReflectionProxyter (ReflectionList::*rlist_add_ref1)(const ReflectionIndex&) = &ReflectionList::AddReflection;
  ReflectionProxyter (ReflectionList::*rlist_add_ref2)(const ReflectionIndex&, const ReflectionProxyter&) = &ReflectionList::AddReflection;

  String (ReflectionList::*rlist_get_name1)(PropertyType) const = &ReflectionList::GetPropertyNameByType;
  String (ReflectionList::*rlist_get_name2)(int) const = &ReflectionList::GetPropertyName;

  PropertyType (ReflectionList::*rlist_get_type1)(const String&) const = &ReflectionList::GetPropertyType;
  PropertyType (ReflectionList::*rlist_get_type2)(int) const = &ReflectionList::GetPropertyType;
  
  unsigned int (ReflectionList::*rlist_add_prop1)(const String&) = &ReflectionList::AddProperty;
  unsigned int (ReflectionList::*rlist_add_prop2)(const String&, PropertyType) = &ReflectionList::AddProperty;

  void (ReflectionList::*rlist_apply1)(ReflectionNonModAlgorithm&) const = &ReflectionList::Apply;
  void (ReflectionList::*rlist_applyip1)(ReflectionNonModAlgorithm& a) const = &ReflectionList::ApplyIP;
  
  ReflectionList (ReflectionList::*rlist_apply2)(ReflectionModIPAlgorithm&) const=&ReflectionList::Apply;
  void (ReflectionList::*rlist_applyip2)(ReflectionModIPAlgorithm& a) =&ReflectionList::ApplyIP;
  
  ReflectionList (ReflectionList::*rlist_apply3)(const ReflectionConstModIPAlgorithm&) const=&ReflectionList::Apply;
  void (ReflectionList::*rlist_applyip3)(const ReflectionConstModIPAlgorithm& a) =&ReflectionList::ApplyIP;
  
  ReflectionList (ReflectionList::*rlist_apply4)(ReflectionModOPAlgorithm&) const=&ReflectionList::Apply;
  void (ReflectionList::*rlist_applyip4)(ReflectionModOPAlgorithm& a) =&ReflectionList::ApplyIP;
  
  ReflectionList (ReflectionList::*rlist_apply5)(const ReflectionConstModOPAlgorithm&) const=&ReflectionList::Apply;
  void (ReflectionList::*rlist_applyip5)(const ReflectionConstModOPAlgorithm& a) =&ReflectionList::ApplyIP;

  void (ReflectionList::*rlist_set_sym1)(const String&) = &ReflectionList::SetSymmetry;
  void (ReflectionList::*rlist_set_sym2)(const Symmetry&) = &ReflectionList::SetSymmetry;

  class_<ReflectionList>("ReflectionList",init<>())
    .def(init<const ReflectionList&,optional<bool> >())
    .def(init<const Lattice&>())
    .def(init<const SpatialUnitCell&>())
    .def(init<const Lattice&, const SpatialUnitCell&>())
    .def(init<const SpatialUnitCell&, const Lattice&>())
    .def("CopyProperties",&ReflectionList::CopyProperties)
    .def("AddProperty",rlist_add_prop1)
    .def("AddProperty",rlist_add_prop2)
    .def("DeleteProperty",&ReflectionList::DeleteProperty)
    .def("HasProperty",&ReflectionList::HasProperty)
    .def("HasPropertyType",&ReflectionList::HasPropertyType)
    .def("RenamePropert",&ReflectionList::RenameProperty)
    .def("SetPropertyType",&ReflectionList::SetPropertyType)
    .def("GetPropertyType",rlist_get_type1)
    .def("GetPropertyIndex",&ReflectionList::GetPropertyIndex)
    .def("GetPropertyCount",&ReflectionList::GetPropertyCount)
    .def("GetSymmetry",&ReflectionList::GetSymmetry)
    .def("SetSymmetry",rlist_set_sym1)
    .def("SetSymmetry",rlist_set_sym2)
    .def("GetPropertyNameByType",rlist_get_name1)
    .def("GetPropertyName",rlist_get_name2)
    .def("GetPropertyType",rlist_get_type2)
    .def("AddReflection",rlist_add_ref1)
    .def("AddReflection",rlist_add_ref2)
    .def("AddProxyter",&ReflectionList::AddProxyter)
    .def("CopyProxyter",&ReflectionList::CopyProxyter)
    .def("Merge",&ReflectionList::Merge)
    .def("Begin",&ReflectionList::Begin)
    .def("Find",&ReflectionList::Find)
    .def("FindFirst",&ReflectionList::FindFirst)
    .def("NumReflections", &ReflectionList::NumReflections) 
    .def("DumpFormatted", &ReflectionList::DumpFormatted)
    .def("Dump", &ReflectionList::Dump)
    .def("DumpHeader", &ReflectionList::DumpHeader)
    .def("SetLattice",&ReflectionList::SetLattice)
    .def("GetLattice",&ReflectionList::GetLattice)
    .def("SetUnitCell",&ReflectionList::SetUnitCell)
    .def("GetUnitCell",&ReflectionList::GetUnitCell)
    .def("__len__", &ReflectionList::NumReflections)
    .def("__iter__", &IteratorFromList)
    .def("Apply",rlist_apply1)
    .def("Apply",rlist_apply2)
    .def("Apply",rlist_apply3)
    .def("Apply",rlist_apply4)
    .def("Apply",rlist_apply5)
    .def("ApplyIP",rlist_applyip1)
    .def("ApplyIP",rlist_applyip2)
    .def("ApplyIP",rlist_applyip3)
    .def("ApplyIP",rlist_applyip4)
    .def("ApplyIP",rlist_applyip5)
    .def("GetResolution",&ReflectionList::GetResolution)
    .def("ClearReflections",&ReflectionList::ClearReflections)
    ;

  def("ImportMtz",ImportMtz);
  def("ExportMtz",ExportMtz);

  def("ImportAph",ImportAph);
  def("ExportAph",ExportAph,export_aph_overloads());

  ReflectionList (*reindex1)(const ReflectionList&,const Mat2&) = ReIndex;
  ReflectionList (*reindex2)(const ReflectionList&,const geom::Vec2&) = ReIndex;
  ReflectionList (*reindex3)(const ReflectionList&,const Mat3&) = ReIndex;
  ReflectionList (*reindex4)(const ReflectionList&,const geom::Vec3&) = ReIndex;

  def("ReIndex",reindex1);
  def("ReIndex",reindex2);
  def("ReIndex",reindex3);
  def("ReIndex",reindex4);

  enum_<PropertyType>("PropertyType")
    .value("PT_INDEX",PT_INDEX)
    .value("PT_INTENSITY",PT_INTENSITY)
    .value("PT_AMPLITUDE",PT_AMPLITUDE)
    .value("PT_ANOMALOUS_DIFFERENCE",PT_ANOMALOUS_DIFFERENCE)
    .value("PT_STDDEV",PT_STDDEV)
    .value("PT_AMPLITUDE2",PT_AMPLITUDE2)
    .value("PT_STDDEVA2",PT_STDDEVA2)
    .value("PT_INTENSITY2",PT_INTENSITY2)
    .value("PT_STDDEVI2",PT_STDDEVI2)
    .value("PT_NORM_AMPLITUDE",PT_NORM_AMPLITUDE)
    .value("PT_PHASE",PT_PHASE)
    .value("PT_WEIGHT",PT_WEIGHT)
    .value("PT_PHASE_PROB",PT_PHASE_PROB)
    .value("PT_BATCH",PT_BATCH)
    .value("PT_MISYM",PT_MISYM)
    .value("PT_INTEGER",PT_INTEGER)
    .value("PT_REAL",PT_REAL)
    .export_values()
    ;


  class_<ReflectionNonModAlgorithm, boost::noncopyable>("ReflectionNonModAlgorithm",no_init);
  class_<ReflectionModIPAlgorithm, boost::noncopyable>("ReflectionModIPAlgorithm",no_init);
  class_<ReflectionConstModIPAlgorithm, boost::noncopyable>("ReflectionConstModIPAlgorithm",no_init);
  class_<ReflectionModOPAlgorithm, boost::noncopyable>("ReflectionModOPAlgorithm",no_init);
  class_<ReflectionConstModOPAlgorithm, boost::noncopyable>("ReflectionConstModOPAlgorithm",no_init);
  
 }
