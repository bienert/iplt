//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflection index

  Author: Ansgar Philippsen
*/

#include <iostream>
#include <iomanip>

#include "reflection_index.hh"
#include "reflection_exception.hh"

namespace iplt { 

ReflectionIndex::ReflectionIndex(int h, int k, Real zstar):
  h_(h), k_(k), zstar_(zstar) 
{}


ReflectionIndex::ReflectionIndex(const ReflectionIndex& i, Real zstar):
  h_(i.h_), k_(i.k_), zstar_(zstar) 
{}
	
ReflectionIndex::ReflectionIndex(const geom::Vec3& v):
  h_(static_cast<int>(round(v[0]))), k_(static_cast<int>(round(v[1]))), zstar_(v[2]) 
{}

ReflectionIndex::ReflectionIndex(const ost::img::Point& hk):
  h_(hk[0]),k_(hk[1]), zstar_(0.0)
{}

ReflectionIndex::ReflectionIndex(Real thickness, const ost::img::Point& hkl):
  h_(hkl[0]),k_(hkl[1]),zstar_(0.0)
{
  if(std::fabs(thickness)<1e-20) {
    throw ReflectionException("ReflectionIndex thickness cannot be zero");
  }
  zstar_=static_cast<Real>(hkl[2])/thickness;
}


int ReflectionIndex::GetH() const
{
  return h_;
}

int ReflectionIndex::GetK() const
{
  return k_;
}

Real ReflectionIndex::GetZStar() const
{
  return zstar_;
}

geom::Vec3 ReflectionIndex::ToVec3() const
{
  return geom::Vec3(static_cast<Real>(h_),static_cast<Real>(k_),zstar_);
}

Real ReflectionIndex::GetL(Real thickness)
{
  return zstar_*thickness;
}

ost::img::Point ReflectionIndex::AsDuplet() const
{
  return ost::img::Point(h_,k_);
}

ost::img::Point ReflectionIndex::AsTriplet(Real thickness) const
{
  return ost::img::Point(h_,k_,static_cast<int>(round(zstar_*thickness)));
}

bool ReflectionIndex::operator==(const ReflectionIndex& indx) const
{
  return IsEqual(indx,1e-8);
}

bool ReflectionIndex::operator!=(const ReflectionIndex& indx) const
{
  return !IsEqual(indx,1e-8);
}

bool ReflectionIndex::IsEqual(const ReflectionIndex& indx, Real epsilon) const
{
  return h_==indx.h_ && k_==indx.k_ && std::abs(zstar_-indx.zstar_)<epsilon;
}


bool ReflectionIndex::operator<(const ReflectionIndex& ref) const
{ 
  Real epsilon=1e-5;
  return h_==ref.h_ ? (k_==ref.k_ ? zstar_-ref.zstar_<-epsilon : k_<ref.k_) : h_<ref.h_;
}

bool ReflectionIndex::operator<=(const ReflectionIndex& ref) const
{
  return *this<ref || *this==ref;
}

bool ReflectionIndex::operator>(const ReflectionIndex& ref) const
{
  return !(*this<ref || *this==ref);
}

bool ReflectionIndex::operator>=(const ReflectionIndex& ref) const
{
  return !(*this<ref);
}

ReflectionIndex ReflectionIndex::operator-() const
{
  return ReflectionIndex(-h_,-k_,-zstar_);
}


std::ostream& operator<<(std::ostream& s, const ReflectionIndex& i)
{
  s << "(" <<std::setw(3)<< i.GetH() << ","<<std::setw(3) << i.GetK() << ","<<std::setw(8) <<std::setprecision(3)<< i.GetZStar() << ")";
  return s;
}


} //ns
