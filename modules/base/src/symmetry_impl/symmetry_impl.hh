//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/


#ifndef IPLT_EX_SYMMETRYIMPL_H
#define IPLT_EX_SYMMETRYIMPL_H

#include <vector>
#include <list>
#include "symmetry_operator.hh"
#include <boost/shared_ptr.hpp>
#include <iplt/reflection_index.hh>
#include <iplt/symmetry_reflection.hh>

namespace iplt {  namespace symmetry {

// this is a safe using directive
using namespace geom;

class SymmetryImplementation
{
  typedef std::vector<SymmetryOperator> Opvec;
public:
  SymmetryImplementation(unsigned int number, String name,String pgname);
  virtual ~SymmetryImplementation(){};
  unsigned int GetSpacegroupNumber();
  String GetSpacegroupName();
  String GetPointgroupName();

  // new symmetry interface using SymmetryReflection
  virtual SymmetryReflection Reduce(const SymmetryReflection& reflection) ;
  virtual SymmetryReflectionList Expand(const SymmetryReflection& reflection,bool create_friedelmates);
  virtual SymmetryReflection Restrict(const SymmetryReflection& reflection);

  virtual SymmetryReflectionList Reduce(const SymmetryReflectionList& reflectionlist);
  virtual SymmetryReflectionList Expand(const SymmetryReflectionList& reflectionlist,bool create_friedelmates);
  virtual SymmetryReflectionList Restrict(const SymmetryReflectionList& reflectionlist);

  virtual bool IsInAsymmetricUnit(const SymmetryReflection& reflection);  
  virtual bool IsCentric(const SymmetryReflection& reflection) const;  

  //old deprecated interface
  virtual Real Symmetrize(ReflectionIndex r,Real phase);
  virtual ReflectionIndex Symmetrize(ReflectionIndex r);
  // Get all Positions in real space within unit cell, which are symmetry related
  virtual std::vector<Vec3> GenerateSymmetryRelatedPositions(Vec3 pos);
  // Get all Reflections in fourier space within unit cell, which are symmetry related
  virtual std::vector<ReflectionIndex> GenerateSymmetryRelatedPoints(ReflectionIndex p,bool create_friedelmates=true);
  // Check if Index is in asymmertric unit
  virtual bool IsInAsymmetricUnit(ReflectionIndex r)=0;
  bool IsCentric(const ReflectionIndex& r) const;
  bool IsSystematicAbsence(const ReflectionIndex& index);
  void Init();  
protected:
  Opvec opvec_;
  unsigned int number_;
  String name_;
  String pgname_;
  unsigned int centrics_[12];
  bool is_in_centric_zone_(unsigned int zone,const ReflectionIndex& r);
  //method to set up the centric zones, using Randy Read's implementation (adapted from ccp4)
  void setup_centric_zones_();
};


typedef boost::shared_ptr<SymmetryImplementation> SymmetryImplementationPtr;


}} //ns

#endif
