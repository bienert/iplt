
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// This file was autogenerated from a slightly modified 
// version of the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/

#include <iplt/symmetry_impl/symmetry_impl_holder.hh>
#include <iplt/symmetry_impl/symmetry_impl.hh>
#include "PG_m_4.hh"

namespace iplt {  namespace symmetry {

	
void SymmetryImplementationHolder::create_PG_m_4()
{
	addsymmetry_(SymmetryImplementationPtr(new P__m_4),81,"P -4,P -4");
	addsymmetry_(SymmetryImplementationPtr(new I__m_4),82,"I -4,I -4");

}

}} //ns
