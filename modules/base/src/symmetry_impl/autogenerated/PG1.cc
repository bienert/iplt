
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/////////////////////////////////////////////////////////
// 
// The asymmetric unit definitions for P1 were adapted from
// the the ccp4 syminfo.lib.
// 
//            http://www.ccp4.ac.uk/main.html
// 
/////////////////////////////////////////////////////////

/*
  author: Andreas Schenk
*/


#include "PG1.hh"

namespace iplt {  namespace symmetry {

P_1::P_1():SymmetryImplementation(1,"P 1","PG1")
{
}

bool P_1::IsInAsymmetricUnit(ReflectionIndex index)
{
  // asymmetric unit h>=0 instead of zstar >=0 (original ccp4 definition) because zstar>=0 would split lattice lines in two
  return index.GetH()>0 || (index.GetH()==0 && (index.GetK()>0 || (index.GetK()==0 && index.GetZStar()>=0)));
}

}} //ns
