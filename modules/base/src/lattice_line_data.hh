//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/
#ifndef LATTICE_LINE_DATA_HH_
#define LATTICE_LINE_DATA_HH_

//#include <iplt/dllexport.hh>
#include <iplt/reflection_list.hh>

namespace iplt { 

class DLLEXPORT_IPLT_BASE LatticeLineData
{
public:
	LatticeLineData(const ReflectionList& rlist,String f_label="", String phi_label="");
  Real GetAmplitude(ReflectionIndex ri) const;
  Real GetPhase(ReflectionIndex ri) const;
  Complex Get(ReflectionIndex ri) const;
	virtual ~LatticeLineData();
protected:
  Real sinc(Real x) const;
  ReflectionList rlist_;
  String f_label_;
  String phi_label_;
};

} //ns

#endif /*LATTICE_LINE_DATA_HH_*/
