//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_CELL_INFO_H
#define IPLT_EX_CELL_INFO_H

/*
  provide wrappers for Cell from/to Info

  Author: Ansgar Philippsen
*/

#include <ost/info/info_fw.hh>

namespace iplt { 

class SpatialUnitCell;
class ReciprocalUnitCell;

namespace cell_detail {

class CellImpl;

// actual implementation
CellImpl Extract(const ost::info::InfoGroup& g);

}

/*!
  Extract a spatial unit cell object from the given InfoGroup
*/
DLLEXPORT_IPLT_BASE SpatialUnitCell InfoToSpatialUnitCell(const ost::info::InfoGroup& g);

/*!
  Extract a reciprocal unit cell object from the given InfoGroup
*/
DLLEXPORT_IPLT_BASE ReciprocalUnitCell InfoToReciprocalUnitCell(const ost::info::InfoGroup& g);


/*! DEPRECATED */
DLLEXPORT_IPLT_BASE SpatialUnitCell ExtractSpatialUnitCell(const ost::info::InfoGroup& g);

/*! DEPRECATED */
DLLEXPORT_IPLT_BASE ReciprocalUnitCell ExtractReciprocalUnitCell(const ost::info::InfoGroup& g);

} //ns

#endif
