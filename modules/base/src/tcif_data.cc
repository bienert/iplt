//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/

#include "tcif_data.hh"

namespace iplt { 

TCIFData::TCIFData(const MicroscopeData& mic_data,
		   const Defocus& defocus,
		   const TiltGeometry& tilt):
  MicroscopeData(mic_data),
  Defocus(defocus),
  TiltGeometry(tilt)
{
}

void TCIFData::SetDefocus(const Defocus& d)
{
  dx_=d.GetDefocusX();
  dy_=d.GetDefocusY();
  angle_=d.GetAstigmatismAngle();
}

void TCIFData::SetMicroscopeData(const MicroscopeData& m)
{
  cs_=m.GetSphericalAberration();
  kv_=m.GetAccelerationVoltage();
  cc_=m.GetChromaticAberration();
  ca_=m.GetAmplitudeContrast();
  de_=m.GetEnergySpread();
  lambda_=m.GetWavelength();
}

void TCIFData::SetTiltGeometry(const TiltGeometry& t)
{
  tilt_angle_=t.GetTiltAngle();
  x_axis_angle_=t.GetXAxisAngle();
}

void TCIFDataToInfo(const TCIFData& md, ost::info::InfoGroup& ig)
{
  using namespace info;
  InfoGroup tcifgroup=ig.CreateGroup("TCIFData");
  MicroscopeDataToInfo(md,tcifgroup);
  DefocusToInfo(md,tcifgroup);
  TiltGeometryToInfo(md,tcifgroup);
}
TCIFData InfoToTCIFData(const ost::info::InfoGroup& ig)
{
  using namespace info;
  InfoGroup tcifgroup=ig.GetGroup("TCIFData");
  return TCIFData(InfoToMicroscopeData(tcifgroup),InfoToDefocus(tcifgroup),InfoToTiltGeometry(tcifgroup));
}

} //ns
