//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  2D lattice

  Author: Ansgar Philippsen
*/

#ifndef IPLT_ALG_LATTICE_H
#define IPLT_ALG_LATTICE_H

#include <vector>

#include <ost/img/extent.hh>
#include <ost/geom/geom.hh>
#include <ost/img/point.hh>
#include <ost/img/point_list.hh>

#include <ost/info/info_fw.hh>
#include <iplt/module_config.hh>

namespace iplt {
    using namespace ost::img;
    using namespace ost;

//! encapsulates a 2D lattice
/*!
  A lattice consists of two non-parallel vectors
  and a specific offset. Since it is usually associated
  with an image, its units are meant to be pixels!
*/
class DLLEXPORT_IPLT_BASE Lattice {
public:
  //! Default is {1,0} {0,1} {0,0}
  Lattice();
  
  //! initialization with two non-parallel vectors
  Lattice(const geom::Vec2& v1, const geom::Vec2& v2, const geom::Vec2& offset=geom::Vec2(0.0,0.0));

  //! initialization with two non-parallel vectors
  Lattice(const geom::Vec2& v1, const geom::Vec2& v2, const geom::Vec2& offset, Real k3, Real s3);

  Lattice(const geom::Vec2& v1,const ost::img::Point& p1,const geom::Vec2& v2,const ost::img::Point& p2,const geom::Vec2& v3,const ost::img::Point& p3);
  Lattice(const geom::Vec2& v1,const ost::img::Point& p1,const geom::Vec2& v2,const ost::img::Point& p2,const geom::Vec2& v3,const ost::img::Point& p3,Real k3,Real s3=0);

  //! initialization
  /*!
    calculates a lattice based and vectors and indices (instead of the default
    (1,0) and (0,1) )
  */
  Lattice(const geom::Vec2& v1, const ost::img::Point& indx1, const geom::Vec2& v2, const ost::img::Point& indx2, const geom::Vec2& offset=geom::Vec2(0.0,0.0));

  //! set first vector
  void SetFirst(const geom::Vec2& v) {vec1_=v;}
  //! retrieve first vector
  const geom::Vec2& GetFirst() const {return vec1_;}
  //! set second vector
  void SetSecond(const geom::Vec2& v) {vec2_=v;}
  //! retrieve second vector
  const geom::Vec2& GetSecond() const {return vec2_;}
  //! retrieve overall offset
  const geom::Vec2& GetOffset() const {return off_;}
  //! set overall offset
  void SetOffset(const geom::Vec2& o) {off_=o;}

  void SetBarrelDistortion(Real k3) {barrel_k3_=k3;}
  Real GetBarrelDistortion() const {return barrel_k3_;}

  void SetSpiralDistortion(Real k3) {spiral_k3_=k3;}
  Real GetSpiralDistortion() const {return spiral_k3_;}

  //! calc distance of a ost::img::Point to  nearest lattice ost::img::Point
  Real DistanceTo(const ost::img::Point& p) const;
  Real DistanceTo(const geom::Vec2& v) const;
  
  //! returns position based on index
  geom::Vec2 CalcPosition(const ost::img::Point& hk) const;
  geom::Vec2 CalcPosition(const geom::Vec2& hk) const;

  //! return the components of a ost::img::Point
  /*!
    A ost::img::Point on the lattice should fullfill the following equation,
    where p is the ost::img::Point, a the first vector, b the second, and o the
    offset, and n,m are integers
    
    p_x = n * a_x + m * b_x + o_x
    p_y = n * a_y + m * b_y + o_y
    
    For ost::img::Points not on the lattice, m and n will have fractional components.
    This method returns m and n in the result geom::Vec2, with both the integer
    and fractional part
  */
  geom::Vec2 CalcComponents(const ost::img::Point& p) const;
  geom::Vec2 CalcComponents(const geom::Vec2& v) const;

  bool operator==(const Lattice& lat) const {return equal(lat);}
  bool operator!=(const Lattice& lat) const {return !equal(lat);}

private:
  geom::Vec2 calc_components_with_distortion_(const geom::Vec2& in) const;
  geom::Vec2 calc_components_without_distortion_(const geom::Vec2& in) const;
  geom::Vec2 vec1_, vec2_, off_;
  Real barrel_k3_;
  Real spiral_k3_;

  bool equal(const Lattice& lat) const;

  void assert_non_parallel();
};

typedef std::vector<Lattice> LatticeList;


DLLEXPORT_IPLT_BASE ost::img::PointList Predict(const Lattice&, const ost::img::Extent&);

// output convenience
DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& os, const Lattice &l);

//! invert domain of lattice spatial <-> reciprocal
DLLEXPORT_IPLT_BASE Lattice InvertLattice(const Lattice& lat);

//! DEPRECATED
DLLEXPORT_IPLT_BASE Lattice ExtractLattice(const ost::info::InfoGroup& g);

//! DEPRECATED
DLLEXPORT_IPLT_BASE void StoreLattice(const Lattice& lat, ost::info::InfoGroup g);

//! Get a lattice from an info group
/*!
  A lattice object is constructed from the parameters
  in the given info group.
*/
DLLEXPORT_IPLT_BASE Lattice InfoToLattice(const ost::info::InfoGroup& g);

//! Store lattice in given info group
DLLEXPORT_IPLT_BASE void LatticeToInfo(const Lattice& lat, ost::info::InfoGroup g);

} //ns


#endif
