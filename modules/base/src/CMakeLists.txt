add_subdirectory(symmetry_impl)
add_subdirectory(mtzlib)


set(DNG_IPLT_EX_SOURCES 
ctf_func_base.cc
lattice.cc
reflection_index.cc
reflection_list.cc
reflection_list_impl.cc
reflection_io.cc
reflection_proxyter.cc
tilt.cc
unit_cell.cc
unit_cell_info.cc
symmetry.cc
symmetry_exception.cc
symmetry_reflection.cc
defocus.cc
microscope_data.cc
tcif_data.cc
lattice_line_data.cc
)

set(DNG_IPLT_EX_HEADERS
ctf_func_base.hh
lattice.hh
reflection.hh
reflection_algorithm.hh
reflection_common.hh
reflection_exception.hh
reflection_index.hh
reflection_list.hh
reflection_list_impl.hh
reflection_io.hh
reflection_proxyter.hh
tilt.hh
unit_cell.hh
unit_cell_info.hh
symmetry.hh
symmetry_exception.hh
symmetry_reflection.hh
defocus.hh
microscope_data.hh
tcif_data.hh
lattice_line_data.hh
module_config.hh
)

module(NAME base
       SOURCES "${DNG_IPLT_EX_SOURCES}"
       ${DNG_IPLT_EX_MTZLIB_SOURCES}
       ${DNG_IPLT_EX_SYMMETRY_SOURCES}
       ${DNG_IPLT_EX_SYMMETRY_AUTO_SOURCES}
       HEADERS  ${DNG_IPLT_EX_MTZLIB_HEADERS} 
                ${DNG_IPLT_EX_SYMMETRY_HEADERS} 
                ${DNG_IPLT_EX_SYMMETRY_AUTO_HEADERS} 
                ${DNG_IPLT_EX_HEADERS} 
       HEADER_OUTPUT_DIR iplt
       DEPENDS_ON ost_img
       LINK ${BOOST_LIBRARIES}  ${BOOST_PYTHON_LIBRARIES} ${GSL_LIBRARIES}
       PREFIX iplt
       )
