//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/


#ifndef IPLT_EX_SYMMETRY_H
#define IPLT_EX_SYMMETRY_H

#include <vector>
#include <string>

#include <ost/geom/geom.hh>

#include <iplt/reflection_index.hh>
#include <iplt/symmetry_impl/symmetry_impl.hh>
#include <iplt/symmetry_impl/symmetry_impl_holder.hh>

namespace iplt { 

class DLLEXPORT_IPLT_BASE Symmetry{
public:
  Symmetry(const String& name);
  Symmetry(unsigned id);
  unsigned int GetSpacegroupNumber() const;
  String GetSpacegroupName() const;
  String GetPointgroupName() const;

  // new symmetry interface using SymmetryReflection
  SymmetryReflection Reduce(const SymmetryReflection& reflection) const;
  SymmetryReflectionList Expand(const SymmetryReflection& reflection,bool create_friedelmates) const;
  SymmetryReflection Restrict(const SymmetryReflection& reflection) const;

  SymmetryReflectionList Reduce(const SymmetryReflectionList& reflectionlist) const;
  SymmetryReflectionList Expand(const SymmetryReflectionList& reflectionlist,bool create_friedelmates) const;
  SymmetryReflectionList Restrict(const SymmetryReflectionList& reflectionlist) const;

  // Check if Index is in asymmetric unit
  bool IsInAsymmetricUnit(const SymmetryReflection& reflection) const;  
  bool IsInAsymmetricUnit(const ReflectionIndex& r) const;
  // Check if reflection is centric
  bool IsCentric(const SymmetryReflection& reflection) const;  
  bool IsCentric(const ReflectionIndex& r) const;
  // Check if reflection is systematic absence
  bool IsSystematicAbsence(const SymmetryReflection& reflection) const;  
  bool IsSystematicAbsence(const ReflectionIndex& reflection) const;  




  // Old symmetry interface.Methods are deprecated
  Real Symmetrize(const ReflectionIndex& r,Real phase) const;
  ReflectionIndex Symmetrize(const ReflectionIndex& r) const;
  // Get all Positions in real space within unit cell, which are symmetry related
  std::vector<geom::Vec3> GenerateSymmetryRelatedPositions(const geom::Vec3& p) const;
  // Get all Reflections in fourier space within unit cell, which are symmetry related
  std::vector<ReflectionIndex> GenerateSymmetryRelatedPoints(const ReflectionIndex& p,bool createfriedel_mates=true) const;
protected:
  symmetry::SymmetryImplementationPtr impl_;
};

DLLEXPORT_IPLT_BASE std::vector<unsigned int> GetImplementedSymmetries();

} //ns

#endif
