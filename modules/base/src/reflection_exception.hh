//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Reflection exception

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_REFLECTION_EXCEPTION_H
#define IPLT_EX_REFLECTION_EXCEPTION_H

#include <stdexcept>

namespace iplt { 

//! Exception thrown from reflection class library
class DLLEXPORT_IPLT_BASE ReflectionException: public std::runtime_error {
public:
  // this re-declaration of the virtual destructor is required
  // to ensure typeinfo visibility across shared library boundaries!
  virtual ~ReflectionException() throw() {}
  ReflectionException(const String& m): std::runtime_error(m) {}
};

} //ns

#endif
