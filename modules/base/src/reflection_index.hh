//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_REFLECTION_INDEX_H
#define IPLT_EX_REFLECTION_INDEX_H

#include <ost/base.hh>
#include <ost/img/point.hh>
#include <iplt/module_config.hh>

namespace iplt { 

//! Encapsulates the concept of a reflection index
/*!
  In the nature of 2D electron crystallography, a reflection
  index is characterized by an integer duplet (h,k) and the
  reciprocal length along the third dimension, called zstar
  ('z*'), in units of (A)^-1

  Given the thickness of a unit cell (in A), zstar can be 
  converted into the third reflection index l (albeit fractional)
  simply with l = zstar * thickness

  The ReflectionIndex is used in the ReflectionList as a key
  in a hash table, with all three values (h,k and zstar) being
  used in the sorting order. For this reason, there are no 
  methods to set values after instancing.

  
*/
class DLLEXPORT_IPLT_BASE ReflectionIndex {
public:
  //! Initialization with a hk duplet and (optional) zstar
  ReflectionIndex(int h, int k, Real zstar=0.0);
  //! Initialization with a hk duplet from another index and new zstar
  ReflectionIndex(const ReflectionIndex& i, Real zstar);

  //! Initialization with a geom::Vec3 (h,k,zstar) h and k will be rounded
  explicit ReflectionIndex(const geom::Vec3& v);

  //! Initialization from a hkl triplet
  /*!
    Requires a thickness (in A) for l to zstar conversion
  */
  ReflectionIndex(Real thickness, const ost::img::Point& hkl);

  //! Initialization from a hk duplet
  ReflectionIndex(const ost::img::Point& hk);

  //! Retrieve H (first) component
  int GetH() const;
  //! Retrieve K (second) component
  int GetK() const;

  //! Retrieve zstar (third) component, in reciprocal A
  Real GetZStar() const;

  //! Convenience method, returns vector triplet of (h,k,zstar)
  geom::Vec3 ToVec3() const;

  //! returns (h,k,0) index as point, zstar is ignored
  ost::img::Point AsDuplet() const;

  //! Returns reflection as hkl triplet
  /*!
    Requires a thickness (in A) for zstar to l conversion;
    the result will be rounded to the nearest integer
  */
  ost::img::Point AsTriplet(Real thickness) const;

  //! Retrieve fractional l
  /*!
    Requires thickness (in A) for zstar to l conversion
  */
  Real GetL(Real thickness);

  bool operator==(const ReflectionIndex& indx) const;
  bool operator!=(const ReflectionIndex& indx) const;

  //! Less operator for special sorting
  /*! 
    sorting order is h, k, z*
  */
  bool operator<(const ReflectionIndex& indx) const;

  bool operator<=(const ReflectionIndex& indx) const;
  bool operator>(const ReflectionIndex& indx) const;
  bool operator>=(const ReflectionIndex& indx) const;

  ReflectionIndex operator-() const;

  // explicit comparison with an epsilon threshold for zstar
  bool IsEqual(const ReflectionIndex& indx, Real epsilon=1e-8) const;

private:
  int h_, k_;
  Real zstar_;

};

DLLEXPORT_IPLT_BASE std::ostream& operator<<(std::ostream& s, const ReflectionIndex& i);

} //ns

#endif
