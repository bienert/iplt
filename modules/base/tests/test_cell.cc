//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <iostream>
#include <ost/info/info.hh>
#include <iplt/unit_cell.hh>
#include <iplt/unit_cell_info.hh>

using namespace iplt;

extern Real tolerance;
extern boost::test_tools::predicate_result compare_Vec2(const Vec2&,const Vec2&);


BOOST_AUTO_TEST_SUITE( iplt_base )


BOOST_AUTO_TEST_CASE(cell_spatial_creation)
{
  std::ostringstream mesg;
  // default
  SpatialUnitCell sc1;

  // test with wrong angle (ie <90 deg)
  // test lattice with wrong angle (ie < 90 deg)
  SpatialUnitCell sc2(10.0,20.0,60*M_PI/180.0);
  mesg << "expected (10.0,0.0) but got " << sc2.GetVecA();
  BOOST_CHECK_MESSAGE(geom::Equal(sc2.GetVecA(),Vec2(10.0,0.0)),mesg.str());
  mesg.str("");
  geom::Vec2 bvec(20.0*cos(120.0*M_PI/180.0),-20.0*sin(120*M_PI/180.0));
  mesg << "expected " << bvec << " but got " << sc2.GetVecB();
  BOOST_CHECK_MESSAGE(geom::Equal(sc2.GetVecB(),bvec),mesg.str());
  BOOST_CHECK_CLOSE(geom::Angle(sc2.GetVecA(),sc2.GetVecB()),120.0*M_PI/180.0,tolerance);

  // test lattice with wrong angle (ie < 90 deg)
  SpatialUnitCell sc3(Lattice(geom::Vec2(1.0,0.0),Vec2(0.3,1.0)),Vec3(1.0,1.0,1.0),1.0);
  mesg << "expected (1.0,0.0) but got " << sc3.GetVecA();
  BOOST_CHECK_MESSAGE(geom::Equal(sc3.GetVecA(),Vec2(1.0,0.0)),mesg.str());
  mesg.str("");
  mesg << "expected (-0.3,-1.0) but got " << sc3.GetVecB();
  BOOST_CHECK_MESSAGE(geom::Equal(sc3.GetVecB(),Vec2(-0.3,-1.0)),mesg.str());
  BOOST_CHECK(geom::Angle(sc3.GetVecA(),sc3.GetVecB())>=M_PI_2);
  
  // test with small vector lengths
  SpatialUnitCell sc4(1e-10,1e-10,147.0*M_PI/180.0);
  mesg.str("");
  mesg << "expected gamma of 147 but got " << sc4.GetGamma()/M_PI*180.0;
  BOOST_CHECK_MESSAGE(std::fabs(sc4.GetGamma()-147.0*M_PI/180.0)<1e-5,mesg.str());
}

BOOST_AUTO_TEST_CASE(cell_reciprocal_creation)
{
  std::ostringstream mesg;
  // default
  ReciprocalUnitCell rc1;
  // test lattice with wrong angle (ie > 90 deg)
  ReciprocalUnitCell rc2(Lattice(geom::Vec2(1.0,0.0),Vec2(-0.3,1.0)),Vec3(1.0,1.0,1.0),1.0);
  mesg << "expected (1.0,0.0) but got " << rc2.GetVecA();
  BOOST_CHECK_MESSAGE(geom::Equal(rc2.GetVecA(),Vec2(1.0,0.0)),mesg.str());
  mesg.str("");
  mesg << "expected (0.3,-1.0) but got " << rc2.GetVecB();
  BOOST_CHECK_MESSAGE(geom::Equal(rc2.GetVecB(),Vec2(0.3,-1.0)),mesg.str());
  BOOST_CHECK(geom::Angle(rc2.GetVecA(),rc2.GetVecB())<=M_PI_2);
}

BOOST_AUTO_TEST_CASE(cell_inversion)
{
  Real g=33.0*M_PI/180.0;
  SpatialUnitCell sc(geom::Vec2(8.0,0.0),Vec2(5.0*cos(g),5.0*sin(g)),1.0);
  ReciprocalUnitCell rc(sc);
  SpatialUnitCell sc2(rc);
  //TODO complete test
}


BOOST_AUTO_TEST_CASE(cell_extract)
{
  using namespace ost::info;
  InfoHandle i = LoadInfo("test_info.xml");
  InfoGroup g = i.Root().GetGroup("test_cell1");
  SpatialUnitCell sc = ExtractSpatialUnitCell(g);

  BOOST_CHECK(compare_Vec2(sc.GetVecA(),Vec2(20.0,0.0)));
  BOOST_CHECK(compare_Vec2(sc.GetVecB(),Vec2(0.0,25.0)));
  BOOST_CHECK_CLOSE(sc.GetC(),30.0,tolerance);

  g = i.Root().GetGroup("test_cell2");
  ReciprocalUnitCell rc = ExtractReciprocalUnitCell(g);

  BOOST_CHECK(compare_Vec2(rc.GetVecA(),Vec2(0.1,0.0)));
  BOOST_CHECK(compare_Vec2(rc.GetVecB(),Vec2(0.0,0.2)));
  BOOST_CHECK_CLOSE(rc.GetC(),0.3,tolerance);
}

BOOST_AUTO_TEST_SUITE_END()

