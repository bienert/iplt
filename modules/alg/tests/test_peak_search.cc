//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen, Valerio Mariani
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <iplt/alg/peak_search.hh>
#include <ost/img/image.hh>

using namespace ost::img;
using namespace iplt::alg;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(peak_search_simple_2d)
{
  ost::img::ImageHandle im =  CreateImage(ost::img::Size(20,20));
  im.SetReal(ost::img::Point(3,3), 1.0);
  im.SetReal(ost::img::Point(7,5), 1.0);
  PeakSearch ps(2,1.0);
  im.Apply(ps);
  PeakList peaks = ps.GetPeakList();

  std::ostringstream msg;
  msg << "expected 2 peaks but found " << peaks.size();
  BOOST_REQUIRE_MESSAGE(peaks.size()==2,msg.str());
  BOOST_CHECK(peaks[0]==Point(3,3));
  BOOST_CHECK(peaks[1]==Point(7,5));
}

BOOST_AUTO_TEST_CASE(peak_search_plateau)
{
  ost::img::ImageHandle im =  CreateImage(ost::img::Size(20,20),ost::img::REAL,ost::img::SPATIAL);
  im.SetReal(ost::img::Point(2,2), 1.0);
  im.SetReal(ost::img::Point(1,1), 1.0);
  im.SetReal(ost::img::Point(1,2), 1.0);
  im.SetReal(ost::img::Point(1,3), 1.0);
  im.SetReal(ost::img::Point(2,1), 1.0);
  im.SetReal(ost::img::Point(2,3), 1.0);
  im.SetReal(ost::img::Point(3,1), 1.0);
  im.SetReal(ost::img::Point(3,2), 1.0);
  im.SetReal(ost::img::Point(3,3), 1.0);
  
  PeakSearch ps = PeakSearch(2,0.1,1);
  im.Apply(ps);
  PeakList peaks = ps.GetPeakList();

  std::ostringstream msg;
  msg << "expected one peak but found " << peaks.size();
  BOOST_REQUIRE_MESSAGE(peaks.size()==1,msg.str());
  BOOST_CHECK(peaks[0]==Point(2,2));
}

BOOST_AUTO_TEST_CASE(peak_search_simple_3d)
{
  ost::img::ImageHandle im =  CreateImage(ost::img::Size(20,20,20));
  im.SetReal(ost::img::Point(3,3,8), 1.0);
  im.SetReal(ost::img::Point(7,5,17), 1.0);
  PeakSearch ps(2,1.0);
  im.Apply(ps);
  PeakList peaks = ps.GetPeakList();

  std::ostringstream msg;
  msg << "expected 2 peaks but found " << peaks.size();
  BOOST_REQUIRE_MESSAGE(peaks.size()==2,msg.str());
  BOOST_CHECK(peaks[0]==Point(3,3,8));
  BOOST_CHECK(peaks[1]==Point(7,5,17));
}

BOOST_AUTO_TEST_CASE(peak_search_cube)
{
  ost::img::ImageHandle im =  CreateImage(ost::img::Size(20,20,20),ost::img::REAL,ost::img::SPATIAL);
  im.SetReal(ost::img::Point(2,2,1), 1.0);
  im.SetReal(ost::img::Point(1,1,1), 1.0);
  im.SetReal(ost::img::Point(1,2,1), 1.0);
  im.SetReal(ost::img::Point(1,3,1), 1.0);
  im.SetReal(ost::img::Point(2,1,1), 1.0);
  im.SetReal(ost::img::Point(2,3,1), 1.0);
  im.SetReal(ost::img::Point(3,1,1), 1.0);
  im.SetReal(ost::img::Point(3,2,1), 1.0);
  im.SetReal(ost::img::Point(3,3,1), 1.0);

  im.SetReal(ost::img::Point(2,2,2), 1.0);
  im.SetReal(ost::img::Point(1,1,2), 1.0);
  im.SetReal(ost::img::Point(1,2,2), 1.0);
  im.SetReal(ost::img::Point(1,3,2), 1.0);
  im.SetReal(ost::img::Point(2,1,2), 1.0);
  im.SetReal(ost::img::Point(2,3,2), 1.0);
  im.SetReal(ost::img::Point(3,1,2), 1.0);
  im.SetReal(ost::img::Point(3,2,2), 1.0);
  im.SetReal(ost::img::Point(3,3,2), 1.0); 

  im.SetReal(ost::img::Point(2,2,3), 1.0);
  im.SetReal(ost::img::Point(1,1,3), 1.0);
  im.SetReal(ost::img::Point(1,2,3), 1.0);
  im.SetReal(ost::img::Point(1,3,3), 1.0);
  im.SetReal(ost::img::Point(2,1,3), 1.0);
  im.SetReal(ost::img::Point(2,3,3), 1.0);
  im.SetReal(ost::img::Point(3,1,3), 1.0);
  im.SetReal(ost::img::Point(3,2,3), 1.0);
  im.SetReal(ost::img::Point(3,3,3), 1.0); 
 
  PeakSearch ps = PeakSearch(2,0.1,1);
  im.Apply(ps);
  PeakList peaks = ps.GetPeakList();

  std::ostringstream msg;
  msg << "expected one peak but found " << peaks.size();
  BOOST_REQUIRE_MESSAGE(peaks.size()==1,msg.str());
  BOOST_CHECK(peaks[0]==Point(2,2,2));

  ps.ClearPeakList();
  peaks = ps.GetPeakList();
  std::ostringstream msg2;
  msg2 << "expected zero peaks but found " << peaks.size();
  BOOST_REQUIRE_MESSAGE(peaks.size()==0,msg2.str());
}


BOOST_AUTO_TEST_SUITE_END()

