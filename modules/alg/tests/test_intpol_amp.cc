//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/img/value_util.hh>
#include <iplt/alg/intpol_amp.hh>
#include <iplt/reflection.hh>

using namespace iplt;
using namespace iplt::alg;

BOOST_AUTO_TEST_SUITE( iplt_alg )


BOOST_AUTO_TEST_CASE(intpol_amp)
{
  try {
    ReflectionList rl;
    rl.AddProperty("fracl");
    rl.AddProperty("sigfracl");
    rl.AddProperty("a");
    rl.AddProperty("siga");
    int h=0;
    int k=0;
    for(int l=0;l<20;++l) {
      ReflectionProxyter rp = rl.AddReflection(ReflectionIndex(h,k));
      rp.Set("fracl",Real(l));
      rp.Set("sigfracl",1.0);
      rp.Set("a",(Real(l)+Random<Real>()-0.5)*13.7-8.7);
      rp.Set("siga",1.0);
    }
  
    IntpolAmp ia(rl,"fracl","sigfracl","a","siga",5.0);
  
    //ia.Dump();
  
    //std::cerr << ia.GetInterpolatedValue(ost::img::Point(0,0),11.5) << " " << 11.5*13.7-8.7 << std::endl;
    //TODO write more meaningfull test
  } catch(...) {
    BOOST_FAIL("Exception encountered while testing IntpolAmp");
  }
}


BOOST_AUTO_TEST_SUITE_END()


