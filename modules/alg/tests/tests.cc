//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Andreas Schenk, Ansgar Philippsen
*/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE iplt_alg
#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <ost/geom/geom.hh>


extern const Real tolerance=0.001;

boost::test_tools::predicate_result
compare_Vec2( const geom::Vec2& v1, const geom::Vec2& v2 )
{
  bool flag=true;
  boost::test_tools::predicate_result res( false );
  boost::test_tools::close_at_tolerance<Real> close_test(::boost::test_tools::percent_tolerance(tolerance));
  if(v1[0]==0.0){
    if(!boost::test_tools::check_is_small(v2[0],tolerance)){
     flag=false;
     res.message() << "Value for x differs: (" << v1[0] << "!=" << v2[0] << "). ";
    }
  }else if (v2[0]==0.0){
    if(!boost::test_tools::check_is_small(v1[0],tolerance)){
     flag=false;
     res.message() << "Value for x differs: (" << v1[0] << "!=" << v2[0] << "). ";
    }
  }else{
    if(!close_test(v1[0],v2[0])){
     flag=false;
     res.message() << "Value for x differs: (" << v1[0] << "!=" << v2[0] << "). ";
    }
  }
  if(v1[1]==0.0){
    if(!boost::test_tools::check_is_small(v2[1],tolerance)){
     flag=false;
     res.message() << "Value for y differs: (" << v1[1] << "!=" << v2[1] << "). ";
    }
  }else if (v2[1]==0.0){
    if(!boost::test_tools::check_is_small(v1[1],tolerance)){
     flag=false;
     res.message() << "Value for y differs: (" << v1[1] << "!=" << v2[1] << "). ";
    }
  }else{
    if(!close_test(v1[1],v2[1])){
     flag=false;
     res.message() << "Value for y differs: (" << v1[1] << "!=" << v2[1] << "). ";
    }
  }
  if(flag){
    return true;
  }else{
    return res;
  }
}

