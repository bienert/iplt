#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import math,random
import iplt
import ex

SetVerbosityLevel(5)

S=33.4
B=-0.1
E=0.1
N=50

linfit=ex.alg.LinearFit()
expfit=ex.alg.ExpFit(True)

pd1=gui.PlotData()
pd2=gui.PlotData()

for x in range(N):
    y=S*math.exp(B*float(x))
    y*=(1.0+(random.random()-0.5)*E)
    expfit.Add(x,y,1.0)
    pd1.AddXY(x,y)
    if y>0.0:
        linfit.AddDatapoint(float(x),math.log(y),1.0)
        pd2.AddXY(x,math.log(y))

expfit.Apply()
linfit.Apply()

print "orig:   S=%g B=%g E=%g"%(S,B,E)
print "expfit: S=%g B=%g"%(expfit.GetS(),expfit.GetB())
print "linfit: S=%g B=%g"%(math.exp(linfit.GetOffset()),linfit.GetScale())

pv1=gui.CreatePlotViewer()
pv1.AddData(pd1)
pv2=gui.CreatePlotViewer()
pv2.AddData(pd2)
