//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <cstdlib>

#include <iostream>
using namespace std;

#include <ost/img/image.hh>
#include <ost/img/value_util.hh>
#include <iplt/alg/gauss2d.hh>
#include <iplt/alg/fit_gauss2d_expl.hh>


using namespace iplt;
using namespace iplt::alg;

int main(int argc, char* argv[])
{
  if(argc<4) {
    cerr << "usage " << argv[0] << " mul add noise [seed]" << endl;
    return -1;
  }

  int N=12;

  double mul = atof(argv[1]);
  double add = atof(argv[2]);
  double noise = atof(argv[3]);

  srand48( (argc>4) ? atoi(argv[4]) : time(0) );

  //ImageHandle im = CreateImage( (ost::img::Extent(ost::img::Point(-100-N/2,-100-N/2),ost::img::Size(N,N))) );
  ExplGauss2DEntryList expl_list;
  
  FuncGauss2D f(1.0, // A 
	      3.0, 1.5, // Bx, By
	      0.0, // C
	      2.5-100.0, -1.3-100.0, // ux, uy 
	      13.0, //w
	      0.0,0.0 // Px, Py
	      );
  f.SetPixelSampling(1.0);
  f.SetExtent( (ost::img::Extent(ost::img::Point(-100-N/2,-100-N/2),ost::img::Size(N,N)))) ;

  double sum=0;
  for(ost::img::ExtentIterator it(f.GetExtent()); !it.AtEnd(); ++it) {
    double val = f.GetReal(it)+(noise*Random<double>());
    //im.SetReal(it,val*mul+add);
    ExplGauss2DEntry ent( (ost::img::Point(it))[0], (ost::img::Point(it))[1], val*mul+add);
    expl_list.push_back(ent);
    sum+=ent.value;
  }
  double ave=sum/double(expl_list.size());
  double sq_diff=0;
  for(ExplGauss2DEntryList::iterator it=expl_list.begin();it!=expl_list.end();it++)
  {
  	double diff=ave-it-> value;
  	sq_diff+=diff*diff;
  }
  double sdev=sq_diff/(double(expl_list.size())-1);
  double b0 = 0.25*double(N)/sqrt(log(2.0));

  FitExplGauss2D fit(1.0*mul, // A
		 b0,b0,// Bx, By
		 0.0+add, // C
		 3.0-100.0, -1.0-100.0, // u
		 0.0, // w
		 0.0,0.0 // Px
		 );

  fit.SetSigma(sdev);

  cerr << "original func: " << f << endl;
  cerr << "starting with: " << fit << endl;

  fit.Fit(expl_list);

  cerr << "found        : " << fit << endl;
  cerr << "used " << fit.GetIterationCount() << " iterations" << endl;
  
  FuncGauss2D f2 = fit.AsFunction();

  cerr << "gof=" <<  fit.GetGOF() << " qual=" << fit.GetQuality() << endl;
  
  return 0;
}
