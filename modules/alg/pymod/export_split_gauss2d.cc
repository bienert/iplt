//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/gauss2d.hh>
#include <iplt/alg/split_gauss2d.hh>
#include <iplt/alg/fit_split_gauss2d.hh>

void export_split_gauss2d() 
{
  using namespace ost::img;
  using namespace iplt::alg;

  class_<ParamsSplitGauss2D,bases<iplt::alg::ParamsGauss2D> >("ParamsSplitGauss2D", no_init)
    .def("SetDelta",&ParamsSplitGauss2D::SetDelta)
    .def("GetDelta",&ParamsSplitGauss2D::GetDelta)
    .def(self_ns::str(self))
    ;
      
  class_<FuncSplitGauss2D, bases<iplt::alg::FuncGauss2D> >("FuncSplitGauss2D", init<optional<Real, Real, Real, Real, Real, Real, Real,Real,Real,Real> >() )
    .def("SetDelta",&FuncSplitGauss2D::SetDelta)
    .def("GetDelta",&FuncSplitGauss2D::GetDelta)
    .def(self_ns::str(self))
    ;

  class_<FitSplitGauss2D, bases<NonModAlgorithm, ParamsSplitGauss2D> >("FitSplitGauss2D",init<optional<Real,Real,Real,Real,Real,Real,Real,Real,Real,Real> >())
    .def("SetSigma",&FitSplitGauss2D::SetSigma)
    .def("SetLim",&FitSplitGauss2D::SetLim)
    .def("SetMaxIter",&FitSplitGauss2D::SetMaxIter)
    .def("AsFunction",&FitSplitGauss2D::AsFunction)
    .def("GetGOF",&FitSplitGauss2D::GetGOF)
    .def("GetChi",&FitSplitGauss2D::GetChi)
    .def("GetSigAmp",&FitSplitGauss2D::GetSigAmp)
    .def("GetQuality",&FitSplitGauss2D::GetQuality)
    .def("GetAbsQuality",&FitSplitGauss2D::GetAbsQuality)
    .def("GetRelQuality",&FitSplitGauss2D::GetRelQuality)
    .def("GetIterationCount",&FitSplitGauss2D::GetIterationCount)
    .def(self_ns::str(self))
    ;
}
