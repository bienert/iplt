//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Giani Signorell
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/lattice_search.hh>
#include <ost/info/info.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

namespace {

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(set_ps_params,
					 LatticeSearch::SetPeakSearchParams,
					 4,6);
} // anon ns

void export_lattice_search()
{

  // lattice search algorithm
  class_<LatticeSearch, bases<NonModAlgorithm> >("LatticeSearch",  
						init<>() )
    .def("SetGaussFilterStrength",&LatticeSearch::SetGaussFilterStrength)
    .def("SetPeakSearchLimits",&LatticeSearch::SetPeakSearchLimits)
    .def("SetPeakSearchParams",&LatticeSearch::SetPeakSearchParams,set_ps_params() )
    .def("SetDVIPeakSearchParams",&LatticeSearch::SetDVIPeakSearchParams)
    .def("AddPeakSearchExclusion",&LatticeSearch::AddPeakSearchExclusion)
    .def("SetMinimalLatticeLength",&LatticeSearch::SetMinimalLatticeLength)
    .def("SetMaximalLatticeLength",&LatticeSearch::SetMaximalLatticeLength)
    .def("SetMinimalQuality",&LatticeSearch::SetMinimalQuality)
    .def("GetLattice",&LatticeSearch::GetLattice,
	 return_value_policy<copy_const_reference>() )
    .def("GetTempLattice",&LatticeSearch::GetTempLattice,
	 return_value_policy<copy_const_reference>() )
    .def("GetClosePoints",&LatticeSearch::GetClosePoints)
    .def("GetImagePeaks",&LatticeSearch::GetImagePeaks)
    .def("GetImagePeaks2",&LatticeSearch::GetImagePeaks2)
    .def("GetBestPoints",&LatticeSearch::GetBestPoints)
    .def("GetVectorImage",&LatticeSearch::GetVectorImage)
    ;

  void (*update1)(LatticeSearch&,const ost::info::InfoGroup&) = UpdateFromInfo;
  def("UpdateFromInfo",update1);

}
