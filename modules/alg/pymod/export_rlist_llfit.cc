
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/rlist_llfit.hh>

using namespace iplt::alg;
using namespace iplt;
using namespace ost::img;


void export_rlist_llfit()
{
  class_<RlistLLFit, bases<ReflectionModOPAlgorithm> >("RlistLLFit", init<const LLFitBasePtr&, Real,unsigned int,optional<unsigned int,const String&,const String&> >())
  .def("GetDiscretizedData",&RlistLLFit::GetDiscretizedData)
    ;

}



