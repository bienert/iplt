//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <iplt/alg/gauss2d.hh>
#include <iplt/alg/fit_gauss2d.hh>
#include <iplt/alg/fit_gauss2d_expl.hh>

using namespace boost::python;
using namespace ost::img;
using namespace iplt::alg;


void export_Gauss2D()
{
  class_<ParamsGauss2D>("ParamsGauss2D", init<Real,Real,Real,Real,Real,Real,Real,Real,Real>() )
    .def("SetA",&ParamsGauss2D::SetA)
    .def("GetA",&ParamsGauss2D::GetA)
    .def("SetBx",&ParamsGauss2D::SetBx)
    .def("GetBx",&ParamsGauss2D::GetBx)
    .def("SetBy",&ParamsGauss2D::SetBy)
    .def("GetBy",&ParamsGauss2D::GetBy)
    .def("GetB",&ParamsGauss2D::GetB)
    .def("SetB",&ParamsGauss2D::SetB)
    .def("SetC",&ParamsGauss2D::SetC)
    .def("GetC",&ParamsGauss2D::GetC)
    .def("SetUx",&ParamsGauss2D::SetUx)
    .def("SetU",&ParamsGauss2D::SetU)
    .def("GetUx",&ParamsGauss2D::GetUx)
    .def("GetU",&ParamsGauss2D::GetU)
    .def("SetUy",&ParamsGauss2D::SetUy)
    .def("SetU", &ParamsGauss2D::SetU)
    .def("GetUy",&ParamsGauss2D::GetUy)
    .def("SetW",&ParamsGauss2D::SetW)
    .def("GetW",&ParamsGauss2D::GetW)
    .def("GetU",&ParamsGauss2D::GetU)
    .def("SetPx",&ParamsGauss2D::SetPx)
    .def("GetPx",&ParamsGauss2D::GetPx)
    .def("SetPy",&ParamsGauss2D::SetPy)
    .def("GetPy",&ParamsGauss2D::GetPy)
    .def("GetP",&ParamsGauss2D::GetP)
    .def("GetVolume",&ParamsGauss2D::GetVolume)
    ;


  class_<FuncGauss2D, bases<RealFunction, ParamsGauss2D> >("FuncGauss2D", init<optional<Real, Real, Real, Real, Real, Real, Real,Real,Real> >() )
    .def(init<const ParamsGauss2D&>())
    .def("SetA",&FuncGauss2D::SetA)
    .def("SetBx",&FuncGauss2D::SetBx)
    .def("SetBy",&FuncGauss2D::SetBy)
    .def("SetC",&FuncGauss2D::SetC)
    .def("SetUx",&FuncGauss2D::SetUx)
    .def("SetUy",&FuncGauss2D::SetUy)
    .def("SetW",&FuncGauss2D::SetW)
    .def("SetPx",&FuncGauss2D::SetPx)
    .def("SetPy",&FuncGauss2D::SetPy)
    ;

  class_<FitGauss2D, bases<NonModAlgorithm, ParamsGauss2D> >("FitGauss2D", init<optional<Real, Real, Real, Real, Real, Real, Real,Real,Real> >() )
    .def("SetSigma",&FitGauss2D::SetSigma)
    .def("SetLim",&FitGauss2D::SetLim)
    .def("SetMaxIter",&FitGauss2D::SetMaxIter)
    .def("GetMaxIter",&FitGauss2D::GetMaxIter)
    .def("AsFunction",&FitGauss2D::AsFunction)
    .def("GetGOF",&FitGauss2D::GetGOF)
    .def("GetChi",&FitGauss2D::GetChi)
    .def("GetSigAmp",&FitGauss2D::GetSigAmp)
    .def("GetQuality",&FitGauss2D::GetQuality)
    .def("GetAbsQuality",&FitGauss2D::GetAbsQuality)
    .def("GetRelQuality",&FitGauss2D::GetRelQuality)
    .def("GetIterationCount",&FitGauss2D::GetIterationCount)
    .def(self_ns::str(self))
    ;

  class_<FitExplGauss2D, bases<ParamsGauss2D> >("FitExplGauss2D", init<optional<Real, Real, Real, Real, Real, Real, Real,Real,Real> >() )
    .def("SetSigma",&FitExplGauss2D::SetSigma)
    .def("SetLim",&FitExplGauss2D::SetLim)
    .def("SetMaxIter",&FitExplGauss2D::SetMaxIter)
    .def("AsFunction",&FitExplGauss2D::AsFunction)
    .def("GetGOF",&FitExplGauss2D::GetGOF)
    .def("GetChi",&FitExplGauss2D::GetChi)
    .def("GetSigAmp",&FitExplGauss2D::GetSigAmp)
    .def("GetQuality",&FitExplGauss2D::GetQuality)
    .def("GetAbsQuality",&FitExplGauss2D::GetAbsQuality)
    .def("GetRelQuality",&FitExplGauss2D::GetRelQuality)
    .def("GetIterationCount",&FitExplGauss2D::GetIterationCount)
    .def("Fit",&FitExplGauss2D::Fit)
    .def(self_ns::str(self))
    ;

  class_<Gauss2DPeak, bases<Point,ParamsGauss2D> >("Gauss2DPeak", init<>() )
    .def(init<const ost::img::Point&, const ParamsGauss2D&>() )
    ;

  class_<ExplGauss2DEntry>("ExplGauss2DEntry", init<int, int, Real>() )
    .def_readwrite("x", &ExplGauss2DEntry::x)
    .def_readwrite("y", &ExplGauss2DEntry::y)
    .def_readwrite("value", &ExplGauss2DEntry::value)
    ;


  class_<iplt::alg::ExplGauss2DEntryList>("ExplGauss2DEntryList", init<>())
    .def(vector_indexing_suite<iplt::alg::ExplGauss2DEntryList>())
    ;

}
