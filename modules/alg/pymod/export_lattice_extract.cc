//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <boost/python.hpp>
using namespace boost::python;

#include <iplt/alg/lattice_extract.hh>
#include <ost/info/info.hh>

using namespace ost;
using namespace ost::img;
using namespace iplt;
using namespace iplt::alg;

void export_lattice_extract()
{
  // lattice extract algorithm
  class_<LatticeExtract, bases<NonModAlgorithm> >("LatticeExtract",
						 init<const Lattice&,int,int, bool,bool>() )
    .def("GetLattice",&LatticeExtract::GetLattice,
	 return_value_policy<copy_const_reference>() )
    .def("SetLattice",&LatticeExtract::SetLattice)
    .def("GetStartSize",&LatticeExtract::GetStartSize)
    .def("GetEndSize",&LatticeExtract::GetEndSize)
    .def("SetFitSize",&LatticeExtract::SetFitSize)
    .def("GetList",&LatticeExtract::GetList)
    .def("GetCorrectedImage",&LatticeExtract::GetCorrectedImage)
    ;

  void (*update2)(LatticeExtract&,const ost::info::InfoGroup&) = UpdateFromInfo;
  def("UpdateFromInfo",update2);
}
