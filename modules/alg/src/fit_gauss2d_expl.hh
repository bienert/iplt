//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Gauss2D fitting

  Author: Ansgar Philippsen
*/

#ifndef IPLT_ALG_FIT_GAUSS2D_EXPL_H
#define IPLT_ALG_FIT_GAUSS2D_EXPL_H

#include "gauss2d.hh"

#include <ost/img/algorithm.hh>
#include <ost/img/image.hh>
#include <ost/message.hh>

namespace iplt {  namespace alg {

struct DLLEXPORT_IPLT_ALG ExplGauss2DEntry {
  ExplGauss2DEntry(int xx, int yy, double vv):
    x(xx),y(yy),value(vv) {}

  bool operator==(const ExplGauss2DEntry &e) const
  {
    return x==e.x && y==e.y && value==e.value;
  } 
  int x,y;
  double value;
};



typedef std::vector<ExplGauss2DEntry> ExplGauss2DEntryList;

//! 2D Gaussian explicit fitting algorithm
class DLLEXPORT_IPLT_ALG FitExplGauss2D: public ParamsGauss2D
{
public:
  enum {
    ID_A=0,
    ID_BX, ID_BY,
    ID_UX, ID_UY,
    ID_C,
    ID_W,
    ID_PX, ID_PY
  };

  FitExplGauss2D(double A=1.0, double Bx=1.0, double By=1.0, double C=0.0, double ux=0.0, double uy=0.0, double w=0.0, double Px=0.0, double Py=0.0);

  void SetSigma(double s) {sigma_ = s;}
  void SetMaxIter(int n) {max_iter_ = n;}
  void SetLim(double l1, double l2) {lim1_=l1; lim2_=l2;}

  double GetChi() const {return chi_;}
  double GetGOF() const {return gof_;}
  double GetQuality() const {return qual_;}
  double GetAbsQuality() const {return qual_;}
  double GetRelQuality() const {return qual2_;}
  int GetIterationCount() const {return itcount_;}
  double GetSigAmp() const {return sigamp_;}

  // return as function
  FuncGauss2D AsFunction() const;

  void Fit(const ExplGauss2DEntryList& entry_list);

private:
  int max_iter_;
  double chi_, gof_, qual_, qual2_, sigamp_;
  double sigma_;
  double lim1_, lim2_;
  int itcount_;
};


DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream& o, const FitExplGauss2D& f);

}} // namespaces

#endif
