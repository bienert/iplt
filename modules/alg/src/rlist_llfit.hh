//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef RLIST_LLFIT_HH
#define RLIST_LLFIT_HH

#include <iplt/reflection_algorithm.hh>
#include <iplt/alg/module_config.hh>
#include <iplt/alg/llfit_base.hh>

namespace iplt {  namespace alg {

class DLLEXPORT_IPLT_ALG RlistLLFit : public ReflectionModOPAlgorithm
{
public:
  enum guessmode {
    GUESSMODE_BINAVERAGE,
    GUESSMODE_CONST,
    GUESSMODE_RANDOM
  };
    RlistLLFit(const LLFitBasePtr& llfit, Real max_resolution, unsigned int min_bincount, unsigned int bootstrapping_iterations=0,const String& iobs_name="",const String& sigiobs_name="");
    virtual ReflectionList Visit(const ReflectionList& rlist);
    ReflectionList GetDiscretizedData();
protected:
    LLFitBasePtr llfit_;
    Real max_resolution_;
    unsigned int min_bincount_;
    unsigned int bootstrapping_iterations_;
    ReflectionList discretized_;
    String iobs_name_;
    String sigiobs_name_;
};

}} //ns

#endif // RLIST_LLFIT_HH
