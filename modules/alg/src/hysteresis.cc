//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  author: Andreas Schenk
*/

#include "hysteresis.hh"
#include <ost/img/alg/norm.hh>

namespace iplt {  namespace alg {

//! Everything above upperthreshold ist recognised as edge pixel. Everything above lowerthreshold is recognised as edge pixel if it continues an edge. Both values are between 0 and 1.
Hysteresis::Hysteresis(Real upperthreshold, Real lowerthreshold):
  ost::img::ConstModIPAlgorithm("Hysteresis"), upperthreshold_(upperthreshold), lowerthreshold_(lowerthreshold)
{}

void Hysteresis::Visit(ost::img::ImageHandle& i) const
{
    ost::img::NormalizerPtr nptr=ost::img::alg::CreateLinearRangeNormalizer(i,0.0,1.0).GetImplementation();

  i.Apply(*(nptr.get()));
  for(ost::img::ExtentIterator it(i.GetExtent()); !it.AtEnd(); ++it) {
    if(i.GetReal(it)<=1.0 && i.GetReal(it)>=upperthreshold_)
        set_edge(i,(ost::img::Point) it);
  }	
  for(ost::img::ExtentIterator it(i.GetExtent()); !it.AtEnd(); ++it) {
    if(i.GetReal(it)<=1.0 )
      i.SetReal(it,0.0);
  }	
  nptr=ost::img::alg::CreateLinearRangeNormalizer(i,0.0,1.0).GetImplementation();
  i.Apply(*(nptr.get()));
}

void Hysteresis::set_edge(ost::img::ImageHandle& i,const ost::img::Point& p) const
{
  i.SetReal(p,1.1);
  for(ost::img::ExtentIterator it(ost::img::Extent(ost::img::Size(3,3),p)); !it.AtEnd(); ++it) {
      if(p!=ost::img::Point(it)) {
      if(i.GetReal(it)<=1.0) {
        if(i.GetReal(it)>=lowerthreshold_) {
            set_edge(i,(ost::img::Point) it);
        } else {
	      i.SetReal(it,0.0);
	    }	
      }
    }
  }
}

}}//ns
