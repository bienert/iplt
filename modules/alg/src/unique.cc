//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/units.hh>
#include <iplt/symmetry_reflection.hh>
#include "unique.hh"

namespace iplt {  namespace alg {

ReflectionList Unique::Visit(const ReflectionList& r) const
{
  if(r.NumReflections()==0){
    return r;
  }
  String phaid=r.GetPropertyNameByType(iplt::PT_PHASE);
  ReflectionList result;
  result.CopyProperties(r);
  if (phaid=="") {
    ReflectionProxyter it=r.Begin();
    ReflectionIndex idx=it.GetIndex();
    unsigned int num=0;
    Real sums[r.GetPropertyCount()];
    for(unsigned int i=0;i<r.GetPropertyCount();++i){
      sums[i]=0.0;
    }
    for (; !it.AtEnd(); ++it) {
      if (idx==it.GetIndex()) {
        num++;
        for(unsigned int i=0;i<r.GetPropertyCount();++i){
          sums[i]+=it.Get(i);
        }
      } else {
        ReflectionProxyter tmp=result.AddReflection(idx);
        for(unsigned int i=0;i<r.GetPropertyCount();++i){
          tmp.Set(i,sums[i]/(static_cast<Real>(num)));
          sums[i]=it.Get(i);
        }
        num=1;
      }
    }
    ReflectionProxyter tmp=result.AddReflection(idx);
    for(unsigned int i=0;i<r.GetPropertyCount();++i){
      tmp.Set(i,sums[i]/(static_cast<Real>(num)));
    }
  } else {
    String ampid=r.GetPropertyNameByType(iplt::PT_AMPLITUDE);
    if (ampid=="") {
      throw("no amplitude data found");
    }
    ReflectionProxyter it=r.Begin();
    SymmetryReflection sr(it.GetIndex(), 0.0,0.0);
    unsigned int num=0;
    Real sums[r.GetPropertyCount()];
    for(unsigned int i=0;i<r.GetPropertyCount();++i){
      sums[i]=0.0;
    }
    for (; !it.AtEnd(); ++it) {
      if (sr.GetReflectionIndex()==it.GetIndex()) {
        sr=sr+SymmetryReflection(it.GetIndex(), it.Get(ampid), it.Get(phaid));
        num++;
        for(unsigned int i=0;i<r.GetPropertyCount();++i){
          sums[i]+=it.Get(i);
        }
      } else {
        ReflectionProxyter tmp=result.AddReflection(sr.GetReflectionIndex());
        sr=sr/(static_cast<Real>(num));
        tmp.Set(ampid, sr.GetAmplitude());
        tmp.Set(phaid, sr.GetPhase());
        for(unsigned int i=0;i<r.GetPropertyCount();++i){
          if(r.GetPropertyName(i)!=ampid && r.GetPropertyName(i)!=phaid){
            tmp.Set(i,sums[i]/(static_cast<Real>(num)));
            sums[i]=it.Get(i);
          }
        }
        sr=SymmetryReflection(it.GetIndex(), it.Get(ampid), it.Get(phaid));
        num=1;
      }
    }
    ReflectionProxyter tmp=result.AddReflection(sr.GetReflectionIndex());
    sr=sr/(static_cast<Real>(num));
    tmp.Set(ampid, sr.GetAmplitude());
    tmp.Set(phaid, sr.GetPhase());
    for(unsigned int i=0;i<r.GetPropertyCount();++i){
      if(r.GetPropertyName(i)!=ampid && r.GetPropertyName(i)!=phaid){
        tmp.Set(i,sums[i]/(static_cast<Real>(num)));
      }
    }
  }
  return result;
}

}} // ns
