//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <iostream>

#include <ost/log.hh>
#include <ost/base.hh>
#include <ost/img/value_util.hh>

#include "llfit_base.hh"

namespace iplt {  namespace alg {

Real Sinc(Real x, Real w, Real b2)
{
  if(x==0.0) return 1.0;
  Real pwx=M_PI*w*x;
  return b2==0.0 ? sin(pwx)/pwx : exp(-b2*pwx*pwx)*sin(pwx)/pwx;
}


LLFitBase::LLFitBase(unsigned int count, Real sampling, guessmode gm):
  count_(count), sampling_(sampling),
  intens_list_(),
  phase_list_(),
  max_iter_(1000),iter_count_(0),
  lim1_(1e-3),lim2_(1e-3),
  curve_(),dirty_curve_(true),curve_count_(10000),
  max_zstar_(1.0),max_intens_(1.0),
  relative_weighting_(1.0),
  bfac_(0.0),
  guessmode_(gm)
{
}

LLFitBase::~LLFitBase()
{}


namespace {
bool check_zstar_range(Real zstar, Real sampling)
{
  if(zstar>(0.5/sampling)) {
    LOG_DEBUG( "ignored entry due to zstar out of bounds (" << zstar << " > " << 0.5/sampling << ")" );
    return false;
  } else if(zstar<(-0.5/sampling)) {
    LOG_DEBUG( "ignored entry due to zstar out of bounds (" << zstar << " < " << -0.5/sampling << ")" );
    return false;
  }
  return true;
}
} // anon ns

void LLFitBase::AddIntens(Real zstar, Real intens, Real sig_intens)
{
  if(check_zstar_range(zstar,sampling_) && sig_intens>0.0) {
    detail::IntensEntry en(zstar,intens,sig_intens);
    intens_list_.push_back(en);
    LOG_DEBUG( "adding entry with zstar=" << zstar << " int= " << intens << " sigint= " << sig_intens );
  }
}

void LLFitBase::AddPhase(Real zstar, Real phase, Real sig_phase)
{
  if(check_zstar_range(zstar,sampling_) && sig_phase>0.0) {
    detail::PhaseEntry en={zstar,phase,sig_phase};
    phase_list_.push_back(en);
    LOG_DEBUG( "adding entry with zstar=" << zstar << " phi= " << phase << " sigphi= " << sig_phase );
  }
}

void LLFitBase::Clear()
{
  intens_list_.clear();
  phase_list_.clear();
}

void LLFitBase::SetMaxIter(unsigned int m)
{
  max_iter_=m;
}
unsigned int LLFitBase::GetMaxIter()
{
  return max_iter_;
}

unsigned int LLFitBase::GetIterCount() const
{
  return iter_count_;
}

unsigned int LLFitBase::GetCount() const
{
  return count_;
}
void LLFitBase::SetCount(unsigned int count)
{
  count_=count;
  Cleanup();
}


Real LLFitBase::GetSampling() const
{
  return sampling_;
}

void LLFitBase::SetSampling(Real sampling)
{
  sampling_=sampling;
  Cleanup();
}

void LLFitBase::SetIterationLimits(Real l1,Real l2)
{
  lim1_=l1;
  lim2_=l2;
}

void LLFitBase::SetRelativeZWeight(Real w)
{
  relative_weighting_=w;
}

std::pair<Real,Real> LLFitBase::CalcPhaseAt(Real zstar) const
{
  return std::make_pair(0.0,1.0);
}

LLFitAmpCurvePoint LLFitBase::ClosestIntensAt(Real zstar, Real zstar_w, Real intens, Real intens_w)
{
  return do_closest_at(zstar,zstar_w,intens,intens_w,false);
}

LLFitAmpCurvePoint LLFitBase::RelativeClosestIntensAt(Real zstar, Real intens)
{
  return do_closest_at(zstar,1.0,intens,1.0,true);
}

LLFitAmpCurvePoint LLFitBase::do_closest_at(Real zstar, Real zstar_w, Real intens, Real intens_w, bool relative)
{
  if(dirty_curve_) {
    curve_.clear();
    max_intens_ = 1e-100;
    Real curve_start = 0.0;
    Real curve_end = 0.5/sampling_;
    max_zstar_ = curve_end;
    Real f = (curve_end-curve_start)/static_cast<Real>(curve_count_);
    for(int n=0;n<curve_count_;++n) {
      Real zs = static_cast<Real>(n)*f+curve_start;
      std::pair<Real,Real> result = CalcIntensAt(zs);
      LLFitAmpCurvePoint cp = {zs,result.first,result.second};
      curve_.push_back(cp);
      max_intens_ = std::max(result.first,max_intens_);
    }
    dirty_curve_=false;
  }

  Real r_dist2 = 1e100;
  LLFitAmpCurvePoint r_cp;
  if(relative) {
    for(std::vector<LLFitAmpCurvePoint>::const_iterator it=curve_.begin();it!=curve_.end();++it) {
      Real d1 = relative_weighting_*(it->zfit-zstar)/max_zstar_;
      Real d2 = (it->ifit-intens)/max_intens_;
      Real dd = d1*d1+d2*d2;
      if(dd<r_dist2) {
	r_dist2 = dd;
	r_cp=(*it);
      }
    }
  } else {
    // major code duplication on purpose to avoid the 'if(relative)' in the loop
    for(std::vector<LLFitAmpCurvePoint>::const_iterator it=curve_.begin();it!=curve_.end();++it) {
      Real d1 = relative_weighting_*(it->zfit-zstar)*zstar_w;
      Real d2 = (it->ifit-intens)*intens_w;
      Real dd = d1*d1+d2*d2;
      if(dd<r_dist2) {
	r_dist2 = dd;
	r_cp=(*it);
      }
    }
  }
  return r_cp;
}
      
void LLFitBase::Cleanup()
{
  intens_list_.clear();
  phase_list_.clear();
  curve_.clear();
  dirty_curve_=true;
}

void LLFitBase::SetBFactor(Real b)
{
  if(b<0.0) b=0.0;
  bfac_=b;
}

Real LLFitBase::GetBFactor() const
{
  return bfac_;
}

}} // ns
