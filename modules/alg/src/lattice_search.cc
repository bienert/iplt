//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  2D lattice search on image

  Authors: Giani Signorell, Ansgar Philippsen
*/

#include <iostream>
#include <sstream>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>

#include <ost/base.hh>
#include <ost/img/point.hh>
#include <ost/log.hh>

#include <ost/info/info.hh>

#include <ost/img/alg/stat.hh>
#include <ost/img/alg/gaussian.hh>

#include <iplt/alg/fit_gauss2d.hh>

#include "lattice_search.hh"

namespace iplt {  namespace alg {

using namespace ::ost::img::alg;

LatticeSearch::LatticeSearch() :
  NonModAlgorithm("LatticeSearch"),
  lattice_(Lattice(geom::Vec2(100.0,0.0),Vec2(0.0,100.0))),
  tmp_lattice_(Lattice(geom::Vec2(100.0,0.0),Vec2(0.0,100.0)))
{
// REFACTOR: constructor initialization list
  _gfI_sigma = 1.0; // initial gauss filter strength

  _i_peaks_absmin = 5; // hardcoded lower limit for minimal peaks
  _i_peaks_min = 10; // minimal number of peaks to accept in peaksearch
  _i_peaks_max = 500; // maximal number of peaks to accept in peaksearch
  _i_peaks_maxiter = 10; // maximal number of tries to adjust peaksearch params

  _psI_rad = 10;    // peaksearch ctor param
  _psI_sens = 0.01; // "
  _psI_irad = 5;    // "
  _psI_isens = 0;   // "
  _psI_iclim = 0;   // "
  _psI_icval = 0.0; // "

  _gfV_sigma = 0.5; // first vector image smoothing strength

  _v_peaks_min = 20; // minimal peaks for vimage
  _v_peaks_max = 500; // maximal peaks for vimage

  _psV_rad = 3;     // vimage peaksearch ctor params
  _psV_sens = 10.0; // "
  _psV_irad = 1;    // "
  _psV_isens = 0.0; // "

  _vWidth = 500;
  _vHeight = 500;

  _q_desired = 0.1; //0.5
  _q_limit = 1.0; //0.01->takes almost all (risk of long calculations 
                //for no results...
  _iter_limit = 10;

  lat_min_length_=3.0;
  lat_max_length_=std::numeric_limits<int>::max();

  _aPx = 1;

  _latfit_maxiter=5; // maximal iterations during lattice fitting

  col_tolerance_ = 5.0*M_PI/180.0; // 5 deg default
}

/*
  main entry point of algorithm
*/

void LatticeSearch::Visit(const ost::img::ConstImageHandle& img)
{  
  LOG_DEBUG( "LatticeSearch: " << "image statistics" );

  Stat stat;

  img.Apply(stat);
  _im_mean = stat.GetMean();
  LOG_DEBUG( "LatticeSearch: " << "stat: " << stat );

  ConstImageHandle workimg; // prepare working image handle

  if(_gfI_sigma == 0.0) {
    // work with orig image
    workimg=img.Copy(true);
  } else{
    // use gaussian filtered one
    LOG_VERBOSE( "LatticeSearch: " << "applying gaussian filter to workimg" );
    ost::img::alg::GaussianFilter gf_i = GaussianFilter(_gfI_sigma);
    workimg = img.Apply(gf_i);
  }

  LOG_VERBOSE( "LatticeSearch: " << "searching candidate peaks and first lattice vectors" );

  tmp_lattice_ = get_best_points(workimg);

  LOG_VERBOSE( "LatticeSearch: " << "determining preliminary offset" );
  lattice_ = Lattice(tmp_lattice_.GetFirst(),
			 tmp_lattice_.GetSecond(),
			 determine_offset(tmp_lattice_, workimg));
  LOG_VERBOSE( "LatticeSearch: " << "lattice found: " << lattice_ );
}


////////////////////////////////
// private methods



Lattice LatticeSearch::get_best_points(const ost::img::ConstImageHandle& img)
{
  // set up iterative search
  bool repeat_all = true;
  int iteration = 0;
  geom::Vec2 bp_1;
  geom::Vec2 bp_2;
  Real best_quality = 0.0;
  
  while(repeat_all) {
    /*
      search peaks in original image, ensure that number
      of peaks is within a sensible limit for proceeding
      vector image routine to work properly.
    */
    Vec2List image_peaks_float=gen_image_peaks(img);

    LOG_DEBUG( "LatticeSearch: " << "generating first vector image" );
 
    ost::img::ImageHandle vector_image = gen_vector_image(image_peaks_float);
    
    if(_gfV_sigma == 0.0){
    } else {
      GaussianFilter gf_v = GaussianFilter(_gfV_sigma);
      vector_image.ApplyIP(gf_v);
    }

    LOG_DEBUG( "LatticeSearch: " << "searching peaks in first vector image" );

    Vec2List vector_peaks = gen_vector_peaks(vector_image);
 
    // generate vector image from the vector image an search for 
    // peaks there

    LOG_DEBUG( "LatticeSearch: " << "generating second vector image" );
    LOG_DEBUG( "LatticeSearch: " << "using " << vector_peaks.size() << " peaks of first vector image" );

    ost::img::ImageHandle vi_vector_image = gen_vector_image(vector_peaks);

    if(_gfV_sigma == 0.0){
    } else {
      GaussianFilter gf_v = GaussianFilter(_gfV_sigma);
      vi_vector_image.ApplyIP(gf_v);
    }

    LOG_DEBUG( "LatticeSearch: " << "searching peaks in second vector image" );

    Vec2List vi_vector_peaks = gen_vector_peaks(vi_vector_image);

    LOG_DEBUG( "LatticeSearch: " << "looking for 'close' points" );
    LOG_DEBUG( "LatticeSearch: " << "using " << vi_vector_peaks.size() << " peaks from second vector image" );

    geom::Vec2 cp1,cp2;

    if(!gen_close_points(vi_vector_peaks, cp1, cp2)) {
      throw ost::Error("fatal error: no close points identified");
    }

    if(cp1==Vec2() || cp2==Vec2()) {
      throw ost::Error("fatal error: no close points identified");
    }

    LOG_DEBUG( "LatticeSearch: " << "determining quality of close points" );

    Real quality = calc_quality( cp1,cp2, vi_vector_image, _aPx);

#if 1
    // hack to avoid quality calculation
    quality=_q_desired;
#endif
    
    LOG_DEBUG( "LatticeSearch: " << "Quality = " << quality );
    //replace best points
    if(quality > best_quality){
      best_quality = quality;
      bp_1 = cp1;
      bp_2 = cp2;
      _best_vector_image =vi_vector_image; //to see vi_vi
    }

    //changes for better quality...
    iteration++;
    LOG_DEBUG( "LatticeSearch: " << "Refining quality" );
    if(best_quality < _q_desired){
      LOG_DEBUG( "LatticeSearch: " << "Best quality " << best_quality << " is below desired value of " << _q_desired );
      if(iteration >= _iter_limit){
	LOG_DEBUG( "LatticeSearch: " << "All iterations exhausted,");
	if(best_quality < _q_limit){
	  throw(Error("overall quality criteria not reached!"));
	}
	LOG_DEBUG( "LatticeSearch: " << " will continue with quality of: " << best_quality );
	repeat_all = false;
      } else {
	if(iteration < 3){
	  _psI_sens = _psI_sens * 2.0;
	  LOG_DEBUG( "LatticeSearch: " << " _psI_sens changed to: " << _psI_sens );
	} else if(iteration < 5) {
	  _psI_rad = _psI_rad + 5;
	  LOG_DEBUG( "LatticeSearch: " << " _psI_rad changed to: " << _psI_rad );
	} else if(iteration < 8) {
	  _psV_sens = _psV_sens * 2.0;
	  LOG_DEBUG( "LatticeSearch: " << " _psV_sens changed to: " << _psV_sens );
	} else {
	  _psV_rad = _psV_rad + 5;
	  LOG_DEBUG( "LatticeSearch: " << " _psV_rad changed to: " << _psV_rad );
	}
	LOG_DEBUG( "LatticeSearch: " << "commencing new overall search" );
      }
    } else {
      repeat_all = false;
    }
  }

  _best_points.push_back(bp_1);
  _best_points.push_back(bp_2);

  LOG_DEBUG( "LatticeSearch: " << "Best points: " << bp_1 << " and " << bp_2 );
  LOG_DEBUG( "LatticeSearch: " << "Best quality: " << best_quality );

  // use peak search radius for fit
  //Point sub_offset(_psV_rad,_psV_rad);
  // use smaller radius for fit
  geom::Vec2 sub_offset(6,6);

  LOG_DEBUG( "LatticeSearch: " << "fitting peaks to subpixels" );

  ost::img::ImageHandle subimg1=_best_vector_image.Extract(ost::img::Extent(ost::img::Point(bp_1-sub_offset),ost::img::Point( bp_1+sub_offset)));
  ost::img::ImageHandle subimg2=_best_vector_image.Extract(ost::img::Extent(ost::img::Point(bp_2-sub_offset), ost::img::Point(bp_2+sub_offset)));

  Real b0 = 1.0/sqrt(log(2.0));

  alg::FitGauss2D gauss2d_fit1(_best_vector_image.GetReal(ost::img::Point(bp_1)), // A
			       b0, // Bx
			       b0, // By
			       0.0, // C
			       (Real)bp_1[0], // Ux
			       (Real)bp_1[1], // Uy
			       0.0 // w
			       );
  gauss2d_fit1.SetMaxIter(100); // TODO fix hardcoded value 
  gauss2d_fit1.SetLim(1e-12,1e-12); // TODO fix hardcoded value
  
  alg::FitGauss2D gauss2d_fit2(_best_vector_image.GetReal(ost::img::Point(bp_2)), // A
			       b0, // Bx
			       b0, // By
			       0.0, // C
			       (Real)bp_2[0], // Ux
			       (Real)bp_2[1], // Uy
			       0.0 // w
			       );
  gauss2d_fit2.SetMaxIter(100); // TODO fix hardcoded value 
  gauss2d_fit2.SetLim(1e-12,1e-12); // TODO fix hardcoded value
  
  
  LOG_DEBUG( "LatticeSearch: " << "subimg1 prefit: " << gauss2d_fit1 << " " );
  subimg1.Apply(gauss2d_fit1);
  LOG_DEBUG( "LatticeSearch: " << "subimg1 postfit:" << gauss2d_fit1 << " " );

  LOG_DEBUG( "LatticeSearch: " << "subimg2 prefit: " << gauss2d_fit2 << " " );
  subimg2.Apply(gauss2d_fit2);
  LOG_DEBUG( "LatticeSearch: " << "subimg2 postfit:" << gauss2d_fit2 << " " );

  // TODO add GOF criteria for abort
  
  geom::Vec2 bvec_1(gauss2d_fit1.GetU());
  geom::Vec2 bvec_2(gauss2d_fit2.GetU());

  LOG_DEBUG( "LatticeSearch: " << "Fitted bp: " << bvec_1 <<" and " << bvec_2 );
  return Lattice(bvec_1, bvec_2);
}

/*
  for each peak, determine normalized lattice difference
  use peak position with smallest diff as offset

  _image_peaks is used as the peak list
*/
Vec2 LatticeSearch::determine_offset(const Lattice& lat, 
				     const ost::img::ConstImageHandle& img)
{
  PeakList::const_iterator it=_image_peaks.begin();

  Lattice tmplat(lat);
  tmplat.SetOffset((*it).ToVec2());
  Real min_diff = calc_lattice_dist(lat);
  ost::img::Point min_off(*it);

  for(++it; it!=_image_peaks.end(); ++it) {
    tmplat.SetOffset((*it).ToVec2());
    Real diff = calc_lattice_dist(tmplat);
    LOG_DEBUG( "LatticeSearch: " << "for Peak " << *it << " r=" << diff );
    if(diff<min_diff) {
      min_diff=diff;
      min_off=*it;
    }
  }

#if 0
  // move found offset to approximately center position
  tmplat=lat;
  tmplat.SetOffset(min_off.ToVec2());
  geom::Vec2 nm(tmplat.CalcComponents(img.GetExtent().GetCenter()));

  Real n = rint(nm[0]);
  Real m = rint(nm[1]);
  
  return geom::Vec2(n*tmplat.GetFirst()+
	      m*tmplat.GetSecond()+
	      tmplat.GetOffset()
	      );
#else
  return min_off.ToVec2();
#endif

}

Vec2List LatticeSearch::gen_image_peaks(const ost::img::ConstImageHandle& img)
{
  bool ips_repeat = true;
  Real iSensFact = 0.5;
  Real iSensFactLim = 1.5;
  Real iSensFactInc = 0.1;
  int iter_count=0;
  Vec2List result;
  while(ips_repeat){
    iter_count++;
    if(iter_count > _i_peaks_maxiter) {
      throw ost::Error("maximum iteration count for peak-search reached");
    }

    LOG_DEBUG( "LatticeSearch: " << "running peak search with " << _psI_rad << " " << _psI_sens << " " << _psI_irad << " " << _psI_isens << " " << _psI_iclim << " " << _psI_icval );
    PeakSearch ps_i = PeakSearch(_psI_rad,_psI_sens,
				 _psI_irad,_psI_isens,
				 _psI_iclim, _psI_icval);

    for (std::vector<Extent>::const_iterator it=ext_list_.begin();
	 it!=ext_list_.end();++it) {
      ps_i.AddExclusion((*it));
    }
    img.Apply(ps_i);
    _image_peaks = ps_i.GetPeakList();
    result.clear();
    for(PeakList::const_iterator it=_image_peaks.begin();it!=_image_peaks.end();++it){
      ::ost::img::alg::Stat stat;
      ost::img::ImageHandle subimage=img.Extract(ost::img::Extent(ost::img::Size(3,3),*it));
      subimage.ApplyIP(stat);
      result.push_back(geom::Vec2(stat.GetCenterOfMass()));
    }
    int n_peaks = _image_peaks.size();    
    if(n_peaks < _i_peaks_min){
      LOG_DEBUG( "LatticeSearch: " << "only " << n_peaks << " image peaks found, limit is " << _i_peaks_min );
      _psI_sens = iSensFact * _psI_sens;
      LOG_DEBUG( "LatticeSearch: " << " _psI_sens changed to: " << _psI_sens );
      iSensFact += iSensFactInc;
      if(iSensFact > iSensFactLim){
	if(n_peaks < _i_peaks_absmin){
	  throw(Error("n_peaks less then absolute minimum"));
	}
	ips_repeat = false;
	LOG_DEBUG( "LatticeSearch: " << "peak count not in desired range = " << n_peaks );
      }
    } else if(n_peaks > _i_peaks_max){
      LOG_DEBUG( "LatticeSearch: " << "too many (" << n_peaks << ") image peaks found, limit is " << _i_peaks_max );
      _psI_sens = _psI_sens/iSensFact;
      LOG_DEBUG( "LatticeSearch: " << " _psI_sens changed to: " << _psI_sens );
      iSensFact += iSensFactInc;
      if(iSensFact > iSensFactLim){
	ips_repeat = false;
	LOG_DEBUG( "LatticeSearch: " << "peak count not in desired range = " << n_peaks );
      }
    } else {
      LOG_DEBUG( "LatticeSearch: " << "PeakSearchSensitivity refined to " <<  _psI_sens );
      LOG_DEBUG( "LatticeSearch: " << n_peaks << " peaks found, moving on..." );
      ips_repeat = false;
    } 
  }
  return result;
}

ImageHandle LatticeSearch::gen_vector_image( Vec2List image_peaks )
{
  Real fac = 1.0/static_cast<Real>(image_peaks.size());
  
  ost::img::Extent ext = ost::img::Extent(ost::img::Size(_vWidth, _vHeight),ost::img::Point(0,0));
  ost::img::ImageHandle resultImage=CreateImage(ext);
  
  for(Vec2List::const_iterator it = image_peaks.begin(); 
      it != image_peaks.end(); ++it){
    
    for(Vec2List::const_iterator it_inner = image_peaks.begin(); 
	it_inner != image_peaks.end(); ++it_inner){
      if(*it == *it_inner){
      }else{
	Vec2 diff = *it - *it_inner;
        //do linear interpolation
        geom::Vec2 diff_base(floor(diff[0]),floor(diff[1]));
        geom::Vec2 frac=diff-diff_base;
        resultImage.SetReal(ost::img::Point(diff_base)           , (resultImage.GetReal(ost::img::Point(diff_base)) + fac*(1.0-frac[0])*(1.0-frac[1])));
        resultImage.SetReal(ost::img::Point(diff_base)+Point(1,0), (resultImage.GetReal(ost::img::Point(diff_base)+Point(1,0)) + fac*(    frac[0])*(1.0-frac[1])));
        resultImage.SetReal(ost::img::Point(diff_base)+Point(0,1), (resultImage.GetReal(ost::img::Point(diff_base)+Point(0,1)) + fac*(1.0-frac[0])*(    frac[1])));
        resultImage.SetReal(ost::img::Point(diff_base)+Point(1,1), (resultImage.GetReal(ost::img::Point(diff_base)+Point(1,1)) + fac*(    frac[0])*(    frac[1])));
      }
    }
  }
  return resultImage;
}

Vec2List LatticeSearch::gen_vector_peaks(const ost::img::ConstImageHandle& vi)
{
  //TODO: add selection criterium for the peaks
  PeakList vector_peaks;
  bool vps_repeat = true;
  Real vSensFact = 0.5;
    Vec2List result;
  while(vps_repeat){
    PeakSearch ps_v = PeakSearch(_psV_rad,_psV_sens,
				 _psV_irad,_psV_isens);
    vi.Apply(ps_v);
    vector_peaks = ps_v.GetPeakList();
    result.clear();
    for(PeakList::const_iterator it=vector_peaks.begin();it!=vector_peaks.end();++it){
      ::ost::img::alg::Stat stat;
      ost::img::ImageHandle subimage=vi.Extract(ost::img::Extent(ost::img::Size(3,3),*it));
      subimage.ApplyIP(stat);
      result.push_back(geom::Vec2(stat.GetCenterOfMass()));
    }
    int n_peaks = vector_peaks.size();
    if(n_peaks < _v_peaks_min){
      LOG_DEBUG( "LatticeSearch: " << "only " << n_peaks << " diff peaks found, limit is " << _v_peaks_min );
      _psV_sens = _psV_sens*vSensFact;
      LOG_DEBUG( "LatticeSearch: " << " _psV_sens changed to: " << _psV_sens );
      vSensFact = vSensFact + 0.1;
      if(vSensFact > 0.9) {
	vps_repeat = false;
	LOG_DEBUG( "LatticeSearch: " << "n_peaks not in desired range = " << n_peaks );
      }
    } else if(n_peaks > _v_peaks_max) {
      LOG_DEBUG( "LatticeSearch: " << "too many (" << n_peaks << ") diff peaks found, limit is " << _v_peaks_min );
      _psV_sens = _psV_sens/vSensFact;
      LOG_DEBUG( " _psV_sens changed to: " << _psV_sens );
      vSensFact = vSensFact + 0.1;
      if(vSensFact > 0.9){
	vps_repeat = false;
	LOG_DEBUG( "LatticeSearch: " << "n_peaks not in desired range = " << n_peaks );
      }
    } else{
      vps_repeat = false; 
    }
  }
  return result;
}

namespace {
  // helper class to sort peaks according to distance from origin
  class cmp_peaks {
  public:
    bool operator()(const geom::Vec2& p1, const geom::Vec2& p2)
    {
      return Length2(p1) < Length2(p2);
    }
  };

  // helper class to sort by distance from y-axis
  class cmp_peaks_c1 {
  public:
    bool operator()(const geom::Vec2& p1, const geom::Vec2& p2)
    {
      return (p1[0]==p2[0]) ? std::abs(p1[1])<std::abs(p2[1]) : p1[0]<p2[0];
    }
  };

  // helper class to limit peaks based on their distance from origin (length)
  class limit_peaks {
  public:
    limit_peaks(Real lmin, Real lmax):
      lmin_(lmin*lmin),
      lmax_(lmax*lmax)
    {}

    bool operator()(const geom::Vec2& p)
    {
      Real l=static_cast<Real>(Length2(p));
      return ! (l<=lmax_ && l>=lmin_);
    }
  private:
    Real lmin_,lmax_;
  };

}  // anon ns

bool LatticeSearch::gen_close_points(const Vec2List& vpl, geom::Vec2 &cp1, geom::Vec2& cp2)
{
  Vec2List sorted_list(vpl);

  // sort according to distance from origin
  /*
    std::sort would work in principle, but in some cases bizarr memory
    corruption was observed, hence the use of stable_sort
  */
  std::stable_sort(sorted_list.begin(),
		   sorted_list.end(),
		   cmp_peaks() );

  // remove peaks outside of limits
  LOG_DEBUG( "LatticeSearch: " << "removing vector image peaks outside limits " << lat_min_length_ << " < D < " << lat_max_length_ );

  Vec2List filtered_list;
  for(Vec2List::const_iterator it=sorted_list.begin();it!=sorted_list.end();++it) {
    Real l=static_cast<Real>(Length(*it));
    if(l>=lat_min_length_ && l<=lat_max_length_) {
      filtered_list.push_back(*it);
    }
    if(filtered_list.size()>20) {
      break;
    }
  }

  if(filtered_list.size()<2) {
    return false;
  }

  LOG_DEBUG( "LatticeSearch: " << "sorted filtered list (size "<< filtered_list.size() << ")" );
  for(Vec2List::const_iterator it=filtered_list.begin(); it!=filtered_list.end(); ++it) {
    ost::img::Point p(*it);
    LOG_DEBUG( "LatticeSearch: " << " " << p << "  " << std::sqrt(static_cast<Real>(p[0]*p[0]+p[1]*p[1]+p[2]*p[2])) );
  }

  /*
    The first candidate comes from the ensemble of all peaks with
    the shortest distance from the origin, but including a small error 
    margin of one pixel. It should be the one with positive x and smallest
    y components.

    note: the mrc algorithm attempts to take the lowest y component (ie
    without the fabs())
  */
  cp1=Vec2();
  {
    Vec2List::const_iterator it=filtered_list.begin();
    cp1=*it; // if all else fails, use first peak
    Real mindist = Length2(*it)+1;
    Real miny = std::numeric_limits<Real>::max();
    for(;it!=filtered_list.end();++it) {
      geom::Vec2 tmpp=*it;
      if(Length2(tmpp)<=mindist) { // one of the short distances
	if(tmpp[0]>=0) { // x positive
	  if(std::abs(tmpp[1])<miny) { // smallest absolute y
	    miny=abs(tmpp[1]);
	    cp1=tmpp;
	  }
	}
      } else {
	/*
	  this is a valid stop criterion because the list is
	  sorted based on distance from the origin
	*/
	break;
      }
    }
  }

  LOG_DEBUG( "LatticeSearch: " << "first point: " << cp1 );

  /*
    The second candidate should fulfill the following criteria:
    1.  distance from origin as small as possible
    2.  not colinear with cand1
    3.  angle smaller than 180deg in CCW direction
  */
  cp2=Vec2();
  {
    for(Vec2List::const_iterator it=filtered_list.begin();it!=filtered_list.end();++it) {
      geom::Vec2 tmpp=*it;
      if(!are_colinear(tmpp,cp1)) {
	if( Cross(Vec3(cp1),Vec3(tmpp))[2]>0.0 ) {
	  cp2=tmpp;
	  break;
	}
      }
    }
  }

  LOG_DEBUG( "LatticeSearch: " << "second point: " << cp2 );

  return true;
}

bool LatticeSearch::are_colinear(const geom::Vec2& p1, const geom::Vec2& p2)
{
  return std::fabs(std::sin(Angle(p1,p2)))<col_tolerance_;
}

Real LatticeSearch::calc_quality(const geom::Vec2& cp1, const geom::Vec2& cp2, ost::img::ImageHandle vImg, int aPx)
{
  geom::Vec2 first = cp1;
  geom::Vec2 second = cp2;
  geom::Vec2 pt1 = geom::Vec2(0,0);
  geom::Vec2 pt2 = geom::Vec2(0,0);
  geom::Vec2 cPt = geom::Vec2(0,0);
  //should be calculated or given as a parameter:
  int dir1 = 5;
  int dir2 = 5;

  Real quality = 0.0;
  for(int i=0; i <= dir1; i++) {
    for(int j=0; j <= dir2; j++) {
      for(int i_inner = -aPx; i_inner <= aPx; i_inner++) {
	for (int j_inner = -aPx; j_inner <= aPx; j_inner++) {
	  quality = quality + 
	    vImg.GetReal(ost::img::Point(cPt + geom::Vec2(i_inner,j_inner)) );
	}
      }
      pt1= pt1 + first;
      cPt = pt1 + pt2;
    }
    pt1 = geom::Vec2(0,0);
    pt2 = pt2 + second;
  }
  return quality;
}


/*
  Calculate the normalized distance of each peak to a given lattice.

  Assuming that each given peak is a true one, it should fullfill the 
  following equation, where p is the peak, a the first vector, b the 
  second, and o the offset, and n,m are integers

  p_x = n * a_x + m * b_x + o_x
  p_y = n * a_y + m * b_y + o_y

  A peak not on the grid will result in fractional n,m components, 
  which are accumulated to give the total difference.

  Uses _image_peaks as peak list
*/

Real LatticeSearch::calc_lattice_dist(const Lattice& lat)
{
  geom::Vec2 o(lat.GetOffset());
  geom::Vec2 a(lat.GetFirst());
  geom::Vec2 b(lat.GetSecond());

  //Vec2 res(0.0,0.0);
  Real res=0.0;
  for(PeakList::const_iterator it=_image_peaks.begin(); it!=_image_peaks.end(); ++it) {

    geom::Vec2 nm = lat.CalcComponents((*it));

    // fractional component to nearest int
    Real fn=nm[0]-round(nm[0]);
    Real fm=nm[1]-round(nm[1]);

    // distance to grid point
#if 0
    Real dist = Length(geom::Vec2(fn*a[0]+fm*b[0],fn*a[1]+fm*b[1]));
    // use squared distance to weigh distant points more
    res += dist*dist;
#else
    res+=(fn*fn+fm*fm)*it->GetValue();
#endif
  }

  // normalize to number of peaks
  return res/(Real)(_image_peaks.size());
}

#define LS_GET_FLOAT_FROM_INFO(GROUP,ITEM,VAR,NAME)	   \
  if( GROUP .HasItem( ITEM )) {                    \
    VAR = GROUP .GetItem( ITEM ).AsFloat();     \
    LOG_VERBOSE( "LatticeSearch: " << "setting " << NAME << " to " << VAR ); \
  }

#define LS_GET_INT_FROM_INFO(GROUP,ITEM,VAR,NAME)	   \
  if( GROUP .HasItem( ITEM )) {                    \
    VAR = GROUP .GetItem( ITEM ).AsInt();     \
    LOG_VERBOSE( "LatticeSearch: " << "setting " << NAME << " to " << VAR ); \
  }


void LatticeSearch::UpdateFromInfo(const ost::info::InfoGroup& g)
{
  LS_GET_FLOAT_FROM_INFO(g,"GaussFilterStrength",_gfI_sigma,"GaussFilterStrength");
  LS_GET_FLOAT_FROM_INFO(g,"PeakSearchSensitivity",_psI_sens,"PeakSearchSensitivity");
  LS_GET_FLOAT_FROM_INFO(g,"MinimalLatticeLength",lat_min_length_,"MinimalLatticeLength");
  LS_GET_FLOAT_FROM_INFO(g,"MaximalLatticeLength",lat_max_length_,"MaximalLatticeLength");
  if(g.HasGroup("PeakSearchParams")) {
    ost::info::InfoGroup psg = g.GetGroup("PeakSearchParams");
    LS_GET_INT_FROM_INFO(psg,"OuterWindowSize",_psI_rad,"OuterWindowSize");
    LS_GET_FLOAT_FROM_INFO(psg,"OuterWindowSensitivity",_psI_sens,"OuterWindowSensitivity");
    LS_GET_INT_FROM_INFO(psg,"InnerWindowSize",_psI_irad,"InnerWindowSize");
    LS_GET_FLOAT_FROM_INFO(psg,"InnerWindowSensitivity",_psI_isens,"InnerWindowSensitivity");
  }
  if(g.HasGroup("DifferenceVectorImage")) {
    ost::info::InfoGroup g2=g.GetGroup("DifferenceVectorImage");
    LS_GET_FLOAT_FROM_INFO(g2,"GaussFilterStrength",_gfV_sigma,"DVIGaussFilterStrength");
    if(g2.HasGroup("PeakSearchParams")) {
      ost::info::InfoGroup psg = g2.GetGroup("PeakSearchParams");
      LS_GET_INT_FROM_INFO(psg,"OuterWindowSize",_psV_rad,"DVIOuterWindowSize");
      LS_GET_FLOAT_FROM_INFO(psg,"OuterWindowSensitivity",_psV_sens,"DVIOuterWindowSensitivity");
      LS_GET_INT_FROM_INFO(psg,"InnerWindowSize",_psV_irad,"DVIInnerWindowSize");
      LS_GET_FLOAT_FROM_INFO(psg,"InnerWindowSensitivity",_psV_isens,"DVIInnerWindowSensitivity");
    }
  }
}

#undef LS_GET_FLOAT_FROM_INFO
#undef LS_GET_INT_FROM_INFO

void UpdateFromInfo(LatticeSearch& ls, const ost::info::InfoGroup& g)
{
  ls.UpdateFromInfo(g);
}


}} // namespaces
