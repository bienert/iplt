//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  author: Andreas Schenk
*/

#include <ost/img/image.hh>
#include <ost/img/alg/convolute.hh>
#include <ost/img/progress.hh>

#include "sobel.hh"

namespace iplt { namespace alg { 

Sobel::Sobel(void):
  ost::img::ConstModOPAlgorithm("Sobel")
{}

ost::img::ImageHandle Sobel::Visit(const ost::img::ConstImageHandle& i) const
{
  ost::img::ImageHandle gx = CreateImage(ost::img::Extent(ost::img::Size(3,3),ost::img::Point(0,0)),ost::img::REAL,ost::img::SPATIAL);
  gx.SetReal(ost::img::Point(-1,-1),-1.0);
  gx.SetReal(ost::img::Point(-1, 0), 0.0);
  gx.SetReal(ost::img::Point(-1, 1), 1.0);
  gx.SetReal(ost::img::Point( 0,-1),-2.0);
  gx.SetReal(ost::img::Point( 0, 0), 0.0);
  gx.SetReal(ost::img::Point( 0, 1), 2.0);
  gx.SetReal(ost::img::Point( 1,-1),-1.0);
  gx.SetReal(ost::img::Point( 1, 0), 0.0);
  gx.SetReal(ost::img::Point( 1, 1), 1.0);
  
  ost::img::ImageHandle gy = CreateImage(ost::img::Extent(ost::img::Size(3,3),ost::img::Point(0,0)),ost::img::REAL,ost::img::SPATIAL);
  gy.SetReal(ost::img::Point(-1,-1), 1.0);
  gy.SetReal(ost::img::Point(-1, 0), 2.0);
  gy.SetReal(ost::img::Point(-1, 1), 1.0);
  gy.SetReal(ost::img::Point( 0,-1), 0.0);
  gy.SetReal(ost::img::Point( 0, 0), 0.0);
  gy.SetReal(ost::img::Point( 0, 1), 0.0);
  gy.SetReal(ost::img::Point( 1,-1),-1.0);
  gy.SetReal(ost::img::Point( 1, 0),-2.0);
  gy.SetReal(ost::img::Point( 1, 1),-1.0);
  
  ost::img::ImageHandle result = CreateImage(i.GetExtent(),ost::img::COMPLEX,ost::img::SPATIAL);
  ost::img::Progress::Instance().Register(this,4);
  ost::img::ImageHandle dd= i.Apply(ost::img::alg::ExplicitConvolute(gx));
  ost::img::Progress::Instance().AdvanceProgress(this);
  
  for(ost::img::ExtentIterator it(result.GetExtent());!it.AtEnd();++it) {
    result.SetComplex(it,dd.GetReal(it));
  }
  ost::img::Progress::Instance().AdvanceProgress(this);
  
  dd= i.Apply(ost::img::alg::ExplicitConvolute(gy));
  ost::img::Progress::Instance().AdvanceProgress(this);
  for(ost::img::ExtentIterator it(result.GetExtent());!it.AtEnd();++it) {
    Complex tmp=result.GetComplex(it);
    tmp+=Complex(0,dd.GetReal(it));
    result.SetComplex(it,tmp);
  }
  ost::img::Progress::Instance().AdvanceProgress(this);
  ost::img::Progress::Instance().DeRegister(this);
  
  return result;
}

}} // ns
