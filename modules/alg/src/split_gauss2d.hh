//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  split gaussian 2D

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_SPLIT_GAUSS2D_H
#define IPLT_EX_SPLIT_GAUSS2D_H

#include <iplt/alg/gauss2d.hh>

namespace iplt {  namespace alg {

/*! Split Gauss2D Parameters

  Stores the Parameters for a Pseudo Split Gauss2D function,
  given by:

  x0' = x-ux
  
  x1' = x0'-delta

  x2' = x0'+delta

  y' = y-uy

  x1" = x1' cos[w] - y' sin[w]

  x2" = x2' cos[w] - y' sin[w]

  y" = x0' sin[w] + y' cos[w]

  f(x,y) = A (exp[(x1"/Bx)^2]+exp[(x2"/Bx)^2]) exp[(y"/By)^2] + Px * x0' + Py * y' + C

  
  Note: This is not a true is-an relationship, but 
  refactoring ParamsGauss2D to add another layer in
  order to follow strict OOD codex seems overkill.
*/
class DLLEXPORT_IPLT_ALG ParamsSplitGauss2D: public alg::ParamsGauss2D
{
public:
  ParamsSplitGauss2D(Real A, 
		      Real Bx, Real By, Real C, 
		      Real ux, Real uy, Real w, 
		      Real Px, Real Py, Real delta):
    ParamsGauss2D(A,Bx,By,C,ux,uy,w,Px,Py),delta_(delta)
  {}
    

  ParamsSplitGauss2D():
    ParamsGauss2D(),delta_(0.0)
  {}

  Real GetDelta() const {return delta_;}
  void SetDelta(Real d) {delta_=d;}

protected:
  Real delta_;
};

DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream&, const ParamsSplitGauss2D&);

/*!
  Function implementation of split gauss
 */
class DLLEXPORT_IPLT_ALG FuncSplitGauss2D: public alg::FuncGauss2D
{
public:
  FuncSplitGauss2D():
    FuncGauss2D(),delta_(0.0)
  {}
  FuncSplitGauss2D(Real A, Real Bx=1.0, Real By=1.0, Real C=0.0, Real ux=0.0, Real uy=0.0, Real w=0.0, Real Px=0.0, Real Py=0.0,Real delta=0.0):
    FuncGauss2D(A,Bx,By,C,ux,uy,w,Px,Py),delta_(delta)
  {}

  // ost::img::RealFunction interface implementation
  virtual Real Func(const ost::img::Point &p) const;

  Real GetDelta() const {return delta_;}
  void SetDelta(Real d) {delta_=d;}

protected:
  Real delta_;
};

DLLEXPORT_IPLT_ALG std::ostream& operator<<(std::ostream&, const FuncSplitGauss2D&);

}} // ns

#endif
