//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  reflection list binning

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_RBIN_H
#define IPLT_EX_RBIN_H

#include <vector>

#include <ost/base.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

/*
  Resolution shell binning

  In essence builds a histogramm of amplitudes in resolution
  shells, using either the reciprocal of the resolution or
  the reciprocal of the squared resolution.  

*/
class DLLEXPORT_IPLT_ALG ResolutionBin {
public:
  //! default ctor with count=1, lower limit=1 and higher limit=2
  ResolutionBin();
  //! Initialize with number of bins and resolution bounds
  /*!
    bincount: number of bins to use
    reslow: 'lower' resolution limit in A
    reshi: ' higher' resolution limit in A
    
    optional boolean: indicate wether resolutions are squared before binning
    their reciprocal values, which is for instance used by the wilson fit 
    facility
  */
  ResolutionBin(int bincount, Real lim_low,Real lim_hi, bool use_rsq=false);

  //! Add value at resolution (in A)
  void Add(Real resol, Real val, Real w=1.0);

  //! Retrieve average value from specified bin
  /*!
    ave=sum[value_i*weight_i]/sum[weight_i]
  */
  Real GetBinAverage(int n) const;

  //! Retrieve std dev from specified bin
  /*!
    sdev^2 = sum[value_i*weight_i-<ave>]/sum[weight_i]
  */
  Real GetBinStdDev(int n) const;

  //! Return number of bins
  int GetBinCount() const;

  //! Retrieve lower resolution bound of this bin
  Real GetBinResolution(int n);

private:
  int bincount_;
  Real lim_low_,lim_hi_;
  bool use_rsq_;

  Real binsize_;

  struct BinEntry {
    Real value;
    Real weight;
  };
  
  typedef std::vector<BinEntry> BinEntryList;

  std::vector<BinEntryList> bin_;
};

}} // ns

#endif
