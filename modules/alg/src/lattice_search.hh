//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  2D lattice search on image

  Authors: Giani Signorell, Ansgar Philippsen
*/

#ifndef IPLT_ALG_LATTICE_SEARCH_H
#define IPLT_ALG_LATTICE_SEARCH_H

#include <vector>

#include <ost/geom/geom.hh>
#include <ost/img/algorithm.hh>
#include <ost/img/point.hh>
#include <ost/log.hh>

#include <ost/info/info_fw.hh>

#include <iplt/alg/peak_search.hh>

#include <iplt/lattice.hh>

namespace iplt {  namespace alg {

        typedef std::vector<geom::Vec2> Vec2List;

//! Lattice Search
/*!
  Use difference vector algorithm to determine
  a lattice of peaks within an image
*/
class DLLEXPORT_IPLT_ALG LatticeSearch: public NonModAlgorithm {
public:
  //! @name Initialization
  //@{
  LatticeSearch();
  //@}

  //! @name Parameters
  //@{

  //! Strength of initial gauss smoothing
  /*!
    Prior to any peak-search and lattice-fitting, the input
    image will be smoothed with a gauss filter. This parameter
    determines the strength of the smoothing, or 0.0 to
    deactivate it. The default is 1.0
  */
  void SetGaussFilterStrength(Real s) {_gfI_sigma = s;}

  //! Limiting parameters for peak search on raw image
  /*!
    The peak-search on the initial image will attempt to find at least
    minpeaks (always at least 5!), and at most maxpeaks. The maximal
    number of adjustments is given by maxiter. Defaults are minpeaks=10,
    maxpeaks=500, maxiter=5
  */
  void SetPeakSearchLimits(int minpeaks, int maxpeaks, int maxiter=5) {
    _i_peaks_min = std::max(minpeaks,_i_peaks_absmin);
    _i_peaks_max = maxpeaks;
    _i_peaks_maxiter = maxiter;
  }

  //! Actual peak search parameters
  /*!
    These are the values passed to the PeakSearch ctor. They 
    default to rad=10,sens=0.01, irad=5, isens=0.0
  */
  void SetPeakSearchParams(int owin, Real osens, 
			   int iwin, Real isens,
			   int iclim=0,Real icval=0.0) {
    _psI_rad=owin;
    _psI_sens=osens;
    _psI_irad=std::min(_psI_rad-1, iwin);
    _psI_isens=isens;
    _psI_iclim=iclim;
    _psI_icval=icval;
  }

  void SetPeakSearchSensitivity(Real s) {
    _psI_sens=s;
  }    

  //! add exclusion region for peak searches
  void AddPeakSearchExclusion(const ost::img::Extent&e) {
    ext_list_.push_back(e);
  }

  //! maximal lattice fit iterations
  void SetLatticeFitMaxIterations(int n) {
    _latfit_maxiter = n;
  }

  //! Defaults to 0.1
  void SetMinimalQuality(Real q) {
    _q_desired=q;
  }

  //! Colinear tolerance in degrees, default 5
  void SetColinearTolerance(Real angle) {
    col_tolerance_ = 1.0-cos(angle*M_PI/180.0);
  }

  void SetMinimalLatticeLength(Real l) {
    lat_min_length_=l;
  }

  void SetMaximalLatticeLength(Real l) {
    lat_max_length_=l;
  }

  void SetDVIGaussianFilterStrengh(Real d) {
    _gfV_sigma=d;
  }

  /*
    defaults are 3, 10, 1, 0
   */
  void SetDVIPeakSearchParams(int owin, Real osens, 
			      int iwin, Real isens)
  {
    _psV_rad=owin;
    _psV_sens=osens;
    _psV_irad=std::min(_psV_rad-1, iwin);
    _psV_isens=isens;
  }
  

  //@}

  /*! @name Results
   */
  //@{
  //! Retrieve found lattice
  const Lattice& GetLattice() const {return lattice_;}
  //@}

  //! @name NonModAlgorithm interface
  //@{
  virtual void Visit(const ost::img::ConstImageHandle& i);
  virtual void Visit(const ost::img::Function& f) {
    throw(Error("LatticeSearch not supported for functions"));
  }
  //@}

  void UpdateFromInfo(const ost::info::InfoGroup& g);

  /*! @name Debug Interface
    Do not use this, only for internal debugging, might change at any time
  */  
  //@{
  const Lattice& GetTempLattice() const {return tmp_lattice_;}
  ost::img::PointList GetClosePoints() const {return _close_points;}
  Vec2List GetBestPoints() const {return _best_points;}
  ost::img::ImageHandle GetVectorImage() const {return _best_vector_image;}
  PeakList GetImagePeaks() const {return _image_peaks;}
  PeakList GetImagePeaks2() const {return _image_peaks2;}
  //@}

private:
  Lattice get_best_points(const ost::img::ConstImageHandle& img);

  geom::Vec2 determine_offset(const Lattice& lat,const ost::img::ConstImageHandle& img );
  
  Real calc_quality(const geom::Vec2& cp1, const geom::Vec2& cp2, ost::img::ImageHandle vImg, int aPx);

  Vec2List gen_image_peaks(const ost::img::ConstImageHandle& img);

  ost::img::ImageHandle gen_vector_image( Vec2List image_peaks );

  Vec2List gen_vector_peaks(const ost::img::ConstImageHandle& vi);

  bool gen_close_points(const Vec2List& vpl, geom::Vec2& p1, geom::Vec2& p2);

  Real calc_lattice_dist(const Lattice& lat);

  Lattice fit_best_points(const geom::Vec2& p1, const geom::Vec2& p2);

  bool are_colinear(const geom::Vec2& p1, const geom::Vec2& p2);

  ///////////////////////////////////////

  Lattice lattice_; // result

  Lattice tmp_lattice_; //pre fit lattice

  PeakList _image_peaks; // peaks found in the original image
  PeakList _image_peaks2; // peaks used in the last refinement step
 
  ost::img::ImageHandle _best_vector_image;
  
  Real _gfI_sigma;
  
  int _i_peaks_absmin;
  int _i_peaks_min;
  int _i_peaks_max;
  int _i_peaks_maxiter;
  
  int _psI_rad;
  Real _psI_sens;
  int _psI_irad;
  Real _psI_isens;
  int _psI_iclim;
  Real _psI_icval;
  
  Real _gfV_sigma;
  
  int _v_peaks_min;
  int _v_peaks_max;
  
  int _psV_rad;
  Real _psV_sens;
  int _psV_irad;
  Real _psV_isens;
  
  int _vWidth;
  int _vHeight;
  
  Real _q_desired;
  Real _q_limit;
  Real _iter_limit;

  Real lat_min_length_;
  Real lat_max_length_;
  
  ost::img::PointList _close_points;
  Vec2List _best_points;
  
  int _aPx;
  
  Real _quality;
  Real _im_mean;

  // maximal iterations for lattice fitting
  int _latfit_maxiter;

  /*
    colinear tolerance
    given in degree, but stored as 1.0-(cos(TOL))
  */
  Real col_tolerance_;

  std::vector<Extent> ext_list_;
};


//! Set lattice search parameters based on info group
/*!
  The parameters from the given LatticeSearch object are modified
  according to the given InfoGroup instance. The resulting 
  LatticeSearch object is returned.

  Note that only the parameters that are actually present in
  the info group are used, allowing cumulative use of this
  function.
*/
DLLEXPORT_IPLT_ALG void UpdateFromInfo(LatticeSearch& ls, const ost::info::InfoGroup& g);


}} //namespaces

#endif
