//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <cmath>

#include <ost/img/alg/fft.hh>
#include <ost/img/data.hh>
#include "lattice_filters.hh"

namespace iplt {  namespace alg { namespace filter_detail {


// Lattice Filter

class LatticeFilterBase 
{

public:

  LatticeFilterBase (Lattice lattice=Lattice(),unsigned int size=1):
    lattice_(lattice),
    size_(size)
  {}

  template <typename T, class D>
  void VisitState(ost::img::ImageStateImpl<T,D>& is) const 
  {
    for(ost::img::ExtentIterator it(is.GetExtent());!it.AtEnd();++it) {
      ost::img::Point p=ost::img::Point(it);
      geom::Vec2 comp = lattice_.CalcComponents(p);
      ost::img::Point p2=ost::img::Point(static_cast<int>(round(comp[0])),static_cast<int>(round(comp[1])));
      Real value=Length(p.ToVec2()-lattice_.CalcPosition(p2))<=size_ && p2!=ost::img::Point(0,0)? 1 : 0;
      is.Value(it)*=value;
    }
  }

  static String GetAlgorithmName() {return "LatticeFilter"; }

private:
  Lattice lattice_;
  unsigned int size_;
};
typedef ost::img::ImageStateConstModIPAlgorithm<LatticeFilterBase> LatticeFilter;

// Gaussian Lattice Filter

class GaussianLatticeFilterBase 
{

public:

  GaussianLatticeFilterBase (Lattice lattice=Lattice(),unsigned int size=1):
    lattice_(lattice),
    size_(size)
  {}

  template <typename T, class D>
  void VisitState(ost::img::ImageStateImpl<T,D>& is) const 
  {
    for(ost::img::ExtentIterator it(is.GetExtent());!it.AtEnd();++it) {
      ost::img::Point p=ost::img::Point(it);
      geom::Vec2 comp = lattice_.CalcComponents(p);
      ost::img::Point p2=ost::img::Point(static_cast<int>(round(comp[0])),static_cast<int>(round(comp[1])));
      Real value=Length(p.ToVec2()-lattice_.CalcPosition(p2));
      is.Value(it)*= p2==ost::img::Point(0,0)? 0.0 : exp(-value*value/(size_*size_));
    }
  }

  static String GetAlgorithmName() {return "GaussianLatticeFilter"; }

private:
  Lattice lattice_;
  unsigned int size_;
};
typedef ost::img::ImageStateConstModIPAlgorithm<GaussianLatticeFilterBase> GaussianLatticeFilter;


} // ns


void LatticeFilter::Visit(ost::img::ImageHandle& image) const 
{
   if(image.IsSpatial()) {
     filter_detail::LatticeFilter lattice_filter(lattice_,size_); 
     image.ApplyIP(ost::img::alg::FFT());
     image.ApplyIP(lattice_filter); 
     image.ApplyIP(ost::img::alg::FFT());
   } else {
     filter_detail::LatticeFilter lattice_filter(lattice_,size_); 
     image.ApplyIP(lattice_filter); 
   }
}                
void GaussianLatticeFilter::Visit(ost::img::ImageHandle& image) const 
{
   if(image.IsSpatial()) {
     filter_detail::GaussianLatticeFilter lattice_filter(lattice_,size_); 
     image.ApplyIP(ost::img::alg::FFT());
     image.ApplyIP(lattice_filter); 
     image.ApplyIP(ost::img::alg::FFT());
   } else {
     filter_detail::GaussianLatticeFilter lattice_filter(lattice_,size_); 
     image.ApplyIP(lattice_filter); 
   }
}        



}} // ns


#ifndef NO_EXPL_INST
template class ost::img::ImageStateConstModIPAlgorithm<iplt::alg::filter_detail::LatticeFilterBase>;
template class ost::img::ImageStateConstModIPAlgorithm<iplt::alg::filter_detail::GaussianLatticeFilterBase>;
#endif
