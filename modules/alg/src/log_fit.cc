//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/log.hh>

#include "log_fit.hh"

namespace iplt {  namespace alg {

namespace {

struct LogFitParams 
{
  LogFitList* value_list;
  bool zero_offset;
};

template<bool b1,bool b2>
int log_fit_func_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  LogFitParams* prm = reinterpret_cast<LogFitParams*>(params);

  Real S = gsl_vector_get(x,0);
  Real B = gsl_vector_get(x,1);
  Real C=0.0;
  if(!prm->zero_offset) {
    C = gsl_vector_get(x,2);
  }

  int indx=0;
  for(LogFitList::const_iterator it=prm->value_list->begin();it!=prm->value_list->end();++it) {
    Real f_xy = S*log(B+it->x)+C;
    if(b1) {
      gsl_vector_set(f,indx,(f_xy-it->y)*it->w);
    }

    if(b2) {
      Real der_S = it->w * log(B+it->x);
      Real der_B = it->w * S/(B+it->x);

      gsl_matrix_set(J,indx,0,der_S);
      gsl_matrix_set(J,indx,1,der_B);
      if(!prm->zero_offset) {
	gsl_matrix_set(J,indx,2,it->w*1.0); // der_C
      }
    }

    ++indx;
  }
  return GSL_SUCCESS;
}

static int log_fit_func_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return log_fit_func_tmpl<true,false>(x,params,f,0);
}

static int log_fit_func_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return log_fit_func_tmpl<false,true>(x,params,0,J);
}

static int log_fit_func_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return log_fit_func_tmpl<true,true>(x,params,f,J);
}

} // anon ns

LogFit::LogFit(bool zo):
  list_(),zero_offset_(zo),
  S_(1.0),B_(1.0),C_(0.0),chi_(0.0),
  maxiter_(1000),lim1_(1.0e-20),lim2_(1.0e-20)
{}

void LogFit::Add(Real x, Real y, Real w)
{
  list_.push_back(LogFitEntry(x,y,w));
}

void LogFit::Add(Real x, Real y)
{
  this->Add(x,y,1.0);
}

void LogFit::Apply()
{
  if(zero_offset_) {
    LOG_VERBOSE( "log fit with zero offset" );
  } else {
    LOG_VERBOSE( "log fit" );
  }

  if(list_.size()<4) {
    LOG_INFO( "not enough points for log fit, returning unity" );
    S_ = 1.0;
    B_ = 0.0;
    C_ = 0.0;
    chi_ = 0.0;
    return;
  }

  int num_p = zero_offset_ ? 2 : 3;

  // fill list_ from binning
  LogFitParams params = {&list_,zero_offset_};
  
  gsl_vector* X = gsl_vector_alloc(num_p);
  gsl_vector_set(X,0,S_);
  gsl_vector_set(X,1,B_);
  if(!zero_offset_) {
    gsl_vector_set(X,2,C_);
  }
  
  gsl_multifit_function_fdf func;
  func.f = &log_fit_func_f;
  func.df = &log_fit_func_df;
  func.fdf = &log_fit_func_fdf;
  func.n = list_.size();
  func.p = num_p;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, list_.size(),num_p);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  unsigned int iter=0;
  int status=GSL_CONTINUE;
  LOG_DEBUG( "running gsl_multifit solver" );

  while (status == GSL_CONTINUE && iter < maxiter_) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1_, lim2_);
  } 

  S_ = gsl_vector_get(solver->x,0);
  B_ = gsl_vector_get(solver->x,1);
  if(!zero_offset_) {
    C_ = gsl_vector_get(solver->x,2);
  }
  chi_ = gsl_blas_dnrm2(solver->f);

  LOG_DEBUG( "fit result after " << iter << " iterations: S=" << S_ << " B=" << B_ << " C=" << C_ << " chi=" << chi_ );

  gsl_multifit_fdfsolver_free (solver);

  gsl_vector_free(X);

}

Real LogFit::GetS() const
{
  return S_;
}

Real LogFit::GetB() const
{
  return B_;
}

Real LogFit::GetC() const
{
  return C_;
}

Real LogFit::GetChi() const
{
  return chi_;
}

void LogFit::SetMaxIter(unsigned int m)
{
  maxiter_=m;
}

void LogFit::SetLimits(Real l1, Real l2)
{
  lim1_=l1;
  lim2_=l2;
}

}}