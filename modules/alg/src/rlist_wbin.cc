//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen
*/

#include "rlist_wbin.hh"

namespace iplt {  namespace alg {

WeightedBin RListWBin(const ReflectionList& rlist, int bcount, Real rlow, Real rhigh, const String& m_i, const String& m_sigi)
{
  int i_i = rlist.GetPropertyIndex(m_i);
  int i_sigi = rlist.GetPropertyIndex(m_sigi);

  WeightedBin wbin(bcount,1.0/(rlow*rlow),1.0/(rhigh*rhigh));

  for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp) {
    Real resol=rp.GetResolution();
    Real rr=1.0/(resol*resol);
    Real iobs = rp.Get(i_i);
    Real sigiobs = rp.Get(i_sigi);
    if(sigiobs>0.0) {
      wbin.Add(rr,iobs,1.0/(sigiobs*sigiobs));
    }
  }

  return wbin;
}

}} // ns
