//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/
#include "ctf.hh"

using namespace std;
using namespace ost::img;

namespace iplt { namespace alg {

CTF::CTF(const TCIFData& data):
    ModOPAlgorithm("CTF"),
    ctf_(data)
{
}

// algorithm interface
ImageHandle CTF::Visit(const ost::img::ConstImageHandle& i)
{
  ost::img::ImageHandle result=i.Copy(false);
  result.SetSpatialSampling(i.GetSpatialSampling());
  result.SetSpatialOrigin(i.GetSpatialOrigin());
  geom::Vec3 sampling(i.GetPixelSampling());
  for(ost::img::ExtentIterator it(i.GetExtent());!it.AtEnd();++it) {
    geom::Vec2 xy=Vec2(CompMultiply(Point(it).ToVec3(),sampling));
    result.SetComplex(it,i.GetComplex(it)*ctf_.DampedCTF(xy));
  }  
  return result;
}

}}//ns
