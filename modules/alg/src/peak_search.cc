//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Giani Signorell, Ansgar Philippsen, Valerio Mariani, Andreas Schenk
*/

#include <iostream>

#include <ost/message.hh>
#include <ost/log.hh>
#include <ost/img/alg/stat.hh>

#include "peak_search.hh"

namespace iplt {  namespace alg {
PeakSearchBase::PeakSearchBase(unsigned int outer_win, Real outer_sens,
			       unsigned int inner_win, Real inner_sens,
			       int inner_count_lim,Real inner_count_val,
             unsigned int mode):
  outer_win_(outer_win),
  inner_win_(inner_win),
  outer_sens_(outer_sens),
  inner_sens_(inner_sens),
  inner_count_lim_(inner_count_lim),
  inner_count_sens_(inner_count_val),
  thd_value_(0.0),
  thd_set_(false),
  mode_(0),
  peaks_(),
  ext_list_()
{
  SetMode(mode);
  check_values();
}

template <typename T, class D>
void PeakSearchBase::VisitState(const ost::img::ImageStateImpl<T,D>& isi)
{
  switch(mode_){
  case ABSOLUTE:
  case RELATIVE_TO_STDEV:
    visit_const_sens<T,D>(isi);
   break;
  case RELATIVE_TO_PEAK:
    visit_variable_sens<T,D>(isi);
    break;
  default:
    visit_const_sens<T,D>(isi);
    break;
  }
}

template <typename T, class D>
void PeakSearchBase::visit_const_sens(const ost::img::ImageStateImpl<T,D>& isi)
{
  ost::img::alg::Stat stat;
  isi.Apply(stat);

  //set peak search parameters
  Real outer_limit = outer_sens_;
  Real inner_limit = inner_sens_;
  Real inner_count_stdval=inner_count_sens_;
  //set the minimal threshhold
  if(!thd_set_ || thd_value_< stat.GetMinimum()){
    thd_value_ = stat.GetMinimum();
  }
  if(mode_==RELATIVE_TO_STDEV){
    Real stdDev = stat.GetStandardDeviation();
    outer_limit *= stdDev;
    inner_limit *= stdDev;
    inner_count_stdval *= stdDev;
  }

  // log output
  LOG_INFO("Searching peaks using following parameters:" << std::endl);
  LOG_INFO(" outer window: " << outer_win_ << std::endl);
  LOG_INFO(" inner window: " << inner_win_ << std::endl);
  LOG_INFO(" outer limit: " << outer_limit << std::endl);
  LOG_INFO(" inner limit: " << inner_limit << std::endl);
  LOG_INFO(" inner count limit: " << inner_count_lim_ << std::endl);
  LOG_INFO(" inner count value: " << inner_count_stdval << std::endl);
  LOG_INFO(" threshold : " << thd_value_ << std::endl);
  if(ext_list_.size()==0) {
    LOG_INFO(" excluded regions: none" << std::endl);
  } else {
    LOG_INFO(" excluded regions:" << std::endl);
    for(ExtList::const_iterator it=ext_list_.begin();it!=ext_list_.end();++it) {
      LOG_INFO("  " << (*it) << std::endl);
    }
  }

  ost::img::Extent img_ext = isi.GetExtent();
  int outer_win_edge=outer_win_*2+1;
  ost::img::Extent img_size = isi.GetSize();
  ost::img::Size win_size(std::min<int>(outer_win_edge,img_size.GetWidth()), std::min<int>(outer_win_edge,img_size.GetHeight()),std::min<int>(outer_win_edge,img_size.GetDepth()));

  for(ost::img::ExtentIterator img_it(img_ext);!img_it.AtEnd();++img_it) {
      Real valCand = ost::img::Val2Val<T,Real>(isi.Value(img_it));
    //value has to be at least thd big ans should not be excluded
    if( valCand <= thd_value_ || is_excluded(img_it)){
      continue;
    }
    unsigned int inner_count=0;
    int inner_win_edge = inner_win_*2 + 1;
    ost::img::Extent win_ext(win_size,ost::img::Point(img_it));
    //exclude peaks if part of outer win is outside image
    if( !img_ext.Contains(win_ext)){
      continue;
    }
    ost::img::Extent inner_win_ext(ost::img::Size(inner_win_edge,inner_win_edge,inner_win_edge),img_it);

    ost::img::ExtentIterator search_it(Overlap(img_ext,win_ext));
    for(;!search_it.AtEnd();++search_it) {
      Real val = ost::img::Val2Val<T,Real>(isi.Value(search_it));
      if( ost::img::Point(search_it) != ost::img::Point(img_it) ) { // ignore center point
        Real valdiff=valCand-val;
        if(inner_win_ext.Contains(search_it)) {
          /*Candidate is only accepted if it's value is at least
          inner_limit_' higher than all other values in the
          given inner_radius */
          if( valdiff < inner_limit ){
            break;
          }
          /*
          accumulate inner count if value of this inner point
          not smaller than certain threshold
          */
          if(val >= inner_count_stdval) {
            inner_count++;
          }
        } else {
          /* Candidate is only accepted if it's value is at least
          'limit' higher than all other values in the given
          radius */
          if( valdiff < outer_limit  ){
            break;
          }
        }
      }
    }
    if(search_it.AtEnd() && inner_count >= inner_count_lim_ ){
      ost::img::Peak peak(img_it,valCand);
      LOG_INFO("peak found at " << ost::img::Point(peak) << std::endl);
      peaks_.push_back(peak);
    }
  }
}

template <typename T, class D>
void PeakSearchBase::visit_variable_sens(const ost::img::ImageStateImpl<T,D>& isi)
{
  ost::img::alg::Stat stat;
  isi.Apply(stat);

  //set the minimal threshhold
  if(!thd_set_ || thd_value_< stat.GetMinimum()){
    thd_value_ = stat.GetMinimum();
  }

  // log output
  LOG_INFO("Searching peaks using following parameters:" << std::endl);
  LOG_INFO(" outer window: " << outer_win_ << std::endl);
  LOG_INFO(" inner window: " << inner_win_ << std::endl);
  LOG_INFO(" inner count limit: " << inner_count_lim_ << std::endl);
  LOG_INFO(" threshold : " << thd_value_ << std::endl);
  if(ext_list_.size()==0) {
    LOG_INFO(" excluded regions: none" << std::endl);
  } else {
    LOG_INFO(" excluded regions:" << std::endl);
    for(ExtList::const_iterator it=ext_list_.begin();it!=ext_list_.end();++it) {
      LOG_INFO("  " << (*it) << std::endl);
    }
  }

  ost::img::Extent img_ext = isi.GetExtent();
  int outer_win_edge=outer_win_*2+1;
  ost::img::Extent img_size = isi.GetSize();
  ost::img::Size win_size(std::min<int>(outer_win_edge,img_size.GetWidth()), std::min<int>(outer_win_edge,img_size.GetHeight()),std::min<int>(outer_win_edge,img_size.GetDepth()));

  for(ost::img::ExtentIterator img_it(img_ext);!img_it.AtEnd();++img_it) {
    Real valCand = ost::img::Val2Val<T,Real>(isi.Value(img_it));
    //value has to be at least thd big and should not be excluded
    if( valCand <= thd_value_ || is_excluded(img_it)){
      continue;
    }

    //set peak search parameters
    Real outer_limit = outer_sens_*valCand;
    Real inner_limit = inner_sens_*valCand;
    Real inner_count_stdval=inner_count_sens_*valCand;
    LOG_VERBOSE(" outer limit: " << outer_limit << std::endl);
    LOG_VERBOSE(" inner limit: " << inner_limit << std::endl);
    LOG_VERBOSE(" inner count value: " << inner_count_stdval << std::endl);

    unsigned int inner_count=0;
    int inner_win_edge = inner_win_*2 + 1;
    ost::img::Extent win_ext(win_size,ost::img::Point(img_it));
    //exclude peaks if part of outer win is outside image
    if( !img_ext.Contains(win_ext)){
      continue;
    }
    ost::img::Extent inner_win_ext(ost::img::Size(inner_win_edge,inner_win_edge,inner_win_edge),img_it);

    ost::img::ExtentIterator search_it(Overlap(img_ext,win_ext));
    for(;!search_it.AtEnd();++search_it) {
      Real val = ost::img::Val2Val<T,Real>(isi.Value(search_it));
      if( ost::img::Point(search_it) != ost::img::Point(img_it) ) { // ignore center point
        Real valdiff=valCand-val;
        if(inner_win_ext.Contains(search_it)) {
          /*Candidate is only accepted if it's value is at least
          inner_limit_' higher than all other values in the
          given inner_radius */
          if( valdiff < inner_limit ){
            break;
          }
          /*
          accumulate inner count if value of this inner point
          not smaller than certain threshold
          */
          if(val >= inner_count_stdval) {
            inner_count++;
          }
        } else {
          /* Candidate is only accepted if it's value is at least
          'limit' higher than all other values in the given
          radius */
          if( valdiff < outer_limit  ){
            break;
          }
        }
      }
    }
    if(search_it.AtEnd() && inner_count >= inner_count_lim_ ){
      ost::img::Peak peak(img_it,valCand);
      LOG_INFO("peak found at " << ost::img::Point(peak) <<std::endl);
      peaks_.push_back(peak);
    }
  }
}

void PeakSearchBase::VisitFunction(const ost::img::Function& f)
{
  throw ost::Error("VisitFunction not implemented");
}

ost::img::PeakList PeakSearchBase::GetPeakList() const
{
  return peaks_;
}


void PeakSearchBase::SetThreshold( Real minimum )
{
  thd_value_ = minimum;
  thd_set_ = true;
}

void PeakSearchBase::ClearPeakList()
{
  peaks_.clear();
}

void PeakSearchBase::SetOuterWindowSize(unsigned int outer_win)
{
  outer_win_=outer_win;
}
unsigned int PeakSearchBase::GetOuterWindowSize()
{
  return outer_win_;
}
void PeakSearchBase::SetOuterSensitivity(Real outer_sens)
{
  outer_sens_=outer_sens;
}
Real PeakSearchBase::GetOuterSensitivity()
{
  return outer_sens_;
}

void PeakSearchBase::SetInnerCountLimit(unsigned int inner_count_lim)
{
  inner_count_lim_=inner_count_lim;
}
unsigned int PeakSearchBase::GetInnerCountLimit()
{
  return inner_count_lim_;
}
void PeakSearchBase::SetInnerCountSensitivity(Real inner_count_sens)
{
  inner_count_sens_=inner_count_sens;
}
Real PeakSearchBase::GetInnerCountSensitivity()
{
  return inner_count_sens_;
}

void PeakSearchBase::SetInnerWindowSize(unsigned int inner_win)
{
  inner_win_=inner_win;
}
unsigned int PeakSearchBase::GetInnerWindowSize()
{
  return inner_win_;
}
void PeakSearchBase::SetInnerSensitivity(Real inner_sens)
{
  inner_sens_=inner_sens;
}
Real PeakSearchBase::GetInnerSensitivity()
{
  return inner_sens_;
}

void PeakSearchBase::SetMode( unsigned int mode )
{
  if(mode>MODES_START && mode < MODES_END){
    mode_=mode;
  }else{
    LOG_INFO("Ignoring invalid peak search mode." << std::endl);
  }
}
unsigned int PeakSearchBase::GetMode()
{
  return mode_;
}

void PeakSearchBase::AddExclusion(const ost::img::Extent&e)
{
  ext_list_.push_back(e);
}

// private methods


void PeakSearchBase::check_values()
{
  if(outer_win_<1)
    {
      LOG_INFO("WARNING: radius must be at least 1, reset to 1" << std::endl);
      outer_win_=1;
    }
  if( outer_win_ <= inner_win_)
    {
      inner_win_ = outer_win_ -1;
      LOG_INFO("WARNING: Inner peak radius bigger or equal to outer");
      LOG_INFO(" peak radius ---->the inner peak radius was set ");
      LOG_INFO("to outer peak radius - 1" << std::endl);
    }
}

bool PeakSearchBase::is_excluded(const ost::img::Point& p)
{
  for(ExtList::const_iterator it=ext_list_.begin();it!=ext_list_.end();++it) {
    if(it->Contains(p)) return true;
  }
  return false;
}

}} // namespaces

template class ost::img::ImageStateNonModAlgorithm<iplt::alg::PeakSearchBase>;
