//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk
*/

#include <boost/shared_ptr.hpp>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/log.hh>

#include "refine_lattice.hh"

namespace iplt {  namespace alg {

namespace {

struct gsl_vector_deleter {
  void operator()(gsl_vector* p) {
    gsl_vector_free(p);
  }
};

typedef boost::shared_ptr<gsl_vector> gsl_vector_ptr;
  

struct gsl_matrix_deleter {
  void operator()(gsl_matrix* p) {
    gsl_matrix_free(p);
  }
};

typedef boost::shared_ptr<gsl_matrix> gsl_matrix_ptr;


struct gsl_multifit_deleter {
  void operator()(gsl_multifit_linear_workspace* p) {
    gsl_multifit_linear_free(p);
  }
};

typedef boost::shared_ptr<gsl_multifit_linear_workspace> gsl_multifit_ptr;
  

/*
  If k3 is assumed to be zero, the system to be solved reduces to 
  linear version:

  L_i = u_i * P + v_i * Q  + O 

  Splitting into x and y component, an overdetermined linear equation
  system is constructed for each component, and solved using SVD.

  Ax=b

       .       .      .
      u_{i-1} v_{i-1} 1 
  A = u_{i}   v_{i}   1
      u_{i+1} v_{i+1} 1
       .       .      .

               .
       P       L_{i-1}
  x =  Q     b=L_{i}
       O       L_{i+1}
               .
*/

Lattice refine_lattice_without_distortion(const LatticePointList& lpl, bool fit_origin)
{
  uint sizeN = lpl.NumEntries(); 

 
  uint sizeP= fit_origin ? 3 : 2;

  if(sizeN<sizeP) throw(Error("not enough points to fit lattice"));
 
  // construct solution matrix A
  gsl_matrix_ptr A(gsl_matrix_alloc(sizeN,sizeP),gsl_matrix_deleter());
  gsl_matrix_ptr COV(gsl_matrix_alloc(sizeP,sizeP),gsl_matrix_deleter());

  gsl_vector_ptr b(gsl_vector_alloc(sizeN),gsl_vector_deleter());
  gsl_vector_ptr w(gsl_vector_alloc(sizeN),gsl_vector_deleter());

  // workspace
  gsl_vector_ptr tmp(gsl_vector_alloc(A->size2),gsl_vector_deleter());
  
  // solution vector
  gsl_vector_ptr x(gsl_vector_alloc(A->size2),gsl_vector_deleter());

  geom::Vec2 P(0.0,0.0);
  geom::Vec2 Q(P);
  geom::Vec2 O(P);

  Real chisq[]={0.0,0.0};

  for(int xy=0;xy<2;++xy) {

    // fill A and b
    int c=0;
    for(LatticePointProxyter lpp=lpl.Begin();!lpp.AtEnd(); ++lpp) {
      gsl_matrix_set(A.get(), c, 0, static_cast<Real>(lpp.GetIndex()[0]));
      gsl_matrix_set(A.get(), c, 1, static_cast<Real>(lpp.GetIndex()[1]));
      if(fit_origin){
        gsl_matrix_set(A.get(), c, 2, 1.0);
      }
      
      gsl_vector_set(b.get(), c, lpp.LPoint().GetFit().GetU()[xy]);
      gsl_vector_set(w.get(), c, 1.0); // TODO weight
      ++c;
    }

    gsl_multifit_ptr workspace(gsl_multifit_linear_alloc(sizeN,sizeP),gsl_multifit_deleter());
    double ttmp;
    gsl_multifit_wlinear(A.get(), w.get(), b.get(), x.get(), COV.get(), &ttmp, workspace.get());
    chisq[xy] = ttmp;

    P[xy]=gsl_vector_get(x.get(),0);
    Q[xy]=gsl_vector_get(x.get(),1);
    if(fit_origin){
      O[xy]=gsl_vector_get(x.get(),2);
    }
  }

  
  double gof[2] = {chisq[0]*chisq[0]/(Real)(sizeN-sizeP),
                   chisq[1]*chisq[1]/(Real)(sizeN-sizeP)};
    LOG_DEBUG( "reflat: goodness of fit: " << gof[0] << " " << gof[1] );

  Lattice nrvo(P,Q,O);
  return nrvo;
}

/*
  Non-linear least squares fitting of parameters (Ax,Ay), (Bx,By), 
  (Ox,Oy), and k3 for the lattice points at (Px,Py), given the known
  indices (u,v):

  Px = (u Ax + v Bx) (1 + k3((u Ax + v Bx)^2 + (u Ay + v By)^2))+(u Ay + v By)*s3*((u Ax + v Bx)^2 + (u Ay + v By)^2)) + Ox
  Py = (u Ay + v By) (1 + k3((u Ax + v Bx)^2 + (u Ay + v By)^2))+(u Ax + v Bx)*s3*((u Ax + v Bx)^2 + (u Ay + v By)^2)) + Oy

*/

struct reflat_params {
  const LatticePointList& lpl;
  bool fit_origin;
};

template <bool b1, bool b2>
int reflat_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  const LatticePointList& lpl = reinterpret_cast<reflat_params*>(params)->lpl;
  bool fit_origin=reinterpret_cast<reflat_params*>(params)->fit_origin;
  Real Ax = gsl_vector_get(x, 0);
  Real Ay = gsl_vector_get(x, 1);
  Real Bx = gsl_vector_get(x, 2);
  Real By = gsl_vector_get(x, 3);
  Real k3 = gsl_vector_get(x, 4);
  Real s3 = gsl_vector_get(x, 5);
  Real Ox=0.0,Oy=0.0;
  if(fit_origin){
    Ox = gsl_vector_get(x, 6);
    Oy = gsl_vector_get(x, 7);
  }
   
  int c=0; // counter for gsl vector and matrix positions
  for(LatticePointProxyter lpp=lpl.Begin();!lpp.AtEnd(); ++lpp) {
    Real u = static_cast<Real>(lpp.GetIndex()[0]);
    Real v = static_cast<Real>(lpp.GetIndex()[1]);
    // some precomp values
    Real xcomp = u*Ax+v*Bx;
    Real xcomp2 = xcomp*xcomp;
    Real ycomp = u*Ay+v*By;
    Real xycomp = xcomp*ycomp;
    Real ycomp2 = ycomp*ycomp;
    Real xycomp2 = xcomp2+ycomp2;

    Real inv_sigma=1.0; // TODO

    if(b1) { // function value
      Real Px = xcomp*(1.0+k3*xycomp2)+ycomp*s3*xycomp2+Ox;
      Real Py = ycomp*(1.0+k3*xycomp2)+xcomp*s3*xycomp2+Oy;
      geom::Vec2 Fxy = lpp.LPoint().GetFit().GetU(); // position determined by gaussian fit
      gsl_vector_set(f, c*2+0, (Px-Fxy[0])*inv_sigma);
      gsl_vector_set(f, c*2+1, (Py-Fxy[1])*inv_sigma);
    }
    if(b2) { // derivative values
      //Real xycomp = xcomp+ycomp;
      Real pre1x = 1.0+k3*(3.0*xcomp2+xycomp2)+2*s3*xycomp;
      Real pre1y = 1.0+k3*(3.0*ycomp2+xycomp2)+2*s3*xycomp;
      Real pre2 = 2.0*k3*xycomp+s3*xycomp2;

      gsl_matrix_set(J, c*2+0, 0, u*pre1x*inv_sigma); // dPx/dAx
      gsl_matrix_set(J, c*2+0, 1, u*pre2*inv_sigma); // dPx/dAy
      gsl_matrix_set(J, c*2+0, 2, v*pre1x*inv_sigma); // dPx/dBx
      gsl_matrix_set(J, c*2+0, 3, v*pre2*inv_sigma); // dPx/dBy
      gsl_matrix_set(J, c*2+0, 4, xcomp*xycomp2*inv_sigma); // dPx/dk3
      gsl_matrix_set(J, c*2+0, 5, ycomp*xycomp2*inv_sigma); // dPx/ds3
      gsl_matrix_set(J, c*2+1, 0, u*pre2*inv_sigma); // dPy/dAx
      gsl_matrix_set(J, c*2+1, 1, u*pre1y*inv_sigma); // dPy/dAy
      gsl_matrix_set(J, c*2+1, 2, v*pre2*inv_sigma); // dPy/dBx
      gsl_matrix_set(J, c*2+1, 3, v*pre1y*inv_sigma); // dPy/dBy
      gsl_matrix_set(J, c*2+1, 4, ycomp*xycomp2*inv_sigma); // dPy/dk3
      gsl_matrix_set(J, c*2+1, 5, xcomp*xycomp2*inv_sigma); // dPy/ds3

      if(fit_origin){
        gsl_matrix_set(J, c*2+0, 6, 1.0*inv_sigma); // dPx/dOx
        gsl_matrix_set(J, c*2+0, 7, 0.0*inv_sigma); // dPx/dOy

        gsl_matrix_set(J, c*2+1, 6, 0.0*inv_sigma); // dPy/dOx
        gsl_matrix_set(J, c*2+1, 7, 1.0*inv_sigma); // dPy/dOy
      }
    }
    ++c;
  }
  return GSL_SUCCESS;
}

int reflat_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return reflat_tmpl<true,false>(x,params,f,0);
}

int reflat_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return reflat_tmpl<false,true>(x,params,0,J);
}

int reflat_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return reflat_tmpl<true,true>(x,params,f,J);
}

Lattice refine_lattice_with_distortion(const LatticePointList& lpl, bool fit_origin)
{
  int paramN=6;
  if(fit_origin){
    paramN=8;
  }
  int valueN=lpl.NumEntries()*2;
  Lattice lattice = lpl.GetLattice();

  if(valueN<paramN) throw(Error("not enough points to fit lattice"));

  gsl_vector_ptr X(gsl_vector_alloc(paramN),gsl_vector_deleter());

  // initial guess
  gsl_vector_set(X.get(),0,lattice.GetFirst()[0]); // Ax
  gsl_vector_set(X.get(),1,lattice.GetFirst()[1]); // Ay
  gsl_vector_set(X.get(),2,lattice.GetSecond()[0]); // Bx
  gsl_vector_set(X.get(),3,lattice.GetSecond()[1]); // By
  gsl_vector_set(X.get(),4,lattice.GetBarrelDistortion()); // k3
  gsl_vector_set(X.get(),5,lattice.GetSpiralDistortion()); // s3
  if(fit_origin){
    gsl_vector_set(X.get(),6,lattice.GetOffset()[0]); // Ox
    gsl_vector_set(X.get(),7,lattice.GetOffset()[1]); // Oy
  }

  // prepare the functions
  gsl_multifit_function_fdf func;
  func.f = &reflat_f;
  func.df = &reflat_df;
  func.fdf = &reflat_fdf;
  func.n = valueN;
  func.p = paramN;
  reflat_params params={lpl,fit_origin};
  func.params = &params;

  // levenberg-marquardt solver
  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X.get());

  int iter=0;
  int status=GSL_CONTINUE;
  int max_iter=1000;
  
  while (status == GSL_CONTINUE && iter < max_iter) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);

    status = gsl_multifit_test_delta (solver->dx, solver->x, 1e-40,1e-40);
  } 

  Lattice newlat(geom::Vec2(gsl_vector_get(solver->x, 0),gsl_vector_get(solver->x,1)),
		             geom::Vec2(gsl_vector_get(solver->x, 2),gsl_vector_get(solver->x,3)));
  newlat.SetBarrelDistortion(gsl_vector_get(solver->x, 4));
  newlat.SetSpiralDistortion(gsl_vector_get(solver->x, 5));
  if(fit_origin){
    newlat.SetOffset(geom::Vec2(gsl_vector_get(solver->x, 6),gsl_vector_get(solver->x,7)));
  }

  Real chi = gsl_blas_dnrm2(solver->f);
  Real gof = (chi*chi)/ (Real)(valueN - paramN);

  LOG_DEBUG( "reflat: " << newlat << ", k3=" << gsl_vector_get(solver->x,4) << ", s3=" << gsl_vector_get(solver->x,5) << "; chi="<< chi << " gof=" << gof << " iter=" << iter );

  gsl_multifit_fdfsolver_free (solver);
	       
  return newlat;
}

} // anon ns

Lattice RefineLattice(const LatticePointList& lpl, bool fit_distortions, bool fit_origin)
{
  return fit_distortions ? 
    refine_lattice_with_distortion(lpl,fit_origin) : 
    refine_lattice_without_distortion(lpl,fit_origin);
}

}} // ns
