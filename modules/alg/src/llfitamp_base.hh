//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#ifndef IPLT_EX_LLFITAMP_BASE_HH
#define IPLT_EX_LLFITAMP_BASE_HH

#include <vector>

#include <ost/base.hh>
#include <ost/geom/geom.hh>

#include "llfitamp_fw.hh"

namespace iplt {  namespace alg {

namespace detail {

struct DataEntry {
  Real zstar;
  Real intens;
  Real sig_intens; // this is sigma_intensity
};

typedef std::vector<DataEntry> DataList;

struct LLFitAmpParams {
  DataList& data;
  Real sampling;
  int count;
  Real bfac;
};

} // ns detail

struct LLFitCurvePoint {
  Real zfit;
  Real ifit;
  Real sigifit;
};

// base class for all amplitude based lattice line fits
class DLLEXPORT_IPLT_ALG LLFitAmpBase {
public:
  //! initialize with pixel-count and pixel-sampling
  LLFitAmpBase(unsigned int count, Real sampling);

  virtual ~LLFitAmpBase();

  //! do actual fitting, returning the list of spatial points
  virtual void Apply() = 0;

  // calculate value and error from fit at given zstar
  virtual std::pair<Real,Real> CalcAt(Real zstar) const = 0;

  // return closest point (as intens, zstar) on curve to given pair
  LLFitCurvePoint ClosestAt(Real intens, Real zstar);

  //! set initial guess, size of vector must match count used in ctor
  virtual void SetGuess(const std::vector<Real>& guess);

  // return actual number of iterations until convergence
  virtual unsigned int GetIterCount() const;

  virtual std::vector<Complex> GetComponents() const = 0;

  virtual void Cleanup();

  // return pixel count
  unsigned int GetCount() const;

  // return pixel sampling
  Real GetSampling() const;

  //! add intensity and its sigma at reciprocal z
  void Add(Real zstar, Real intens, Real sig_intens);
  
  //! clear data list
  void Clear();

  // set maximal iterations of fitting routine
  void SetMaxIter(unsigned int m);

  void SetIterationLimits(Real,Real);

  void SetRelativeZWeight(Real w);

  void FitBFactor(bool f);

  void SetBFactor(Real b);
  Real GetBFactor() const;

protected:
  unsigned int count_;
  Real sampling_;
  detail::DataList data_list_;
  std::vector<Real> guess_;
  unsigned int max_iter_;
  unsigned int iter_count_;
  Real lim1_,lim2_;
  std::vector<LLFitCurvePoint> curve_;
  bool dirty_curve_;
  int curve_count_;
  Real max_zstar_;
  Real max_intens_;
  Real relative_weighting_;
  bool fit_bfac_;
  Real bfac_;
};

}} // ns

#endif

