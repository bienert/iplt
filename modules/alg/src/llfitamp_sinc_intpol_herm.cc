//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

#include <ost/log.hh>
#include <ost/message.hh>
#include <ost/img/value_util.hh>

#include <iplt/alg/weighted_bin.hh>

#include "llfitamp_sinc_intpol.hh"

#undef LOG_ERROR
#undef LOG_INFO
#undef LOG_VERBOSE
#undef LOG_DEBUG

#if OST_VERSION_MAJOR==1 && OST_VERSION_MINOR==0
#  define LOG_ERROR(m) if(true){std::cout << m << std::endl;}
#  define LOG_INFO(m) if(::ost::Logger::Instance().GetLogLevel()>=::ost::Logger::NORMAL){std::cout << m << std::endl;}
#  define LOG_VERBOSE(m) if(::ost::Logger::Instance().GetLogLevel()>=::ost::Logger::VERBOSE){std::cout << m << std::endl;}
#  define LOG_DEBUG(m) if(::ost::Logger::Instance().GetLogLevel()>=::ost::Logger::DEBUG){std::cout << m << std::endl;}
#else
#  define LOG_ERROR(m) if(true){std::cout << m << std::endl;}
#  define LOG_INFO(m) if(::ost::Logger::Instance().GetVerbosityLevel()>=::ost::Logger::NORMAL){std::cout << m << std::endl;}
#  define LOG_VERBOSE(m) if(::ost::Logger::Instance().GetVerbosityLevel()>=::ost::Logger::VERBOSE){std::cout << m << std::endl;}
#  define LOG_DEBUG(m) if(::ost::Logger::Instance().GetVerbosityLevel()>=::ost::Logger::DEBUG){std::cout << m << std::endl;}
#endif

namespace iplt {  namespace alg {

LLFitAmpSincIntpolHerm::LLFitAmpSincIntpolHerm(unsigned int count, Real sampling, guessmode gm):
  LLFitBase(count,sampling,gm),
  sc_(),
  sv_()
{
}

LLFitBasePtr LLFitAmpSincIntpolHerm::Clone()
{
    return LLFitBasePtr(new LLFitAmpSincIntpolHerm(*this));
}

namespace {

template <bool func, bool derivatives, bool even>
int T_funcSI(const gsl_vector* X, void* vparams, gsl_vector* f, gsl_matrix* J)
{
  /*
    X contains Re and Im parts of the sinc components

      X[n*2-1] = Re of comp n
      X[n*2+0] = Im of comp n
    with special case for n=0, which contains 
      X[0] = Re of comp 0 (Im is always zero)
    and special case for n=N/2, which contains
      X[n*2-1] = Re of comp N/2 (Im is always zero) for even N
      and
      X[n*2-1] = Re of comp N/2
      X[n*2+0] = Im of comp N/2 for odd N
  */
  detail::LLFitParams* params = reinterpret_cast<detail::LLFitParams*>(vparams);

  Real W=static_cast<Real>(params->count)*params->sampling;
  Real iND = 1.0/W;
  Real bfac2 = params->bfac*params->bfac;

  // book keeping vectors
  std::vector<Real> sinc_term1(params->count/2+1);
  std::vector<Real> sinc_term2(params->count/2+1);

  int index=0;
  for(detail::IntensList::const_iterator it=params->intens.begin(); it!=params->intens.end();++it) {
    Real sum_re=0.0;
    Real sum_im=0.0;
    Real i_sigma = 1.0/it->sig_intens;

    // special case 0 reflection
    Real R0 = gsl_vector_get(X,0);
    sinc_term1[0] = Sinc(it->zstar,W,bfac2);
    sum_re += R0 * sinc_term1[0];


    // remaining reflections from 1 to N/2-1
    for(int n=1;n<params->count/2;++n) {
      Real pn = static_cast<Real>(n)*iND;

      Real Rn = gsl_vector_get(X,n*2-1);
      Real In = gsl_vector_get(X,n*2+0);

      sinc_term1[n]=Sinc(it->zstar-pn,W,bfac2);
      sinc_term2[n]=Sinc(it->zstar+pn,W,bfac2);

      sum_re+=Rn*(sinc_term1[n]+sinc_term2[n]);
      sum_im+=In*(sinc_term1[n]-sinc_term2[n]);
    }
    // special case N/2 reflection
    Real pN2 = static_cast<Real>(params->count/2)*iND;
    sinc_term1[params->count/2] = Sinc(it->zstar-pN2,W,bfac2);
    sinc_term2[params->count/2] = Sinc(it->zstar+pN2,W,bfac2);
    if(even){
      Real RN2 = gsl_vector_get(X,params->count-1);
      sum_re+=RN2*(sinc_term1[params->count/2]+sinc_term2[params->count/2]);
    }else{
      Real RN2 = gsl_vector_get(X,params->count-2);
      Real IN2 = gsl_vector_get(X,params->count-1);
      sum_re+=RN2*(sinc_term1[params->count/2]+sinc_term2[params->count/2]);
      sum_im+=IN2*(sinc_term1[params->count/2]-sinc_term2[params->count/2]);
    }


    if(func) {
      Real fx = sum_re*sum_re+sum_im*sum_im;
      gsl_vector_set(f,index,(fx-it->intens)*i_sigma);
    }
    if(derivatives) {
      // special case for 0 reflection
      Real der_R0 = 2.0*sinc_term1[0]*sum_re;
      gsl_matrix_set(J,index,0,der_R0*i_sigma);
      // remaining reflections
      for(int c=1;c<params->count/2;++c) {
        Real der_Rn = 2.0*(sinc_term1[c]+sinc_term2[c])*sum_re;
        Real der_In = 2.0*(sinc_term1[c]-sinc_term2[c])*sum_im;

        gsl_matrix_set(J,index,c*2-1,der_Rn*i_sigma);
        gsl_matrix_set(J,index,c*2-0,der_In*i_sigma);
      }
      // special case for N/2 reflection
      if(even){
        Real der_RN2 = 2.0*(sinc_term1[params->count/2]+sinc_term2[params->count/2])*sum_re;
        gsl_matrix_set(J,index,params->count-1,der_RN2*i_sigma);
      }else{
        Real der_RN2 = 2.0*(sinc_term1[params->count/2]+sinc_term2[params->count/2])*sum_re;
        Real der_IN2 = 2.0*(sinc_term1[params->count/2]-sinc_term2[params->count/2])*sum_im;
        gsl_matrix_set(J,index,params->count-2,der_RN2*i_sigma);
        gsl_matrix_set(J,index,params->count-1,der_IN2*i_sigma);
      }
    }

    ++index;
  }

  return GSL_SUCCESS;
}

static int SI_func_f_even(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcSI<true,false,true>(x,params,f,0);
}

static int SI_func_df_even(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcSI<false,true,true>(x,params, 0, J);
}

static int SI_func_fdf_even(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcSI<true,true,true>(x,params, f, J);
}

static int SI_func_f_odd(const gsl_vector* x, void* params, gsl_vector* f)
{
  return T_funcSI<true,false,false>(x,params,f,0);
}

static int SI_func_df_odd(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return T_funcSI<false,true,false>(x,params, 0, J);
}

static int SI_func_fdf_odd(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return T_funcSI<true,true,false>(x,params, f, J);
}
} // anon ns

void LLFitAmpSincIntpolHerm::Apply()
{
  //only half the fourier space needs to be defined
  int herm_count = count_/2;

  detail::LLFitParams params = {intens_list_,
				phase_list_,
				sampling_,
        count_,
				bfac_};

  // Re/Im pair
  int paramN = count_;

  int valueN = intens_list_.size();

  if(valueN < paramN) {
    throw ost::Error("Error in LLFitAmpSincIntpolHerm: dataN < paramN  ");
  }

  gsl_vector* X = gsl_vector_alloc(paramN);

  // initial values from guess
  switch(guessmode_){
  case GUESSMODE_BINAVERAGE:{
    Real guess_bin_delta = 1.0/(static_cast<Real>(count_)*sampling_);
    Real guess_res_low = 0.0-guess_bin_delta*0.5;
    Real guess_res_hi = herm_count*guess_bin_delta/sampling_+guess_bin_delta*0.5;
    WeightedBin bin_list(herm_count+1,guess_res_low,guess_res_hi);
    for(detail::IntensList::const_iterator it=intens_list_.begin();it!=intens_list_.end();++it){
      bin_list.Add(it->zstar,it->intens,1.0/(std::max<Real>(it->sig_intens*it->sig_intens,1e-20)));
    }
    gsl_vector_set(X,0,sqrt(bin_list.GetAverage(0))); // R0
    for (int n=1;n<herm_count;++n) {
      Real amp=sqrt(bin_list.GetAverage(n));
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,n*2-1,amp*cos(phi)); // Rn
      gsl_vector_set(X,n*2  ,amp*sin(phi)); // In
    }
    if(count_%2==0){
      gsl_vector_set(X,count_-1,sqrt(bin_list.GetAverage(herm_count))); // RN2
    }else{
      Real amp=sqrt(bin_list.GetAverage(herm_count));
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,count_-2,amp*cos(phi)); // RN2
      gsl_vector_set(X,count_-1,amp*sin(phi)); // IN2
    }
  }
    break;
  case GUESSMODE_CONST:
    gsl_vector_set(X,0,1.0); // R0
    for (int n=0;n<herm_count-1;++n) {
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,n*2+1,cos(phi)); // Rn
      gsl_vector_set(X,n*2+2,sin(phi)); // In
    }
    if(count_%2==0){
      gsl_vector_set(X,count_-1,1.0); // RN2
    }else{
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,count_-2,cos(phi)); // RN2
      gsl_vector_set(X,count_-1,sin(phi)); // IN2
    }
    break;
  case GUESSMODE_RANDOM:
    gsl_vector_set(X,0,ost::img::Random<Real>()); // R0
    for (int n=0;n<herm_count-1;++n) {
      Real amp=ost::img::Random<Real>();
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,n*2+1,amp*cos(phi)); // Rn
      gsl_vector_set(X,n*2+2,amp*sin(phi)); // In
    }
    if(count_%2==0){
      gsl_vector_set(X,count_-1,ost::img::Random<Real>()); // RN2
    }else{
      Real amp=ost::img::Random<Real>();
      Real phi = ost::img::Random<Real>()*2.0*M_PI;
      gsl_vector_set(X,count_-2,amp*cos(phi)); // RN2
      gsl_vector_set(X,count_-1,amp*sin(phi)); // IN2
    }
    break;
  default:
    throw ost::Error("Error in LLFitAmpSincIntpolHerm: Unknown guessmode");
  }

  
  gsl_multifit_function_fdf func;
  if(count_%2==0){
    func.f = &SI_func_f_even;
    func.df = &SI_func_df_even;
    func.fdf = &SI_func_fdf_even;
  }else{
    func.f = &SI_func_f_odd;
    func.df = &SI_func_df_odd;
    func.fdf = &SI_func_fdf_odd;
  }
  func.n = valueN;
  func.p = paramN;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;

  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, valueN, paramN);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  LOG_VERBOSE( "commencing iterations" );
  LOG_DEBUG( "max_iter=" << max_iter_ << " lim1=" << lim1_ << " lim2=" << lim2_ );
  
  unsigned int iter=0;
  int status=GSL_CONTINUE;
  // actual iterations

  while (status == GSL_CONTINUE && iter < max_iter_) {
    ++iter;
    
    gsl_multifit_fdfsolver_iterate (solver);

    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1_, lim2_);
  } 
  
  LOG_VERBOSE( "done with iterations (" << iter << "): " << gsl_strerror (status) );

  iter_count_=iter;
  
  Real chi=gsl_blas_dnrm2(solver->f);
  if(chi!=chi) { // NaN
    LOG_VERBOSE( "chi is NaN, set to 1e100" );
    chi=1e100;
  }
  Real dof = static_cast<Real>(valueN-paramN);
  Real errf = std::min(1.0,chi/sqrt(dof));
  LOG_VERBOSE( "chi = " << chi );
  LOG_VERBOSE( "chisq/dof = " << (chi*chi)/dof );

  LOG_DEBUG( "stddev factor = " << errf );

  gsl_matrix* covar = gsl_matrix_alloc(paramN,paramN);
  gsl_multifit_covar(solver->J, 0.0, covar);

  sc_.clear();
  sv_.clear();

  // special case 0 reflection
  Real R0=gsl_vector_get(solver->x,0);
  LOG_DEBUG( "llfit results" );
  sc_.push_back(Complex(R0,0.0));
  sv_.push_back(Complex(gsl_matrix_get(covar,0,0),0.0));
  LOG_DEBUG( " 0: " << sc_.back() << " (" << sv_.back() << ")" );
  for(int c=1;c<herm_count;++c) {
    Real Rn=gsl_vector_get(solver->x,c*2-1);
    Real In=gsl_vector_get(solver->x,c*2+0);
    Real sRn2=(gsl_matrix_get(covar,c*2-1,c*2-1));
    Real sIn2=(gsl_matrix_get(covar,c*2+0,c*2+0));
    sc_.push_back(Complex(Rn,In));
    sv_.push_back(Complex(sRn2,sIn2));
    LOG_DEBUG( " " << c << ": " << sc_.back() << " (" << sv_.back() << ")" );
  } 
  // special case N/2 reflection
  if(count_%2==0){
    Real RN2=gsl_vector_get(solver->x,count_-1);
    sc_.push_back(Complex(RN2,0.0));
    sv_.push_back(Complex(gsl_matrix_get(covar,count_-1,count_-1),0.0));
  }else{
    Real RN2=gsl_vector_get(solver->x,count_-2);
    Real IN2=gsl_vector_get(solver->x,count_-1);
    sc_.push_back(Complex(RN2,IN2));
    sv_.push_back(Complex(gsl_matrix_get(covar,count_-2,count_-2),gsl_matrix_get(covar,count_-1,count_-1)));
  }
  LOG_DEBUG( " " << herm_count << ": " << sc_.back() << " (" << sv_.back() << ")" );

  gsl_matrix_free(covar);
  gsl_multifit_fdfsolver_free(solver);
  gsl_vector_free(X);
}

std::pair<Real,Real> LLFitAmpSincIntpolHerm::CalcIntensAt(Real zstar) const
{
  Real w = static_cast<Real>(count_)*sampling_;
  Real iw = 1.0/w;
  Real bfac2 = bfac_*bfac_;
  /*
    sc_ goes from 0 to N/2, inclusively.
    for even count_ sc_[0].imag() and sc_[N].imag() are zero
    and for odd count_  sc_[0].imag() is zero
  */
  Real sinc0 = Sinc(zstar,w,bfac2);
  Real sum_re=sc_[0].real()*sinc0;
  // sv_ stores variance
  Real sum_re_sig2=sv_[0].real()*sinc0*sinc0;
  Real sum_im=0.0;
  Real sum_im_sig2=0.0;
  for(unsigned int n=1;n<sc_.size();++n) {
    Real zsc = static_cast<Real>(n)*iw;

    Real s_mi=Sinc(zstar-zsc,w,bfac2);
    Real s_pl=Sinc(zstar+zsc,w,bfac2);
    
    sum_re+=sc_[n].real()*(s_mi+s_pl);
    sum_re_sig2+=sv_[n].real()*(s_mi+s_pl)*(s_mi+s_pl);
    sum_im+=sc_[n].imag()*(s_mi-s_pl);
    sum_im_sig2+=sv_[n].imag()*(s_mi-s_pl)*(s_mi-s_pl);
  }

  Real icalc = sum_re*sum_re+sum_im*sum_im;
  /*
    sigma is derived as follows
      sigma[amp^2] = 2 * amp^2 * sqrt(sig_re^2/re^2 + sig_im^2/im^2)
  */
  Real t_re = 0.0;
  Real t_im = 0.0;
  Real sum_re2 = sum_re*sum_re;
  Real sum_im2 = sum_im*sum_im;
  if(sum_re_sig2>0.0 && sum_re2>0.0) {
    t_re = sum_re_sig2/sum_re2;
  }
  if(sum_im_sig2>0.0 && sum_im2>0.0) {
    t_im = sum_im_sig2/sum_im2;
  }

  Real sigicalc = 2.0*icalc*sqrt(t_re+t_im);
  return std::make_pair(icalc,sigicalc);
}

std::map<Real,Complex> LLFitAmpSincIntpolHerm::GetComponents() const
{
  Real stepsize=1.0/(sampling_*count_);
  std::map<Real,Complex> result;
  for(int i=0;i<sc_.size();++i){
    result[stepsize*i]=sc_[i];
  }
  return result;
}

}} // ns
