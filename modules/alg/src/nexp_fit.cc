//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>

#include <ost/log.hh>
#include "nexp_fit.hh"

namespace iplt { namespace alg{




namespace {

struct ExpFitParams 
{
  NExpFitList* value_list;
  bool zero_offset;
  unsigned int num;
};

template<bool b1,bool b2>
int nexp_fit_func_tmpl(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  ExpFitParams* prm = reinterpret_cast<ExpFitParams*>(params);

  std::vector<Real> S;
  std::vector<Real> B;
  for(unsigned int i=0;i<prm->num;++i){
    S.push_back(gsl_vector_get(x,2*i));
    B.push_back(gsl_vector_get(x,2*i+1));
  }
  Real C=0.0;
  if(!prm->zero_offset) {
    C = gsl_vector_get(x,prm->num*2);
  }

  int indx=0;
  for(NExpFitList::const_iterator it=prm->value_list->begin();it!=prm->value_list->end();++it) {
    if(b1) {
      Real f_xy=C;
      for(unsigned int i=0;i<prm->num;++i){
        f_xy += S[i]*exp(-B[i]*it->x);
      }
      gsl_vector_set(f,indx,(f_xy-it->y)*it->w);
    }

    if(b2) {
      for(unsigned int i=0;i<prm->num;++i){
        Real der_S = it->w * exp(-B[i]*it->x);
        Real der_B = it->w * (-S[i]*exp(-B[i]*it->x))*it->x;
        gsl_matrix_set(J,indx,2*i,der_S);
        gsl_matrix_set(J,indx,2*i+1,der_B);
      }
      if(!prm->zero_offset) {
         gsl_matrix_set(J,indx,prm->num*2,it->w*1.0); // der_C
      }
    }

    ++indx;
  }
  return GSL_SUCCESS;
}

static int nexp_fit_func_f(const gsl_vector* x, void* params, gsl_vector* f)
{
  return nexp_fit_func_tmpl<true,false>(x,params,f,0);
}

static int nexp_fit_func_df(const gsl_vector* x, void* params, gsl_matrix* J)
{
  return nexp_fit_func_tmpl<false,true>(x,params,0,J);
}

static int nexp_fit_func_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
{
  return nexp_fit_func_tmpl<true,true>(x,params,f,J);
}

} // anon ns

NExpFit::NExpFit(unsigned int num, bool zo):
  num_(num),
  list_(),
  zero_offset_(zo),
  svec_(),
  bvec_(),
  C_(0.0),
  chi_(0.0)
{
}

NExpFit::~NExpFit()
{
}


void NExpFit::Add(Real x, Real y, Real w)
{
  list_.push_back(NExpFitEntry(x,y,w));
}

void NExpFit::Add(Real x, Real y)
{
  this->Add(x,y,1.0);
}

void NExpFit::Apply()
{
  if(zero_offset_) {
    LOG_VERBOSE( "nexp fit with zero offset" );
  } else {
    LOG_VERBOSE( "nexp fit" );
  }

  if(list_.size()<num_*2+(zero_offset_ ? 0 : 1)) {
    LOG_INFO( "not enough points for nexp fit, returning unity" );
    chi_ = 0.0;
    return;
  }

  int num_p = zero_offset_ ? 2*num_ : 2*num_+1;

  // fill list_ from binning
  ExpFitParams params = {&list_,zero_offset_,num_};
  
  gsl_vector* X = gsl_vector_alloc(num_p);
  for(unsigned int i=0;i<num_;++i){
    gsl_vector_set(X,2*i,15000.0);
    gsl_vector_set(X,2*i+1,1/static_cast<Real>(i+1));
  }
  if(!zero_offset_) {
    gsl_vector_set(X,num_*2,C_);
  }
  
  gsl_multifit_function_fdf func;
  func.f = &nexp_fit_func_f;
  func.df = &nexp_fit_func_df;
  func.fdf = &nexp_fit_func_fdf;
  func.n = list_.size();
  func.p = num_p;
  func.params = &params;

  const gsl_multifit_fdfsolver_type *solver_type = gsl_multifit_fdfsolver_lmsder;
  gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc (solver_type, list_.size(),num_p);
  gsl_multifit_fdfsolver_set (solver, &func, X);

  Real lim1=1e-10;
  Real lim2=1e-10;
  int iter=0;
  int max_iter=1000;
  int status=GSL_CONTINUE;
  LOG_DEBUG( "running gsl_multifit solver" );

  while (status == GSL_CONTINUE && iter < max_iter) {
    ++iter;

    gsl_multifit_fdfsolver_iterate (solver);
    status = gsl_multifit_test_delta (solver->dx, solver->x, lim1, lim2);
  } 
  svec_.clear();
  bvec_.clear();
  for(unsigned int i=0;i<num_;++i){
    svec_.push_back(gsl_vector_get(solver->x,2*i));
    bvec_.push_back(gsl_vector_get(solver->x,2*i+1));
  }
  if(!zero_offset_) {
    C_ = gsl_vector_get(solver->x,2*num_);
  }
  chi_ = gsl_blas_dnrm2(solver->f);

  gsl_multifit_fdfsolver_free (solver);

  gsl_vector_free(X);

}

std::vector<Real> NExpFit::GetSVec() const
{
  return svec_;
}

std::vector<Real> NExpFit::GetBVec() const
{
  return bvec_;
}

Real NExpFit::GetC() const
{
  return C_;
}

Real NExpFit::GetChi() const
{
  return chi_;
}




}}//ns
