//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  exp plot fitting

  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_EXP_FIT_HH
#define IPLT_EX_EXP_FIT_HH

#include <iplt/reflection_algorithm.hh>
#include <iplt/alg/module_config.hh>

namespace iplt {  namespace alg {

struct ExpFitEntry {
  ExpFitEntry(Real xx=0.0, Real yy=0.0, Real ww=0.0):
    x(xx),y(yy),w(ww)
  {}
  Real x,y,w;
};

typedef std::vector< ExpFitEntry > ExpFitList;

/*
  y = S Exp[ B x ] + C
*/
class DLLEXPORT_IPLT_ALG ExpFit
{

public:
  ExpFit(bool zero_offset=false);

  // add x and y values with default weight of 1
  void Add(Real x, Real y);
  // add x and y values with weight, which is the reciprocal variance
  void Add(Real x, Real y, Real w);

  Real GetS() const;
  std::pair<Real,Real> GetSsigS() const;
  Real GetB() const;
  std::pair<Real,Real> GetBsigB() const;
  Real GetC() const;
  std::pair<Real,Real> GetCsigC() const;
  Real GetChi() const;

  void SetMaxIter(unsigned int m);
  void SetLimits(Real l1, Real l2);

  void Apply();

  std::pair<Real,Real> Estimate(Real x) const;

private:
  ExpFitList list_;
  bool zero_offset_;
  Real S_,B_,C_;
  Real sigS_, sigB_, sigC_;
  Real chi_;
  unsigned int maxiter_;
  Real lim1_;
  Real lim2_;
};

}} // ns

#endif

