//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#ifndef IPLT_EX_PEAK_INT_IMPL_HH
#define IPLT_EX_PEAK_INT_IMPL_HH

#include <ost/img/algorithm.hh>
#include <ost/img/image_state.hh>
#include <ost/log.hh>

namespace iplt {  namespace alg { namespace detail {

class PeakIntegrationBase {
public:
    PeakIntegrationBase(const ost::img::Extent& peak_region);

  Real GetPeakSum() const {return peak_sum_;}
  Real GetPeakVolume() const {return peak_volume_;}
  int GetPeakCount() const {return peak_count_;}
  Real GetPeakSigma() const {return peak_sigma_;}
  Real GetBackgroundMean() const {return bg_mean_;}
  Real GetBackgroundSigma() const {return bg_sigma_;}
  int GetBackgroundCount() const {return bg_count_;}

protected:
  ost::img::Extent peak_region_;
  Real peak_sum_;
  Real peak_volume_;
  Real peak_sigma_;
  int peak_count_;
  Real bg_mean_;
  Real bg_sigma_;
  int bg_count_;

  void reset();
};


class SimplePeakIntegrationBase: public PeakIntegrationBase 
{
public:
    SimplePeakIntegrationBase(): PeakIntegrationBase(ost::img::Extent()) {}
  SimplePeakIntegrationBase(const ost::img::Extent& peak_region):
    PeakIntegrationBase(peak_region) {}
  
  template <typename T, class D>
          void VisitState(const ost::img::ImageStateImpl<T,D>& isi);

  void VisitFunction(const ost::img::Function& f) {
      throw ost::Error("VisitFunction not implemented for PeakIntegration");
  }

  static String GetAlgorithmName() {return "SimplePeakIntegration";}

};

typedef ost::img::ImageStateNonModAlgorithm<SimplePeakIntegrationBase> SimplePeakIntegration;


class G2PeakIntegrationBase: public PeakIntegrationBase 
{
public:
    G2PeakIntegrationBase(): PeakIntegrationBase(ost::img::Extent()) {}
  G2PeakIntegrationBase(const ost::img::Extent& peak_region):
    PeakIntegrationBase(peak_region) {}
  
  template <typename T, class D>
          void VisitState(const ost::img::ImageStateImpl<T,D>& isi);

  void VisitFunction(const ost::img::Function& f) {
      throw ost::Error("VisitFunction not implemented for PeakIntegration");
  }

  static String GetAlgorithmName() {return "G2PeakIntegration";}

};

typedef ost::img::ImageStateNonModAlgorithm<G2PeakIntegrationBase> G2PeakIntegration;




}}}
#endif
