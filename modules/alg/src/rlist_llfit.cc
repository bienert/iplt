//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include <QFuture>
#include <QtConcurrentMap>
#include <ost/img/value_util.hh>
#include "rlist_llfit.hh"
#include <iplt/reflection_proxyter.hh>
#include <iplt/alg/llfitamp_composite.hh>
#include <iplt/alg/weighted_bin.hh>


namespace iplt {  namespace alg {

typedef std::pair<ReflectionList,ReflectionList> ReflectionListPair;

namespace detail {
  class fit_helper_ {
  public:
      typedef ReflectionListPair result_type;

      fit_helper_(const LLFitBasePtr& llfit, Real max_resolution, unsigned int min_bincount, unsigned int bootstrapping_iterations, const String& iobs_name,const String& sigiobs_name):
          llfit_(llfit),
          max_resolution_(max_resolution),
          min_bincount_(min_bincount),
          bootstrapping_iterations_(bootstrapping_iterations),
          iobs_name_(iobs_name),
          sigiobs_name_(sigiobs_name)
      {
      }
      ReflectionListPair operator()(const ReflectionListPair& rlistpair)
      {
        try{
          LLFitBasePtr combined_llfit;
          if(bootstrapping_iterations_==0){
              LLFitBasePtr subset_llfit=llfit_->Clone();
              Real best_zstar=find_resolution_limit_(subset_llfit,rlistpair.first);
              Real best_sampling=0.5/best_zstar;
              unsigned int best_count = std::min<unsigned int>(static_cast<unsigned int>(static_cast<Real>(subset_llfit->GetCount())*subset_llfit->GetSampling()/best_sampling),subset_llfit->GetCount());
              subset_llfit->SetSampling(llfit_->GetCount()*llfit_->GetSampling()/static_cast<Real>(best_count));
              subset_llfit->SetCount(best_count);
              for(ReflectionProxyter rp=rlistpair.first.Begin();!rp.AtEnd();++rp){
                  subset_llfit->AddIntens(rp.GetIndex().GetZStar(),rp.Get(iobs_name_),rp.Get(sigiobs_name_));
              }
              subset_llfit->Apply();
              combined_llfit=subset_llfit;
          }else{
              combined_llfit=LLFitBasePtr(new LLFitAmpComposite());
              std::vector<detail::IntensEntry> entries;
              for(ReflectionProxyter rp=rlistpair.first.Begin();!rp.AtEnd();++rp){
                  entries.push_back(detail::IntensEntry(rp.GetIndex().GetZStar(),rp.Get(iobs_name_),rp.Get(sigiobs_name_)));
              }
              ost::img::UniformIntGenerator generator=ost::img::GetUniformIntGenerator(0,rlistpair.first.NumReflections()-1);
              LLFitBasePtr subset_llfit=llfit_->Clone();
              Real best_zstar=find_resolution_limit_(subset_llfit,rlistpair.first);
              Real best_sampling=0.5/best_zstar;
              unsigned int best_count = std::min<unsigned int>(static_cast<unsigned int>(static_cast<Real>(subset_llfit->GetCount())*subset_llfit->GetSampling()/best_sampling),subset_llfit->GetCount());
              subset_llfit->SetSampling(llfit_->GetCount()*llfit_->GetSampling()/static_cast<Real>(best_count));
              subset_llfit->SetCount(best_count);
              for(unsigned int i=0;i<bootstrapping_iterations_;++i){
                  subset_llfit=subset_llfit->Clone();
                  for(unsigned int j=0;j<rlistpair.first.NumReflections();++j){
                     int index=generator(); 
                     subset_llfit->AddIntens(entries[index].zstar,entries[index].intens,entries[index].sig_intens);
                  }
                  subset_llfit->Apply();
                  boost::static_pointer_cast<LLFitAmpComposite>(combined_llfit)->Add(subset_llfit);
              }
          }
          return ReflectionListPair(create_fitted_(rlistpair.first,combined_llfit),create_discretized_(rlistpair.first,combined_llfit));
        } catch (std::exception&  e){
          std::cerr  << "Caught exception: "<< e.what() << std::endl;
          return ReflectionListPair(ReflectionList(),ReflectionList());
        } catch(char * s){
          std::cerr  << "Caught exception: "<< s<< std::endl;
          return ReflectionListPair(ReflectionList(),ReflectionList());
        } catch(...){
          std::cerr  << "Caught undefined exception: "<< std::endl;
          return ReflectionListPair(ReflectionList(),ReflectionList());
        }
      }


      LLFitBasePtr llfit_;
      Real max_resolution_;
      unsigned int min_bincount_;
      unsigned int bootstrapping_iterations_;
      String iobs_name_;
      String sigiobs_name_;

  protected:
      Real find_resolution_limit_(const LLFitBasePtr& llfit, const ReflectionList& rlist){
        WeightedBin wb(llfit->GetCount()/2,0.0,0.5/max_resolution_);
        for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp){
          // using abs(zstar) is a hack to simplify this routine
          // and make it independent of the herm mode
          wb.Add(std::abs<Real>(rp.GetIndex().GetZStar()),rp.Get(iobs_name_),1.0);
        }
        for(unsigned int n=0;n<llfit->GetCount()/2+1;++n){
          unsigned int bin=llfit->GetCount()/2-n;
          if(wb.GetSize(bin)>=min_bincount_){
            return wb.GetUpperLimit(bin);
          }
          //LogDebug("skipping bin %d (%g %g) due to insufficient values (%d<%d)"%(bin,wb.GetLowerLimit(bin),wb.GetUpperLimit(bin),wb.GetSize(bin),min_count))
        }
        return 0.0;
      }

      ReflectionList create_fitted_(const ReflectionList& rlist, const LLFitBasePtr& llfit){
          ReflectionList fitted(rlist);
          fitted.AddProperty("icalc");
          fitted.AddProperty("sigicalc");
          fitted.AddProperty("cflag");

          bool cflag=true;
          if(llfit->GetIterCount()==llfit->GetMaxIter()){
            cflag=false;
          }
          for(ReflectionProxyter rp=fitted.Begin();!rp.AtEnd();++rp){
              Real zstar=rp.GetIndex().GetZStar();
              std::pair<Real,Real> icalcsig=llfit->CalcIntensAt(zstar);
              rp.Set("icalc",icalcsig.first);
              rp.Set("sigicalc",icalcsig.second);
              rp.Set("cflag",cflag);
          }
          return fitted;
      }
      ReflectionList create_discretized_(const ReflectionList& rlist,const LLFitBasePtr& llfit){
          ReflectionList discretized(rlist.GetUnitCell());
          discretized.AddProperty(iobs_name_);
          discretized.AddProperty(sigiobs_name_);
          discretized.AddProperty("cflag");
          discretized.AddProperty("sinc_amp");
          discretized.AddProperty("sinc_phi");

          Point hk=rlist.Begin().GetIndex().AsDuplet();
          std::map<Real,Complex> sinc_components = llfit->GetComponents();
          for(std::map<Real,Complex>::const_iterator it=sinc_components.begin();it!=sinc_components.end();++it){
              ReflectionProxyter rp=discretized.AddReflection(ReflectionIndex(hk[0],hk[1],it->first));
              std::pair<Real,Real> icalcsig=llfit->CalcIntensAt(it->first);
              rp.Set(iobs_name_,icalcsig.first);
              rp.Set(sigiobs_name_,icalcsig.second);
              rp.Set("sinc_amp",abs(it->second));
              rp.Set("sinc_phi",arg(it->second));
              rp.Set("cflag",llfit->GetIterCount()<llfit->GetMaxIter());
          }
          return discretized;
      }
  };

  void merge_helper_(ReflectionListPair &merged, const ReflectionListPair &rlistpair){
      static bool initialized=false;
      if(!initialized){
        merged.first.CopyProperties(rlistpair.first);
        merged.second.CopyProperties(rlistpair.second);
        initialized=true;
      }
      merged.first.Merge(rlistpair.first);
      merged.second.Merge(rlistpair.second);
  }
}

RlistLLFit::RlistLLFit(const LLFitBasePtr& llfit, Real max_resolution, unsigned int min_bincount, unsigned int bootstrapping_iterations, const String& iobs_name,const String& sigiobs_name):
  ReflectionModOPAlgorithm(),
  llfit_(llfit),
  max_resolution_(max_resolution),
  min_bincount_(min_bincount),
  bootstrapping_iterations_(bootstrapping_iterations),
  discretized_(),
  iobs_name_(iobs_name),
  sigiobs_name_(sigiobs_name)
{
}

ReflectionList RlistLLFit::Visit(const ReflectionList& r)
{
 String iobs_name;
  String sigiobs_name;
  if(iobs_name_==""){
      iobs_name=r.GetPropertyNameByType(PT_INTENSITY);
  }else{
      iobs_name=iobs_name_;
  }
  if(sigiobs_name_==""){
      sigiobs_name=r.GetPropertyNameByType(PT_STDDEV);
  }else{
      sigiobs_name=sigiobs_name_;
  }
  QList<std::pair<ReflectionList,ReflectionList> > lattice_lines;
  ReflectionList lattice_line(r,false);
  ReflectionProxyter it=r.Begin();
  Point current_index=it.GetIndex().AsDuplet();
  while(!it.AtEnd()){
    while(current_index==it.GetIndex().AsDuplet()){
      lattice_line.AddReflection(it.GetIndex(),it);
      ++it;
    }
    lattice_lines.append(std::pair<ReflectionList,ReflectionList>(lattice_line,ReflectionList()));
    lattice_line=ReflectionList(r,false);
    current_index=it.GetIndex().AsDuplet();
  }
  QFuture<std::pair<ReflectionList,ReflectionList> > future=QtConcurrent::mappedReduced(lattice_lines,
                                                             detail::fit_helper_(llfit_,max_resolution_,min_bincount_,bootstrapping_iterations_,iobs_name,sigiobs_name),
                                                             detail::merge_helper_);
  discretized_=future.result().second;
  return future.result().first;
}
ReflectionList RlistLLFit::GetDiscretizedData()
{
    return discretized_;
}

}} //ns
