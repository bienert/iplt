//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

#include "llfitamp_composite.hh"
#include <map>

namespace iplt {  namespace alg {




LLFitAmpComposite::LLFitAmpComposite():
  LLFitBase(1,1.0),
  fit_list_()
{}

LLFitBasePtr LLFitAmpComposite::Clone()
{
    return LLFitBasePtr(new LLFitAmpComposite(*this));
}

void LLFitAmpComposite::Apply()
{
  // ignore
}

std::pair<Real,Real> LLFitAmpComposite::CalcIntensAt(Real zstar) const
{
  if(fit_list_.empty()) return std::make_pair(0.0,0.0);

  Real sum=0.0;
  Real sum2=0.0;

  for(std::vector<LLFitBasePtr>::const_iterator it=fit_list_.begin();it!=fit_list_.end();++it) {
    std::pair<Real,Real> res = (*it)->CalcIntensAt(zstar);
    sum+=res.first;
    sum2+=res.first*res.first;
  }

  Real iN = 1.0/static_cast<Real>(fit_list_.size());		
  Real imean = sum*iN;
  Real isig = sqrt(sum2*iN-imean*imean);
  return std::make_pair(imean,isig);
}

unsigned int LLFitAmpComposite::GetIterCount() const
{
  if(fit_list_.empty()) return 0;

  int sum=0;

  for(std::vector<LLFitBasePtr>::const_iterator it=fit_list_.begin();it!=fit_list_.end();++it) {
    sum+= (*it)->GetIterCount();
  }

  return static_cast<int>(static_cast<Real>(sum)/static_cast<Real>(fit_list_.size()));
}

std::map<Real,Complex> LLFitAmpComposite::GetComponents() const
{
  std::map<Real,Complex> res;

  if(fit_list_.empty()) return res;

  Real f = 1.0/static_cast<Real>(fit_list_.size());

  for(std::vector<LLFitBasePtr>::const_iterator it=fit_list_.begin();it!=fit_list_.end();++it) {
    std::map<Real,Complex> cc = (*it)->GetComponents();
    for(std::map<Real,Complex>::const_iterator cit=cc.begin();cit!=cc.end();++cit) {
      res[cit->first]+=cit->second;
    }
  }
  for(std::map<Real,Complex>::iterator cit=res.begin();cit!=res.end();++cit) {
    cit->second=cit->second*f;
  }
  return res;
}

void LLFitAmpComposite::Cleanup()
{
  for(std::vector<LLFitBasePtr>::const_iterator it=fit_list_.begin();it!=fit_list_.end();++it) {
    (*it)->Cleanup();
  }
}

void LLFitAmpComposite::Add(const LLFitBasePtr& f)
{
  if(fit_list_.empty()) {
    count_=f->GetCount();
    sampling_=f->GetSampling();
  }
  fit_list_.push_back(f);
}

}} // ns
