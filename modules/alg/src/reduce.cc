//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/units.hh>
#include "reduce.hh"

namespace iplt {  namespace alg {

ReflectionList Reduce::Visit(const ReflectionList& r) const
{
	String phaid=r.GetPropertyNameByType(iplt::PT_PHASE);
 	ReflectionList result;
  	result.CopyProperties(r);
	if(phaid==""){
   	  for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
            SymmetryReflection sr=symmetry_.Reduce(SymmetryReflection(it.GetIndex(),1.0,0.0));
            result.AddReflection(sr.GetReflectionIndex(),it);
  	  }
	}else{
    	  for(ReflectionProxyter it=r.Begin();!it.AtEnd();++it){
            SymmetryReflection sr=symmetry_.Reduce(SymmetryReflection(it.GetIndex(),1.0,it.Get(phaid)));
            ReflectionProxyter rptmp=result.AddReflection(sr.GetReflectionIndex(),it);
            rptmp.Set(phaid,sr.GetPhase());
    	  }
	}
 	return result;
}

}} // ns
