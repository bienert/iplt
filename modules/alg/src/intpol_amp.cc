//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

// DEPRECATED

#include <iostream>
#include <ostream>
#include <sstream>
#include <vector>

#include <gsl/gsl_fit.h>

#include <ost/log.hh>

#include <iplt/reflection.hh>

#include "intpol_amp.hh"

namespace iplt {  namespace alg {

IntpolAmp::IntpolAmp(const ReflectionList& reflist, 
		     const String& frac_l, 
		     const String& frac_l_ee,
		     const String& val,
		     const String& val_ee,
		     Real window):
  hklist_(),
  window_(window)
{
  // assemble the hk list
  try {
    for(ReflectionProxyter rp=reflist.Begin();!rp.AtEnd();++rp) {
      // prepare values of l entry
      //LEntry l_entry(rp.Get(frac_l),rp.Get(frac_l_ee),
      //rp.Get(val),rp.Get(val_ee));
      LEntry l_entry(rp.GetIndex().GetZStar(),1.0,rp.Get(val),rp.Get(val_ee));
      
      // retrieve map entry (create if it not yet exists)
      HKEntry hk_entry(rp.GetIndex().AsDuplet());
      HKList::iterator it = hklist_.find(hk_entry);
      if(it==hklist_.end()) {
	hklist_.insert(HKList::value_type(hk_entry,LList()));
	it = hklist_.find(hk_entry);
      }
      // add l entry to correct l list
      it->second.push_back(l_entry);
    }

    /*
      now that we have the hk list, each l list
      needs to be sorted according to its frac l
    */
    for(HKList::iterator hk_it = hklist_.begin(); hk_it!=hklist_.end(); ++hk_it) {
      hk_it->second.sort();
    }

  } catch (const ReflectionException &e) {
    std::ostringstream msg;
    msg << "caught ReflectionException: " << e.what();
    throw ost::Error(msg.str());
  }
}

Real IntpolAmp::GetInterpolatedValue(const ost::img::Point& hk, Real frac_l)
{
  Real lim1 = frac_l-window_;
  Real lim2 = frac_l+window_;

  HKList::const_iterator hk_it = hklist_.find(HKEntry(hk));

  if(hk_it==hklist_.end()) {
    // no entry found
    // throw error?
    return 0.0;
  }

  std::vector<double> xv;
  std::vector<double> yv;

  int n=0;
  Real l_last=0,a_last=0;
  Real max=0.0;
  Real sum=0.0;
  for(LList::const_iterator it=hk_it->second.begin();it!=hk_it->second.end();++it) {
    if(it->l>=lim1 && it->l<=lim2) {
      max=std::max(max,it->a);
      xv.push_back(it->l);
      yv.push_back(it->a);
      l_last=it->l;
      a_last=it->a;
      sum+=it->a;
      ++n;
    }
  }

  if(l_last< (lim2+lim1)/2) {
    /*
      add a data point at end of window 
      if all the data points were in left
      half of window
      TODO: find better heuristics
    */
    xv.push_back(lim2);
    yv.push_back(a_last);
  }
  if(n==0){
    // return 0 if there are no data points within window
    return 0.0;
  }
  // do linear fit with l and a in fit list
  double c0,c1,cov00,cov01,cov11,sumsq;

  gsl_fit_linear(&xv[0],1,&yv[0],1,n,&c0,&c1,&cov00,&cov01,&cov11,&sumsq);

  // return value at frac_l
  double a_est,a_est_err;
  gsl_fit_linear_est(frac_l,c0,c1,cov00,cov01,cov11,&a_est,&a_est_err);

  if(a_est>max){
    // return mean value if fit was above max
    return sum/n;
  }
  // amp always >=0
  return a_est<0.0? 0.0 : a_est;
}

void IntpolAmp::Dump() const
{
  for(HKList::const_iterator hk_it = hklist_.begin(); hk_it!=hklist_.end(); ++hk_it) {
    std::cerr << hk_it->first.h << " " << hk_it->first.k << ":" << std::endl;
    for(LList::const_iterator l_it = hk_it->second.begin(); l_it!=hk_it->second.end(); ++l_it) {
      std::cerr << " " << l_it->l << " " << l_it->sigma_l << " " << l_it->a << " " << l_it->sigma_a << std::endl;
    }
  }
}


}} // ns
