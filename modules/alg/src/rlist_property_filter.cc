//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/


#include "rlist_property_filter.hh"

namespace iplt { namespace alg {

ReflectionList RListPropertyFilter::Visit(const ReflectionList& in) const
{
  ReflectionList result(in,false);
  if(use_type_){
    String property=in.GetPropertyNameByType(property_type_);
    for(ReflectionProxyter rp=in.Begin();!rp.AtEnd();++rp) {
      Real val=rp.Get(property);
      if(val>=start_ && val<=end_){
        result.AddReflection(rp.GetIndex(),rp);
      }
    }
  }else{
    for(ReflectionProxyter rp=in.Begin();!rp.AtEnd();++rp) {
      Real val=rp.Get(property_);
      if(val>=start_ && val<=end_){
        result.AddReflection(rp.GetIndex(),rp);
      }
    }
  }
  return result;
}


}} //ns
