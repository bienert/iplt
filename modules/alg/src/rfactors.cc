//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Ansgar Philippsen
*/

#include <iostream>

#include <ost/log.hh>

#include "rfactors.hh"
#include "symmetrize.hh"

namespace iplt {  namespace alg {

////////////////////////////////
//   RCalculatorBase

RCalculatorBase::RCalculatorBase(Real zlim,Real rlow, Real rhigh, int bcount):
  zlim_(zlim),
  bcount_(bcount),
  rlow_(rlow),rhigh_(rhigh),
  ir2_low_(0.0), ir2_high_(1.0), ir2_delta_(1.0),
  rf_(1.0),wrf_(1.0),rcount_(0),
  rfbin_(),wrfbin_(),rcount_bin_(0)
{
  if(bcount!=0) {
    if(rlow<=0.0 || rhigh<=0.0 || rlow<=rhigh || bcount<0) {
      throw ost::Error("invalid resolution range in RMergeCalculator");
    }
    
    ir2_low_ = 1.0/(rlow*rlow);
    ir2_high_ = 1.0/(rhigh*rhigh);
    ir2_delta_ = (ir2_high_ - ir2_low_)/static_cast<Real>(bcount);
  }
}

RCalculatorBase::~RCalculatorBase()
{}

ReflectionList RCalculatorBase::Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs)
{
  return data;
}

Real RCalculatorBase::GetRF() const
{
  return rf_;
}

Real RCalculatorBase::GetWRF() const
{
  return wrf_;
}

int RCalculatorBase::GetRCount() const
{
  return rcount_;
}

Real RCalculatorBase::GetRFBin(int n) const
{
  return rfbin_[n];
}

Real RCalculatorBase::GetWRFBin(int n) const
{
  return wrfbin_[n];
}

int RCalculatorBase::GetRCountBin(int n) const
{
  return rcount_bin_[n];
}

Real RCalculatorBase::GetBinLowerResolutionLimit(int n) const
{
  Real nrvo = n>0 ? static_cast<Real>(n)*ir2_delta_+ir2_low_ : ir2_low_;
  nrvo = 1.0/sqrt(nrvo);
  return nrvo;
}



////////////////////////////////
//   RMergeCalculator

RMergeCalculator::RMergeCalculator(const ReflectionList& merge, const String& miobs, const String& msigiobs, Real zlim, Real rlow, Real rhigh, int bcount):
  RCalculatorBase(zlim,rlow,rhigh,bcount),
  merge_(merge),
  miobs_(miobs),
  msigiobs_(msigiobs),
  entry_map_(),
  rej_level_(3.0)
{
  // pre-organize into hash-table for faster lookup
  for(ReflectionProxyter rp=merge.Begin();!rp.AtEnd();++rp) {
    ost::img::Point hk=rp.GetIndex().AsDuplet();
    Real zstar=rp.GetIndex().GetZStar();
    hk[2] = static_cast<int>(floor(zstar/zlim_));
    Real iobs = rp.Get(miobs);
    Real sigiobs = rp.Get(msigiobs);
    Real we=1e-20;
    if(sigiobs>0.0) {
      we=1.0/(sigiobs*sigiobs);
    }
    detail::RMergeEntry e={zstar,iobs,sigiobs,we};
    entry_map_[hk].push_back(e);
  }
}

ReflectionList RMergeCalculator::Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs)
{
  if(bcount_>0) {
    return do_apply<true>(data,diobs,dsigiobs);
  } else {
    return do_apply<false>(data,diobs,dsigiobs);
  }
}

template <bool BIN>
ReflectionList RMergeCalculator::do_apply(const ReflectionList& data, const String& diobs, const String&dsigiobs)
{
  Real zlim2 = zlim_*zlim_;
  
  // for the overall rfactor
  Real rsum1=0.0;
  Real rsum2=0.0;
  Real wrsum1=0.0;
  Real wrsum2=0.0;
  int rcount=0;

  // for the binned rfactors
  std::vector<Real> rsum1_bin(bcount_);
  std::vector<Real> rsum2_bin(bcount_);
  std::vector<Real> wrsum1_bin(bcount_);
  std::vector<Real> wrsum2_bin(bcount_);
  std::vector<int> rcount_bin(bcount_);

  // these are needed here for the templated if code to work
  Real ires2=1.0;
  int bin=0;
  bool valid_bin=false;

  // main loop over all input data reflections
  for(ReflectionProxyter rp=data.Begin();!rp.AtEnd();++rp) {
    ost::img::Point hk=rp.GetIndex().AsDuplet();
    Real resol = rp.GetResolution();
    if(bcount_>0 && (resol<rhigh_ || resol>rlow_)) continue;

    Real iobs = rp.Get(diobs);
    Real sigiobs = rp.Get(dsigiobs);

    Real zstar=rp.GetIndex().GetZStar();
    hk[2] = static_cast<int>(floor(zstar/zlim_));

    if(BIN) {
      ires2 = 1.0/(resol*resol);
      bin = static_cast<int>((ires2-ir2_low_)/ir2_delta_);
      valid_bin = bin>=0 && bin<bcount_;
    }

    ost::img::Point plist[]={Point(0,0,0),ost::img::Point(0,0,1),ost::img::Point(0,0,-1)};

    // local averaging of zwindow prior to summation
    Real sum_icalc = 0.0;
    Real wsum_icalc = 0.0;
    Real sum2_icalc = 0.0;
    Real wsum2_icalc = 0.0;
    Real ncalc = 0.0;
    Real wncalc = 0.0;
    
    for(int pc=0;pc<3;++pc) {
      detail::RMergeEntryMap::const_iterator emit = entry_map_.find(hk+plist[pc]);
      if(emit!=entry_map_.end()) {
#if 1
	detail::RMergeEntryList::const_iterator it=emit->second.begin();
	for(;it!=emit->second.end();++it) {
	  Real dz = it->zstar-zstar;
	  if(dz*dz<=zlim2) break;
	}
	for(;it!=emit->second.end();++it) {
	  Real dz = it->zstar-zstar;
	  if(dz*dz>zlim2) break;
	  sum_icalc+=it->iobs;
	  sum2_icalc+=it->iobs*it->iobs;
	  ncalc+=1.0;
	  wsum_icalc+=it->we*it->iobs;
	  wsum2_icalc+=it->we*it->we*it->iobs*it->iobs;
	  wncalc+=it->we;
	}
#else
	for(detail::RMergeEntryList::const_iterator it=emit->second.begin();it!=emit->second.end();++it) {
	  Real dz = it->zstar-zstar;
	  if(dz*dz<=zlim2) {
	    sum_icalc+=it->iobs;
	    sum2_icalc+=it->iobs*it->iobs;
	    ncalc+=1.0;
	    wsum_icalc+=it->we*it->iobs;
	    wsum2_icalc+=it->we*it->we*it->iobs*it->iobs;
	    wncalc+=it->we;
	  }
	}
#endif
      }
    }
    if(ncalc>0.0) {
      Real icalc=sum_icalc/ncalc;
      Real isdev=sqrt(sum2_icalc/ncalc-icalc*icalc);
      Real wicalc=wsum_icalc/wncalc;
      //Real wisdev=sqrt(wsum2_icalc/ncalc-wicalc*wicalc);
      Real ii1 = std::abs(iobs-icalc);
      if(ii1>rej_level_*isdev) {
	LOG_DEBUG( "info: " << rej_level_ << " sigma outlier: " << rp.GetIndex() << " @" << rp.GetResolution() << "A" );
      }
      
      Real ii2 = std::abs(iobs);
      rsum1+=ii1;
      rsum2+=ii2;
      rcount++;

      if(BIN) {
	if(valid_bin) {
	  rsum1_bin[bin]+=ii1;
	  rsum2_bin[bin]+=ii2;
	  rcount_bin[bin]++;
	}
      }
      if(sigiobs>0.0) {
	Real we=1.0/(sigiobs*sigiobs);
	Real ii3=std::abs(iobs-wicalc);
	wrsum1+=we*ii3;
	wrsum2+=we*ii2;
	if(BIN) {
	  if(valid_bin) {
	    wrsum1_bin[bin]+=we*ii3;
	    wrsum2_bin[bin]+=we*ii2;
	  }
	}
      }
    }
  }

  if(rsum2==0.0 || wrsum2==0.0) {
    rf_=1.0;
    wrf_=1.0;
    rcount_=0;
  } else {
    rf_= rsum1/rsum2;
    wrf_= wrsum1/wrsum2;
    rcount_ = rcount;
  }

  if(BIN) {
    rfbin_.reserve(bcount_);
    wrfbin_.reserve(bcount_);
    rcount_bin_=rcount_bin;

    for(int i=0;i<bcount_;++i) {
      rfbin_[i]=rsum2_bin[i]>0.0 ? rsum1_bin[i]/rsum2_bin[i] : 1.0;
      wrfbin_[i]=wrsum2_bin[i]>0.0 ? wrsum1_bin[i]/wrsum2_bin[i] : 1.0;
    }
  }
  return ReflectionList(data);
}


void RMergeCalculator::SetSigmaRejectionLevel(Real r)
{
  rej_level_=r;
}



////////////////////////////////
//   RSymCalculator

RSymCalculator::RSymCalculator(const Symmetry& sym, Real zlim,Real rlow, Real rhigh, int bcount):
  RCalculatorBase(zlim,rlow,rhigh,bcount),
  sym_(sym)
{}

namespace {

  struct RSymEntry {
    Real iobs,zstar,sigiobs;
    ReflectionIndex index;
    ReflectionIndex symindex;
  };

}

ReflectionList RSymCalculator::Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs)
{
  ReflectionList rlist_sym(data);
  rlist_sym.AddProperty("symh");
  rlist_sym.AddProperty("symk");
  rlist_sym.AddProperty("symzstar");
  for(ReflectionProxyter rp=rlist_sym.Begin();!rp.AtEnd();++rp) {
    rp.Set("symh",static_cast<Real>(rp.GetIndex().GetK()));
    rp.Set("symk",static_cast<Real>(rp.GetIndex().GetK()));
    rp.Set("symzstar",static_cast<Real>(rp.GetIndex().GetZStar()));
  }
  
  Symmetrize syms(sym_);
  rlist_sym.ApplyIP(syms);

  Real sum1=0.0;
  Real sum2=0.0;
  Real wsum1=0.0;
  Real wsum2=0.0;
  int count=0;

  ReflectionProxyter rp=rlist_sym.Begin();
  ost::img::Point hk = rp.GetIndex().AsDuplet();
  while(rp.IsValid()) {
    Real resol=rp.GetResolution();
    if(bcount_>0 && (resol<rhigh_ || resol>rlow_)) {
      ++rp;
      continue;
    }

    std::vector<RSymEntry> tmp_list;
    // collect same values as current hk
    while(rp.GetIndex().AsDuplet()==hk and rp.IsValid()) {
      ReflectionIndex refsym(static_cast<int>(rp.Get("symh")),
			     static_cast<int>(rp.Get("symk")),
			     rp.Get("symzstar"));
      RSymEntry rse= {rp.Get("iobs"),rp.GetIndex().GetZStar(),rp.Get("sigiobs"),rp.GetIndex(),refsym};
      tmp_list.push_back(rse);
      ++rp;
    }
    hk = rp.GetIndex().AsDuplet();
    for(unsigned int i=0;i<tmp_list.size();++i) {
      // make weighted average for this reflection 'i'
      Real tmp_sum=0.0;
      Real tmp_count=0.0;
      Real tmp_count2=0;
      for(unsigned int k=0;k<tmp_list.size();++k) {
	Real dzstar=std::abs(tmp_list[i].zstar-tmp_list[k].zstar);
	if(dzstar<=zlim_) {
	  if (tmp_list[k].sigiobs > 0.0) {
	    Real we = 1.0/(tmp_list[k].sigiobs*tmp_list[k].sigiobs);
	    tmp_count+=we;
	    tmp_sum+=we*tmp_list[k].iobs;
	    if(i!=k && dzstar>1e-5) {
	      // only use this average if at least one non-friedel was found
	      tmp_count2+=1;
	    }
	  }
	}
      }
                    
      // inner sum (iobs - <iobs>) for this particular i
      if(tmp_count2>0) {
	tmp_sum/=tmp_count;
	if(tmp_list[i].sigiobs>0.0) {
	  Real we = 1.0/(tmp_list[i].sigiobs*tmp_list[i].sigiobs);
	  sum1+=std::abs(tmp_list[i].iobs-tmp_sum);
	  sum2+=std::abs(tmp_list[i].iobs);
	  wsum1+=we*std::abs(tmp_list[i].iobs-tmp_sum);
	  wsum2+=we*std::abs(tmp_list[i].iobs);
	  count+=1;
	}
      }
    }
  }

  rf_ = sum2>0.0 ? sum1/sum2 : 1.0;
  wrf_ = wsum2>0.0 ? wsum1/wsum2 : 1.0;
  rcount_ = count;

  rfbin_.reserve(bcount_);
  wrfbin_.reserve(bcount_);
  //rcount_bin_=rcount_bin;

  for(int i=0;i<bcount_;++i) {
    rfbin_[i]=1.0;
    wrfbin_[i]=1.0;
  }

  return ReflectionList(data);
}


////////////////////////////////
//   RFitCalculator

RFitCalculator::RFitCalculator(const ReflectionList& curve, const String& miobs, const String& msigiobs, Real rlow, Real rhigh, int bcount):
  RCalculatorBase(0.0,rlow,rhigh,bcount),
  entry_map_()
{
  ReflectionProxyter rp=curve.Begin();
  while(rp.IsValid()) {
    ost::img::Point hk = rp.GetIndex().AsDuplet();
    Real zmin = rp.GetIndex().GetZStar();
    Real zstar = zmin;
    std::vector<std::pair<Real,Real> > ilist;
    while(rp.GetIndex().AsDuplet()==hk and rp.IsValid()) {
      zstar = rp.GetIndex().GetZStar();
      ilist.push_back(std::make_pair(rp.Get(miobs),rp.Get(msigiobs)));
      ++rp;
    }
    Real zmax = zstar;
    Real zfactor = 1.0;
    if(!ilist.empty() && zmax>zmin) {
      zfactor=static_cast<Real>(ilist.size())/(zmax-zmin);
    }
    detail::RFitEntry rfe={zmin,zmax,zfactor,ilist};
    entry_map_[hk]=rfe;
  }
}

ReflectionList RFitCalculator::Apply(const ReflectionList& data, const String& diobs, const String& dsigiobs)
{
  // for the overall rfactor
  Real rsum1=0.0;
  Real rsum2=0.0;
  Real wrsum1=0.0;
  Real wrsum2=0.0;
  int rcount=0;

  // for the binned rfactors
  std::vector<Real> rsum1_bin(bcount_);
  std::vector<Real> rsum2_bin(bcount_);
  std::vector<Real> wrsum1_bin(bcount_);
  std::vector<Real> wrsum2_bin(bcount_);
  std::vector<int> rcount_bin(bcount_);

  // main loop over all reflections
  for(ReflectionProxyter rp=data.Begin();!rp.AtEnd();++rp) {
    ost::img::Point hk=rp.GetIndex().AsDuplet();
    Real resol=rp.GetResolution();
    if(bcount_>0 && (resol<rhigh_ || resol>rlow_)) continue;

    detail::RFitEntryMap::const_iterator it = entry_map_.find(hk);

    if(it==entry_map_.end()) continue;
    
    const detail::RFitEntry& rfe = it->second;
    Real zstar=rp.GetIndex().GetZStar();

    if(zstar<rfe.zmin || zstar>rfe.zmax) continue;

    int zi = static_cast<int>((zstar-rfe.zmin)*rfe.zfactor);

    if(zi>static_cast<int>(rfe.ilist.size())) continue;

    Real iobs = rp.Get(diobs);
    Real sigiobs = rp.Get(dsigiobs);
    Real icalc = rfe.ilist[zi].first;
    Real sigicalc = rfe.ilist[zi].second;

    Real ii1 = std::abs(iobs-icalc);
    Real ii2 = std::abs(iobs);
    
    rsum1+=ii1;
    rsum2+=ii2;
    rcount++;

    Real ires2 = 1.0/(rp.GetResolution()*rp.GetResolution());
    int bin = static_cast<int>((ires2-ir2_low_)/ir2_delta_);
    if(bin>=0 && bin<bcount_) {
      rsum1_bin[bin]+=ii1;
      rsum2_bin[bin]+=ii2;
      rcount_bin[bin]++;
    }

    if(sigiobs>0.0 || sigicalc>0.0) {
      Real we=1.0/(sigiobs*sigiobs+sigicalc*sigicalc);
      wrsum1+=we*ii1;
      wrsum2+=we*ii2;
      if(bin>=0 && bin<bcount_) {
	wrsum1_bin[bin]+=we*ii1;
	wrsum2_bin[bin]+=we*ii2;
      }
    }
        
  }

  if(rcount==0) {
    rf_=1.0;
    wrf_=1.0;
  } else {
    rf_= rsum1/rsum2;
    wrf_= wrsum1/wrsum2;
  }

  rcount_ = rcount;

  rfbin_.reserve(bcount_);
  wrfbin_.reserve(bcount_);
  rcount_bin_=rcount_bin;
  
  for(int i=0;i<bcount_;++i) {
    rfbin_[i]=rsum2_bin[i]>0.0 ? rsum1_bin[i]/rsum2_bin[i] : 1.0;
    wrfbin_[i]=wrsum2_bin[i]>0.0 ? wrsum1_bin[i]/wrsum2_bin[i] : 1.0;
  }

  return ReflectionList(data);
}

////////////////////////////////
//   RCalcCalculator
RCalcCalculator::RCalcCalculator(const String& micalc, const String& msigicalc, 
                                 Real rlow, Real rhigh, int bcount):
  RCalculatorBase(0.0,rlow,rhigh,bcount),
  micalc_(micalc),
  msigicalc_(msigicalc)
{}

ReflectionList  RCalcCalculator::Apply(const ReflectionList& data, 
                                       const String& diobs, const String& dsigiobs)
{
  // for the overall rfactor
  Real rsum1=0.0;
  Real rsum2=0.0;
  Real wrsum1=0.0;
  Real wrsum2=0.0;
  int rcount=0;

  // for the binned rfactors
  std::vector<Real> rsum1_bin(bcount_);
  std::vector<Real> rsum2_bin(bcount_);
  std::vector<Real> wrsum1_bin(bcount_);
  std::vector<Real> wrsum2_bin(bcount_);
  std::vector<int> rcount_bin(bcount_);

  for(ReflectionProxyter rp=data.Begin();!rp.AtEnd();++rp) {
    ost::img::Point hk=rp.GetIndex().AsDuplet();
    Real resol=rp.GetResolution();
    if(bcount_>0 && (resol<rhigh_ || resol>rlow_)) continue;

    Real iobs = rp.Get(diobs);
    Real sigiobs = rp.Get(dsigiobs);
    Real icalc = rp.Get(micalc_);
    Real sigicalc = rp.Get(msigicalc_);

    Real ii1 = std::abs(iobs-icalc);
    Real ii2 = std::abs(iobs);
    
    rsum1+=ii1;
    rsum2+=ii2;
    ++rcount;

    Real ires2 = 1.0/(resol*resol);
    int bin = static_cast<int>((ires2-ir2_low_)/ir2_delta_);
    if(bin>=0 && bin<bcount_) {
      rsum1_bin[bin]+=ii1;
      rsum2_bin[bin]+=ii2;
      rcount_bin[bin]++;
    }

    if(sigiobs>0.0 || sigicalc>0.0) {
      Real we=1.0/(sigiobs*sigiobs+sigicalc*sigicalc);
      wrsum1+=we*ii1;
      wrsum2+=we*ii2;
      if(bin>=0 && bin<bcount_) {
	wrsum1_bin[bin]+=we*ii1;
	wrsum2_bin[bin]+=we*ii2;
      }
    }
  }

  if(rcount==0) {
    rf_=1.0;
    wrf_=1.0;
  } else {
    rf_= rsum1/rsum2;
    wrf_= wrsum1/wrsum2;
  }

  rcount_ = rcount;

  rfbin_.reserve(bcount_);
  wrfbin_.reserve(bcount_);
  rcount_bin_=rcount_bin;
  
  for(int i=0;i<bcount_;++i) {
    rfbin_[i]=rsum2_bin[i]>0.0 ? rsum1_bin[i]/rsum2_bin[i] : 1.0;
    wrfbin_[i]=wrsum2_bin[i]>0.0 ? wrsum1_bin[i]/wrsum2_bin[i] : 1.0;
  }

  return ReflectionList(data);
  

  
}

  

}} // ns
