
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#include "rlist_index_stat.hh"

namespace iplt {  namespace alg{

RListIndexStat::RListIndexStat():
  ReflectionNonModAlgorithm(),
  min_h_(),
  max_h_(),
  min_k_(),
  max_k_(),
  min_zstar_(),
  max_zstar_(),
  indexes_()
{
}

RListIndexStat::~RListIndexStat()
{
}

void RListIndexStat::Visit(const ReflectionList& in)
{
	if(in.NumReflections()==0)
		throw(ReflectionException("no reflections found"));
	indexes_.clear();
	ReflectionProxyter rp=in.Begin();
	ReflectionIndex idx=rp.GetIndex();
  min_h_=idx.GetH();
  min_k_=idx.GetK();
  max_k_=idx.GetK();
  min_zstar_=idx.GetZStar();
  max_zstar_=idx.GetZStar();
  indexes_.insert(idx.AsDuplet());
  
  // this algorithm exploits the fact that the reflections in the ReflectionList are sorted according to their index
  ReflectionIndex current_idx(idx);
  while(!rp.AtEnd()){
    current_idx=rp.GetIndex();
    min_k_=std::min<Real>(min_k_,current_idx.GetK());
    while(!rp.AtEnd() && rp.GetIndex().GetH()==current_idx.GetH()){
      current_idx=rp.GetIndex();
      min_zstar_=std::min<Real>(min_zstar_,current_idx.GetZStar());
      indexes_.insert(current_idx.AsDuplet());
      while(!rp.AtEnd() && rp.GetIndex().GetK()==current_idx.GetK()){
        current_idx=rp.GetIndex();
        ++rp; 
      }
      max_zstar_=std::max<Real>(max_zstar_,current_idx.GetZStar());
    }
    max_k_=std::max<Real>(max_k_,current_idx.GetK());
  }
  max_h_=current_idx.GetH();
}

int RListIndexStat::GetMinimumH()
{
	return min_h_;
}
int RListIndexStat::GetMinimumK()
{
	return min_k_;
}
int RListIndexStat::GetMaximumH()
{
	return max_h_;
}
int RListIndexStat::GetMaximumK()
{
	return max_k_;
}
Real RListIndexStat::GetMinimumZStar()
{
	return min_zstar_;
}
Real RListIndexStat::GetMaximumZStar()
{
	return max_zstar_;
}

PointList RListIndexStat::GetIndexList()
{
  // convert to ost::img::PointList to avoid Python wrapper for PointSet
  ost::img::PointList result;
  for(PointSet::const_iterator it=indexes_.begin();it!=indexes_.end();++it){
    result.push_back(*it);
  }
  return result;
}


}} //ns
