//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Author: Andreas Schenk
*/

#include <ost/img/image.hh>
#include <iplt/alg/expand.hh>
#include <ost/log.hh>

#include "create_image.hh"

namespace iplt {  namespace alg {
	
ImageHandle CreateImageFromReflectionList(const ::iplt::ReflectionList& in, unsigned int nr_of_unit_cells, unsigned int dimensions)
{
  if(nr_of_unit_cells==0){
    nr_of_unit_cells=1;
  }
  ReflectionList rlist=in.Apply(Expand());
  String phaseid=rlist.GetPropertyNameByType(iplt::PT_PHASE);
  String ampid=rlist.GetPropertyNameByType(iplt::PT_AMPLITUDE);
  SpatialUnitCell uc=rlist.GetUnitCell();
  Real uc_c=uc.GetC();
  ost::img::ImageHandle image;
  if(dimensions==1){
    image=CreateImage(ost::img::Extent(ost::img::Size(int(round(uc.GetA()*nr_of_unit_cells)),1,1),ost::img::Point(0,0,0)),ost::img::COMPLEX,HALF_FREQUENCY);
  }else if(dimensions==2){
    image=CreateImage(ost::img::Extent(ost::img::Size(int(round(uc.GetA()*nr_of_unit_cells)),int(round(uc.GetB()*nr_of_unit_cells))),ost::img::Point(0,0,0)),ost::img::COMPLEX,HALF_FREQUENCY);
  }else{
    image=CreateImage(ost::img::Extent(ost::img::Size( int(round(uc.GetA()*nr_of_unit_cells)),int(round(uc.GetB()*nr_of_unit_cells)),int(round(uc.GetC()*nr_of_unit_cells))),ost::img::Point(0,0,0)),ost::img::COMPLEX,HALF_FREQUENCY);
  }
  if(rlist.HasPropertyType(iplt::PT_WEIGHT)){
    String weightid=rlist.GetPropertyNameByType(iplt::PT_WEIGHT);
    for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp){
      ReflectionIndex idx=rp.GetIndex();
      if(idx==ReflectionIndex(0,0,0.0)){
        continue;
      }
      Real fobs=rp.Get(ampid);
      Real phiobs=rp.Get(phaseid);
      Real weight=rp.Get(weightid);
      Complex cval=std::polar(fobs,phiobs);
      image.SetComplex(ost::img::Point(idx.GetH()*nr_of_unit_cells,idx.GetK()*nr_of_unit_cells,idx.GetZStar()*uc_c*nr_of_unit_cells),cval*weight);
    }
  }else{
    for(ReflectionProxyter rp=rlist.Begin();!rp.AtEnd();++rp){
      ReflectionIndex idx=rp.GetIndex();
      if(idx==ReflectionIndex(0,0,0.0)){
        continue;
      }
      Real fobs=rp.Get(ampid);
      Real phiobs=rp.Get(phaseid);
      Complex cval=std::polar(fobs,phiobs);
      image.SetComplex(ost::img::Point(idx.GetH()*nr_of_unit_cells,idx.GetK()*nr_of_unit_cells,idx.GetZStar()*uc_c*nr_of_unit_cells),cval);
    }
  }
  return image;
}
  
}} //ns
