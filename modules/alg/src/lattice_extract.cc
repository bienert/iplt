//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  extract values from image based on 2D lattice

  Authors: Ansgar Philippsen, Andreas Schenk, Giani Signorell
*/

#include <cstdlib>
#include <iostream>
#include <sstream>

#include <ost/log.hh>

#include <ost/info/info.hh>

#include "lattice_extract.hh"
#include "peak_integration.hh"

namespace iplt {  namespace alg {

using ::iplt::alg::FitGauss2DError;

LatticeExtract::LatticeExtract(const Lattice& lat, int size_start, int size_end, bool dynamic_fitting, bool pre_check):
    NonModAlgorithm("LatticeExtract"),
    lattice_(lat),
    size_start_(size_start),
    size_end_(size_end),
    dynamic_(dynamic_fitting),
    pre_check_(pre_check),
    lpl_(lattice_),
    corr_img_(CreateImage(ost::img::Size(1)))
{}

void LatticeExtract::Visit(const ost::img::Function& fnc)
{
  ost::img::ImageHandle tmp = GenerateImage(fnc);
  Visit(tmp);
}

void LatticeExtract::Visit(const ost::img::ConstImageHandle& im)
{
  LOG_DEBUG( "creating predicted lattice point list" );
  lpl_ = PredictLatticePoints(lattice_, im.GetExtent());

  LOG_DEBUG( "creating subimg handle" );
  ost::img::ImageHandle subimg;

  LOG_DEBUG( "reseting corrected image" );
  corr_img_.Reset(im.GetExtent(),ost::img::REAL,ost::img::SPATIAL);

  int fit_radius = size_end_+1;

  /* 
     iterate over all predicted points, fit them, and assign
     resulting params
  */
  LOG_DEBUG( "commencing lpl iteration" );
  for(LatticePointProxyter lpp=lpl_.Begin(); !lpp.AtEnd(); ++lpp) {
    try {
      ost::img::Point index(lpp.GetIndex());
      LatticePoint& lp=lpp.LPoint();

      geom::Vec2 pos(lp.GetPosition());
      ost::img::Point ipos(pos);
      LOG_DEBUG( "fitting " << ipos << " i=(" << index[0]<< "," << index[1]<< ") " );

      if(!pi_check(im,ipos)) {
	LOG_DEBUG( "ignored due to pi_check" );
	continue;
      }

      ost::img::Point sub_offset(fit_radius,fit_radius);

      ost::img::Extent sub_ext(ipos-sub_offset, ipos+sub_offset);
      lp.SetRegion(sub_ext);
      subimg = im.Extract(sub_ext);

      alg::Stat stat;
      subimg.Apply(stat);
      lp.SetStat(stat);
      LOG_DEBUG( "stat: " << stat );
      
      Real b0 = 0.25*Real(fit_radius)/sqrt(log(2.0));
      Real A = stat.GetMean()-stat.GetMinimum();
      if(A<=0.0) {
	LOG_DEBUG( "ignored due to: gf A <= 0.0" );
	continue;
      }

      // run gaussian fit on reflection
      alg::FitGauss2D gauss2d_fit(stat.GetMaximum()-stat.GetMinimum(), // A
				  b0, // Bx
				  b0, // By
				  stat.GetMinimum(), // C
				  pos[0], // Ux
				  pos[1], // Uy
				  0.0 // w
				  );
      gauss2d_fit.SetMaxIter(100); // TODO fix hardcoded value 
      gauss2d_fit.SetLim(1e-12,1e-12); // TODO fix hardcoded value
      gauss2d_fit.SetSigma(stat.GetStandardDeviation() );
      LOG_DEBUG( "pre: " << gauss2d_fit );
      subimg.Apply(gauss2d_fit);
      LOG_DEBUG( "fit: " << gauss2d_fit );

      lp.SetFit(gauss2d_fit);

      int ssize=dynamic_ ? size_start_ : size_end_;
      int esize=size_end_;
      int lsize=dynamic_ ? 2 : 0;

      if(dynamic_) {
	LOG_DEBUG( "running dynamic peak extraction" );
      } else {
	// extract intensity using fixed extraction radius
	LOG_DEBUG( "running static peak extraction" );
      }

      PeakIntegration pi(ssize,esize,lsize);
      subimg.Apply(pi);
      lp.SetPI(pi);


      // cut out profile box,
      // bg correct, and assemble into corrected image
      ost::img::Point sub_offset2(pi.GetFinalSize(),pi.GetFinalSize());
      ost::img::Extent subext2=Extent(ipos-sub_offset2, ipos+sub_offset2);
      ost::img::ImageHandle subimg2 = im.Extract(subext2);
      for(ost::img::ExtentIterator subit2(subext2);!subit2.AtEnd();++subit2) {
	Vec2 pvec=Point(ost::img::Point(subit2)-ipos).ToVec2();
	Real bgval=pvec[0]*gauss2d_fit.GetPx()+pvec[1]*gauss2d_fit.GetPy()+gauss2d_fit.GetC();
	subimg2.SetReal(subit2,subimg2.GetReal(subit2)-bgval);
      }
      corr_img_.Paste(subimg2);

    } catch (FitGauss2DError& e) {
      LOG_DEBUG( "ignored due to: " << e.what() );
    }
  }
}

LatticePointList LatticeExtract::GetList() const
{
  return lpl_;
}

ImageHandle LatticeExtract::GetCorrectedImage() const
{
  return corr_img_;
}

bool LatticeExtract::pi_check(const ost::img::ConstImageHandle& im, const ost::img::Point& p)
{
  int fit_radius=size_end_+1;

  ost::img::Point maxpoint=p;

  if(pre_check_) {
    LOG_DEBUG( "running pi check with pre fitting" );
    // find highest point in region around predicted pos
    ost::img::Extent subext(ost::img::Size(11,11),p);
    ost::img::ExtentIterator it(subext);
    Real maxval=im.GetReal(it);
    maxpoint=Point(it);
    ++it;
    for(;!it.AtEnd();++it) {
      Real val=im.GetReal(it);
      if(val>maxval) {
	maxval=val;
	maxpoint=Point(it);
      }
    }
  } else {
    LOG_DEBUG( "running pi check" );
  }

  ost::img::ImageHandle subimg=im.Extract(ost::img::Extent(ost::img::Size(fit_radius*2+1,fit_radius*2+1),maxpoint));
  PeakIntegration pi(2,fit_radius-1,2);
  subimg.Apply(pi);
  return pi.GetQuality()>=1.0;
}

void UpdateFromInfo(LatticeExtract& le, const ost::info::InfoGroup& g)
{
  if(g.HasItem("FitSize")) {
    int v=g.GetItem("FitSize").AsInt();
    LOG_INFO( "setting le end fit size to " << v );
    le.SetFitSize(le.GetStartSize(),v);
  }
  if(g.HasItem("FitSizeStart")) {
    int v=g.GetItem("FitSizeStart").AsInt();
    LOG_INFO( "setting le start fit size to " << v );
    le.SetFitSize(v,le.GetEndSize());
  }
  if(g.HasItem("FitSizeEnd")) {
    int v=g.GetItem("FitSizeEnd").AsInt();
    LOG_INFO( "setting le end fit size to " << v );
    le.SetFitSize(le.GetStartSize(),v);
  }
  if(g.HasItem("Mode")) {
    if(g.GetItem("Mode").GetValue()=="dynamic") {
      LOG_INFO( "setting le dynamic mode" );
      le.SetDynamicMode(true);
    } else {
      LOG_INFO( "setting le static mode" );
      le.SetDynamicMode(false);
    }
  }
}


}} // ns

