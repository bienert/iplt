//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  authors: Andreas Schenk
*/


#ifndef IPLT_EX_ALG_LATTICE_FILTERS_H
#define IPLT_EX_ALG_LATTICE_FILTERS_H

#include <ost/img/algorithm.hh>
#include <ost/img/image_state.hh>
#include <iplt/lattice.hh>
#include <iplt/alg/module_config.hh>


namespace iplt {  namespace alg { 


//! Lattice Filter 
class DLLEXPORT_IPLT_ALG LatticeFilter : public ost::img::ConstModIPAlgorithm
{
  public:

    LatticeFilter(Lattice lattice,unsigned int size):
      ost::img::ConstModIPAlgorithm("LatticeFilter"),
      lattice_(lattice),
      size_(size)
    {}         
    virtual void Visit(ost::img::ImageHandle& ih) const ;
  private:
    Lattice lattice_;
    unsigned int size_;
};
//! Gaussian Lattice Filter 
class DLLEXPORT_IPLT_ALG GaussianLatticeFilter : public ost::img::ConstModIPAlgorithm
{
  public:

    GaussianLatticeFilter(Lattice lattice,unsigned int size):
      ost::img::ConstModIPAlgorithm("GaussianLatticeFilter"),
      lattice_(lattice),
      size_(size)
    {}         
    virtual void Visit(ost::img::ImageHandle& ih) const ;
  private:
    Lattice lattice_;
    unsigned int size_;
};
}} // ns

#endif 
