
//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  author: Andreas Schenk
*/

#ifndef IPLT_EX_ALG_RLIST_INDEX_STAT_H
#define IPLT_EX_ALG_RLIST_INDEX_STAT_H

#include <iplt/reflection_list.hh>
#include <iplt/reflection_algorithm.hh>
#include <iplt/alg/module_config.hh>
#include <set>

namespace iplt {  namespace alg{

class DLLEXPORT_IPLT_ALG RListIndexStat: public ReflectionNonModAlgorithm {
  typedef std::set<Point> PointSet;
public:
	RListIndexStat();
	virtual ~RListIndexStat();
	virtual void Visit(const ReflectionList&);
	int GetMinimumH();
	int GetMinimumK();
	int GetMaximumH();
	int GetMaximumK();
	Real GetMinimumZStar();
	Real GetMaximumZStar();
	PointList GetIndexList();
protected:
	int min_h_;
	int max_h_;
	int min_k_;
	int max_k_;
	Real min_zstar_;
	Real max_zstar_;
	PointSet indexes_;
};



}} //ns

#endif
