//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Simon Berneche, Andreas Schenk
*/

#include <algorithm>
#include <boost/format.hpp>

#include <QCheckBox>
#include <QGroupBox>

#include <ost/geom/geom.hh>

#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include <ost/gui/data_viewer/strategies.hh>

#include <iplt/reflection_index.hh>
#include <iplt/alg/lattice_point.hh>
#include <iplt/alg/tilter.hh>

#include "rlist_overlay.hh"

namespace iplt  {  namespace gui {

using namespace ::iplt::alg;


RListOverlaySettings::RListOverlaySettings(const QColor& ac, const QColor& pc, int ssize, int sstr, 
                                           bool rl, bool sl, const QColor& c1, const QColor& c2, const QColor& c3,
                                           const std::vector<double>& rlim_l,
                                           QWidget* p):
  ost::img::gui::PointlistOverlayBaseSettings(ac,pc,ssize,sstr,p),
  res_lim(rl),
  sym_rel(sl),
  color1(c1),
  color2(c2),
  color3(c3),
  rlim_list_(rlim_l)
{
  QPixmap pm(32,32);
  pm.fill(color1);
  c1_b_=new QPushButton(QIcon(pm),"+z");
  connect(c1_b_,SIGNAL(pressed()), this, SLOT(OnColor1()));

  pm.fill(color2);
  c2_b_=new QPushButton(QIcon(pm),"-z");
  connect(c2_b_,SIGNAL(pressed()), this, SLOT(OnColor2()));

  pm.fill(color3);
  c3_b_=new QPushButton(QIcon(pm),"Res Rings");
  connect(c3_b_,SIGNAL(pressed()), this, SLOT(OnColor3()));

  QHBoxLayout* hb = new QHBoxLayout;
  hb->addWidget(c1_b_);
  hb->addWidget(c2_b_);
  hb->addWidget(c3_b_);

  rlim_edit_=new QLineEdit(this);
  rlim_edit_->setValidator(new QDoubleValidator(0.0,100.0,2,rlim_edit_));
  rlim_widget_=new QListWidget(this);
  for(std::vector<double>::const_iterator it=rlim_list_.begin();it!=rlim_list_.end();++it){
    new QListWidgetItem(QString::number(*it), rlim_widget_);
  }
  QPushButton* b=new QPushButton("Add Resolution Ring");
  connect(b,SIGNAL(pressed()), this, SLOT(OnLimAdd()));
  QPushButton* b2=new QPushButton("Remove Resolution Ring");
  connect(b2,SIGNAL(pressed()), this, SLOT(OnLimRemove()));
  
  
  QVBoxLayout* vb = new QVBoxLayout;
  QHBoxLayout* hb2 = new QHBoxLayout;
  QVBoxLayout* vb2 = new QVBoxLayout;
  QVBoxLayout* vb3 = new QVBoxLayout;
  vb3->addWidget(rlim_edit_);
  vb3->addWidget(rlim_widget_);
  hb2->addLayout(vb3);
  vb2->addWidget(b);
  vb2->addWidget(b2);
  hb2->addLayout(vb2);

  QCheckBox* cb1 = new QCheckBox("Display Resolution Limits");
  cb1->setChecked(res_lim);
  connect(cb1,SIGNAL(stateChanged(int)),this,SLOT(OnResLim(int)));
  QCheckBox* cb2 = new QCheckBox("Hilight Symmetry Related ost::img::Points");
  cb2->setChecked(sym_rel);
  connect(cb2,SIGNAL(stateChanged(int)),this,SLOT(OnSymRel(int)));

  vb->addLayout(hb);
  vb->addLayout(hb2);
  vb->addWidget(cb1);
  vb->addWidget(cb2);
  QGroupBox* gb = new QGroupBox("RList Overlay",this);
  gb->setLayout(vb);
  main_layout_->addWidget(gb);
}
      
void RListOverlaySettings::OnResLim(int v) 
{
  res_lim = (v!=0);
}

void RListOverlaySettings::OnSymRel(int v) 
{
  sym_rel = (v!=0);
}

void RListOverlaySettings::OnColor1()
{
  QColor col=QColorDialog::getColor(color1); 
  if(col.isValid()) {
    color1=col;
    QPixmap pm(32,32);
    pm.fill(col);
    c1_b_->setIcon(QIcon(pm));
  }
}

void RListOverlaySettings::OnColor2()
{
  QColor col=QColorDialog::getColor(color2); 
  if(col.isValid()) {
    color2=col;
    QPixmap pm(32,32);
    pm.fill(col);
    c2_b_->setIcon(QIcon(pm));
  }
}

void RListOverlaySettings::OnColor3()
{
  QColor col=QColorDialog::getColor(color3); 
  if(col.isValid()) {
    color3=col;
    QPixmap pm(32,32);
    pm.fill(col);
    c3_b_->setIcon(QIcon(pm));
  }
}
void RListOverlaySettings::OnLimAdd()
{
  new QListWidgetItem(rlim_edit_->text(), rlim_widget_);
  rlim_list_.push_back(rlim_edit_->text().toDouble());
}

void RListOverlaySettings::OnLimRemove()
{
  double val=rlim_widget_->currentItem()->text().toDouble();
  delete rlim_widget_->currentItem();
  std::vector<double>::iterator pos=std::find(rlim_list_.begin(),rlim_list_.end(),val);
  if(pos!=rlim_list_.end()){
    rlim_list_.erase(pos);
  }
}

/////////////////////////////

RListOverlay::RListOverlay(const ReflectionList& rlist, const String& name):
  LatticeOverlayBase(name,rlist.GetLattice()),
  rlist_(rlist),
  color1_(QColor(QColor(0,255,0))),
  color2_(QColor(QColor(255,255,0))),
  color3_(QColor(QColor(32,147,172))),
  res_limit_flag_(false),
  rlim_list_(),
  sym_points_flag_(true),
  sym_point_list_flag_(false),
  sym_point_list_()
{
  menu_->setTitle(QString::fromStdString(name));

  // use unit cell from reflection list
  // this is missing the symmetry, however!
  SetReferenceCell(rlist_.GetUnitCell());

}

void RListOverlay::OnMenuEvent(QAction* e)
{
  if(e==a_settings_) {
    RListOverlaySettings set(active_color_,passive_color_,symbolsize_,symbolstrength_,
                             res_limit_flag_,sym_points_flag_,color1_,color2_,color3_,
                             rlim_list_,
                             e->parentWidget());
    if(set.exec()==QDialog::Accepted) {
      SetProps(&set);
      res_limit_flag_=set.res_lim;
      sym_points_flag_=set.sym_rel;
      rlim_list_=set.rlim_list_;
      color1_=set.color1;
      color2_=set.color2;
      color3_=set.color3;
    }
  } else {
    LatticeOverlayBase::OnMenuEvent(e);
  }
}

void RListOverlay::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  LatticeOverlayBase::OnDraw(pnt,dvp,is_active);
  if(is_active){
    std::vector<QPoint> pointlist_p, pointlist_n;	
    for (ReflectionProxyter rp=rlist_.Begin();!rp.AtEnd();++rp) {
      QPoint qp=dvp->FracPointToWinCenter(GetLattice().CalcPosition(rp.GetIndex().AsDuplet()));
      if(dvp->IsWithin(qp)) {
	if(rp.GetIndex().GetZStar()<0.0) {
	  pointlist_n.push_back(qp);
	} else {
	  pointlist_p.push_back(qp);
	}
      }
    }
    strategy_->SetSymbolSize(static_cast<int>((GetSymbolSize()-0.5) * dvp->GetZoomScale()));
    strategy_->SetPenColor(color1_);
    for(unsigned int i = 0; i < pointlist_p.size(); i++) {
      strategy_->Draw(pnt,pointlist_p[i]);
    }
    strategy_->SetPenColor(color2_);
    for(unsigned int i = 0; i < pointlist_n.size(); i++) {
      strategy_->Draw(pnt,pointlist_n[i]);
    }

    if(res_limit_flag_){
      DrawResolutionCircles(pnt,dvp,color3_);
    }

    if(sym_points_flag_ && sym_point_list_flag_) {
      DrawSymPoints(pnt,dvp);
    }
  }
}

void RListOverlay::HighlightSymmetryRelatedPoints(const ost::img::Point& index)
{
    ReflectionProxyter rp=rlist_.FindFirst(index);
    sym_point_list_=GetReferenceCell().GetSymmetry().GenerateSymmetryRelatedPoints(rp.GetIndex());
    sym_point_list_flag_=true;
}

bool RListOverlay::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  geom::Vec2 comp = GetLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(e->pos())));
  int h=static_cast<int>(round(comp[0]));
  int k=static_cast<int>(round(comp[1]));
  //  dvp->SetCustomStatus2(wxString::Format(wxT("(%d,%d)"),h,k));

  if(e->buttons()==Qt::LeftButton) {
    sym_point_list_flag_=false;
  }

  std::ostringstream itxt;

  if(HasTilt()) {
    itxt << boost::format("Tilt Geometry:\n Tilt Angle: %5.2f\n X Axis Angle: %5.2f\n\n") % 
      (GetTiltGeometry().GetTiltAngle()*180.0/M_PI) %
      (GetTiltGeometry().GetXAxisAngle()*180.0/M_PI);
    itxt << boost::format("Reciprocal Sampling: %g\n\n") % GetReciprocalSampling();
  }

  if(OnLattice(e->pos(),dvp)){
  
    ReflectionProxyter rp=rlist_.FindFirst(ost::img::Point(h,k));
    if(rp.IsValid()) {
      itxt << boost::format("Index: (%d,%d,%f) %fA\n") %
        rp.GetIndex().GetH() %
        rp.GetIndex().GetK() %
        rp.GetIndex().GetZStar() %
        rp.GetResolution();
      for(unsigned int i=0;i<rlist_.GetPropertyCount();++i) {
        String pname = rlist_.GetPropertyName(i);
	itxt << boost::format("%s (%d): %f\n") %
          pname % rlist_.GetPropertyType(i)% rp.Get(pname);
      }

      if(e->buttons()==Qt::LeftButton) {
	sym_point_list_=GetReferenceCell().GetSymmetry().GenerateSymmetryRelatedPoints(rp.GetIndex());
	sym_point_list_flag_=true;
      }
    }
  }

  emit InfoTextChanged(QString(itxt.str().c_str()));

  return false;
}

bool RListOverlay::OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp)
{
  return LatticeOverlayBase::OnKeyEvent(e,dvp);
}

ReflectionList RListOverlay::GetReflectionList() const
{
  return rlist_;
}

void RListOverlay::DrawResolutionCircles(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const QColor& color)
{
  double sampling_x = dvp->GetPixelSampling()[0];
  double sampling_y = dvp->GetPixelSampling()[1];
  
  pnt.setPen(color);
  pnt.setBrush(Qt::NoBrush);
  
  geom::Vec2 off=GetLattice().GetOffset();
  QPoint center = dvp->FracPointToWinCenter(off);
  
  for(std::vector<double>::const_iterator it=rlim_list_.begin();it!=rlim_list_.end();++it) {
    int radius_x = static_cast<int>(1.0/((*it)*sampling_x)*dvp->GetZoomScale());
    int radius_y = static_cast<int>(1.0/((*it)*sampling_y)*dvp->GetZoomScale());
    
    pnt.drawEllipse(center,radius_x,radius_y);
    pnt.drawText(center.x()+radius_x+10,center.y(),QString("%1").arg(*it,0,'f',2));
  }
}

void RListOverlay::DrawSymPoints(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp)
{
  int size=static_cast<int>((static_cast<double>(GetSymbolSize())-0.5) * dvp->GetZoomScale());
  for(std::vector<ReflectionIndex>::const_iterator it=sym_point_list_.begin();
      it!=sym_point_list_.end();++it) {
    QPoint qp=dvp->FracPointToWinCenter(GetLattice().CalcPosition(it->AsDuplet()));
    strategy_->SetPenColor(active_color_);
    strategy_->SetSymbolSize(size+1);
    strategy_->Draw(pnt,qp);
    strategy_->SetSymbolSize(size+2);
    strategy_->Draw(pnt,qp);
  }
}

void RListOverlay::OnReIndex(const Mat2& mat)
{
  //LOG_VERBOSE( "ReIndexing reflection list with " << mat );
  rlist_=ReIndex(rlist_,mat);
  rlist_.SetLattice(GetLattice());
  rlist_=rlist_.Apply(alg::Tilter());
}

void RListOverlay::AddResolutionRing(double value)
{
  rlim_list_.push_back(value);
}

void RListOverlay::RemoveResolutionRing(double value)
{
	std::vector<double>::iterator pos=find(rlim_list_.begin(),rlim_list_.end(),value);
	if(pos!=rlim_list_.end()){
  		rlim_list_.erase(pos);
	}
}

void RListOverlay::DisplayResolutionRings(bool flag)
{
  res_limit_flag_=flag;
}

}}//ns
