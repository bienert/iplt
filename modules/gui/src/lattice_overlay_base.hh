//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk, Johan Hebert
*/

#ifndef IPLT_LATTICE_OVERLAY_BASE_HH
#define IPLT_LATTICE_OVERLAY_BASE_HH

#include <QAction>

#include <ost/gui/data_viewer/pointlist_overlay_base.hh>
#include <iplt/lattice.hh>
#include <iplt/unit_cell.hh>
#include <iplt/tilt.hh>
#include <iplt/gui/module_config.hh>

namespace iplt  {  namespace gui {


// TODO: spatial and reciprocal lattice differentiation	
        class DLLEXPORT_IPLT_GUI LatticeOverlayBase: public ost::img::gui::PointlistOverlayBase
{
public:
  LatticeOverlayBase(const String& name, const Lattice& lat);

  virtual void OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active);

  virtual bool OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse);

  virtual bool OnKeyEvent(QKeyEvent* e,  ost::img::gui::DataViewerPanel* dvp);
  virtual void OnMenuEvent(QAction* e);

  void SetLattice(const Lattice& l);
  const Lattice& GetLattice() const;

  void SetReferenceCell(const SpatialUnitCell& s);

  SpatialUnitCell GetReferenceCell() const;

protected:

  LatticeTiltGeometry GetTiltGeometry() const;
  LatticeTiltGeometry GetEditTiltGeometry() const;

  bool HasTilt() const;

  bool OnLattice(const QPoint& p,  ost::img::gui::DataViewerPanel* dvp);

  // generate re-indexing matrix to transform pa -> pb
  Mat2 GenReIndexMat(const ost::img::Point& pa, const ost::img::Point& pb);

  std::pair<Mat2,Lattice> PrepReIndex(const Lattice&, int keycode, const ost::img::Point& ref);

  // draw lattice in given color
  void DrawLattice(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const Lattice& lattice, const QColor& color, const QColor& axes_color);

  // draw tilt in given color
  void DrawTilt(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const Lattice& lattice, const TiltGeometry& tg, const QColor& color);

  void DrawArrow(QPainter& pnt, QPoint& p0, QPoint& p1, double l,double w);

  void SetEditFlag(bool f);
  bool GetEditFlag() const;
  void SetEditLattice(const Lattice& l);
  Lattice GetEditLattice() const;

  virtual void OnReIndex(const Mat2& mat);

  double GetReciprocalSampling() const {return rsampling_;}

private:
  Lattice lattice_;
  SpatialUnitCell refcell_;
  bool tilt_flag_;
  LatticeTiltGeometry tilt_geometry_;
  LatticeTiltGeometry edit_tilt_geometry_;
  bool edit_flag_;
  Lattice edit_lattice_;
  Mat2 edit_mat_;
  double rsampling_;

  QAction* a_show_tilt_;
};

}} // ns


#endif
