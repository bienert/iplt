//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------
/*
  Authors: Ansgar Philippsen
*/

#include <QColor>

#include <iplt/alg/gauss2d.hh>
#include <ost/gui/data_viewer/overlay_base.hh>
#include <iplt/gui/module_config.hh>

namespace iplt {  namespace gui {

class DLLEXPORT_IPLT_GUI  Gauss2DOverlay: public ost::img::gui::Overlay
{
public:
  Gauss2DOverlay(const alg::ParamsGauss2D& p);

  virtual void OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active);

private:
  alg::ParamsGauss2D params_;

  QColor active_color1_;
  QColor active_color2_;
  QColor active_color3_;
  QColor passive_color_;
};

}}  //ns
