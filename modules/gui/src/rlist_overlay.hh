//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Simon Berneche, Andreas Schenk
*/

#ifndef IPLT_EX_GUI_RLIST_OVERLAY_HH
#define IPLT_EX_GUI_RLIST_OVERLAY_HH

#include <QListWidget>
#include <iplt/lattice.hh>
#include <iplt/reflection_list.hh>
#include <iplt/tilt.hh>
#include <iplt/alg/lattice_point.hh>
#include <iplt/gui/module_config.hh>

#include "lattice_overlay_base.hh"

namespace iplt  {  namespace gui {

// these are safe using directives
using ::iplt::alg::LatticePoint;

class DLLEXPORT_IPLT_GUI RListOverlaySettings: public ost::img::gui::PointlistOverlayBaseSettings
{
public:
  RListOverlaySettings(const QColor& ac, const QColor& pc, int ssize, int sstr, 
                       bool rl, bool sl, const QColor& c1, const QColor& c2, const QColor& c3,
                       const std::vector<double>& rlim_list,
                       QWidget* p);

public slots:
  void OnResLim(int v);
  void OnSymRel(int v);
  void OnColor1();
  void OnColor2();
  void OnColor3();
  void OnLimAdd();
  void OnLimRemove();

public:
  bool res_lim;
  bool sym_rel;
  QColor color1,color2,color3;
  std::vector<double> rlim_list_;

private:
  QPushButton* c1_b_;
  QPushButton* c2_b_;
  QPushButton* c3_b_;
  QListWidget* rlim_widget_;
  QLineEdit* rlim_edit_;
};


class DLLEXPORT_IPLT_GUI RListOverlay: public LatticeOverlayBase
{
public:
  RListOverlay(const ReflectionList& rlist, const String& name="ReflectionList");

  virtual void OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active);
  virtual void OnMenuEvent(QAction* e);
  virtual bool OnMouseEvent(QMouseEvent* e, ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse);
  virtual bool OnKeyEvent(QKeyEvent* e, ost::img::gui::DataViewerPanel* dvp);

  ReflectionList GetReflectionList() const;
  
  virtual void AddResolutionRing(double value);
  virtual void RemoveResolutionRing(double value);
  virtual void DisplayResolutionRings(bool flag);
  virtual void HighlightSymmetryRelatedPoints(const ost::img::Point& index);

protected:

  void DrawResolutionCircles(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const QColor& color);
  void DrawSymPoints(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp);

  virtual void OnReIndex(const Mat2& mat);

  ReflectionList rlist_;
  QColor color1_; // z*>0
  QColor color2_; // z*<0
  QColor color3_; // resolution circles
  bool res_limit_flag_;
  std::vector<double> rlim_list_;
  bool sym_points_flag_;
  bool sym_point_list_flag_;
  std::vector<ReflectionIndex> sym_point_list_;
};

}} // ns


#endif
