//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
  Authors: Ansgar Philippsen, Andreas Schenk, Johan Hebert
*/

#include <QCursor>

#include <ost/gui/data_viewer/data_viewer_panel.hh>
#include <ost/geom/geom.hh>

#include <iplt/alg/lattice_point.hh>

#include "lattice_overlay_base.hh"

namespace iplt  {  namespace gui {

using namespace ::iplt::alg;
using namespace ::iplt::gui;

LatticeOverlayBase::LatticeOverlayBase(const String& name, const Lattice& lat):
  ost::img::gui::PointlistOverlayBase(name),
  lattice_(lat),
  refcell_(),
  tilt_flag_(false),
  tilt_geometry_(0.0,0.0),
  edit_tilt_geometry_(0.0,0.0),
  edit_flag_(false),
  edit_lattice_(lat),
  edit_mat_(1.0,0.0,0.0,1.0),
  rsampling_(0.0)
{
  menu_->setTitle(QString::fromStdString(name));
  a_show_tilt_=new QAction("Show Tilt Axis",this);
  a_show_tilt_->setCheckable(true);
  a_show_tilt_->setChecked(false);
  menu_->addSeparator();
  menu_->addAction(a_show_tilt_);
}

void LatticeOverlayBase::OnMenuEvent(QAction* e)
{
  if(e==a_show_tilt_) {
    tilt_flag_=e->isChecked();
  } else {
    ost::img::gui::PointlistOverlayBase::OnMenuEvent(e);
  }
}

void LatticeOverlayBase::SetLattice(const Lattice& l)
{
  lattice_=l;
  tilt_geometry_=CalcTiltFromReciprocalLattice(lattice_,ReciprocalUnitCell(refcell_));
  SetEditLattice(l);
}

const Lattice& LatticeOverlayBase::GetLattice() const 
{
  return lattice_;
}

void LatticeOverlayBase::SetReferenceCell(const SpatialUnitCell& s)
{
  refcell_=s;
  edit_tilt_geometry_=CalcTiltFromReciprocalLattice(edit_lattice_,ReciprocalUnitCell(refcell_));
  tilt_geometry_=CalcTiltFromReciprocalLattice(lattice_,ReciprocalUnitCell(s));
  // the calculated axis angle is for the spatial domain
  tilt_flag_=true;
  a_show_tilt_->setChecked(true);
  rsampling_=edit_tilt_geometry_.CalcReciprocalSampling(edit_lattice_);
}

SpatialUnitCell LatticeOverlayBase::GetReferenceCell() const
{
  return refcell_;
}

LatticeTiltGeometry LatticeOverlayBase::GetTiltGeometry() const
{
  return tilt_geometry_;
}
LatticeTiltGeometry LatticeOverlayBase::GetEditTiltGeometry() const
{
  return edit_tilt_geometry_;
}

bool LatticeOverlayBase::HasTilt() const
{
  return tilt_flag_;
}

void LatticeOverlayBase::DrawArrow(QPainter& pnt, QPoint& p0, QPoint& p1, double l,double w)
{
  pnt.drawLine(p0,p1);

  QPolygon qpoly;
  qpoly << p1;
  geom::Vec2 v0= geom::Vec2(p0.x(), p0.y());
  geom::Vec2 v1= geom::Vec2(p1.x(), p1.y());
  if(Length(v1-v0)>0){
    geom::Vec2 ve=Vec2(v1-v0)/Length(v1-v0);
    geom::Vec2 vn=Vec2(ve[1],-ve[0]);
    geom::Vec2 v2=v1-l*ve+w/2*vn;
    geom::Vec2 v3=v1-l*ve-w/2*vn;
    qpoly << QPoint(static_cast<int>(v2[0]),static_cast<int>(v2[1]));
    qpoly << QPoint(static_cast<int>(v3[0]),static_cast<int>(v3[1]));
    pnt.drawPolygon(qpoly);
  }
}

void LatticeOverlayBase::DrawLattice(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const Lattice& lattice, const QColor& color, const QColor& axes_color)
{
  LatticePointList lpl=PredictLatticePoints(lattice, dvp->GetExtent());
  std::vector<QPoint> qpointlist;
  std::vector<QPoint> axes_qpointlist;
  for(LatticePointProxyter it = lpl.Begin(); !it.AtEnd(); ++it) {
    QPoint qp=dvp->FracPointToWinCenter(lattice.CalcPosition(it.GetIndex()));
    if(dvp->IsWithin(qp)) {
      if(it.GetIndex()[0]==0 || it.GetIndex()[1]==0) {
        axes_qpointlist.push_back(qp);
      } else {
        qpointlist.push_back(qp);
      }
    }
  }

  DrawPointList(pnt,dvp,color,qpointlist);
  DrawPointList(pnt,dvp,axes_color,axes_qpointlist);

  //draw Vector A and B
  geom::Vec2 vo = lattice.GetOffset();
  geom::Vec2 va = lattice.CalcPosition(ost::img::Point(1,0))-vo;
  geom::Vec2 vb = lattice.CalcPosition(ost::img::Point(0,1))-vo;
  QPoint p0 = dvp->FracPointToWinCenter(vo);
  if(dvp->IsWithin(p0)) {
    QPoint pa = dvp->FracPointToWinCenter(vo+va);
    QPoint pb = dvp->FracPointToWinCenter(vo+vb);
    double minlength=std::min(Length(va),Length(vb));
    double arrowlength=std::min(minlength/3*dvp->GetZoomScale(),20.0);
    pnt.setPen(QPen(color,1));
    pnt.setBrush(QBrush(color));
    DrawArrow(pnt,p0,pa,arrowlength,arrowlength/2);
    DrawArrow(pnt,p0,pb,arrowlength,arrowlength/2);
    pnt.setBrush(Qt::NoBrush);
    
    //draw Arc
    QPoint parca = dvp->FracPointToWinCenter(vo+va/Length(va)*minlength/2);
    QPoint parcb = dvp->FracPointToWinCenter(vo+vb/Length(vb)*minlength/2);
    if(Cross(Vec3(va),Vec3(vb))[2]>=0.0) {
      //pnt.drawArc(parcb.x, parcb.y, parca.x, parca.y, p0.x, p0.y);
    } else {
      //pnt.drawArc(parca.x, parca.y, parcb.x, parcb.y, p0.x, p0.y);
    }
    
    //Labels
    pnt.setPen(color);
    pnt.drawText(pa, "A");
    pnt.drawText(pb, "B");
  }
}

void LatticeOverlayBase::DrawTilt(QPainter& pnt, ost::img::gui::DataViewerPanel* dvp, const Lattice& lattice, const TiltGeometry& tg, const QColor& color)
{
  pnt.setBrush(QBrush(color));
  pnt.setPen(QPen(color,1));
  double cosa=std::cos(tg.GetXAxisAngle());
  double sina=std::sin(tg.GetXAxisAngle());
  geom::Vec2 v0(cosa,sina);
  geom::Vec2 v1(-sina,cosa);
  geom::Vec2 off=lattice.GetOffset();
  //draw tilt axis
  QPoint p1=dvp->FracPointToWinCenter(-1000.0*v0+off);
  QPoint p2=dvp->FracPointToWinCenter(1000.0*v0+off);
  pnt.drawLine(p1,p2);
  //draw tilt direction
  QPoint p3=dvp->FracPointToWinCenter(100.0*v0+off);
  QPoint p4=dvp->FracPointToWinCenter(90.0*v0+5.0*v1+off);
  QPoint p5=dvp->FracPointToWinCenter(90.0*v0-5.0*v1+off);
  pnt.drawLine(p3,p4);
  pnt.drawLine(p3,p5);
  QPoint pp=dvp->FracPointToWinCenter(100.0*v0+20.0*v1+off);
  QPoint pm=dvp->FracPointToWinCenter(100.0*v0-20.0*v1+off);
  if(tg.GetTiltAngle()<0.0){
    QPoint ptmp=pp;
    pp=pm;
    pm=ptmp;
  }
  double zs=dvp->GetZoomScale();
  pnt.drawLine(pp.x()-static_cast<int>(4.0*zs),pp.y(),
               pp.x()+static_cast<int>(5.0*zs),pp.y());
  pnt.drawLine(pp.x(),pp.y()-static_cast<int>(4.0*zs),
               pp.x(),pp.y()+static_cast<int>(5.0*zs));
  pnt.drawLine(pm.x()-static_cast<int>(4.0*zs),pm.y(),
               pm.x()+static_cast<int>(5.0*zs),pm.y());
  //draw tilt angle
  double arcsize=40.0;
  QPoint pc(0,static_cast<int>(arcsize));
  /*
  dc.DrawArc(pc.x+static_cast<int>(arcsize),
	     pc.y,
	     pc.x+static_cast<int>(arcsize*std::cos(tg.GetTiltAngle())),
	     pc.y-static_cast<int>(arcsize*std::sin(tg.GetTiltAngle())),
	     pc.x,pc.y);
  */
}

void LatticeOverlayBase::OnDraw(QPainter& pnt,  ost::img::gui::DataViewerPanel* dvp, bool is_active)
{
  QColor color=is_active ? active_color_ : passive_color_;
  QColor axes_color=is_active ? active_color_.lighter(180) : passive_color_;
  if(edit_flag_){
    color.setAlphaF(0.2);
    axes_color.setAlphaF(0.2);
  }
  DrawLattice(pnt,dvp,lattice_,color, axes_color);

  if(tilt_flag_) {
    DrawTilt(pnt,dvp,
       lattice_, tilt_geometry_,
       is_active ? active_color_ : passive_color_ );
  }

  if(edit_flag_ && is_active) {
    QColor edit_color = active_color_.toHsv();
    edit_color.setHsv(edit_color.hue()+45,edit_color.saturation(),edit_color.value());
    DrawLattice(pnt,dvp,edit_lattice_, edit_color, edit_color.lighter(180));
    if(tilt_flag_) {
      DrawTilt(pnt,dvp, edit_lattice_, edit_tilt_geometry_, edit_color);
    }
  }
}


bool LatticeOverlayBase::OnMouseEvent(QMouseEvent* e,  ost::img::gui::DataViewerPanel* dvp, const QPoint& lastmouse)
{
  return false;
}

bool LatticeOverlayBase::OnLattice(const QPoint& mousepos,  ost::img::gui::DataViewerPanel* dvp)
{
  geom::Vec2 comp = lattice_.CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(mousepos)));
  QPoint index = dvp->FracPointToWinCenter(lattice_.CalcPosition(ost::img::Point(static_cast<int>(round(comp[0])),static_cast<int>(round(comp[1])))));
  int i = static_cast<int>((GetSymbolSize()-0.5)* dvp->GetZoomScale()) ;
  int dmx=mousepos.x()-index.x();
  int dmy=mousepos.y()-index.y();
  if(GetSymbolShape()==1){
    if(std::abs(dmx)<=i  && std::abs(dmy)<=i) {
      return true;
    }
  } else {
    if(dmx*dmx+dmy*dmy<=i*i){
      return true;
    }
  }
  return false;
}

bool LatticeOverlayBase::OnKeyEvent(QKeyEvent* e,  ost::img::gui::DataViewerPanel* dvp)
{
  QPoint mousepos = dvp->mapFromGlobal(QCursor::pos());
  geom::Vec2 fix_comp = GetLattice().CalcComponents(ost::img::Point(dvp->WinToFracPointCenter(mousepos)));
  ost::img::Point fix_p=Point(static_cast<int>(round(fix_comp[0])),
		    static_cast<int>(round(fix_comp[1])));

  if (e->key()=='A' ||
      e->key()=='B' ||
      e->key()=='C' ||
      e->key()=='S' ||
      e->key()=='H' ||
      e->key()=='J' ||
      e->key()=='T') {
    edit_flag_=true;
    std::pair<Mat2,Lattice> prep = PrepReIndex(lattice_,e->key(),fix_p);
    edit_mat_ = prep.first;
    SetEditLattice(prep.second);
    dvp->update();
    return true;
  } else if (e->key()=='R') { // reset
    edit_flag_=false;
    SetEditLattice(lattice_);
    edit_mat_=Mat2(1.0,0.0,0.0,1.0);
    dvp->update();
    return true;
  } else if (e->key()=='F') { // fix new lattice
    SetLattice(edit_lattice_);
    SetEditFlag(false);
    dvp->update();
    OnReIndex(edit_mat_);
    edit_mat_=Mat2(1.0,0.0,0.0,1.0);
    return true;
  } else if (e->key()=='O') { // reset origin
    if(GetEditFlag()) {
      edit_lattice_.SetOffset(Vec2(0.0,0.0));
    } else {
      SetEditFlag(true);
      Lattice lat=lattice_;
      lat.SetOffset(Vec2(0.0,0.0));
      SetEditLattice(lat);
    }
    dvp->update();
    return true;
  }
  return false;
}


Mat2 LatticeOverlayBase::GenReIndexMat(const ost::img::Point& pa, const ost::img::Point& pb)
{
  int den1=pa[0]*pb[1];
  int den2=pb[0]*pa[1];

  Mat2 nrvo;

  if(den1-den2!=0) {
    double den12=static_cast<double>(den1-den2);
    double den21=static_cast<double>(den2-den1);
    nrvo(0,0) = pb[1]==0 ? 0.0 : static_cast<double>(pb[1])/den12;
    nrvo(0,1) = pb[0]==0 ? 0.0 : static_cast<double>(pb[0])/den21;
    nrvo(1,0) = pa[1]==0 ? 0.0 : static_cast<double>(pa[1])/den21;
    nrvo(1,1) = pa[0]==0 ? 0.0 : static_cast<double>(pa[0])/den12;
  } else {
    // ignore (print warning message?)
  }

  return nrvo;
}

std::pair<Mat2,Lattice> LatticeOverlayBase::PrepReIndex(const Lattice& lat2, int keycode, const ost::img::Point& ref)
{
  Mat2 rmat(1.0,0.0,0.0,1.0);
  Lattice lat=lat2;
  geom::Vec2 va=lat.GetFirst();
  geom::Vec2 vb=lat.GetSecond();
  if(keycode=='A') {
    rmat=GenReIndexMat(ref,ost::img::Point(0,1));
  } else if (keycode=='B') {
    rmat=GenReIndexMat(ost::img::Point(1,0),ref);
  } else if (keycode=='C') {
    rmat=Mat2(1.0,0.0,
	      0.0,1.0);
    lat.SetOffset(lat.CalcPosition(ref));
  } else if (keycode=='S') {
    rmat=Mat2(0.0,1.0,
	      1.0,0.0);
  } else if (keycode=='T') {
    rmat=Mat2(-1.0,0.0,
	      0.0,-1.0);
  } else if (keycode=='J') {
    rmat=Mat2(ref[0]==0 ? 1.0 : 3.0 ,0.0,
	      0.0, ref[1]==0 ? 1.0 : 3.0);
  } else if (keycode=='H') {
    rmat=Mat2(ref[0]==0 ? 1.0 : 2.0 ,0.0,
	      0.0, ref[1]==0 ? 1.0 : 2.0);
  }
  Mat2 irmat=Invert(rmat);
  Lattice newlat(lat.GetFirst()*irmat(0,0)+lat.GetSecond()*irmat(1,0),
		 lat.GetFirst()*irmat(0,1)+lat.GetSecond()*irmat(1,1),
		 lat.GetOffset(),
		 lat.GetBarrelDistortion(),lat.GetSpiralDistortion());
  return std::make_pair(rmat,newlat);
}

void LatticeOverlayBase::SetEditFlag(bool f)
{
  edit_flag_=f;
}

bool LatticeOverlayBase::GetEditFlag() const
{
  return edit_flag_;
}

void LatticeOverlayBase::SetEditLattice(const Lattice& l)
{
  edit_lattice_=l;
  edit_tilt_geometry_=CalcTiltFromReciprocalLattice(edit_lattice_,ReciprocalUnitCell(refcell_));
  rsampling_=edit_tilt_geometry_.CalcReciprocalSampling(edit_lattice_);
}

Lattice LatticeOverlayBase::GetEditLattice() const
{
  return edit_lattice_;
}

void LatticeOverlayBase::OnReIndex(const Mat2& mat)
{
}

}}//ns
