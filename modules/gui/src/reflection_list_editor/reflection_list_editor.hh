//------------------------------------------------------------------------------
// This file is part of the IPLT project <www.iplt.org>
//
// Copyright (C) 2003-2010 by the IPLT authors
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3.0 of the License, or (at your option) any later
// version.
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//------------------------------------------------------------------------------

/*
	Author: Andreas Schenk
	
*/


#ifndef REFLECTION_LIST_EDITOR_HH_
#define REFLECTION_LIST_EDITOR_HH_


#include <wx/wx.h>
#include <wx/grid.h>

#include <iplt/reflection_list.hh>
#include <ost/gui/wxaui.hh>
#include <iplt/dllexport.hh>

namespace iplt{namespace ex{namespace gui{


//forward declarations 
class HKGrid;
class ZStarList;

class DLLEXPORT_IPLT_GUI ReflectionListEditor: public wxFrame
{
public:
  ReflectionListEditor(ReflectionList& rlist);
	virtual ~ReflectionListEditor();
  void OnCellSelect(wxGridEvent& e);
  void OnRangeSelect(wxGridRangeSelectEvent& e);
private:
  ZStarList* zstar_list_;
  HKGrid* hkgrid_;
  wxFrameManager frame_manager_;
DECLARE_EVENT_TABLE()
};

}}//ns

#endif /*REFLECTION_LIST_EDITOR_HH_*/
