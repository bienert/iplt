set(IPLT_PROC_PLOTVIEWER_PYMOD_MODULES
__init__.py
plot_viewer.py
matplotlib_panel.py
legend_panel.py
)

set(IPLT_PLOTVIEWER_UI_FILES
plot_viewer_form.ui
)

pymod( NAME plotviewer
       OUTPUT_DIR iplt/proc/plotviewer
       PY ${IPLT_PROC_PLOTVIEWER_PYMOD_MODULES}
       UI ${IPLT_PLOTVIEWER_UI_FILES}  
     )
