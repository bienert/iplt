#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure,SubplotParams
from matplotlib import patches
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from math import exp
from matplotlib.lines import *
from matplotlib.collections import *
from matplotlib.cbook import Stack
from matplotlib import rcParams
import time
rcParams['font.size'] = 8


class ExtendedStack(Stack):
  def current(self):
    return self._elements[self._pos]

class MatplotlibPanel(FigureCanvas):
  DataPicked = pyqtSignal(float,float,str)
  def __init__(self, parent=None):
    figure = Figure(figsize=(5,3),
                    dpi=100,
                    facecolor='white',
                    subplotpars=SubplotParams(left=0.125, bottom=0.0875, right=0.95, top=0.95))
    self.axes_ = figure.add_subplot(111)
    self.limits_stack_=ExtendedStack()
    self.Clear()
    FigureCanvas.__init__(self, figure)
    self.setParent(parent)
    self.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
    self.updateGeometry()
    self.mpl_connect('scroll_event', self.OnMouseWheel)
    self.mpl_connect('button_press_event', self.OnMousePress)
    self.mpl_connect('button_release_event', self.OnMouseRelease)
    self.mpl_connect('motion_notify_event', self.OnMouseMove)
    self.mpl_connect('pick_event', self.OnPick)
    forward=QAction(QString("Forward"),self)
    forward.triggered.connect(self.Forward)
    self.addAction(forward)
    back=QAction(QString("Back"),self)
    back.triggered.connect(self.Back)
    self.addAction(back)
    self.mouse_pressed_=0
    self.mouse_moved_=False
    self.zoom_min_=0.02 # minimum fraction of view to display rubberband and allow zooming
    self.pickdata_=[0.0,0.0,0.0,'',None]
    self.doubleclick_interval_=0.5
    self.PushLimits()
    self.setFocusPolicy(Qt.ClickFocus)

  def GetAxes(self):
    return self.axes_

  def OnMouseWheel(self,event):
    (xmin,xmax)=self.axes_.get_xlim()
    (ymin,ymax)=self.axes_.get_ylim()
    scalefactor=exp(event.step/10.0)
    if event.xdata!=None and event.ydata!=None:
      #inside plot resize both axes
      self.axes_.axis(xmin=event.xdata-(event.xdata-xmin)/scalefactor,
                      xmax=event.xdata+(xmax-event.xdata)/scalefactor,
                      ymin=event.ydata-(event.ydata-ymin)/scalefactor,
                      ymax=event.ydata+(ymax-event.ydata)/scalefactor)
    else:
      # only scale one axis
      # workaround for broken Xaxis.contains() yaxis.contains()
      xdata,ydata = self.axes_.transData.inverted().transform_point((event.x,event.y))
      if ydata<ymin or ydata>ymax:
        self.axes_.set_xlim(xmin=xdata-(xdata-xmin)/scalefactor,xmax=xdata+(xmax-xdata)/scalefactor)
      elif xdata<xmin or xdata>xmax:
        self.axes_.set_ylim(ymin=ydata-(ydata-ymin)/scalefactor,ymax=ydata+(ymax-ydata)/scalefactor)
    self.PushLimits()
    self.draw_idle()
    
  def OnMousePress(self,event):
    if self.mouse_pressed_:
      return
    self.mouse_moved_=False
    self.mouse_pressed_=event.button
    if event.xdata!=None and event.ydata!=None:
      self.mouse_data_pos_ = (event.xdata,event.ydata)
    else:
      self.mouse_data_pos_ = self.axes_.transData.inverted().transform_point((event.x,event.y))
    if event.button==1:
      self.rubberband_.set_visible(True)
      self.rubberband_.set_xy(self.mouse_data_pos_)
      self.rubberband_.set_width(0.0)
      self.rubberband_.set_height(0.0)
    if event.button==3:
      self.hline_.set_visible(True)
      self.vline_.set_visible(True)
      self.hline_.set_ydata([self.mouse_data_pos_[1],self.mouse_data_pos_[1]])
      self.vline_.set_xdata([self.mouse_data_pos_[0],self.mouse_data_pos_[0]])
    self.draw_idle()
   
  def OnKeyPress(self,event):
    print 'you pressed', event.key
    
  def OnMouseRelease(self,event):
    if self.mouse_pressed_!=event.button:
      return
    self.mouse_pressed_=0
    if event.button==1:
      self.rubberband_.set_visible(False)
      p1=self.rubberband_.get_xy()
      p2=(p1[0]+self.rubberband_.get_width(),p1[1]+self.rubberband_.get_height())
      (xmin,xmax)=self.axes_.get_xlim()
      (ymin,ymax)=self.axes_.get_ylim()
      if abs(p2[0]-p1[0])> abs(xmax-xmin)*self.zoom_min_ and abs(p2[1]-p1[1])> abs(ymax-ymin)*self.zoom_min_:
        self.axes_.axis(xmin=min(p1[0],p2[0]),
                        xmax=max(p1[0],p2[0]),
                        ymin=min(p1[1],p2[1]),
                        ymax=max(p1[1],p2[1]))
        self.PushLimits()
    if event.button==3:
      self.hline_.set_visible(False)
      self.vline_.set_visible(False)
      if  self.mouse_moved_:
        self.PushLimits()
      else:
        contextmenu=QMenu()
        contextmenu.addActions(self.actions())
        contextmenu.exec_(QCursor.pos())
    self.mouse_moved_=False
    self.draw_idle()

  def OnMouseMove(self,event):
    if not self.mouse_pressed_:
      return
    self.mouse_moved_=True
    (xmin,xmax)=self.axes_.get_xlim()
    (ymin,ymax)=self.axes_.get_ylim()
    if event.xdata!=None and event.ydata!=None:
      new_mouse_data_pos = (event.xdata,event.ydata)
    else:
      new_mouse_data_pos = self.axes_.transData.inverted().transform_point((event.x,event.y))
    dx=self.mouse_data_pos_[0]-new_mouse_data_pos[0]
    dy=self.mouse_data_pos_[1]-new_mouse_data_pos[1]
    if self.mouse_pressed_==1:
      (xmin,xmax)=self.axes_.get_xlim()
      (ymin,ymax)=self.axes_.get_ylim()
      if abs(dx)> abs(xmax-xmin)*self.zoom_min_ and abs(dy)> abs(ymax-ymin)*self.zoom_min_:
        self.rubberband_.set_visible(True)
      else:
        self.rubberband_.set_visible(False)
      self.rubberband_.set_width(-dx)
      self.rubberband_.set_height(-dy)
    if self.mouse_pressed_==3:
      if new_mouse_data_pos[1]<ymin or new_mouse_data_pos[1]>ymax:
        dx=self.mouse_data_pos_[0]-new_mouse_data_pos[0]
        self.axes_.set_xlim(xmin=xmin+dx,xmax=xmax+dx)
      elif new_mouse_data_pos[0]<xmin or new_mouse_data_pos[0]>xmax:
        dy=self.mouse_data_pos_[1]-new_mouse_data_pos[1]
        self.axes_.set_ylim(ymin=ymin+dy,ymax=ymax+dy)
      else:
        self.axes_.axis(xmin=xmin+dx,
                        xmax=xmax+dx,
                        ymin=ymin+dy,
                        ymax=ymax+dy)
    self.draw_idle()

  def OnPick(self,event):
    if not event.mouseevent.button==1:
      return
    picktime=time.time()
    if picktime-self.pickdata_[0]<self.doubleclick_interval_ and self.pickdata_[4]!=event.artist:
      return
    index=event.ind[0]
    if type(event.artist)== Line2D:
      xdata, ydata = event.artist.get_data()
      x=xdata[index]
      y=ydata[index]
    elif type(event.artist)== CircleCollection:
     (x,y)=event.artist.properties()['offsets'][index]
    if event.ind[0]<len(event.artist.infotext_):
      info=event.artist.infotext_[event.ind[0]]
    else:
      info=''
    QToolTip.showText(QCursor.pos(),"x:%f\ny:%f\n%s" % (x,y,str(info)),self)
    if picktime-self.pickdata_[0]<self.doubleclick_interval_ and self.pickdata_[1:]==[x,y,str(info),event.artist]:
      self.DataPicked.emit(x,y,str(info))
    self.pickdata_=[picktime,x,y,str(info),event.artist]

  def Clear(self):
    self.axes_.clear()
    self.hline_=self.axes_.axhline(color='blue',linewidth='0.5')
    self.vline_=self.axes_.axvline(color='blue',linewidth='0.5')
    self.hline_.set_visible(False)
    self.vline_.set_visible(False)
    self.rubberband_ = patches.Rectangle((0,0),1,1, facecolor="none", edgecolor="black",linewidth='0.5',linestyle='dashed')
    self.rubberband_.set_visible(False)
    self.axes_.add_patch(self.rubberband_)
    self.ClearLimitsStack()

  def AutoscaleView(self):
    self.axes_.autoscale_view(tight=True)
    self.PushLimits()
  
  def PushLimits(self):
      if not self.limits_stack_.empty():
        (cxmin,cxmax,cymin,cymax)=self.limits_stack_.current()
      else:
        (cxmin,cxmax,cymin,cymax)=(0,0,0,0)
      (xmin,xmax)=self.axes_.get_xlim()
      (ymin,ymax)=self.axes_.get_ylim()
      if cxmin!=xmin or cxmax!=xmax or cymin!=ymin or cymax!=ymax:
        self.limits_stack_.push((xmin,xmax,ymin,ymax))
  
  def ClearLimitsStack(self):
      self.limits_stack_.clear()
  
  def Forward(self):
    (xmin,xmax,ymin,ymax)=self.limits_stack_.forward()
    self.axes_.axis(xmin=xmin,
                     xmax=xmax,
                     ymin=ymin,
                     ymax=ymax)
    self.draw()

  def Back(self):
    (xmin,xmax,ymin,ymax)=self.limits_stack_.back()
    self.axes_.axis(xmin=xmin,
                     xmax=xmax,
                     ymin=ymin,
                     ymax=ymax)
    self.draw()
    
  def SetMinimumX(self,x):
    (xmin,xmax)=self.axes_.get_xlim()
    self.GetAxes().set_xlim(x,xmax)
    self.PushLimits()

  def SetMaximumX(self,x):
    (xmin,xmax)=self.axes_.get_xlim()
    self.GetAxes().set_xlim(xmin,x)
    self.PushLimits()

  def SetMinimumY(self,y):
    (ymin,ymax)=self.axes_.get_ylim()
    self.GetAxes().set_ylim(y,ymax)
    self.PushLimits()

  def SetMaximumY(self,y):
    (ymin,ymax)=self.axes_.get_ylim()
    self.GetAxes().set_ylim(ymin,y)
    self.PushLimits()

  def scatter(self,*args,**kwargs):
    result=self.axes_.scatter(*args,**kwargs)
    self.PushLimits()
    return result
    
  def plot(self,*args,**kwargs):
    result=self.axes_.plot(*args,**kwargs)
    self.PushLimits()
    return result

  def errorbar(self,*args,**kwargs):
    result=self.axes_.errorbar(*args,**kwargs)
    self.PushLimits()
    return result

  def keyPressEvent(self, event):
    if event.key() == Qt.Key_Backspace or event.key() == Qt.Key_Left:
      self.Back()
    if event.key() == Qt.Key_Right:
      self.Forward()

  