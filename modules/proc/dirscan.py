#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


def filescan(dirmask="",filemask=None,wdir="."):
    reslist=[]
    import os, os.path, re
    dir_re = re.compile(dirmask)
    for root, dirs, files in os.walk(wdir):
        #copy dirs to avoid problems with in place modification of dirs
        tmpdirs=[]
        tmpdirs.extend(dirs) 
        for dir in tmpdirs:
            if not dir_re.match(dir):
                dirs.remove(dir)
        if len(files)>0:
            if filemask!=None:
                file_re = re.compile(filemask)
                for filename in files:
                    filepath = os.path.join(root,filename)
                    if os.path.isfile(filepath) and file_re.match(filename):
                        tmp=[]
                        tmp.extend(os.path.split(root))
                        tmp.append(filename)
                        reslist.append(tmp)
                        continue
            elif len(dirs)==0:
                tmp=[]
                tmp.extend(os.path.split(root))
                tmp.append('')
                reslist.append(tmp)
    comp=lambda x,y: cmp(x[1].lower(), y[1].lower())
    reslist.sort(comp)
    return reslist

def dirscan(dirmask="",wdir="."):
    return filescan(dirmask,None,wdir)
