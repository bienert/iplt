#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import sys
from ost.img import *
import ost.info as info
import iplt as ex
import iplt.proc.diff_single_image as single_image
from ost import PushVerbosityLevel

PushVerbosityLevel(4)

sys.argv=sys.argv[1:]

if len(sys.argv)<2:
    raise Exception("usage: SCRIPT image lpl")

im = LoadImage(sys.argv[0])
inf = info.CreateInfo()
    
proc = single_image.ProcessDiffractionImage(inf)

(lpl,tg,tc) = proc.ApplySearchAndExtract(im)

rp = ex.alg.ConvertToReflectionList(lpl)

ex.ExportMtz(rp,sys.argv[1])
