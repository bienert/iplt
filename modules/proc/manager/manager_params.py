#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys, os, os.path, math
from ost.img import *
import iplt as ex

def build_double_spinbox(val,dec,rlow,rhigh,suff,step):
  sb=QDoubleSpinBox()
  sb.setDecimals(dec)
  sb.setRange(rlow,rhigh)
  sb.setSuffix(suff)
  sb.setSingleStep(step)
  sb.setValue(val)
  return sb

def build_int_spinbox(val,rlow,rhigh,suff,step):
  sb=QSpinBox()
  sb.setRange(rlow,rhigh)
  sb.setSuffix(suff)
  sb.setSingleStep(step)
  sb.setValue(val)
  return sb

class ManagerParamsFormWidget:
  def __init__(self,pkey):
    self.pkey_=pkey
  def Update(self,pdict):
    pass

class ManagerParamsFormDoubleSpinBox(QDoubleSpinBox,ManagerParamsFormWidget):
  def __init__(self,pkey,parent=None):
    QDoubleSpinBox.__init__(self,parent)
    ManagerParamsFormWidget.__init__(self,pkey)
    QObject.connect(self,SIGNAL("valueChanged(double)"),self.OnChange)
  def OnChange(self,val):
    self.emit(SIGNAL("ValueChanged"),self.pkey_,val)
  def Update(self,pdict):
    self.setValue(pdict[self.pkey_])

class ManagerParamsFormIntSpinBox(QSpinBox,ManagerParamsFormWidget):
  def __init__(self,pkey,parent=None):
    QSpinBox.__init__(self,parent)
    ManagerParamsFormWidget.__init__(self,pkey)
    QObject.connect(self,SIGNAL("valueChanged(int)"),self.OnChange)
  def OnChange(self,val):
    self.emit(SIGNAL("ValueChanged"),self.pkey_,val)
  def Update(self,pdict):
    self.setValue(pdict[self.pkey_])

class ManagerParamsFormCheckBox(QCheckBox,ManagerParamsFormWidget):
  def __init__(self,pkey,parent=None):
    QCheckBox.__init__(self,parent)
    ManagerParamsFormWidget.__init__(self,pkey)
    QObject.connect(self,SIGNAL("stateChanged(int)"),self.OnChange)
  def OnChange(self,val):
    self.emit(SIGNAL("ValueChanged"),self.pkey_,val)
  def Update(self,pdict):
    self.setChecked(pdict[self.pkey_])

class ManagerParamsFormDoubleValue(QLineEdit,ManagerParamsFormWidget):
  def __init__(self,pkey,parent=None):
    QLineEdit.__init__(self,parent)
    ManagerParamsFormWidget.__init__(self,pkey)
    self.setDragEnabled(False)
    self.setMaxLength(10)
    val=QDoubleValidator(self)
    val.setNotation(QDoubleValidator.ScientificNotation)
    self.setValidator(val)
    QObject.connect(self,SIGNAL("returnPressed()"),self.OnChange)
  def OnChange(self):
    self.emit(SIGNAL("ValueChanged"),self.pkey_,self.text())
  def Update(self,pdict):
    self.setText("%g"%pdict[self.pkey_])
    
class ManagerParamsFormButtonGroup(QButtonGroup,ManagerParamsFormWidget):
  def __init__(self,pkey,parent=None):
    QButtonGroup.__init__(self,parent)
    ManagerParamsFormWidget.__init__(self,pkey)
    QObject.connect(self,SIGNAL("buttonClicked(int)"),self.OnChange)
  def OnChange(self,bid):
    self.emit(SIGNAL("ValueChanged"),self.pkey_,bid)
  def Update(self,pdict):
    self.button(pdict[self.pkey_]).setChecked(True)


class ManagerParamsForm(QGroupBox):
  def __init__(self,name,pdict,parent=None):
    QGroupBox.__init__(self,name,parent)
    self.pdict_=pdict
    self.setContentsMargins(2,10,2,5)
    self.grid_=QGridLayout(self)
    self.grid_.setVerticalSpacing(2)
    self.grid_.setHorizontalSpacing(4)
    self.grid_.setColumnStretch(3,1)
    self.crow_=0
    self.setLayout(self.grid_)

  def SetPDict(self,p):
    self.pdict_=p

  def DoubleValue(self,key):
    val=self.pdict_[key]
    dv=ManagerParamsFormDoubleValue(key)
    dv.setText("%g"%val)
    QObject.connect(dv,SIGNAL("ValueChanged"),self.OnValueChanged)
    QObject.connect(self,SIGNAL("UpdateValues"),dv.Update)
    return dv

  def DoubleSpinBox(self,key,dec,rlow,rhigh,suff,step):
    val=self.pdict_[key]
    sb=ManagerParamsFormDoubleSpinBox(key)
    QObject.connect(sb,SIGNAL("ValueChanged"),self.OnValueChanged)
    QObject.connect(self,SIGNAL("UpdateValues"),sb.Update)
    sb.setDecimals(dec)
    sb.setRange(rlow,rhigh)
    sb.setSuffix(suff)
    sb.setSingleStep(step)
    sb.setValue(val)
    return sb

  def IntSpinBox(self,key,rlow,rhigh,suff,step):
    val=self.pdict_[key]
    sb=ManagerParamsFormIntSpinBox(key)
    QObject.connect(sb,SIGNAL("ValueChanged"),self.OnValueChanged)
    QObject.connect(self,SIGNAL("UpdateValues"),sb.Update)
    sb.setRange(rlow,rhigh)
    sb.setSuffix(suff)
    sb.setSingleStep(step)
    sb.setValue(val)
    return sb

  def CheckBox(self,key):
    val=self.pdict_[key]
    cb=ManagerParamsFormCheckBox(key)
    cb.setChecked(val)
    QObject.connect(cb,SIGNAL("ValueChanged"),self.OnValueChanged)
    QObject.connect(self,SIGNAL("UpdateValues"),cb.Update)
    return cb

  def OnValueChanged(self,pkey,val):
    self.pdict_[pkey]=val
    
  def OnUpdateValues(self,pdict):
    self.pdict_=pdict
    self.emit(SIGNAL("UpdateValues"),self.pdict_)
    
  def Add(self,label,widget,tt=None):
    self.grid_.addWidget(QLabel(label),self.crow_,0,Qt.AlignRight)
    self.grid_.addWidget(QLabel(":"),self.crow_,1,Qt.AlignCenter)
    self.grid_.addWidget(widget,self.crow_,2)
    if tt:
      hw=QLabel()
      hw.setPixmap(QApplication.style().standardIcon(QStyle.SP_TitleBarContextHelpButton).pixmap(16,16))
      hw.setToolTip("<P>"+tt+"</P>")
      self.grid_.addWidget(hw,self.crow_,3)
    else:
      self.grid_.addWidget(QLabel(""),self.crow_,3)
    self.grid_.setRowStretch(self.crow_,0)
    self.crow_+=1
    self.grid_.setRowStretch(self.crow_,1)

  def AddHeader(self,label):
    self.grid.addWidget(QLabel(label),self.crow,0,1,4,Qt.AlignCenter)
    self.grid_.setRowStretch(self.crow_,0)
    self.crow_+=1
    self.grid_.setRowStretch(self.crow_,1)

  def AddChoiceBox(self,key,name,label_list):
    val=self.pdict_[key]
    top=QVBoxLayout()
    bg=ManagerParamsFormButtonGroup(key,self)
    for i in range(len(label_list)):
      but=QRadioButton(label_list[i],self)
      if i==val:
        but.setChecked(True)
      bg.addButton(but,i)
      top.addWidget(but)
    QObject.connect(bg,SIGNAL("ValueChanged"),self.OnValueChanged)
    QObject.connect(self,SIGNAL("UpdateValues"),bg.Update)
    gb=QGroupBox(name)
    gb.setContentsMargins(1,8,1,1)
    gb.setLayout(top)
    self.grid_.addWidget(gb,self.crow_,0,1,4)
    self.grid_.setRowStretch(self.crow_,0)
    self.crow_+=1
    self.grid_.setRowStretch(self.crow_,1)


class CellParams(ManagerParamsForm):
  def __init__(self,ucell,parent=None):
    ManagerParamsForm.__init__(self,"UnitCell",None,parent)
    w_sym_list=QComboBox()
    sym_list=ex.GetImplementedSymmetries()
    act=0
    for sid in sym_list:
      sym=ex.Symmetry(sid)
      symn=sym.GetSpacegroupName()
      w_sym_list.addItem(symn)
      if sym.GetSpacegroupNumber()==ucell.GetSymmetry().GetSpacegroupNumber():
        act=w_sym_list.count()-1
      w_sym_list.setCurrentIndex(act)
      w_sym_list.setMaxVisibleItems(10)
    self.UpdatePDict(ucell)
    self.Add("Spacegroup",
             w_sym_list,
             "Crystallographic Spacegroup")
    self.Add("A",
             self.DoubleSpinBox("ucell_a",2,0.0,10000.0,"A",1.0),
             "Length of first unit cell axis in A")
    self.Add("B",
             self.DoubleSpinBox("ucell_b",2,0.0,10000.0,"A",1.0),
             "Length of second unit cell axis in A")
    self.Add("Gamma",
             self.DoubleSpinBox("ucell_g",2,90.0,180,"deg",1.0),
             "Angle between unit cell axes in degrees")
    self.Add("Thickness",
             self.DoubleSpinBox("ucell_c",2,0.0,10000.0,"A",10.0),
             "Thickness of Crystal along z, in A")

  def UpdatePDict(self,ucell):
    pdict={}
    pdict["ucell_a"]=ucell.GetA()
    pdict["ucell_b"]=ucell.GetB()
    pdict["ucell_g"]=ucell.GetGamma()*180.0/math.pi
    pdict["ucell_c"]=ucell.GetC()
    self.SetPDict(pdict)
                  
  def OnUpdateValues(self,pdict):
    self.UpdatePDict(pdict["ucell"])
    ManagerParamsForm.OnUpdateValues(self,self.pdict_)
