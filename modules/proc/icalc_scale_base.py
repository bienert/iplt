#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import math
from ost.img import *
import iplt as ex
from ost import LogVerbose,LogInfo,LogError,LogVerbose

#
# two scaler classes for iobs (sigiobs) + icalc (sigicalc) reflection data
#

class ICalcScaler:
  def __init__(self,rlow,rhigh,bcount):
    self.rlow_=rlow
    self.rhigh_rhigh
    self.bcount_=bcount
    self.iobs_name="iobs"
    self.sigiobs_name="sigiobs"
    self.icalc_name="icalc"
    self.sigicalc_name="sigicalc"

  def SetWBinSigmaCutof(self,dummy):
    pass
  
  def SetWBinISigICutof(self,dummy):
    pass

  def SetFilterRLim(self,dummy1,dummy2,dummy3):
    pass

  def Apply(self,rlist2,unity_scaling,ds_name):
    rlist = ex.ReflectionList(rlist2)

    if unity_scaling:
      for rp in rlist:
        rp.Set("scale",1.0)
        rp.Set("sigscale",1.0)
      return rlist
    
    linfit=ex.alg.LinearFit()
    linfit.ForceOrigin(False)

    for rp in rlist:
      iobs=rp.Get(self.iobs_name)
      sigiobs=rp.Get(self.sigiobs_name)
      icalc=rp.Get(self.icalc_name)
      sigicalc=rp.Get(self.sigicalc_name)
      if sigiobs>0.0 and sigicalc>0.0:
        res=rp.GetResolution()
        if res>0.0 and res>=self.rlow_ and res<=self.rhigh_:
          y=math.log(icalc/iobs)
          sigy=math.sqrt( sigicalc*sigicalc/(icalc*icalc) + sigiobs*sigiobs/(iobs*iobs) )
          linfit.AddDatapoint(1.0/(res*res),y,1.0/(sigy*sigy))

    linfit.Apply()
    LogInfo("%s: icalc scale: %g bfac: %g"%(ds_name,
                                            math.exp(linfit.GetOffset()),
                                            linfit.GetScale()))

    
    for rp in rlist_ds:
      res=rp.GetResolution()
      (logF,siglogF) = linfit.Estimate(1.0/(res*res))
      F = math.exp(logF)
      rp.Set("iobs",rp.Get("iobs")*F)
      rp.Set("sigiobs",rp.Get("sigiobs")*F)
      rp.Set("scale",F)
      rp.Set("sigscale",F)



class ICalcWBinScaler:
  def __init__(self,rlow,rhigh,bcount):
    self.rlow_=rlow
    self.rhigh_rhigh
    self.bcount_=bcount

  def SetWBinSigmaCutof(self,dummy):
    pass
  
  def SetWBinISigICutof(self,dummy):
    pass

  def SetFilterRLim(self,dummy1,dummy2,dummy3):
    pass

  def Apply(self,rlist2,unity_scaling,ds_name):
    rlist = ex.ReflectionList(rlist2)

    if unity_scaling:
      for rp in rlist:
        rp.Set("scale",1.0)
        rp.Set("sigscale",1.0)
      return rlist
    
    wbin_iobs = ex.alg.WeightedBin(self.bcount_,
                                   1.0/(self.rlow_*self.rlow_),
                                   1.0/(self.rhigh_*self.rhigh_))
    wbin_ifit = ex.alg.WeightedBin(self.bcount_,
                                   1.0/(self.rlow_*self.rlow_),
                                   1.0/(self.rhigh_*self.rhigh_))

    for rp in rlist:
      rr=1.0/(rp.GetResolution()*rp.GetResolution())
      iobs=rp.Get("iobs")
      sigiobs=rp.Get("sigiobs")
      ifit=rp.Get("ifit")
      sigifit=rp.Get("sigifit")
      if sigiobs>0.0 and sigifit>0.0:
        wbin_iobs.Add(rr,iobs,1.0/(sigiobs*sigiobs))
        wbin_ifit.Add(rr,ifit,1.0/(sigifit*sigifit))

      expfit=ex.alg.ExpFit(True) # no offset fitting

      for n in range(wbin_iobs.GetBinCount()):
        x=wbin_iobs.GetLimit(n)
        I2 = wbin_iobs.GetAverage(n)
        S2 = wbin_iobs.GetStdDev(n)
        I1 = wbin_ifit.GetAverage(n)
        S1 = wbin_ifit.GetStdDev(n)
        if abs(I2)>1e-10 and abs(I1)>1e-10 and wbin_iobs.GetSize(n)>5:
          y=(I1/I2)
          sigy2=( S1*S1/(I1*I1) + S2*S2/(I2*I2) ) * y * y
          expfit.Add(x,y,1.0/sigy2)
            
      expfit.Apply()

      (B,sigB) = expfit.GetBsigB()
      (S,sigS) = expfit.GetSsigS()
      (C,sigC) = expfit.GetCsigC()

      LogInfo("%s: refine scale: %g (%g)   bfac: %g (%g)   off: %g (%g)"%(ds_name,S,sigS,B,sigB,C,sigC))

      for rp in rlist:
        x=1.0/(rp.GetResolution()*rp.GetResolution())
        (F,sigF) = expfit.Estimate(x)
        I2 = rp.Get("iobs")
        I2F = I2*F
        rp.Set("iobs",I2F)
        S2 = rp.Get("sigiobs")
        S2F = S2*F
        rp.Set("sigiobs",S2F)
        rp.Set("scale",F)
        rp.Set("sigscale",sigF)

      return rlist
