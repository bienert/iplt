#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

import math,os,re,os.path
from ost.img import *
from ost.info import *
import iplt as ex
from iplt.proc.dirscan import dirscan
from diff_iterative_subcommand import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
import iplt

class DiffSummary(DiffIterativeSubcommand):
    def __init__(self):
      DiffIterativeSubcommand.__init__(self,"summary","summary","")
        
    def Run(self,dentrylist,args,options):
        fname="summary.log"
        sum=file(fname,"w")
        r2d = 180.0/math.pi

        pinfo = LoadInfo("project.xml")
        ucell = ex.InfoToReciprocalUnitCell(pinfo.Root().GetGroup("DiffProcessing/OrigUnitCell"))

        for dentry in dentrylist:
            LogInfo("working on %s"%dentry[1])
            sum.write("%-10s: "%(dentry[1]))

            iinfofile=os.path.join(dentry[0],dentry[1],"info.xml")
            if not os.path.isfile(iinfofile):
                sum.write("no info file\n")
                continue
            
            iinfo=LoadInfo(iinfofile)
            igroup=iinfo.Root().GetGroup("DiffProcessingResults")
            if not igroup.HasGroup("InitialLattice"):
                sum.write("no lattice found\n")
                continue
            ilat = iplt.InfoToLattice(igroup.GetGroup("InitialLattice"))

            orix = igroup.GetGroup("Origin").GetItem("x").AsFloat()
            oriy = igroup.GetGroup("Origin").GetItem("y").AsFloat()

            int_mtz=os.path.join(dentry[0],dentry[1],dentry[1]+"_int.mtz")
            if not os.path.exists(int_mtz) or not igroup.HasGroup("RefinedLattice"):
                sum.write("no integration run\n")
                continue

            tilt_mtz=os.path.join(dentry[0],dentry[1],dentry[1]+"_tilt.mtz")
            if not os.path.exists(tilt_mtz):
                sum.write("no tilt geometry determined\n")
                continue
            tg=iplt.InfoToLatticeTiltGeometry(igroup.GetGroup("InitialTiltGeometry"))

            sym_mtz=os.path.join(dentry[0],dentry[1],dentry[1]+"_sym.mtz")
            if not os.path.exists(tilt_mtz):
                sum.write("no symmetrization\n")
                continue

            rlist=ex.ImportMtz(sym_mtz)

            rlat = rlist.GetLattice()
    
            rsampling = tg.CalcReciprocalSampling(rlat)
            
            rf=igroup.GetItem("RFriedel").AsFloat()
            rf2=0.0
            if igroup.HasItem("RFriedel2"):
                rf2=igroup.GetItem("RFriedel2").AsFloat()

            rsym=0.0
            wrsym=0.0
            rsym_count=0
            if igroup.HasItem("RSym"):
                rsym=igroup.GetItem("RSym").AsFloat()
                rsym_count=igroup.GetItem("RSymCount").AsInt()
            if igroup.HasItem("WRSym"):
                wrsym=igroup.GetItem("WRSym").AsFloat()
                
            sum.write("Nref=%4d Rfr=%4.2f (%4.2f)  Rsym=%4.2f (%4.2f) (%d)  TA=%6.1f XA=%6.1f  RS=%5.5g "%(
                rlist.NumReflections(),
                rf,rf2,rsym,wrsym,rsym_count,
                tg.GetTiltAngle()*r2d,
                tg.GetXAxisAngle()*r2d,
                rsampling
                )) 
            
            if os.path.exists(os.path.join(dentry[0],dentry[1],"ignore")):
                sum.write("*ignored*")

            sum.write("\n")
    
    
        sum.close()
