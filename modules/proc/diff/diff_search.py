#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------
#
#
# Authors: Andreas Schenk, Ansgar Philippsen
#
import iplt as ex
import iplt.alg
from ost.img import *
from diff_iterative_subcommand import *
import time
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.geom import *

class DiffSearch(DiffIterativeSubcommand):
    def __init__(self):
      DiffIterativeSubcommand.__init__(self,"search","determine lattice","diff_search.log")

    def ProcessDirectory(self,options):
        mask=~self.GetTotalMask()
        image=self.LoadDatasetImage()
        gname="DiffProcessingResults/Origin"
        if self.GetInfoHandle().Root().HasGroup(gname):
            g=self.GetInfoHandle().Root().GetGroup(gname)
            ori_float=Vec2(g.GetItem('x').AsFloat(),g.GetItem('y').AsFloat())
        else:
            ori_float=None
        LogVerbose("DiffSearch: applying mask")
        image.ApplyIP(alg.MaskImage(mask,False))
        #if GetVerbosityLevel()>3:
            #SaveImage(image,self.GetNamedFilePath("_mask.tif"))
        # perform actual lattice search
        LogVerbose("DiffSearch: initializing lattice search")
        lattice_search = ex.alg.LatticeSearch()
        if ori_float:
            rad = GetIntInfoItem(self.GetInfoHandle().Root(),
                                 "DiffProcessing/LatticeExtract/CenterMaskRadius",
                                 0)
            if rad>0:
                lattice_search.AddPeakSearchExclusion(Extent(Size(rad*2,rad*2),Point(ori_float)))
            
        # find suitable info group
        if self.GetInfoHandle().Root().HasGroup("DiffProcessing/LatticeSearch"):
            LogVerbose("DiffSearch: updating lattice search object from info")
            ex.alg.UpdateFromInfo(lattice_search,self.GetInfoHandle().Root().GetGroup("DiffProcessing/LatticeSearch"))
        LogVerbose("DiffSearch: running lattice search")
        try:
            image.Apply(lattice_search)
        except Exception, e:
            vi = lattice_search.GetVectorImage();
            LogVerbose("DiffSearch: dumping vector image to tmp_search_vecimg.png")
            SaveImage(vi,self.GetFilePath("tmp_search_vecimg.png"))
            LogVerbose("DiffSearch: dumping peak list to tmp_search_peaks.dat")
            iplt.alg.ExportPeakList(lattice_search.GetImagePeaks(),self.GetFilePath("tmp_search_peaks.dat"))
            LogError("caught exception during lattice search, skipping: %s"%str(e))
            return

        # todo add flag to keep temp files instead of using verbosity level
        #if GetVerbosityLevel()>3:
        if 1:
            # dump some debug stuff
            vi = lattice_search.GetVectorImage();
            LogVerbose("DiffSearch: dumping vector image to tmp_search_vecimg.png")
            SaveImage(vi,self.GetFilePath("tmp_search_vecimg.png"))
            LogVerbose("DiffSearch: dumping peak list to tmp_search_peaks.dat")
            iplt.alg.ExportPeakList(lattice_search.GetImagePeaks(),self.GetFilePath("tmp_search_peaks.dat"))

        lat=lattice_search.GetLattice()
        # note: the lattice needs to be ab swapped
        # to have the longer reciprocal vector as A
        lat=ex.Lattice(lat.GetSecond(),-lat.GetFirst(),lat.GetOffset())
        LogVerbose("DiffSearch: PrelimLattice determined: %s"%(str(lat)))

        if not ori_float:
            LogVerbose("DiffSearch: no origin found, using center of image")
            ori_float = image.GetExtent().GetCenter().ToVec2()

        comp=lat.CalcComponents(ori_float)
        ori=lat.CalcPosition(Vec2(round(comp[0]),round(comp[1])))
        lat.SetOffset(ori)


        LogInfo("DiffSearch: Lattice determined: %s"%(str(lat)))

        LogVerbose("DiffSearch: writing lattice to info.xml")
        g=self.GetInfoHandle().Root().RetrieveGroup("DiffProcessingResults/InitialLattice")
        ex.LatticeToInfo(lat,g)
        g.SetAttribute("modified",time.asctime())

    def UpToDate(self,options):
        """
        returns true if diff search state is up to date
        """
        if options.force_flag:
            return False
        if self.info_handle_.Root().HasGroup("DiffProcessingResults/InitialLattice"):
            gg1=self.info_handle_.Root().GetGroup("DiffProcessingResults/InitialLattice")
            if self.info_handle_.Root().HasGroup("DiffProcessingResults/Origin"):
                gg2=self.info_handle_.Root().GetGroup("DiffProcessingResults/Origin")
                if self.InfoIsNewer(gg2,gg1):
                    return False # origin is newer than initial lattice
            return True # initial lattice is present
        return False

    def CheckPreCondition(self,options):
        """
        returns true if pre-condition is met to run diff search
        """
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffOri: missing info.xml")
            return False
        if self.info_handle_.Root().HasGroup("DiffProcessingResults/Origin"):
            return True
        else:
            if not options.skip_ori:
                LogInfo("DiffSearch: missing origin, skipping")
                return False
            else:
                LogInfo("DiffSearch: ignoring missing origin")
                return True

    def Cleanup(self,wdir):
        try:
            os.remove(os.path.join(wdir,"tmp_search_vecimg.png"))
            os.remove(os.path.join(wdir,"tmp_search_peaks.dat"))
        except OSError:
            pass
