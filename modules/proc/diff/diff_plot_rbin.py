#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


"""
plot resolution bins of given mtz file
"""

import sys,os,os.path
from ost.img import *
import iplt as ex
import iplt.proc.plot_fit as plot_fit

sys.argv=sys.argv[1:]

if len(sys.argv)<2:
    raise Exception("missing parameters: mtz_input eps_output [bincount] [limlow] [limhi] [limisigi]")

mtzin = sys.argv[0]
epsout = sys.argv[1]

bincount=10
limlow=100
limhi=4
limsig=0
if len(sys.argv)>2:
    bincount=int(sys.argv[2])
if len(sys.argv)>3:
    limlow=float(sys.argv[3])
if len(sys.argv)>4:
    limhi=float(sys.argv[4])
if len(sys.argv)>5:
    limsig=float(sys.argv[5])

rlist = ex.ImportMtz(mtzin)

rbin=ex.alg.ResolutionBin(bincount,limlow,limhi)

for rp in rlist:
    sigr=rp.Get("fobs")/rp.Get("sigfobs")
    if sigr>limsig:
        rbin.Add(rp.GetResolution(),rp.Get("fobs"))

plot_fit.PlotRBin(rbin,epsout)

