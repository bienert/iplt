#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

"""
"""

import sys,os,os.path
from ost.img import *
import iplt as ex
import iplt.proc.plot_fit as plot_fit
import pyx

class idsymbol(pyx.graph.style.symbol):
    def __init__(self, idname="id", **kwargs):
        self.idname = idname
        pyx.graph.style.symbol.__init__(self, **kwargs)

    def columnnames(self, privatedata, sharedata, graph, columnnames):
        if self.idname not in columnnames:
            raise ValueError("column '%s' missing" % self.idname)
        return [self.idname] + pyx.graph.style.symbol.columnnames(self, privatedata, sharedata, graph, columnnames)

    def drawpoint(self, privatedata, sharedata, graph):
        if sharedata.vposvalid:
            x_pt, y_pt = graph.vpos_pt(*sharedata.vpos)
            text = str(int(sharedata.point[self.idname]))
            graph.text_pt(x_pt, y_pt, text)
    
def plot_ll(rlist,indx,dir,zstar_tag,amp_tag,id_tag):
    """
    plots the lattice line of the given reflection list and index
    assumes that directory exists and is writeable
    """
    l=[]
    rp=rlist.Find(indx)
    if not rp.IsValid():
        return
    ri=rp.__iter__()
    if rlist.HasProperty(id_tag):
        while(rp.IsValid() and rp.GetIndex()==indx):
            l.append([rp.Get(zstar_tag),rp.Get(amp_tag),rp.Get(id_tag)])
            rp=ri.next()
    else:
        while(rp.IsValid() and rp.GetIndex()==indx):
            l.append([rp.Get(zstar_tag),rp.Get(amp_tag),"0"])
            rp=ri.next()

    if len(l)>1:
        try:
            eps_file="%s/ll_%d_%d.eps"%(dir,indx[0],indx[1])
            c=pyx.canvas.canvas()
            g=pyx.graph.graphxy(width=8,
                                x=pyx.graph.axis.linear(min=-0.01, max=0.2,title="z*"),
                                y=pyx.graph.axis.linear(title="A"))
            #g.plot(pyx.graph.data.list(l,x=1,y=2,id=3),[idsymbol()])
            g.plot(pyx.graph.data.list(l,x=1,y=2))
            c.insert(g)
            c.text(0,5.5,"%d %d"%(indx[0],indx[1]))
            c.writeEPSfile(eps_file,paperformat=pyx.document.paperformat.A4,fittosize=1)
        except OverflowError:
            try:
                os.remove(eps_file)
            except OSError:
                pass


def check_lim(indx,lim):
    if not lim:
        return True
    else:
        if abs(indx[0])<=lim and abs(indx[1])<=lim:
            return True
    return False
        
sys.argv=sys.argv[1:]

if len(sys.argv)<2:
    raise Exception("missing parameters: mtzin dir [lim]")

mtzin=ex.ImportMtz(sys.argv[0])
dir=sys.argv[1]
if len(sys.argv)>2:
    lim=int(sys.argv[2])
else:
    lim=None

try:
    os.makedirs(dir)
except OSError:
    pass

rp=mtzin.Begin()
ri=rp.__iter__()
try:
    while rp.IsValid():
        indx=rp.GetIndex()
        if check_lim(indx,lim):
            print "plotting %d %d"%(indx.GetH(),indx.GetK())
            plot_ll(mtzin,indx,dir,"zstar","fobs","id")
        while(rp.GetIndex()==indx):
            rp=ri.next()
except StopIteration:
    pass
