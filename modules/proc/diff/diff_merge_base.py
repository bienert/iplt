#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


import os,os.path
from ost.img import *
from ost.info import *
import iplt as ex


class DiffMergeBase:
  def __init__(self):
    self.pdict_={}

  def __getitem__(self,x):
    return self.pdict_[x]

  def __setitem__(self,x,v):
    self.pdict_[x]=v

  def LoadParameters(self,pinfo):

    info_root=pinfo.Root()

    # grab parameters from info

    # required: the image mask to work with
    if not info_root.HasItem("DiffProcessing/ImageMask"):
      raise Exception("ImageMask not found in info")
    self["img_mask"] = info_root.GetItem("DiffProcessing/ImageMask").GetValue()
    self["img_mask_pref"] = self["img_mask"][:3]

    # required: the unit cell
    if not info_root.HasGroup("DiffProcessing/OrigUnitCell"):
      raise Exception("OrigUnitCell not found in info")
    ig = info_root.GetGroup("DiffProcessing/OrigUnitCell")
    self["ucell"] = ex.InfoToSpatialUnitCell(ig)

    # low and high resolution limits for scaling
    self["scale_rlow"]=GetFloatInfoItem(info_root,
                                      "DiffProcessing/Scale/Resolution/low",
                                      40.0)
    self["scale_rhigh"]=GetFloatInfoItem(info_root,
                                       "DiffProcessing/Scale/Resolution/high",
                                       4.0)
    # bins to be used in the weighted bin scaling
    self["scale_bcount"]=GetIntInfoItem(info_root,
                                      "DiffProcessing/Scale/BinCount",
                                      20)
    # minimal tilt upon which to invoke anisotropic scaling  
    self["aniso_min_tilt"] = GetFloatInfoItem(info_root,
                                            "DiffProcessing/Scale/AnisoMinTilt",
                                            90.0)
    # stripe width for anisotropic scaling algorithm
    self["aniso_swidth"] = GetFloatInfoItem(info_root,
                                          "DiffProcessing/Scale/AnisoStripeWidth",
                                          200.0)

    # i/sigi cutoff for filter based on weighted bin
    self["wbin_isigi_cutoff"] = GetFloatInfoItem(info_root,
                                               "DiffProcessing/Scale/WBinISigICutoff",
                                               0.0)
    # sigma cutoff for filter based on weighted bin
    self["wbin_sigma_cutoff"] = GetFloatInfoItem(info_root,
                                               "DiffProcessing/Scale/WBinSigmaCutoff",
                                               10.0)

    # flag to set model based scaling
    self["use_model_ref"]=GetBoolInfoItem(info_root,
                                        "DiffProcessing/Scale/UseModel",
                                        False)
    # filename of model to be used for scaling
    self["model_mtz"]=GetStringInfoItem(info_root,
                                      "DiffProcessing/Scale/ModelMtz",
                                      "")
    # filename of model map to be used for scaling
    self["model_map"]=GetStringInfoItem(info_root,
                                      "DiffProcessing/Scale/ModelMap",
                                      "")

    # flag to activate Rmerge calculation after scaling
    self["calc_rmerge"]=GetBoolInfoItem(info_root,
                                      "DiffProcessing/Scale/CalcRMerge",
                                      True)

    # z window width for Rmerge calculation
    self["calc_rmerge_zlim"]=GetFloatInfoItem(info_root,
                                            "DiffProcessing/Scale/CalcRMergeZLim",
                                            0.01)
    
    # sigma rejection level for Rmerge calculation
    self["calc_rmerge_rej_level"]=GetFloatInfoItem(info_root,
                                                 "DiffProcessing/Scale/CalcRMergeSigmaRejectLevel",
                                                 3.0)

    # q1 coefficient for llfit weighting algorithm
    self["quality_t1"] = GetFloatInfoItem(info_root,
                                        "DiffProcessing/LLFit/Weighting/T1",
                                        10.0)
    # q2  " 
    self["quality_t2"] = GetFloatInfoItem(info_root,
                                        "DiffProcessing/LLFit/Weighting/T2",
                                        40.0)

    # llfit weighting mode
    self["weight_mode"] = GetIntInfoItem(info_root,
                                           "DiffProcessing/LLFit/Weighting/Mode",
                                           1)
    # quality cutoff - lower quality reflections - based on q1 + q2 - will be ignored
    self["quality_cutoff"] = GetFloatInfoItem(info_root,
                                            "DiffProcessing/LLFit/Weighting/Cutoff",
                                            0.1)

    # global weighting - fuzz factor
    self["weight_global"] = GetFloatInfoItem(info_root,
                                           "DiffProcessing/LLFit/Weighting/Global",
                                           1.0)

    # max iterations for llfit
    self["niter"] = GetIntInfoItem(info_root,
                                     "DiffProcessing/LLFit/MaxIterations",
                                     1000)

    # iteration precision limit for llfit
    self["iterlim"] = GetFloatInfoItem(info_root,
                                     "DiffProcessing/LLFit/IterationPrecision",
                                     1e-5)

    # bootstrap flag for llfit
    self["bootstrap_flag"] = GetBoolInfoItem(info_root,
                                           "DiffProcessing/LLFit/BootstrapFlag",
                                           True)
    
    # bootstrap iterations to run
    self["bootstrap_iter"] = GetIntInfoItem(info_root,
                                          "DiffProcessing/LLFit/BootstrapIterations",
                                          10)

    # mode to set initial guess values for llfit
    self["llfit_guess_mode"] = GetIntInfoItem(info_root,
                                            "DiffProcessing/LLFit/GuessMode",
                                            0)
    # overall llfit iterations - only makes sense with guess mode 2
    self["llfit_iter"] = GetIntInfoItem(info_root,
                                      "DiffProcessing/LLFit/Iterations",
                                      1)

    # use non-coverged iterations
    self["llfit_iter_conv_flag"]=GetBoolInfoItem(info_root,
                                               "DiffProcessing/LLFit/IterationsUseNonConvergedFlag",
                                               True)
    # bfactor in llfit
    self["llfit_bfac"]=GetFloatInfoItem(info_root,
                                      "DiffProcessing/LLFit/BFactor",
                                      0.0)

    # use phantoms based on sigma-z* to improve llfit
    self["llfit_phantom_mode"]=GetBoolInfoItem(info_root,
                                             "DiffProcessing/LLFit/PhantomMode",
                                             False)

    self["llfit_alg"]=GetIntInfoItem(info_root,
                                   "DiffProcessing/LLFit/Algorithm",
                                   0)
    
    self["llfit_force_herm"]=GetIntInfoItem(info_root,
                                          "DiffProcessing/LLFit/ForceHermMode",
                                          -1)

    self["llref_scaling_mode"]=GetIntInfoItem(info_root,
                                            "DiffProcessing/LLFit/RefineScaleMode",
                                            0)
    
    # overall low resolution limit of merge
    self["rlim_low"]=GetFloatInfoItem(info_root,
                                    "DiffProcessing/ResLim/rlow",
                                    100.0)
    # overall high resolution limit of merge
    self["rlim_high"]=GetFloatInfoItem(info_root,
                                     "DiffProcessing/ResLim/rhigh",
                                     2.0)

    # zweight in xy post-llfit refinement
    self["refine_zweight"]=GetFloatInfoItem(info_root,
                                          "DiffProcessing/Refine/ZWeight",
                                          8.0)

    # curve fit mode in post-llfit refinement
    self["refine_curve_fit_mode"]=GetIntInfoItem(info_root,
                                               "DiffProcessing/Refine/CurveFitMode",
                                               0)

    self["rsym_limit"]=GetFloatInfoItem(info_root,
                                      "DiffProcessing/Merge/RSymLimit",
                                      100.0)
        
    # adjust the bin-size for the scaling filter to match the scaling res/bin settings
    bin_size = (1.0/(self["scale_rhigh"]*self["scale_rhigh"])-1.0/(self["scale_rlow"]*self["scale_rlow"]))/float(self["scale_bcount"])
    self["scale_filter_rlow"]=self["scale_rlow"]
    self["scale_filter_rhigh"]=2.0
    self["scale_filter_bcount"]=int((1.0/(self["scale_filter_rhigh"]*self["scale_filter_rhigh"])-1.0/(self["scale_filter_rlow"]*self["scale_filter_rlow"]))/bin_size)


  def SaveParameters(self,info_file):

    iinfo = LoadInfo(info_file)
    info_root=iinfo.Root()

    ig=info_root.RetrieveGroup("DiffProcessing/OrigUnitCell")
    ex.SpatialUnitCellToInfo(ig,self["ucell"])

    SetFloatInfoItem(info_root,"DiffProcessing/Scale/Resolution/low",self["scale_rlow"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/Resolution/high",self["scale_rhigh"])
    SetIntInfoItem(info_root,"DiffProcessing/Scale/BinCount",self["scale_bcount"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/AnisoMinTilt",self["aniso_min_tilt"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/AnisoStripeWidth",self["aniso_swidth"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/WBinISigICutoff",self["wbin_isigi_cutoff"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/WBinSigmaCutoff",self["wbin_sigma_cutoff"])
    SetBoolInfoItem(info_root,"DiffProcessing/Scale/UseModel",self["use_model_ref"])
    SetStringInfoItem(info_root,"DiffProcessing/Scale/ModelMtz",self["model_mtz"])
    SetStringInfoItem(info_root,"DiffProcessing/Scale/ModelMap",self["model_map"])
    SetBoolInfoItem(info_root,"DiffProcessing/Scale/CalcRMerge",self["calc_rmerge"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/CalcRMergeZLim",self["calc_rmerge_zlim"])
    SetFloatInfoItem(info_root,"DiffProcessing/Scale/CalcRMergeSigmaRejectLevel",self["calc_rmerge_rej_level"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/Weighting/T1",self["quality_t1"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/Weighting/T2",self["quality_t2"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/Weighting/Mode",self["weight_mode"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/Weighting/Cutoff",self["quality_cutoff"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/Weighting/Global",self["weight_global"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/MaxIterations",self["niter"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/IterationPrecision",self["iterlim"])
    SetBoolInfoItem(info_root,"DiffProcessing/LLFit/BootstrapFlag",self["bootstrap_flag"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/BootstrapIterations",self["bootstrap_iter"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/GuessMode",self["llfit_guess_mode"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/Iterations",self["llfit_iter"])
    SetBoolInfoItem(info_root,"DiffProcessing/LLFit/IterationsUseNonConvergedFlag",self["llfit_iter_conv_flag"])
    SetFloatInfoItem(info_root,"DiffProcessing/LLFit/BFactor",self["llfit_bfac"])
    SetBoolInfoItem(info_root,"DiffProcessing/LLFit/PhantomMode",self["llfit_phantom_mode"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/Algorithm",self["llfit_alg"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/ForceHermMode",self["llfit_force_herm"])
    SetIntInfoItem(info_root,"DiffProcessing/LLFit/RefineScaleMode",self["llref_scaling_mode"])
    SetFloatInfoItem(info_root,"DiffProcessing/ResLim/rlow",self["rlim_low"])
    SetFloatInfoItem(info_root,"DiffProcessing/ResLim/rhigh",self["rlim_high"])
    SetFloatInfoItem(info_root,"DiffProcessing/Refine/ZWeight",self["refine_zweight"])
    SetIntInfoItem(info_root,"DiffProcessing/Refine/CurveFitMode",self["refine_curve_fit_mode"])
    SetFloatInfoItem(info_root,"DiffProcessing/Merge/RSymLimit",self["rsym_limit"])
    
