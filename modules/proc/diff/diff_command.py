#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

from ost.info import *
from iplt.proc.command import *
from iplt.proc.subcommand import *
from diff_init import *
from diff_find_mask import *
from diff_ori import *
from diff_search import *
from diff_extract import *
from diff_integrate import *
from diff_tilt import *
from diff_sym import *
from diff_all import *
from diff_summary import *
from diff_plot import DiffPlot

class DiffCommand(Command):
    def __init__(self):
        Command.__init__(self,"iplt_diff")
        self.AddSubCommand(DiffInit())
        self.AddSubCommand(DiffFindMask())
        self.AddSubCommand(DiffOri())
        self.AddSubCommand(DiffSearch())
        self.AddSubCommand(DiffExtract())
        self.AddSubCommand(DiffIntegrate())
        self.AddSubCommand(DiffTilt())
        self.AddSubCommand(DiffSym())
        self.AddSubCommand(DiffAll())
        self.AddSubCommand(DiffSummary())
        self.AddSubCommand(DiffPlot())
        self.AddOption("-f", "--force",
                       action="store_true",default=False,dest="force_flag",
                       help="force processing even when up-to-date results are present")
        self.SetGlobalImageMask()
        
    def SetGlobalImageMask(self):
        if not os.path.isfile("project.xml"):
            raise Exception("no project.xml found")
        proj_info_ = LoadInfo("project.xml")
        if not proj_info_.Root().HasGroup("DiffProcessing"):
            raise Exception("no DiffProcessing group found in project.xml")
        if not proj_info_.Root().HasItem("DiffProcessing/ImageMask"):
            raise Exception("DiffProcessing group in project.xml missing ImageMask item")
        # global image mask
        self.SetImageMask(proj_info_.Root().GetItem("DiffProcessing/ImageMask").GetValue())
