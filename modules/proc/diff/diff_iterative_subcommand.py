#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------
#
#
# Author: Andreas Schenk, Ansgar Philippsen
#
import os
from ost.img import *
from ost.info import *
from ost.io import *
from iplt.proc.subcommand import *
import time
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.geom import *
import iplt

class DiffIterativeSubcommand(IterativeSubcommand):
    def __init__(self,name,help,logfile):
        IterativeSubcommand.__init__(self,name,help,logfile)
        self.info_handle_=None

    def PreprocessDirectory(self,options):
        if not os.path.isfile(self.GetFilePath("info.xml")):
            self.info_handle_=CreateInfo()
            return

        self.info_handle_=LoadInfo(self.GetFilePath("info.xml"))
        LogInfo("Diff: Reading project.xml")
        project_xml_path=self.GetFilePath("../project.xml")
        if os.path.isfile(project_xml_path):
            proj_info_ = LoadInfo(project_xml_path)
            self.info_handle_.AddDefault(proj_info_)
        else:
            LogInfo("Diff: Skipping non-existing project.xml")

    
    def PostprocessDirectory(self,options):
        if os.path.isfile(self.GetFilePath("info.xml")):
            try:
                self.info_handle_.Export(self.GetFilePath("info.xml"))
            except:
                LogError("Failed to write info.xml") 

    def CheckPreCondition(self,options):
        return os.path.isfile(self.GetFilePath("info.xml"))

    def GetInfoHandle(self):
        return self.info_handle_

    def GetImagePath(self):
        if self.info_handle_.Root().HasItem("DiffProcessing/ImageFileMask"):
            return self.AssembleImagePath(self.info_handle_.Root().GetItem("DiffProcessing/ImageFileMask").GetValue())
        return self.AssembleImagePath(None)


    def GetImageFileType(self):
        if self.info_handle_.Root().HasItem("DiffProcessing/ImageFileType"):
            return self.info_handle_.Root().GetItem("DiffProcessing/ImageFileType")
        return "auto"

    def LoadDatasetImage(self):
        """
        convenience loading method to encapsulate the location and naming
        of the image based on the information in info.xml and project.xml
        """
        return LoadImage(self.GetImagePath())

    def GetBeamStopMask(self):
        gname = "DiffProcessing/BeamStopMask"
        if self.GetInfoHandle().Root().HasGroup(gname):
            ig=self.GetInfoHandle().Root().GetGroup(gname)
        else:
            LogInfo("no beam stop mask defined in project.xml")
            return None
        LogVerbose("Diff: Creating mask from project.xml definition")
        bstop_mask_=InfoToMask(ig)
        return bstop_mask_
    
    def GetExclusionMask(self):
        if self.GetInfoHandle().Root().HasGroup("DiffProcessing/ExclusionMask"):
            LogVerbose("Diff: Creating exclusion mask from project.xml definition")
            ig=self.GetInfoHandle().Root().GetGroup("DiffProcessing/ExclusionMask")
            excl_mask_=InfoToMask(ig)
        else:
            excl_mask_=Mask(Circle2())
        return excl_mask_
            
    def ShiftBeamStopMask(self,mask):
        gname="DiffProcessingResults/DiffFindMask/Shift"
        if self.GetInfoHandle().Root().HasGroup(gname):
            ig=self.GetInfoHandle().Root().GetGroup(gname)
        else:
            print "ShiftMask: no beam stop mask shift defined in info.xml"
            raise Exception(" no beam stop mask shift defined")
        v=Vec2(ig.GetItem("x").AsFloat(),ig.GetItem("y").AsFloat())
        LogVerbose("Diff: Applying mask shift of %s"%(str(v)))
        mask.Shift(v)
        return mask

    def GetTotalMask(self):
        mask = self.GetBeamStopMask()
        if mask:
            mask=self.ShiftBeamStopMask(mask)
            mask.Expand(3)
        else:
            mask = Mask(Circle2())

        mask = mask | self.GetExclusionMask()
        return mask

    def get_lattice_(self,name):
        """
        returns lattice of given name with its timestamp
        """
        if self.GetInfoHandle().Root().HasGroup(name):
            gg=self.info_handle_.Root().GetGroup(name)
            lat = iplt.InfoToLattice(gg)
            tim=time.localtime()
            if gg.HasAttribute("modified"):
                tim = time.strptime(gg.GetAttribute("modified"))
            return (lat,tim)
        return (None,None)
