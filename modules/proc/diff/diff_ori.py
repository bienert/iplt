#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# determine origin of (0,0) reflection in a diff pattern
#
# Authors: Ansgar Philippsen, Andreas Schenk
#

import sys,os,os.path,time,random
from ost.img import *
import ost.info as info
import iplt as ex
import ost.img.alg as tf
from math import  log,exp
from ost import LogVerbose,LogInfo,LogError,LogVerbose

from diff_iterative_subcommand import*

class DiffOri(DiffIterativeSubcommand):
    def __init__(self):
        DiffIterativeSubcommand.__init__(self,"origin","find position of (0,0) reflection","diff_ori.log")
    def ExtendParser(self,command):
        command.AddOption("--fast-origin",
                          action="store_true", default=False,dest="fast_ori",
                          help="faster but less accurate ori determination")
        command.AddOption("--skip-origin",
                          action="store_true", default=False,dest="skip_ori",
                          help="skip origin detection")
    def ProcessDirectory(self,options):
        try:
            reduction=4
            halfwidthratio=0.2
            maxampratio=1.5
            mask=self.GetTotalMask()
            fullimage=self.LoadDatasetImage()
            centerimage=fullimage.Extract(Extent(Size(fullimage.GetExtent().GetSize()[0]/2,fullimage.GetExtent().GetSize()[1]/2),fullimage.GetExtent().GetCenter()))
            shrink=tf.DiscreteShrink(Size(reduction,reduction))
            image=centerimage.Apply(shrink)
#            image=fullimage.Apply(shrink)
            mask.Scale(1.0/float(reduction))
            mask.Expand(3)
            
            #if GetVerbosityLevel()>3:
            #    SaveImage(image,self.GetFilePath("tmp_masked1.tif"))

            stat=alg.Stat()
            bgfit=ex.alg.FitBackground()
            LogVerbose("DiffOri: searching origin on masked image %s"%(str(image.GetExtent())))
            #if GetVerbosityLevel()>3:
            #    tmpimg=image.Copy(False)
            for p in image:
                pos=Point(p).ToVec2()
                if mask.IsOutside(pos):
                    if options.fast_ori and random.random()>0.05: # 5% cutoff
                        continue
                    extimg=image.Extract(Extent(Size(5,5),Point(p)))
                    extimg.Apply(stat)
                    stddev = stat.GetStandardDeviation()
                    if stddev<1.0:
                        stddev=1.0
                    val=image.GetReal(p)
                    bgfit.Add(p[0],p[1],val,1.0/(stddev*stddev))
                    #if GetVerbosityLevel()>3:
                    #    tmpimg.SetReal(p,val)
            #if GetVerbosityLevel()>3:
            #    SaveImage(tmpimg,self.GetFilePath("tmp_masked2.tif"))
            bgfit.Apply()
            s1=bgfit.GetS1()
            s2=bgfit.GetS2()
            b1=bgfit.GetB1()/reduction
            b2=bgfit.GetB2()/reduction
            ori=bgfit.GetOrigin()*reduction
            c=bgfit.GetC()

            g=self.GetInfoHandle().Root().RetrieveGroup("DiffProcessingResults/Origin")
            iit=g.RetrieveItem("x")
            iit.SetFloat(ori[0])
            iit=g.RetrieveItem("y")
            iit.SetFloat(ori[1])
            iit=g.RetrieveItem("S1")
            iit.SetFloat(s1)
            iit=g.RetrieveItem("S2")
            iit.SetFloat(s2)
            iit=g.RetrieveItem("B1")
            iit.SetFloat(b1)
            iit=g.RetrieveItem("B2")
            iit.SetFloat(b2)
            iit=g.RetrieveItem("C")
            iit.SetFloat(c)
            g.SetAttribute("modified",time.asctime())
             
            LogVerbose("Found origin: "+str(ori)+("\nS1:%f B1:%f S2:%f B2:%f C:%f"% (s1,b1,s2,b2,c) ))
            
            mask.Scale(reduction)
            itmp=fullimage-bgfit.AsImage(fullimage.GetExtent());
            itmp.ApplyIP(alg.MaskImage(~mask,False))
            #itmp=CreateImage(fullimage.GetExtent())
            #for p in itmp:
                #if mask.IsInside(p.ToVec2()):
                    #r=Length(p.ToVec2()-ori)
                    #val=fullimage.GetReal(p)-(s1*exp(b1*r)+s2*exp(b2*r)+c)
                    #itmp.SetReal(p,val)
            SaveImage(itmp,self.GetNamedFilePath("_bgcorr.tif"))
        except Exception, e:
            LogError("DiffOri: caught exception: %s"%(str(e)))



    def UpToDate(self,options):
        """return true if origin state is up to date"""
        if options.force_flag:
            return False
        if options.skip_ori:
            return True
        if self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/Origin"):
            g1=self.GetInfoHandle().Root().GetGroup("DiffProcessingResults/Origin")
            g2=self.GetInfoHandle().Root().GetGroup("DiffProcessingResults/DiffFindMask/Shift")
            return self.InfoIsNewer(g1,g2)
        return False

    def CheckPreCondition(self,options):
        """
        returns true if pre-condition is met to run ori search
        """
        if not os.path.isfile(self.GetFilePath("info.xml")):
            LogInfo("DiffOri: missing info.xml")
            return False
        if self.GetInfoHandle().Root().HasGroup("DiffProcessingResults/DiffFindMask/Shift"):
            return True
        else:
            LogInfo("DiffOri: missing mask shift")
            return False


