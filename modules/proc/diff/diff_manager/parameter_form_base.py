#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4 import QtCore
from ost import info

class ParameterFormBase(QWidget):
  infoChanged=QtCore.pyqtSignal()
  
  def __init__(self,parent=None):
    QWidget.__init__(self,parent)
    self.info_=None

  def SetInfo(self,i):
    self.info_=i
    self.UpdateValues()
    
  def showEvent(self,event):
    if self.info_:
      self.UpdateValues()
      
  def UpdateValues(self):
    pass
      
  def BlockChildren(self,b):  
    self.recursive_block(self, b)
    
  def recursive_block(self,obj,b):
		obj.blockSignals(b)
		clist = obj.children()
		if list:
			for cobj in clist:
				if cobj != obj:
				  self.recursive_block(cobj, b)

