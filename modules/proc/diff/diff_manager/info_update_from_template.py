#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from ost import info

# info visitor has to be applied to template to traverse all the groups in the template
class InfoUpdateFromTemplateVisitor(info.InfoVisitor):
  def __init__(self,handle):
    info.InfoVisitor.__init__(self)
    self.handle_=handle
    self.groupnames_=[]
    
  def GetGroupToUpdate(self):
    if len(self.groupnames_)==0:
      return self.handle_.Root()
    path=self.groupnames_[0]
    for name in self.groupnames_[1:]:
      path+='/'+name
    return self.handle_.Root().GetGroup(path)
   
  def VisitGroup(self,group):
    if group.GetName()=='EMDataInfo':
      self.groupnames_.append("")
      return True
    if not self.GetGroupToUpdate().HasGroup(group.GetName()):
      self.GetGroupToUpdate().CreateGroup(group.GetName())
    self.groupnames_.append(group.GetName())
    return True

  def VisitGroupFinish(self,group):
    self.groupnames_.pop()
    
  def VisitItem(self,item):
    if not self.GetGroupToUpdate().HasItem(item.GetName()):
      it=self.GetGroupToUpdate().CreateItem(item.GetName(),item.GetValue())
      it.SetAttribute('type',item.GetAttribute('type'))
      
def UpdateInfoFromTemplate(infofile,template):
  vis=InfoUpdateFromTemplateVisitor(infofile)
  template.Root().Apply(vis)
