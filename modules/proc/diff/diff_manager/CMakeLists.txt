set(IPLT_PROC_DIFF_DIFF_MANAGER_PYMOD_MODULES
__init__.py
diff_manager.py
diff_template_xml.py
extract_form.py
image_table.py
image_tracker.py
info_update_from_template.py
llfit_form.py
merge_table.py
parameter_form_base.py
resolution_limit_form.py
result_browser.py
scale_form.py
search_form.py
sym_plot.py
unit_cell_form.py
)

set(IPLT_DIFF_MANAGER_UI_FILES
ui/diff_manager_form.ui
ui/extract_form.ui
ui/image_table.ui
ui/llfit_form.ui
ui/merge_table.ui
ui/unit_cell_form.ui
ui/search_form.ui
ui/scale_form.ui
ui/resolution_limit_form.ui
)

pymod( NAME diff_manager
       OUTPUT_DIR iplt/proc/diff/diff_manager
       PY ${IPLT_PROC_DIFF_DIFF_MANAGER_PYMOD_MODULES}
       UI ${IPLT_DIFF_MANAGER_UI_FILES}  
     )
