#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------


from PyQt4.QtGui import *
from PyQt4.QtCore import *
from iplt.proc.diff.diff_manager.image_table_ui import Ui_ImageTable
from iplt.proc import dirscan
import os
from ost import info

class ImageTable(QGroupBox,Ui_ImageTable):
  currentImageChanged=pyqtSignal(QString)
  def __init__(self,parent=None):
    QGroupBox.__init__(self,parent)
    self.setupUi(self)
    tooltips=["<P>used in further processing if checked; ignored when unchecked",
               "<P>image id of pattern",
               "<P>RFriedel value",
               "<P>beamstop position was searched",
               "<P>origin was determined",
               "<P>Lattice search was run",
               "<P>Lattice has been verified",
               "<P>Extraction step was run",
               "<P>Integration step was run",
               "<P>Tilt step was run"]
    for i in range(len(tooltips)):
      self.table_.horizontalHeaderItem(i).setToolTip(tooltips[i])
    widths=[40,90,70,20,20,20,20,20,20,20]
    for i in range(len(widths)):
      self.table_.setColumnWidth(i,widths[i])
    pm=QPixmap(8,8)
    pm.fill(Qt.green)
    self.icon_yes_=QIcon(pm)
    pm.fill(Qt.yellow)
    self.icon_no_=QIcon(pm)

  def SetDirMask(self,dirmask):
    self.dirmask_=dirmask
    self.CreateImageList()
    
  def CreateImageList(self):
    dlist = dirscan.dirscan(dirmask=self.dirmask_,wdir='.')
    self.table_.blockSignals(True)
    self.table_.clearContents()
    self.table_.setRowCount(0)
    for d in dlist:
      info_name=os.path.join(d[1],"info.xml")
      if os.path.isfile(info_name):
        row=self.table_.rowCount()
        self.table_.setRowCount(row+1)
        self.table_.setItem(row,1,QTableWidgetItem(d[1]))
        self.CreateImageRow(row)
        self.UpdateImageRow(row)
    self.table_.blockSignals(False)
    self.table_.setCurrentCell(0,0)

  def UpdateImageList(self):
    for i in range(self.table_.rowCount()):
      self.UpdateImageRow(i)
      
  def UpdateCurrentImage(self):
    self.UpdateImageRow(self.table_.currentRow())
      
  def CreateImageRow(self,row):
    image_name=self.table_.item(row,1).text()
    checkbox=QTableWidgetItem()
    checkbox.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled |Qt.ItemIsSelectable)
    self.table_.setItem(row,0,checkbox)
    item=QTableWidgetItem()
    item.setText('')
    self.table_.setItem(row,2,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,3,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,4,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,5,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,6,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,7,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,8,item)
    item=QTableWidgetItem()
    item.setIcon(self.icon_no_)
    self.table_.setItem(row,9,item)

  def UpdateImageRow(self,row):
    self.table_.blockSignals(True)
    image_name=str(self.table_.item(row,1).text())
    if os.path.isfile(os.path.join(image_name,"ignore")):
      self.table_.item(row,0).setCheckState(Qt.Unchecked)
    else:
      self.table_.item(row,0).setCheckState(Qt.Checked)
    image_info=info.LoadInfo(os.path.join(image_name,"info.xml"))
    if image_info.Root().HasItem("DiffProcessingResults/RFriedel"):
      self.table_.item(row,2).setText("%5.4f" % (image_info.Root().GetItem("DiffProcessingResults/RFriedel").AsFloat()))
    else:
      self.table_.item(row,2).setText('')
    if image_info.Root().HasGroup("DiffProcessingResults/DiffFindMask"):
      self.table_.item(row,3).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,3).setIcon(self.icon_no_)
    if image_info.Root().HasGroup("DiffProcessingResults/Origin"):
      self.table_.item(row,4).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,4).setIcon(self.icon_no_)
    if image_info.Root().HasGroup("DiffProcessingResults/InitialLattice"):
      self.table_.item(row,5).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,5).setIcon(self.icon_no_)
    if image_info.Root().HasGroup("DiffProcessingResults/VerifiedLattice"):
      self.table_.item(row,6).setIcon(self.icon_yes_)
      self.table_.item(row,6).setData(Qt.UserRole,QVariant(True))
    else:
      self.table_.item(row,6).setData(Qt.UserRole,QVariant(False))
      self.table_.item(row,6).setIcon(self.icon_no_)
    if os.path.isfile(os.path.join(image_name,image_name+"_ext.mtz")):
      self.table_.item(row,7).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,7).setIcon(self.icon_no_)
    if os.path.isfile(os.path.join(image_name,image_name+"_int.mtz")):
      self.table_.item(row,8).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,8).setIcon(self.icon_no_)
    if os.path.isfile(os.path.join(image_name,image_name+"_tilt.mtz")):
      self.table_.item(row,9).setIcon(self.icon_yes_)
    else:
      self.table_.item(row,9).setIcon(self.icon_no_)
    self.table_.blockSignals(False)

  def onCurrentCellChanged(self,row,column,oldrow,oldcolumn):
    self.currentImageChanged.emit(self.table_.item(row,1).text())

  def onCellChanged(self,row,column):
    if row==0:
      image_name=str(self.table_.item(row,1).text())
      if self.table_.item(row,column).checkState()==Qt.Checked:
        os.remove(os.path.join(image_name,"ignore"))
      else:
        f=open(os.path.join(image_name,"ignore"),'w')
        f.close()

  def GetImageList(self):
    images=[]
    for row in range(self.table_.rowCount()):
      if self.table_.item(row,0).checkState() == Qt.Checked:
        images.append(str(self.table_.item(row,1).text()))
    return images
    
  def GetNonVerifiedImageList(self):
    images=[]
    for row in range(self.table_.rowCount()):
      if self.table_.item(row,0).checkState() == Qt.Checked and \
         not self.table_.item(row,5).data(Qt.UserRole).toBool():
        images.append(str(self.table_.item(row,1).text()))
    return images
