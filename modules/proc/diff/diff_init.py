#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Initialize diffraction processing directory
#
# Author: Andreas Schenk
#

import os,re,os.path
from ost.img import *
from ost.info import *
from iplt.proc.subcommand import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose

class DiffInit(IterativeSubcommand):
    def __init__(self):
      IterativeSubcommand.__init__(self,"init","initialize xml file","diff_init.log")
      self.pinfo_=None
      if os.path.isfile("project.xml"):
          pinfo=LoadInfo("project.xml")
          self.pinfo_=pinfo.Root()
          
    def ProcessDirectory(self,options):
        try:
            re_comp = re.compile("(\D.*?)([0-9]+)")
            id_str = re_comp.match(self.GetWorkingDirectory()).group(2)
            info_handle_=None
            info_xml_path_=self.GetFilePath("info.xml")
            if os.path.exists(info_xml_path_):
                LogVerbose("using existing info file")
                info_handle_ = LoadInfo(info_xml_path_)
            else:
                if os.path.isfile(self.GetFilePath("../info.dtd")):
                    LogVerbose("using existing dtd file")
                    info_handle_ = CreateInfo("../info.dtd")
                else:
                    LogVerbose("creating info.xml from scratch")
                    info_handle_ = CreateInfo()

            root=info_handle_.Root()
            g=root.RetrieveGroup("DiffProcessing")
            idata=g.RetrieveGroup('ImageData')
            iit=idata.RetrieveItem('Id')
            iit.SetInt(int(id_str))
            LogVerbose("setting id to %d"%(int(id_str)))
            iit=idata.RetrieveItem('NominalTiltAngle')
            iit.SetFloat(float(id_str[0:2]))
            LogVerbose("setting nominal tilt to %f"%(float(id_str[0:2])))
            LogVerbose("exporting info.xml to %s"%info_xml_path_)
            info_handle_.Export(info_xml_path_)
        except Exception,e:
            print "caught exception in DiffInit:",e
            
    def UpToDate(self,options):
        if options.force_flag:
            return False
        inff=self.GetFilePath("info.xml")
        if os.path.isfile(inff):
            inf=LoadInfo(inff)
            if inf.Root().HasGroup("DiffProcessing/ImageData"):
                return True
        return False
        
    def CheckPreCondition(self,options):
        if not self.pinfo_:
            LogError("DiffInit: missing project.xml")
            return False

        ifm = None
        if self.pinfo_.HasItem("DiffProcessing/ImageFileMask"):
            ifm = self.pinfo_.GetItem("DiffProcessing/ImageFileMask").GetValue()

        image_path = self.AssembleImagePath(ifm)
        
        if os.path.isfile(image_path):
            return True
        else:
            LogError("DiffInit: missing image: "+image_path)
            return False

    def Cleanup(self,wdir):
        try:
            os.remove(os.path.join(wdir,"info.xml"))
        except OSError:
            pass

        
