#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

"""
plot intensities in mtz file as img
pixel index is hk
value is amplitude
"""

import sys,os,os.path
from ost.img import *
import iplt as ex
import iplt.proc.plot_fit as plot_fit

sys.argv=sys.argv[1:]

if len(sys.argv)<1:
    raise Exception("missing parameter: mtzin [size]")

mtzin=sys.argv[0]

if len(sys.argv)>1:
    size=int(sys.argv[1])
else:
    size=64

rlist = ex.ImportMtz(mtzin)
# origin in middle
img1=CreateImage(Extent(Size(size,size),Point(0,0)))
img2=CreateImage(Extent(Size(size,size),Point(0,0)))

for rp in rlist:
    img1.SetReal(rp.GetIndex().AsDuplet(),rp.Get("fobs"))
    img2.SetReal(rp.GetIndex().AsDuplet(),rp.Get("fobs")/rp.Get("sigfobs"))

SaveImage(img1,"hk_amp.png")
SaveImage(img2,"hk_sigamp.png")
