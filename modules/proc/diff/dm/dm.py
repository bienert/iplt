#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys, os, os.path, string, shutil
from iplt.proc.dirscan import dirscan
from ost.img import *
import iplt as ex
from verify_lattice import *
from edit_mask import *
from view_se import *
from view_tilt import *
from merge_manager import *
from img_params import *
from iplt.proc.manager.welcome import *
from iplt.proc.manager.new_image_wizard import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.info import *

template_xml="""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<EMDataInfo xmlns:iplt="http://iplt.org">
<DiffProcessing>
  <!-- The mask for the directory names - mandatory -->
  <ImageMask value="%(prefix)s"/>

  <!-- the mask for the file name (default is %%%%.tif) - the %%%% pattern will be replace by the current dataset name, as assembled from the ImageMask entry -->
  <ImageFileMask value="%%%%.%(fileformat)s" type="string"/>

  
  <!-- Parameters for the diff_mask step. NOTE: the beamstop mask itself is defined further below -->
  <FindMask>
    <!-- if set to true, reduce size by factor of 2 to speed up calculation -->
    <PreShrink value="0" type="bool"/>
    <!-- if set to true, use only center 50%%, reducing image by (another) factor of 2 -->
    <PreCut value="0" type="bool"/>
    <!-- if present, clips the image to this minimum value prior to running the algorithms -->    
    <MinCutoff value="0.0" type="float"/>
    <!-- 
      Beamstop Mask Detection Algorithm:
      Mode 0 (default) uses a histogram analysis to determine a min max clipping window, which 
      is then used in the cross-correlation with the artificial beamstop image (useful in most cases)
      Mode 1 applies a simply gaussian filter prior to running the CC
      Mode 2 uses local sigma thresholding to extract the beamstop shape (required when strong falloff
      of pixel values across beamstop)
    -->
    <Mode value="0" type="int"/>
    <!-- tweak factor for mode 0, default is 1.2 -->
    <HistoMultiplier value="1.2" type="float"/>
    <!-- tweak factor for mode 2, default is 4 -->
    <LocalSigmaSize value="4" type="int"/>
    <!-- tweak factor for mode 2, default is 8.0 -->
    <LocalSigmaLevel value="8.0" type="float"/>
  </FindMask>

  <!-- Parameters for the 'iplt_diff search' step -->
  <LatticeSearch>
    <!-- apply a gaussian filter to the diff pattern prior to peak search - set to 0.0 to turn this feature off -->
    <GaussFilterStrength value="1.0" type="float"/>
    <!-- minimal and maximal lattice vectors (in pixel) to consider, especially the min value is sometimes needed -->
    <MinimalLatticeLength value="30.0" type="float"/>
    <MaximalLatticeLength value="1.0e4" type="float"/>
    <PeakSearchParams>
      <!-- see the PeakSearch ctor for a description of these parameters -->
      <OuterWindowSize value="10" type="int"/>
      <OuterWindowSensitivity value="0.2" type="float"/>
      <InnerWindowSize value="4" type="int"/>
      <InnerWindowSensitivity value="0.0" type="float"/>
    </PeakSearchParams>
  </LatticeSearch>

  <!-- Parameters for the extraction and integration of intensity profiles, i.e. 'iplt_diff extract' and 'iplt_diff integrate' -->
  <LatticeExtract>
    <!-- half-box size, i.e. total integration box will be 2n+1 pixels large -->
    <BoxSize value="7" type="int"/>
    <!-- if this is greater than zero, a box grow algorithm will be employed to find the optimal box size per peak integration -->
    <BoxGrowAmount value="0" type="int"/>
    <!-- size of the background rim, values of 1 or 2 are best -->
    <BackgroundRimSize value="2" type="int"/>
    <!-- flag to turn on the local background correction based on the gaussian fit - recommended -->
    <BackgroundSubtract value="1" type="bool"/>
    <!-- flag to turn on the fit of the spiral and barrel distortion during lattice refinement - not recommended for good datasets-->
    <RefineLatticeDistortions value="0" type="bool"/>
  </LatticeExtract>

  <!-- strong filter for the reflections that are used in the lattice refinement -->
  <StrongFilter>
    <MinimumQuality value="2" type="float"/>
    <MaximumOffset value="2" type="float"/>
    <MaximumHalfWidth value="7.0" type="float"/>
    <MaximumElipticalRatio value="2" type="float"/>
  </StrongFilter>

  <!-- weak filter for the reflections that are subjected to integration -->
  <WeakFilter>
    <MinimumQuality value="1.2" type="float"/>
    <MaximumOffset value="5" type="float"/>
  </WeakFilter>

  <!-- Parameters for 'iplt_diff sym' -->
  <Symmetrize>
    <!-- 0 = do not merge Friedel mates; 1 = merge Friedel mates, keep singular reflections; 2 = merge Friedel mates, throw out singular ones; 3 = MRC style Friedel merging -->
    <FriedelMode value="0" type="int"/>
  </Symmetrize>

  <!-- unit cell parameters and symmetry -->
  <OrigUnitCell>
   <unitcell type="spatial">
    <a value="%(ucell_a)f" type="float"/>
    <b value="%(ucell_b)f" type="float"/>
    <gamma value="%(ucell_g)f" type="float"/>
    <thickness value="%(ucell_c)f" type="float"/>
    <symmetry value="%(spacegroup)s"/>
   </unitcell>
  </OrigUnitCell>

  <!-- pixel sampling of the images in reciprocal angstrom - not really needed -->
  <Sampling value="4.24e-4" type="float"/>

  <!-- global resolution limit of the processing, for individual iplt_diff steps as well as iplt_diff_merge -->
  <ResLim>
   <rlow value="100" type="float"/>
   <rhigh value="2" type="float"/>
  </ResLim>

  <!-- Parameters for the scaling -->
  <Scale>
    <!-- resolution limits -->
    <Resolution>
      <low value="40" type="float"/>
      <high value="2.5" type="float"/>
    </Resolution>
    <!-- number of bins for the weighted bin scaling -->
    <BinCount value="16" type="int"/>
    <!-- cutoffs for the filtering step after scaling, based on the weighted bins -->
    <WBinISigICutoff value="0.1" type="float"/>
    <WBinSigmaCutoff value="10.0" type="float"/>
    <!-- minimum tilt angle (in degree) for which to invoke an anisotropic pre-scaling-->
    <AnisoMinTilt value="90" type="float"/>
  </Scale>

  <!-- Parameters for the lattice line fitting -->
  <LLFit>
    <!-- experimental weighting algorithms, use mode 1 and cutoff 0.0 for all practical purposes -->
    <Weighting>
      <T1 value="10.0" type="float"/>
      <T2 value="40.0" type="float"/>
      <Mode value="1" type="int"/>
      <Cutoff value="0.0" type="float"/>
    </Weighting>
    <!-- iteration limit for the internal non-linear fitting routine -->
    <MaxIterations value="1000" type="int"/>
    <!-- iteration precision for the internal non-linear fitting routine -->
    <IterationPrecision value="1e-2" type="float"/>
    <!-- bootstrapping mode for improved sigma generation, recommended -->
    <BootstrapFlag value="1" type="bool"/>
    <BootstrapIterations value="20" type="int"/>
    <!-- initial guess mode prior to fitting; 0=from weighted bins, 1=random phase, unit amplitude, 2=totally random-->
    <GuessMode value="0" type="int"/>
    <!-- multiple iterations with different starting values, not as powerful as bootstrapping, best kept at 1 -->
    <Iterations value="1" type="int"/>
    <!-- phantom mode includes two additional values at +- sigma z* -->
    <PhantomMode value="0" type="bool"/>
  </LLFit>

  <!-- Parameters for the post llfit refinement -->
  <Refine>
    <!-- lattice line curve fitting mode 0 (based on Imax and z*max) or 1 (bases on sigmaI and sigmaz*) -->
    <CurveFitMode value="0" type="int"/>
    <!-- zstar fudge factor, experimental -->
    <ZWeight value="8.0" type="float"/>
    <!-- flag to turn intensity scale refinement on or off -->
    <RefineScale value="1" type="bool"/>
    <!-- flag to turn tilt geometry refinement on or off -->
    <RefineTilt value="1" type="bool"/>
    <!-- resolution limits to determine new tilt geometry -->
    <RefineTiltRLow value="10" type="float"/>
    <RefineTiltRHigh value="4" type="float"/>     
  </Refine>


  <!-- This will be created automatically by the 'giplt_diff edit_mask' command. Remove the tag if not needed -->
  <BeamStopMask>
  </BeamStopMask>

  </DiffProcessing>
</EMDataInfo>
"""

class ImageTracker(QWidget):
    def __init__(self,im_list,wdir,parent=None):
        QWidget.__init__(self,parent)
        self.im_list_=im_list
        self.current_=-1
        self.wdir_=wdir
        vb=QVBoxLayout()
        self.nb_=QPushButton("Next Pattern")
        QObject.connect(self.nb_,SIGNAL("clicked()"),self.OnNext)
        vb.addWidget(self.nb_)
        self.pb_=QPushButton("Previous Pattern")
        QObject.connect(self.pb_,SIGNAL("clicked()"),self.OnPrev)
        vb.addWidget(self.pb_)
        self.jb_=QPushButton("Jump to Pattern")
        QObject.connect(self.jb_,SIGNAL("clicked()"),self.OnJump)
        vb.addWidget(self.jb_)
        self.setLayout(vb)
    def OnNext(self):
        self.current_+=1
        self.check_vis()
        if self.current_>=len(self.im_list_):
            self.current_=len(self.im_list_)-1
            return
        if os.path.isfile(os.path.join(self.wdir_,self.im_list_[self.current_][0],"ignore")):
            self.OnNext()
        else:
            self.emit(SIGNAL("LoadImage"),self.im_list_[self.current_])
    def OnPrev(self):
        self.current_-=1
        self.check_vis()
        if self.current_<0:
            self.current_=0
            return
        if os.path.isfile(os.path.join(self.wdir_,self.im_list_[self.current_][0],"ignore")):
            self.OnPrev()
        else:
            self.emit(SIGNAL("LoadImage"),self.im_list_[self.current_])

    def OnJump(self):
        (name,ok)=QInputDialog.getText(self,"Jump to Image","Image Name:")
        if not ok:
          return
        self.DoJump(name)

    def DoJump(self,name):
        i=0
        for (imname,filename) in self.im_list_:
            if name == imname:
                if os.path.isfile(os.path.join(self.wdir_,self.im_list_[i][0],"ignore")):
                    return
                self.current_=i
                self.check_vis()
                print self.im_list_[self.current_]
                self.emit(SIGNAL("LoadImage"),self.im_list_[self.current_])
                return 
            i+=1
        msg_box=QMessageBox()
        msg_box.setText("The image %s could not be found." % (name) )
        msg_box.exec_()
            
    def check_vis(self):
        if self.current_+1>=len(self.im_list_):
            self.nb_.setDisabled(True)
        else:
            self.nb_.setEnabled(True)
        if self.current_<=0:
            self.pb_.setDisabled(True)
        else:
            self.pb_.setEnabled(True)
        

class ImageItem(QListWidgetItem):
    def __init__(self,name,loc,parent):
        QListWidgetItem.__init__(self,name,parent)
        self.name_=name
        self.loc_=loc

class DiffManager(QWidget):
    def __init__(self,wdir):
        QWidget.__init__(self,None)

        
        self.setWindowTitle("IPLT Diff Manager")
        self.wdir_=wdir
        if not os.path.isfile(os.path.join(self.wdir_,"project.xml")):
            welcomedialog=WelcomeDialog(self,template_xml)
            if not welcomedialog.exec_():
              print "missing project.xml in %s"%self.wdir_
              sys.exit(-1)
            else:
              self.wdir_=os.getcwd()

        self.pinfo_ = LoadInfo("project.xml")

        self.params_=ImgParams(self.pinfo_)
        
        self.imask_ = GetStringInfoItem(self.pinfo_.Root(),
                                        "DiffProcessing/ImageMask",
                                        "")

        if self.imask_=="":
            print "missing ImageMask tag in project.xml"
            sys.exit(-1)

        self.ifmask_ = GetStringInfoItem(self.pinfo_.Root(),
                                         "DiffProcessing/ImageFileMask",
                                         "%%.tif")

        pm=QPixmap(8,8)
        pm.fill(Qt.green)
        self.icon_yes_=QIcon(pm)
        pm.fill(Qt.yellow)
        self.icon_no_=QIcon(pm)

        self.verify_lattice_view_=None
        self.edit_masks_view_=None
        self.view_se_=None
        self.view_tilt_=None

        top = QVBoxLayout()
        top.addWidget(self.build_genview())
        top.addWidget(self.build_imview())
        top.addWidget(self.build_mergeview())
        self.setLayout(top)


        self.image_managers_={}

    def OnImageItemClick(self,item):
        self.w_im_label_.setText("location: %s"%item.loc_)
        
    def OnMergeItemClick(self,item):
        (count,ok) = item.data(Qt.UserRole).toInt()
        self.merge_stack_.setCurrentIndex(count)
        
    def OnImageItemDoubleClick(self,item):
        self.w_im_label_.setText("location: %s"%item.loc_)
        if item.loc_ in self.image_managers_:
            self.image_managers_[item.loc_].show()
        else:
            dim = ImageManager(item.name_,item.loc_,None)
            dim.show()
            self.image_managers_[item.loc_]=dim
        
    def OnItem(self,item_new,item_old):
        dataset_id=str(item_new.text(1))
        self.w_im_label_.setText(dataset_id)
        if item_new.icon(2).cacheKey()==self.icon_yes_.cacheKey():
            if item_new.icon(3).cacheKey()==self.icon_yes_.cacheKey():
                self.b_verify_lattice_.setText("View Lattice and BeamStop Mask")
            else:
                self.b_verify_lattice_.setText("Verify Lattice and BeamStop Mask")
        else:
            self.b_verify_lattice_.setText("Set Lattice and BeamStop Mask")

        self.b_view_extract_.setEnabled(item_new.icon(4).cacheKey()==self.icon_yes_.cacheKey())
        self.b_view_tilt_.setEnabled(item_new.icon(6).cacheKey()==self.icon_yes_.cacheKey())

    def OnVerifyAllLattices(self):
        self.im_list_w_=ImageTracker(self.im_list_,self.wdir_)
        self.verify_lattice_view_=VerifyLattice(self.pinfo_,self.im_list_w_)
        QObject.connect(self.im_list_w_,SIGNAL("LoadImage"),self.OnLoadVerifiedLattice)
        self.im_list_w_.DoJump(self.w_im_label_.text())

    def OnLoadVerifiedLattice(self,im_list_entry):
        self.verify_lattice_view_.Load(self.wdir_,im_list_entry[0])

    def OnEditMasks(self):
        if not self.edit_masks_view_:
            self.edit_masks_view_=EditMask(self.pinfo_)
        base=str(self.w_im_label_.text())
        self.edit_masks_view_.Load(self.wdir_,base)

    def OnVerifyLattice(self):
        if not self.verify_lattice_view_:
            self.verify_lattice_view_=VerifyLattice(self.pinfo_)
        base=str(self.w_im_label_.text())
        self.verify_lattice_view_.Load(self.wdir_,base)

    def OnViewExtract(self):
        if not self.view_se_:
            self.view_se_=ViewSearchAndExtract(self.pinfo_)
        base=str(self.w_im_label_.text())
        self.view_se_.Load(self.wdir_,base)

    def OnViewTilt(self):
        if not self.view_tilt_:
            self.view_tilt_=ViewTilt(self.pinfo_)
        base=str(self.w_im_label_.text())
        self.view_tilt_.Load(self.wdir_,base)

    def OnReloadParams(self):
        self.pinfo_=LoadInfo("project.xml")
        self.params_.LoadParameters(self.pinfo_)
        self.params_.UpdateValues()

    def OnSaveParams(self):
        pass
        
    def build_genview(self):
        top=QHBoxLayout()
        
        tw=QTabWidget()
        sa=QScrollArea()
        sa.setWidget(CellParams(self.params_["ucell"]))
        tw.addTab(sa,"Unit Cell")

        sp=ManagerParamsForm("Overall Resolution Limits",self.params_)
        sp.Add("Low Resolution Limit",
               sp.DoubleSpinBox("rlow",2,1.0,1000.0,"A",1.0))
        sp.Add("High Resolution Limit",
               sp.DoubleSpinBox("rhigh",2,1.0,1000.0,"A",0.1))
        sp.setToolTip("<P>Hard resolution limit for the entire dataset, reflections outside will be removed during initial merge or any other subsequent step")
        sa=QScrollArea()
        sa.setWidget(sp)
        tw.addTab(sa,"Resolution Limits")

        top.addWidget(tw)
        vb=QVBoxLayout()
        but=QPushButton("Add new images")
        QObject.connect(but,SIGNAL("clicked()"),self.OnAddImages)
        vb.addWidget(but)
        but=QPushButton("Verify Lattices")
        QObject.connect(but,SIGNAL("clicked()"),self.OnVerifyAllLattices)
        vb.addWidget(but)
        gb=QGroupBox("Tasks")
        gb.setLayout(vb)
        top.addWidget(gb)
        top.addStretch(1)
        topw=QGroupBox("Project Settings and Tasks")
        topw.setLayout(top)
        return topw

    def build_imview(self):

        # all of these must be available before build list
        pattern_gb=QGroupBox("Individual Pattern Views")
        vb=QVBoxLayout()
        hb=QHBoxLayout()
        hb.addWidget(QLabel("Selected:"))
        self.w_im_label_ = QLabel("")
        hb.addWidget(self.w_im_label_)
        vb.addLayout(hb,Qt.AlignCenter)
        self.b_edit_masks_=QPushButton("Edit masks")
        QObject.connect(self.b_edit_masks_,SIGNAL("clicked()"),self.OnEditMasks)
        vb.addWidget(self.b_edit_masks_)
        self.b_verify_lattice_=QPushButton("Verify Lattice")
        QObject.connect(self.b_verify_lattice_,SIGNAL("clicked()"),self.OnVerifyLattice)
        vb.addWidget(self.b_verify_lattice_)
        self.b_view_extract_=QPushButton("View Extract Results")
        QObject.connect(self.b_view_extract_,SIGNAL("clicked()"),self.OnViewExtract)
        vb.addWidget(self.b_view_extract_)
        self.b_view_tilt_=QPushButton("View Tilt/z* Results")
        QObject.connect(self.b_view_tilt_,SIGNAL("clicked()"),self.OnViewTilt)
        vb.addWidget(self.b_view_tilt_)
        pattern_gb.setLayout(vb)
        
        # now assemble the list of patterns
        self.w_im_dirs_=self.build_imlist()

        bl=QHBoxLayout()
        bl.addWidget(self.w_im_dirs_)
        
        vb=QVBoxLayout()
        vb.addWidget(pattern_gb)
        vb.addStretch(1)
        bl.addLayout(vb)

        vb=QVBoxLayout()
        vb.addWidget(self.params_,1)
        
        lbut = QPushButton("Reload from info.xml")
        QObject.connect(lbut,SIGNAL("clicked()"),self.OnReloadParams)
        sbut = QPushButton("Save to info.xml")
        QObject.connect(sbut,SIGNAL("clicked()"),self.OnSaveParams)
        hb = QHBoxLayout()
        hb.addWidget(lbut)
        hb.addWidget(sbut)
        vb.addLayout(hb)
        
        bl.addLayout(vb)

        overall_bg=QGroupBox("Patterns")
        overall_bg.setLayout(bl)
        
        return overall_bg

    def build_imlist(self):
        
        tw=QTreeWidget(self)

        hlab=[["use","<P>used in further processing if checked; ignored when unchecked"],
              ["id","<P>image id of pattern"],
              ["S","<P>Lattice search was run"],
              ["V","<P>Lattice has been verified"],
              ["E","<P>Extraction step was run"],
              ["I","<P>Integration step was run"],
              ["T","<P>Tilt step was run"]]

        # this is a hack around a bug in setHeaderItem
        hlabs=[]
        for hh in hlab:
            hlabs.append(hh[0])
        tw.setHeaderLabels(hlabs)
        hitem=tw.headerItem()
        for hh in range(len(hlab)):
            hitem.setToolTip(hh,hlab[hh][1])

        dlist = dirscan.dirscan(dirmask=self.imask_,wdir=self.wdir_)
        self.im_list_=[]
        for d in dlist:
            iname=os.path.join(d[0],d[1],string.replace(self.ifmask_,"%%",d[1]))
            if os.path.isfile(iname):
                self.im_list_.append([d[1],iname])
                ti=QTreeWidgetItem()
                ti.setText(1,d[1])
                if os.path.isfile(os.path.join(d[0],d[1],"ignore")):
                    ti.setCheckState(0,Qt.Unchecked)
                    ti.setTextColor(1,Qt.gray)
                else:
                    ti.setCheckState(0,Qt.Checked)
                iinfo_file=os.path.join(d[0],d[1],"info.xml")
                ti.setIcon(2,self.icon_no_)
                ti.setIcon(3,self.icon_no_)
                ti.setIcon(4,self.icon_no_)
                ti.setIcon(5,self.icon_no_)
                ti.setIcon(6,self.icon_no_)
                if os.path.isfile(iinfo_file):
                    iinfo=LoadInfo(iinfo_file)
                    if iinfo.Root().HasGroup("DiffProcessingResults/InitialLattice"):
                        ti.setIcon(2,self.icon_yes_)
                    if iinfo.Root().HasGroup("DiffProcessingResults/VerifiedLattice"):
                        ti.setIcon(3,self.icon_yes_)
                    if os.path.isfile(os.path.join(d[0],d[1],d[1]+"_ext.mtz")):
                        ti.setIcon(4,self.icon_yes_)
                    if os.path.isfile(os.path.join(d[0],d[1],d[1]+"_int.mtz")):
                        ti.setIcon(5,self.icon_yes_)
                    if os.path.isfile(os.path.join(d[0],d[1],d[1]+"_tilt.mtz")):
                        ti.setIcon(6,self.icon_yes_)
                tw.addTopLevelItem(ti)

        QObject.connect(tw,SIGNAL("currentItemChanged(QTreeWidgetItem *,QTreeWidgetItem *)"),self.OnItem)
        tw.setCurrentItem(tw.topLevelItem(0))
        for i in range(8):
            tw.resizeColumnToContents(i)
        return tw

    def build_mergeview(self):
        mergedirs=[]
        for name in os.listdir(self.wdir_):
            mdir = os.path.join(self.wdir_, name,"merge_00.mtz")
            if os.path.isfile(mdir):
                mergedirs.append([name,int(os.path.getmtime(mdir))])

        # latest changes on top
        mergedirs.sort(lambda x,y: y[1]-x[1])

        w_merge_dirs=QListWidget(self)
        self.merge_stack_=QStackedWidget()
        count=0
                
        for entry in mergedirs:
            name=entry[0]
            li=QListWidgetItem(name)
            li.setData(Qt.UserRole,QVariant(count))
            w_merge_dirs.addItem(li)
            dmm = MergeManager(name,os.path.join(self.wdir_,name),self.pinfo_,None)
            QObject.connect(dmm,SIGNAL("DataClicked(double,double,double,double,double,const QString&)"),self.OnDataClicked)
            self.merge_stack_.addWidget(dmm)
            count+=1

        QObject.connect(w_merge_dirs,SIGNAL("itemClicked(QListWidgetItem*)"),self.OnMergeItemClick)
        if count>0:
            w_merge_dirs.setCurrentRow(0)
        left_l=QVBoxLayout()
        left_l.addWidget(w_merge_dirs,1)
        left_l.addWidget(QLabel("Info:"))
        self.w_merge_label_ = QLabel("")
        left_l.addWidget(self.w_merge_label_)

        top=QHBoxLayout()
        top.addLayout(left_l,1)
        top.addWidget(self.merge_stack_,1)
        
        overall_gb=QGroupBox("Merge Directories")
        overall_gb.setLayout(top)
        
        return overall_gb

    def OnDataClicked(self,x,y,ex,ey,q,inf):
      items=self.w_im_dirs_.findItems(inf,Qt.MatchContains,1)
      if len(items)>0:
        self.raise_()
        self.w_im_dirs_.setCurrentItem(items[0])
      LogVerbose("dm data clicked:%f,%f,%f,%f,%f,%s" % (x,y,ex,ey,q,str(inf)))

    def OnAddImages(self):
      imagewizard=NewImageWizard(self.imask_,4,string.replace(self.ifmask_,"%%.",""))
      if imagewizard.exec_():
        for (filename,newdirname,newfilename,convert) in imagewizard.GetFileList():
          os.mkdir(newdirname)
          if convert:
            itmp=LoadImage(str(filename))
            SaveImage(itmp,str(newdirname+"/"+newfilename))
          else:
            shutil.copyfile(filename,newdirname+"/"+newfilename)
        #todo run init
