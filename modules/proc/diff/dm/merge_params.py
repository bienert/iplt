#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys, os, os.path, math
from ost.img import *
import iplt as ex
from iplt.proc.diff.diff_merge_base import *
from iplt.proc.manager.manager_params import *

class MergeParams(QTabWidget,DiffMergeBase):
    def __init__(self,pinfo,parent=None):
        QTabWidget.__init__(self,parent)
        DiffMergeBase.__init__(self)
        self.LoadParameters(pinfo)

        self.addTab(self.build_general_params(),"General")
        self.addTab(self.build_scale_params(),"Scaling")
        self.addTab(self.build_llfit_params(),"Lattice Line Fitting")

    def build_general_params(self):
        sa=QScrollArea()
        cp=CellParams(self["ucell"],self)
        QObject.connect(self,SIGNAL("UpdateValues"),cp.OnUpdateValues)
        sa.setWidget(cp)
        return sa
        
    def build_scale_params(self):
        df=ManagerParamsForm("Scaling",self)
        df.Add("Low Resolution Cutoff",
               df.DoubleSpinBox("scale_rlow",1,1.0,1000.0,"A",1.0))
        df.Add("High Resolution Cutoff",
               df.DoubleSpinBox("scale_rhigh",1,1.0,1000.0,"A",0.1))
        df.Add("Resolution Bins",
               df.IntSpinBox("scale_bcount",3,50,"",1))
        df.setToolTip("<P>The scaling algorithms assembles N bins in the given resolution range, in equal based distances of 1/r^2, which are then matched to a reference assembled in the same way")
        QObject.connect(self,SIGNAL("UpdateValues"),df.OnUpdateValues)
        sa=QScrollArea()
        sa.setWidget(df)
        return sa

    def build_llfit_params(self):

        df=ManagerParamsForm("Lattice Line Fitting",self)

        df.Add("Enable Bootstrapping",
               df.CheckBox("bootstrap_flag"),
               "Enable a Bootstrapping protocol for each lattice line fit")
        df.Add("Bootstrapping Iterations",
               df.IntSpinBox("bootstrap_iter",1,10000,"",5),
               "If bootstrapping is enabled, run this many iterations per lattice line")
        df.Add("Maximal Fit Iterations",
               df.IntSpinBox("niter",1,10000,"",10),
               "Maximal number of nonlinear fit iterations, may be less if iteration precision limit is reached")
        df.Add("Fit Iteration Precision",
               df.DoubleValue("iterlim"),
               "Precision of non-linear fit; stops iterations if relativ and absolute changes are smaller than this value")

        df.AddChoiceBox("llfit_guess_mode","Guess Mode",
                        ["(0) Amplitude: Bin Average\n    Phase: random",
                         "(1) Amplitude: 1.0\n    Phase: random",
                         "(2) Amplitude and Phase random "])

        df.AddChoiceBox("llfit_alg", "LLFit Algorithm",
                        ["(0) Sum of Sincs Squared",
                         "(1) Sum of Half-Spaced Sincs"])

        QObject.connect(self,SIGNAL("UpdateValues"),df.OnUpdateValues)
        sa=QScrollArea()
        sa.setWidget(df)
        return sa

    def UpdateValues(self):
        self.emit(SIGNAL("UpdateValues"),self.pdict_)
