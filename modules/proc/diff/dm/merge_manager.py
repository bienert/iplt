#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys, os, os.path, time
from ost.img import *
import iplt as ex
import iplt.ex.alg
from iplt.proc.dirscan import dirscan
import iplt.proc.llviewer as llv
from wbin_scaler import *
from merge_params import *
from ost import LogVerbose,LogInfo,LogError,LogVerbose
from ost.info import *

class MergeCycle(QWidget):
    def __init__(self,data,parent=None):
        QWidget.__init__(self,parent)
        self.load_data(data)
        self.build()

    def load_data(self,data):
        split_by_id=iplt.ex.alg.RListSplitByProperty("id")
        self.scale_rlist_=None
        self.scale_im_={}
        if data[0]:
            self.scale_rlist_=ex.ImportMtz(data[0])
            if self.scale_rlist_.HasProperty("id"):
                self.scale_rlist_.Apply(split_by_id)
                self.scale_im_=split_by_id.GetRListMap()

        self.tgref_rlist_=None
        self.tgref_im_={}
        if data[1]:
            self.tgref_rlist_=ex.ImportMtz(data[1])
            if self.tgref_rlist_.HasProperty("id"):
                self.tgref_rlist_.Apply(split_by_id)
                self.tgref_im_=split_by_id.GetRListMap()

        self.llfit_rlist_=None
        self.llfit_im_={}
        if data[2]:
            self.llfit_rlist_=ex.ImportMtz(data[2])
            if self.llfit_rlist_.HasProperty("id"):
                self.llfit_rlist_.Apply(split_by_id)
                self.llfit_im_=split_by_id.GetRListMap()

        
    def build(self):
        tw=QTableWidget(self)
        tw.setDragDropMode(QAbstractItemView.NoDragDrop)
        tw.setDragEnabled(False)
        tw.setSelectionMode(QAbstractItemView.SingleSelection)
        tw.setSelectionBehavior(QAbstractItemView.SelectRows)
        tw.setEditTriggers(QAbstractItemView.NoEditTriggers)

        tw.setRowCount(len(self.scale_im_))
        tw.setColumnCount(4)
        tw.setHorizontalHeaderLabels(["pattern id","scale","tgref","llfit"])
        
        n=0
        for item in self.scale_im_:
            ti=QTableWidgetItem("%06d" % item.key())
            ti.setTextAlignment(Qt.AlignLeft)
            tw.setItem(n,0,ti)
            ti=QTableWidgetItem(str(item.data().NumReflections()))
            ti.setTextAlignment(Qt.AlignCenter)
            tw.setItem(n,1,ti)
            if self.tgref_rlist_ and item.key() in self.tgref_im_:
                ti=QTableWidgetItem(str(self.tgref_im_[item.key()].NumReflections()))
                ti.setTextAlignment(Qt.AlignCenter)
                tw.setItem(n,2,ti)
            if self.llfit_rlist_ and item.key() in self.llfit_im_:
                ti=QTableWidgetItem(str(self.llfit_im_[item.key()].NumReflections()))
                ti.setTextAlignment(Qt.AlignCenter)
                tw.setItem(n,3,ti)
            n+=1

        tw.resizeRowsToContents()
        tw.resizeColumnsToContents()
        
        vb=QVBoxLayout()
        vb.addWidget(tw)
        vb.setStretchFactor(tw,1)
        self.setLayout(vb)
 
class MergeManagerLoadButton(QPushButton):
    def __init__(self,cycle,text,parent=None):
        QPushButton.__init__(self,text,parent)
        self.cycle_=cycle
        QObject.connect(self,SIGNAL("clicked()"),self.OnClick)
        
    def OnClick(self):
        self.emit(SIGNAL("OnClick"),self,self.cycle_)


class MergeManager(QWidget):
    def __init__(self,name,loc,pinfo,parent):
        QWidget.__init__(self,parent)
        self.pinfo_=pinfo
        self.loc_=loc
        self.name_=name
        self.setWindowTitle(name)
        self.merge00_file_=os.path.join(self.loc_,"merge_00.mtz")
        if not os.path.isfile(self.merge00_file_):
            return

        self.load_info()
        
        self.merge00_=None
        self.im_merge00_=None
        self.scaler_=None
        self.data_=[[None,None,None,None,None]]
        self.llv_={}

        top=QHBoxLayout()
        self.tw_=QTabWidget(self)
        self.tw_.addTab(self.build_overview(),"Overview")
        top.addWidget(self.tw_)
        vb=QVBoxLayout()
        self.params_=MergeParams(self.info_)
        vb.addWidget(self.params_,1)
        
        lbut = QPushButton("Reload from info.xml")
        QObject.connect(lbut,SIGNAL("clicked()"),self.OnReloadParams)
        sbut = QPushButton("Save to info.xml")
        QObject.connect(sbut,SIGNAL("clicked()"),self.OnSaveParams)
        hb = QHBoxLayout()
        hb.addWidget(lbut)
        hb.addWidget(sbut)

        vb.addLayout(hb)
        top.addLayout(vb)
        self.setLayout(top)

    def OnReloadParams(self):
        self.load_info()
        self.params_.LoadParameters(self.info_)
        self.params_.UpdateValues()

    def OnSaveParams(self):
        iinfo_file=os.path.join(self.loc_,"info.xml")
        #self.params_.SaveParameters(iinfo_file)
        #self.OnReloadParams()
        
    def OnScaleButton(self):
        split_by_id=iplt.ex.alg.RListSplitByProperty("id")
        if not self.im_merge00_:
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor));
            self.get_merge00().Apply(split_by_id)
            self.im_merge00_=split_by_id.GetRListMap()
            QApplication.restoreOverrideCursor();
        if not self.scaler_:
            self.scaler_=WBinScaler(self.get_merge00(),self.im_merge00_)
            self.scaler_.setWindowTitle("WBin Scaling %s"%self.name_)
        self.scaler_.show()
        
    def OnLoadButtonPress(self,but,cycle):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor));
        dmc=MergeCycle(self.data_[cycle])
        self.tw_.addTab(dmc,"cycle %02d"%cycle)
        QApplication.restoreOverrideCursor();
        but.setText("Data Loaded")
        but.setDisabled(True)

    def OnShowLLFit(self,but,cycle):
        if self.llv_[cycle]:
            if self.llv_[cycle].hidden():
                self.llv_[cycle].show()
            return
        if self.data_[cycle][2]:
            LogVerbose("importing datasets for llviewer")
            rlist=ex.ImportMtz(self.data_[cycle][2])
            dlist=ex.ImportMtz(self.data_[cycle][3])
            clist=ex.ImportMtz(self.data_[cycle][4])
            
            LogVerbose("starting llviewer")
            self.llv_[cycle]=llv.LatticeLineViewer(rlist,dlist,clist)
            QObject.connect(self.llv_[cycle],SIGNAL("DataClicked(double,double,double,double,double,const QString&)"),self,SIGNAL("DataClicked(double,double,double,double,double,const QString&)"))

            self.llv_[cycle].show()

    def get_merge00(self):
        if not self.merge00_:
            self.merge00_=ex.ImportMtz(self.merge00_file_)
        return self.merge00_

    def load_info(self):
        iinfo_file=os.path.join(self.loc_,"info.xml")
        if os.path.isfile(iinfo_file):
            self.info_=LoadInfo(iinfo_file)
        else:
            self.info_=CreateInfo()
        self.info_.AddDefault(self.pinfo_)

    def build_overview(self):
        cycle=0
        topl=QVBoxLayout()
        hb=QHBoxLayout()
        but=QPushButton("WBin Scaling")
        QObject.connect(but,SIGNAL("clicked()"),self.OnScaleButton)
        hb.addWidget(but)
        hb.addStretch(1)
        topl.addLayout(hb)

        gl=QGridLayout()
        cont=True
        while(cont):
            cont=False
            scale_file=os.path.join(self.loc_,"merge_scaled%02d.mtz"%cycle)
            if os.path.isfile(scale_file):
                gl.addWidget(QLabel("scale"),cycle*4,1)
                gl.addWidget(QLabel(time.ctime(os.path.getmtime(scale_file))),cycle*4,2)
                self.data_[cycle][0]=scale_file
                cont=True
            tgref_file=os.path.join(self.loc_,"merge_tgref%02d.mtz"%cycle)
            if os.path.isfile(tgref_file):
                gl.addWidget(QLabel("tgref"),cycle*4+1,1)
                gl.addWidget(QLabel(time.ctime(os.path.getmtime(tgref_file))),cycle*4+1,2)
                self.data_[cycle][1]=tgref_file
                cont=True
            llfit_file=os.path.join(self.loc_,"merge_llfit%02d.mtz"%cycle)
            if os.path.isfile(llfit_file):
                gl.addWidget(QLabel("llfit"),cycle*4+2,1)
                gl.addWidget(QLabel(time.ctime(os.path.getmtime(llfit_file))),cycle*4+2,2)
                self.data_[cycle][2]=llfit_file
                self.data_[cycle][3]=os.path.join(self.loc_,"merge_disc%02d.mtz"%cycle)
                self.data_[cycle][4]=os.path.join(self.loc_,"merge_curve%02d.mtz"%cycle)
                cont=True
            if cont:
                gl.addWidget(QLabel("cycle %02d :"%cycle),cycle*4,0)
                but=MergeManagerLoadButton(cycle,"Load Data")
                gl.addWidget(but,cycle*4+1,0)
                QObject.connect(but,SIGNAL("OnClick"),self.OnLoadButtonPress)
                but=MergeManagerLoadButton(cycle,"Show LLFit")
                gl.addWidget(but,cycle*4+2,0)
                QObject.connect(but,SIGNAL("OnClick"),self.OnShowLLFit)
                gl.setRowMinimumHeight(cycle*4+3,10)
                self.llv_[cycle]=None
                self.data_.append([None,None,None,None,None])
            cycle+=1

        ov=QWidget(self)
        topl.addLayout(gl)
        ov.setLayout(topl)
        sa=QScrollArea(self)
        sa.setWidget(ov)
        return sa

