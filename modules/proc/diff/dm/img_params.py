#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

#
# Author: Ansgar Philippsen
#

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys, os, os.path, math
from ost.img import *
import iplt as ex
from iplt.proc.diff.diff_img_base import *
from iplt.proc.manager.manager_params import *

class ImgParams(QTabWidget,DiffImgBase):
  def __init__(self,pinfo,parent=None):
    QTabWidget.__init__(self,parent)
    DiffImgBase.__init__(self)
    self.LoadParameters(pinfo)
    self.addTab(self.build_search_params(), "Lattice Search")
    self.addTab(self.build_extract_params(), "Lattice Extract/Integrate")

  def build_search_params(self):
    df=ManagerParamsForm("Lattice Search",self)
    df.Add("Gauss Filter Strength",
           df.DoubleSpinBox("ls_gauss_filter_strength",2,0.0,10.0,"",0.1),
           "Strength of the initial Gaussian filter, which is applied prior to running the PeakSearch, upon which the LatticeSearch is based. A value of 0.0 turns it off.")
    df.Add("Minimal Lattice Length",
           df.DoubleSpinBox("ls_min_lat_length",1,1.0,1000.0," pixel",1.0),
           "The minimal length of a lattice vector to be considered; should be less than the expected length, but high enough to avoid noisy signal to interfere with the lattice search")
    df.Add("Maximal Lattice Length",
           df.DoubleSpinBox("ls_max_lat_length",1,1.0,1000.0," pixel",1.0),
           "The maximal length of a lattice vector to be considered; should be more than the expected length; not as critical as the minimal lattice length")
    df.Add("Outer PeakSearch Window Size",
           df.IntSpinBox("ls_outer_win_size",1,1000," pixel",1),
           "Half-size of outer (overall) box for each potential peak, total size is 2N+1 pixels")
    df.Add("Outer PeakSearch Window Sensitivity",
           df.DoubleSpinBox("ls_outer_win_sens",3,0.0,1000.0,"",0.01),
           "Overall peak search sensitivity in the outer (overall) box; lower number means more peaks found, at the expense of finding more noise peaks; higher number means less noise peaks, with the risk of not having sufficient peaks for finding a lattice.")
    df.Add("Inner PeakSearch Window Size",
           df.IntSpinBox("ls_inner_win_size",1,1000," pixel",1),
           "Half-size of inner box for each potential peak, total size is 2N+1 pixels")
    df.Add("Inner PeakSearch Window Sensitivity",
           df.DoubleSpinBox("ls_inner_win_sens",3,0.0,1000.0,"",0.01),
           "Overall peak search sensitivity in the inner box; allows finetuning of peak broadness and plateaus")
    # TODO: params for DiffVecImage
    QObject.connect(self,SIGNAL("UpdateValues"),df.OnUpdateValues)
    sa=QScrollArea()
    sa.setWidget(df)
    return sa

  def build_extract_params(self):
    df=ManagerParamsForm("Lattice Extract and Integrate",self)
    df.Add("Box Size",
           df.IntSpinBox("le_box_size",1,100," pixel",1),
           "Half-box size for extraction and integration, excluding the background rim; total size is 2N+1")
    df.Add("Background Rim Size",
           df.IntSpinBox("le_bg_size",1,10," pixel",1),
           "Width of the background rim around each box")
    df.Add("Box Grow Size",
           df.IntSpinBox("le_box_grow",0,10," pixel",1),
           "Maximal grow amount for each box to find best size")
    df.Add("Center Mask Radius",
           df.IntSpinBox("le_center_mask_radius",1,10000," pixel",10),
           "Inside radial exclusion zone around lattice (not pattern) origin; for masking CCD bleed effects or other pathologies")
    df.Add("Peripheral Mask Radius",
           df.IntSpinBox("le_peripheral_mask_radius",1,10000," pixel",10),
           "Outside radial exclusion zone around lattice (not pattern) origin; for masking obvious noise-only regions beyond the highest diffracting zone")
    df.Add("Refine Lattice Distortions",
           df.CheckBox("le_refine_lattice_distortions"),
           "Include pincushion and barrel distortions in the lattice refinement")
    df.Add("Re-Refine Lattice",
           df.CheckBox("le_re_refine_lattice"),
           "Refine lattice upon re-running even if refined in a previous step")
    QObject.connect(self,SIGNAL("UpdateValues"),df.OnUpdateValues)
    sa=QScrollArea()
    sa.setWidget(df)
    return sa

  def UpdateValues(self):
    self.emit(SIGNAL("UpdateValues"),self.pdict_)
