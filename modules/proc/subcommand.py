#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------
#
# Authors: Andreas Schenk, Ansgar Philippsen
#

import os,os.path
import time
from ost.img import *
from ost import PushVerbosityLevel,PopVerbosityLevel, PushLogSink,PopLogSink,FileLogSink
from ost import LogInfo,LogError,LogVerbose



class Subcommand:
    def __init__(self,name,help,logfile):
        self.name_=name
        self.help_=help
        self.logfile_=logfile

    def Run(self,dentrylist,args,options):
        pass

    def Cleanup(self,wdir):
        pass
        
    def GetLogfile(self):
        return self.logfile_
    def GetHelp(self):
        return self.help_

    def GetName(self):
        return self.name_

    def ExtendParser(self,parser):
        pass

class IterativeSubcommand(Subcommand):
    def __init__(self,name,help,logfile):
        self.current_dir_=""
        self.current_image_=""
        Subcommand.__init__(self,name,help,logfile)

    def Run(self,dentrylist,args,options):
        owd=os.getcwd()
        for dentry in dentrylist:
            self.SetWorkingDirectory(dentry)
            self.PreprocessDirectory(options)
            if self.CheckPreCondition(options):
                if not self.UpToDate(options):
                    LogInfo("running %s for %s"%(self.name_,dentry[1]))
                    if self.GetLogfile():
                        PushLogSink(FileLogSink(self.GetLogfile()))
                    try:
                        self.ProcessDirectory(options)
                    finally:
                        PopLogSink()
                else:
                    pass
                    LogInfo("skipping %s for %s: up to date"%(self.name_,dentry[1]))
            else:
                pass
                LogInfo("skipping %s for %s: pre condition not met"%(self.name_,dentry[1]))
            self.PostprocessDirectory(options)
        os.chdir(owd)

    def AssembleImagePath(self,ifm):
        """
        Assemble the image name from a mask, where %% is replaced
        with the dataset name. The default is to simply append .tif
        to the dataset name
        """
        image_name=self.current_image_+".tif" # default
        if ifm:
            pos = ifm.find("%%")
            if pos>=0:
                image_name=ifm[:pos]+self.current_image_+ifm[pos+2:]
            else:
                image_name=self.current_image_+ifm
        return os.path.join(self.current_dir_,image_name)

    def SetWorkingDirectory(self,dentry):
        self.current_dir_=os.path.join(dentry[0],dentry[1])
        os.chdir(self.current_dir_)
        self.current_image_=dentry[1]

    def GetWorkingDirectory(self):
        return self.current_image_

    def PreprocessDirectory(self,options):
        pass

    def PostprocessDirectory(self,options):
        pass

    def ProcessDirectory(self,options):
        raise Exception("IterativeSubcommand: reached ProcessDirectory")

    def GetFilePath(self,file):
        return os.path.join(self.current_dir_,file)

    def GetNamedFilePath(self,file):
        return os.path.join(self.current_dir_,self.current_image_+file)

    def InfoIsNewer(self,g1,g2):
        if not g1.HasAttribute("modified"):
            return False
        if not g2.HasAttribute("modified"):
            return True
        return time.mktime(time.strptime(g1.GetAttribute("modified")))>time.mktime(time.strptime(g2.GetAttribute("modified")))

    def IsNewer(self,item1,item2):
        if isinstance(item1,str):
            #item1 is a path
            if not os.path.isfile(item1): 
                return False 
            else:
                time1=os.stat(item1).st_mtime
        else:
            if not item1.HasAttribute("modified"):
                return False
            else:
                time1=time.mktime(time.strptime(item1.GetAttribute("modified")))
        if isinstance(item2,str):
            #item2 is a path
            if not os.path.isfile(item2): 
                return True 
            else:
                time2=os.stat(item2).st_mtime
        else:
            if not item2.HasAttribute("modified"):
                return True
            else:
                time2=time.mktime(time.strptime(item2.GetAttribute("modified")))
        return time1>time2
         

    def FileIsNewer(self,file1,file2):
        return self.is_file_newer(self.GetFilePath(file1),self.GetFilePath(file2))
        
    def NamedFileIsNewer(self,file1,file2):
        return self.is_file_newer(self.GetNamedFilePath(file1),self.GetNamedFilePath(file2))
        
    def UpToDate(self,options):
        return False

    def CheckPreCondition(self,options):
        return True

    def is_file_newer(self,fn1,fn2):
        if not os.path.isfile(fn1): 
            return False 
        if not os.path.isfile(fn2): 
            return True
        return os.stat(fn1).st_mtime>=os.stat(fn2).st_mtime



# for debugging purposes
    
class DummySubcommand(Subcommand):
    def __init__(self):
        Subcommand.__init__(self,"dummy","dummy help","dummy.log")

    def Run(self,dentrylist,args,options):
        print dentrylist

class DummyIterativeSubcommand(IterativeSubcommand):
    def __init__(self):
        IterativeSubcommand.__init__(self,"itdummy","iterative dummy help","it_dummy.log")

    def ProcessDirectory(self,options):
        print "ProcessDirectory()"
