#------------------------------------------------------------------------------
# This file is part of the IPLT project <www.iplt.org>
#
# Copyright (C) 2003-2010 by the IPLT authors
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3.0 of the License, or (at your option) any later 
# version.
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#------------------------------------------------------------------------------

import sys,math
from ost.img import *
import iplt as ex
from ost import LogVerbose,LogInfo,LogError,LogVerbose

def calc_friedel(reflist,s_int,s_sig):
    if not reflist.HasProperty(s_int):
        raise Exception("missing '%s' property in reflection list" % (s_int))
    if not reflist.HasProperty(s_sig):
        raise Exception("missing '%s' property in reflection list" % (s_sig))

    diff_sum1=0.0
    mean_sum1=0.0
    diff_sum2=0.0
    mean_sum2=0.0
    for rp in reflist:
        indx=rp.GetIndex()
        resol=rp.GetResolution()
        if(indx.GetZStar()>=0.0):
            rp2=reflist.Find(-indx)
            if rp2.IsValid():
                int1=rp.Get(s_int)
                int2=rp2.Get(s_int)

                sigma1=rp.Get(s_sig)
                sigma2=rp2.Get(s_sig)
                if sigma1==0.0 and sigma2==0.0:
                    we1=0.0
                    we2=0.0
                else:
                    we1=sigma2*sigma2/(sigma1*sigma1+sigma2*sigma2)
                    we2=sigma1*sigma1/(sigma1*sigma1+sigma2*sigma2)

                int_mean = int1*we1+int2*we2
                dint1=int1-int_mean
                dint2=int2-int_mean
                mean_sum1+=int_mean
                diff_sum1+=math.fabs(we1*dint1-we2*dint2)

                int_mean = int1*0.5+int2*0.5
                dint1=int1-int_mean
                dint2=int2-int_mean
                mean_sum2+=int_mean
                diff_sum2+=0.5*math.fabs(dint1-dint2)

                LogVerbose("%s r=%g f1=%g f2=%g |f1-f2|=%g <f>=%g"%(str(indx),resol,int1,int2,math.fabs(int1-int2),int_mean))
            else:
                LogVerbose("%s: no friedel mate"%(str(indx)))

    if mean_sum1==0.0:
        mean_sum1=1.0
    if mean_sum2==0.0:
        mean_sum2=1.0
        
    LogVerbose("%f %f %f %f" % (diff_sum1,mean_sum1,diff_sum2,mean_sum2))
    return (diff_sum1/mean_sum1,diff_sum2/mean_sum2)


