import sip
import ost.img
import ost.img.alg 
from PyQt4 import QtGui, QtCore
from iplt.gui.init_menubar import _InitMenuBar
from ost import gui
import optparse
from ost.gui.helpwidget import help

def Viewer(image,title=""):
  app=gui.GostyApp.Instance()
  viewer=app.CreateDataViewer(image)
  app.perspective.main_area.AddWidget(title, viewer)
  return viewer


def _InitPanels(app, panels):
  panels.AddWidgetToPool('ost.gui.PythonShell', 1)
  panels.AddWidgetToPool('ost.gui.MessageWidget', 1)
  if not panels.Restore("iplt/ui/perspective/panels"):
    panels.AddWidget(gui.PanelPosition.BOTTOM_PANEL, app.py_shell)

def _InitIPLTNextGen():
  app=gui.GostyApp.Instance()
  app.SetAppTitle("IPLT - Iplt Next Generation")
  main_area=app.perspective.main_area
  _InitPanels(app, app.perspective.panels)
  _InitMenuBar(app.perspective.GetMenuBar())
  app.perspective.Restore()

_InitIPLTNextGen()

usage = 'usage: dng [options] [files to load]'
class IPLTOptionParser(optparse.OptionParser):
  def __init__(self, **kwargs):
    optparse.OptionParser.__init__(self, **kwargs)
  def exit(self, status_code, error_message):
    print error_message,
    QtGui.QApplication.instance().exit()
    sys.exit(-1)

def show_help(option, opt, value, parser):
  parser.print_help()
  QtGui.QApplication.instance().exit()
  sys.exit(-1)

parser=IPLTOptionParser(usage=usage,conflict_handler="resolve")
parser.add_option("-h", "--help", action="callback", callback=show_help, help="show this help message and exit")
parser.add_option("-v", "--verbosity_level", action="store", type="int", dest="vlevel", default=0, help="sets the verbosity level [default: %default]")
parser.add_option("-q", "--query", dest="query", default="", help="Selection query to be highlighted automatically upon loading (only used together with -p option or when a PDB file is loaded, otherwise ignored [default: None]")
parser.disable_interspersed_args()
(options, args) = parser.parse_args()

PushVerbosityLevel(options.vlevel)

import __main__
if len(args)>0:
  script=args[0]
  sys.argv=args
  execfile(script, __main__.__dict__)

